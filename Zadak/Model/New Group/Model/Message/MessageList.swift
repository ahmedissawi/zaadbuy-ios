//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 6/15/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct MessageList: Codable {
    
    enum CodingKeys: String, CodingKey {
        case store
        case sellerId = "seller_id"
    }
    
    var store: Store?
    var sellerId: Int?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        store = try container.decodeIfPresent(Store.self, forKey: .store)
        if let value = try? container.decode(String.self, forKey:.sellerId) {
            sellerId = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.sellerId) {
            sellerId = value
        }
    }
    
}
