//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 6/15/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct MessageItems: Codable {

  enum CodingKeys: String, CodingKey {
    case messages
    case seller
  }

  var messages: [Messages]?
  var seller: Seller?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    messages = try container.decodeIfPresent([Messages].self, forKey: .messages)
    seller = try container.decodeIfPresent(Seller.self, forKey: .seller)
  }

}
