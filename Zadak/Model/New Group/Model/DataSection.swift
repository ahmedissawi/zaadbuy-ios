//
//  Data.swift
//
//  Created by Ahmed ios on 6/6/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct DataSection: Codable {

  enum CodingKeys: String, CodingKey {
    case name
    case id
    case descriptionValue = "description"
    case sections
    case slug
    case title
  }

  var name: String?
  var id: Int?
  var descriptionValue: String?
  var sections: [Sections]?
  var slug: String?
  var title: String?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(Int.self, forKey:.name) {                       
name = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.name) {
 name = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.id) {
 id = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.id) {
id = value 
}
    if let value = try? container.decode(Int.self, forKey:.descriptionValue) {                       
descriptionValue = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.descriptionValue) {
 descriptionValue = value                                                                                     
}
    sections = try container.decodeIfPresent([Sections].self, forKey: .sections)
    if let value = try? container.decode(Int.self, forKey:.slug) {                       
slug = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.slug) {
 slug = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.title) {                       
title = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.title) {
 title = value                                                                                     
}
  }

}
