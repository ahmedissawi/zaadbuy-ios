//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 6/12/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct AddressData: Codable {
    
    enum CodingKeys: String, CodingKey {
        case state
        case type
        case name
        case city
        case id
        case country
        case addressLine2 = "address_line_2"
        case countryId = "country_id"
        case addressLine3 = "address_line_3"
        case addressLine1 = "address_line_1"
        case organization
        case cityId = "city_id"
        case phone
        case postcode
        case prefix
    }
    
    var state: String?
    var type: String?
    var name: String?
    var city: String?
    var id: Int?
    var country: String?
    var addressLine2: String?
    var countryId: Int?
    var addressLine3: String?
    var addressLine1: String?
    var organization: String?
    var cityId: Int?
    var phone: String?
    var postcode: String?
    var prefix: String?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.state) {
            state = String(value)
        }else if let value = try? container.decode(String.self, forKey:.state) {
            state = value
        }
        if let value = try? container.decode(Int.self, forKey:.type) {
            type = String(value)
        }else if let value = try? container.decode(String.self, forKey:.type) {
            type = value
        }
        if let value = try? container.decode(Int.self, forKey:.name) {
            name = String(value)
        }else if let value = try? container.decode(String.self, forKey:.name) {
            name = value
        }
        if let value = try? container.decode(Int.self, forKey:.city) {
            city = String(value)
        }else if let value = try? container.decode(String.self, forKey:.city) {
            city = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(Int.self, forKey:.country) {
            country = String(value)
        }else if let value = try? container.decode(String.self, forKey:.country) {
            country = value
        }
        if let value = try? container.decode(Int.self, forKey:.addressLine2) {
            addressLine2 = String(value)
        }else if let value = try? container.decode(String.self, forKey:.addressLine2) {
            addressLine2 = value
        }
        if let value = try? container.decode(String.self, forKey:.countryId) {
            countryId = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.countryId) {
            countryId = value
        }
        if let value = try? container.decode(Int.self, forKey:.addressLine3) {
            addressLine3 = String(value)
        }else if let value = try? container.decode(String.self, forKey:.addressLine3) {
            addressLine3 = value
        }
        if let value = try? container.decode(Int.self, forKey:.addressLine1) {
            addressLine1 = String(value)
        }else if let value = try? container.decode(String.self, forKey:.addressLine1) {
            addressLine1 = value
        }
        if let value = try? container.decode(Int.self, forKey:.organization) {
            organization = String(value)
        }else if let value = try? container.decode(String.self, forKey:.organization) {
            organization = value
        }
        if let value = try? container.decode(String.self, forKey:.cityId) {
            cityId = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.cityId) {
            cityId = value
        }
        if let value = try? container.decode(Int.self, forKey:.phone) {
            phone = String(value)
        }else if let value = try? container.decode(String.self, forKey:.phone) {
            phone = value
        }
        if let value = try? container.decode(Int.self, forKey:.postcode) {
            postcode = String(value)
        }else if let value = try? container.decode(String.self, forKey:.postcode) {
            postcode = value
        }
        if let value = try? container.decode(Int.self, forKey:.prefix) {
            prefix = String(value)
        }else if let value = try? container.decode(String.self, forKey:.prefix) {
            prefix = value
        }
    }
    
}
