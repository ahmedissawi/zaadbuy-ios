//
//  Items.swift
//
//  Created by  Ahmed’s MacBook Pro on 6/15/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct CategoriesProducat: Codable {
    
    enum CodingKeys: String, CodingKey {
        case createdAt = "created_at"
        case image
        case title
        case status
        case category
        case sellerId = "seller_id"
        case seller
        case id
    }
    
    var createdAt: String?
    var image: String?
    var title: String?
    var status: Int?
    var category: String?
    var sellerId: Int?
    var seller: String?
    var id: Int?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.createdAt) {
            createdAt = String(value)
        }else if let value = try? container.decode(String.self, forKey:.createdAt) {
            createdAt = value
        }
        if let value = try? container.decode(Int.self, forKey:.image) {
            image = String(value)
        }else if let value = try? container.decode(String.self, forKey:.image) {
            image = value
        }
        if let value = try? container.decode(Int.self, forKey:.title) {
            title = String(value)
        }else if let value = try? container.decode(String.self, forKey:.title) {
            title = value
        }
        if let value = try? container.decode(String.self, forKey:.status) {
            status = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.status) {
            status = value
        }
        if let value = try? container.decode(Int.self, forKey:.category) {
            category = String(value)
        }else if let value = try? container.decode(String.self, forKey:.category) {
            category = value
        }
        if let value = try? container.decode(String.self, forKey:.sellerId) {
            sellerId = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.sellerId) {
            sellerId = value
        }
        if let value = try? container.decode(Int.self, forKey:.seller) {
            seller = String(value)
        }else if let value = try? container.decode(String.self, forKey:.seller) {
            seller = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
    }
    
}
