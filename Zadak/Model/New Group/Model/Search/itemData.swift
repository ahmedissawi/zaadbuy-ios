//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 6/16/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct itemData: Codable {

  enum CodingKeys: String, CodingKey {
    case nextPageUrl = "next_page_url"
    case perPage = "per_page"
    case filters
    case items
  }

  var nextPageUrl: String?
  var perPage: Int?
  var filters: [Filters]?
  var items: [Items]?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(Int.self, forKey:.nextPageUrl) {                       
nextPageUrl = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.nextPageUrl) {
 nextPageUrl = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.perPage) {
 perPage = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.perPage) {
perPage = value 
}
    filters = try container.decodeIfPresent([Filters].self, forKey: .filters)
    items = try container.decodeIfPresent([Items].self, forKey: .items)
  }

}
