//
//  Data.swift
//
//  Created by Ahmed ios on 6/6/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct UserStruct: Codable {

  enum CodingKeys: String, CodingKey {
    case user
    case auth
  }

  var user: UserData?
  var auth: Auth?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    user = try container.decodeIfPresent(UserData.self, forKey: .user)
    auth = try container.decodeIfPresent(Auth.self, forKey: .auth)
  }

}
