//
//  Profile.swift
//
//  Created by Ahmed ios on 6/6/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Profile: Codable {

  enum CodingKeys: String, CodingKey {
    case createdAt = "created_at"
    case mobile
    case countryId = "country_id"
    case image
    case id
    case country
    case gender
    case language
    case countryJson = "country_json"
  }

  var createdAt: String?
  var mobile: String?
  var countryId: String?
  var image: String?
  var id: Int?
  var country: String?
  var gender: String?
  var language: String?
  var countryJson: CountryJson?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(Int.self, forKey:.createdAt) {                       
createdAt = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.createdAt) {
 createdAt = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.mobile) {                       
mobile = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.mobile) {
 mobile = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.countryId) {                       
countryId = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.countryId) {
 countryId = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.image) {                       
image = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.image) {
 image = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.id) {
 id = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.id) {
id = value 
}
    if let value = try? container.decode(Int.self, forKey:.country) {                       
country = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.country) {
 country = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.gender) {                       
gender = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.gender) {
 gender = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.language) {                       
language = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.language) {
 language = value                                                                                     
}
    countryJson = try container.decodeIfPresent(CountryJson.self, forKey: .countryJson)
  }

}
