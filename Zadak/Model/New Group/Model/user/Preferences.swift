//
//  Preferences.swift
//
//  Created by Ahmed ios on 6/6/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Preferences: Codable {

  enum CodingKeys: String, CodingKey {
    case showPreferences = "show_preferences"
    case showMobile = "show_mobile"
    case showProfilePicture = "show_profile_picture"
    case id
    case showEmail = "show_email"
    case showStatus = "show_status"
  }

  var showPreferences: Int?
  var showMobile: Int?
  var showProfilePicture: Int?
  var id: Int?
  var showEmail: Int?
  var showStatus: Int?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(String.self, forKey:.showPreferences) {
 showPreferences = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.showPreferences) {
showPreferences = value 
}
    if let value = try? container.decode(String.self, forKey:.showMobile) {
 showMobile = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.showMobile) {
showMobile = value 
}
    if let value = try? container.decode(String.self, forKey:.showProfilePicture) {
 showProfilePicture = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.showProfilePicture) {
showProfilePicture = value 
}
    if let value = try? container.decode(String.self, forKey:.id) {
 id = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.id) {
id = value 
}
    if let value = try? container.decode(String.self, forKey:.showEmail) {
 showEmail = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.showEmail) {
showEmail = value 
}
    if let value = try? container.decode(String.self, forKey:.showStatus) {
 showStatus = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.showStatus) {
showStatus = value 
}
  }

}
