//
//  Rates.swift
//
//  Created by  Ahmed’s MacBook Pro on 6/11/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct RatesPrdoucta: Codable {

  enum CodingKeys: String, CodingKey {
    case details
  }

  var details: [Details]?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    details = try container.decodeIfPresent([Details].self, forKey: .details)
  }

}
