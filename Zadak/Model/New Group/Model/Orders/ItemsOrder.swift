//
//  Items.swift
//
//  Created by Ahmed ios on 6/15/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct ItemsOrder: Codable {
    
    enum CodingKeys: String, CodingKey {
        case shippingPrice = "shipping_price"
        case id
        case salePrice = "sale_price"
        case regularPrice = "regular_price"
        case shippingServiceId = "shipping_service_id"
        case shippingService = "shipping_service"
        case title
        case totalPrice = "total_price"
        case quantityShipped = "quantity_shipped"
        case saleDiscount = "sale_discount"
        case itemPrice = "item_price"
        case quantity
    }
    
    var shippingPrice: Int?
    var id: Int?
    var salePrice: Int?
    var regularPrice: Int?
    var shippingServiceId: Int?
    var shippingService: String?
    var title: String?
    var totalPrice: Int?
    var quantityShipped: Int?
    var saleDiscount: Int?
    var itemPrice: Int?
    var quantity: Int?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(String.self, forKey:.shippingPrice) {
            shippingPrice = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.shippingPrice) {
            shippingPrice = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(String.self, forKey:.salePrice) {
            salePrice = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.salePrice) {
            salePrice = value
        }
        if let value = try? container.decode(String.self, forKey:.regularPrice) {
            regularPrice = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.regularPrice) {
            regularPrice = value
        }
        if let value = try? container.decode(String.self, forKey:.shippingServiceId) {
            shippingServiceId = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.shippingServiceId) {
            shippingServiceId = value
        }
        if let value = try? container.decode(Int.self, forKey:.shippingService) {
            shippingService = String(value)
        }else if let value = try? container.decode(String.self, forKey:.shippingService) {
            shippingService = value
        }
        if let value = try? container.decode(Int.self, forKey:.title) {
            title = String(value)
        }else if let value = try? container.decode(String.self, forKey:.title) {
            title = value
        }
        if let value = try? container.decode(String.self, forKey:.totalPrice) {
            totalPrice = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.totalPrice) {
            totalPrice = value
        }
        if let value = try? container.decode(String.self, forKey:.quantityShipped) {
            quantityShipped = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.quantityShipped) {
            quantityShipped = value
        }
        if let value = try? container.decode(String.self, forKey:.saleDiscount) {
            saleDiscount = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.saleDiscount) {
            saleDiscount = value
        }
        if let value = try? container.decode(String.self, forKey:.itemPrice) {
            itemPrice = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.itemPrice) {
            itemPrice = value
        }
        if let value = try? container.decode(String.self, forKey:.quantity) {
            quantity = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.quantity) {
            quantity = value
        }
    }
    
}
