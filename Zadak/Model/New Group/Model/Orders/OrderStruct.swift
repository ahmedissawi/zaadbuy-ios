//
//  Data.swift
//
//  Created by Ahmed ios on 6/15/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct OrderStruct: Codable {

  enum CodingKeys: String, CodingKey {
    case orders
    case perPage = "per_page"
  }

  var orders: [Orders]?
  var perPage: Int?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    orders = try container.decodeIfPresent([Orders].self, forKey: .orders)
    if let value = try? container.decode(String.self, forKey:.perPage) {
 perPage = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.perPage) {
perPage = value 
}
  }

}
