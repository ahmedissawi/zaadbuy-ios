//
//  Items.swift
//
//  Created by Ahmed ios on 6/6/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Items: Codable {
    
    enum CodingKeys: String, CodingKey {
        case title
        case displayInfo = "display_info"
        case descriptionValue = "description"
        case regularPrice = "regular_price"
        case favoriteItem = "favorite_item"
        case salePrice = "sale_price"
        case id
        case quantity
        case subtitle
        case seller
        case mobileImage = "mobile_image"
        case image
        case discount
        case category
        case tabletImage = "tablet_image"
        case target
    }
    
    var title: String?
    var displayInfo: Int?
    var descriptionValue: String?
    var regularPrice: Float?
    var salePrice: Int?
    var id: Int?
    var quantity: Int?
    var subtitle: String?
    var seller: Seller?
    var mobileImage: String?
    var image: String?
    var discount: Int?
    var category: Category?
    var tabletImage: String?
    var target: String?
    var favoriteItem: Bool?

    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        favoriteItem = try container.decodeIfPresent(Bool.self, forKey: .favoriteItem)
        if let value = try? container.decode(Int.self, forKey:.title) {
            title = String(value)
        }else if let value = try? container.decode(String.self, forKey:.title) {
            title = value
        }
        if let value = try? container.decode(String.self, forKey:.displayInfo) {
            displayInfo = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.displayInfo) {
            displayInfo = value
        }
        if let value = try? container.decode(Int.self, forKey:.descriptionValue) {
            descriptionValue = String(value)
        }else if let value = try? container.decode(String.self, forKey:.descriptionValue) {
            descriptionValue = value
        }
        regularPrice = try container.decodeIfPresent(Float.self, forKey: .regularPrice)
        if let value = try? container.decode(String.self, forKey:.salePrice) {
            salePrice = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.salePrice) {
            salePrice = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(String.self, forKey:.quantity) {
            quantity = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.quantity) {
            quantity = value
        }
        if let value = try? container.decode(Int.self, forKey:.subtitle) {
            subtitle = String(value)
        }else if let value = try? container.decode(String.self, forKey:.subtitle) {
            subtitle = value
        }
        seller = try container.decodeIfPresent(Seller.self, forKey: .seller)
        if let value = try? container.decode(Int.self, forKey:.mobileImage) {
            mobileImage = String(value)
        }else if let value = try? container.decode(String.self, forKey:.mobileImage) {
            mobileImage = value
        }
        if let value = try? container.decode(Int.self, forKey:.image) {
            image = String(value)
        }else if let value = try? container.decode(String.self, forKey:.image) {
            image = value
        }
        if let value = try? container.decode(String.self, forKey:.discount) {
            discount = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.discount) {
            discount = value
        }
        category = try container.decodeIfPresent(Category.self, forKey: .category)
        if let value = try? container.decode(Int.self, forKey:.tabletImage) {
            tabletImage = String(value)
        }else if let value = try? container.decode(String.self, forKey:.tabletImage) {
            tabletImage = value
        }
        if let value = try? container.decode(Int.self, forKey:.target) {
            target = String(value)
        }else if let value = try? container.decode(String.self, forKey:.target) {
            target = value
        }
    }
    
}
