//
//  Data.swift
//
//  Created by Ahmed ios on 6/14/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct CheckoutStruct: Codable {

  enum CodingKeys: String, CodingKey {
    case quantity
    case shippingServices = "ShippingServices"
    case itemId = "item_id"
    case image
    case title
    case descriptionValue = "description"
    case price
    case total
    case id
  }

  var quantity: String?
  var shippingServices: [ShippingServices]?
  var itemId: Int?
  var image: String?
  var title: String?
  var descriptionValue: String?
  var price: Int?
  var total: String?
  var id: Int?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(Int.self, forKey:.quantity) {                       
quantity = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.quantity) {
 quantity = value                                                                                     
}
    shippingServices = try container.decodeIfPresent([ShippingServices].self, forKey: .shippingServices)
    if let value = try? container.decode(String.self, forKey:.itemId) {
 itemId = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.itemId) {
itemId = value 
}
    if let value = try? container.decode(Int.self, forKey:.image) {                       
image = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.image) {
 image = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.title) {                       
title = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.title) {
 title = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.descriptionValue) {                       
descriptionValue = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.descriptionValue) {
 descriptionValue = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.price) {
 price = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.price) {
price = value 
}
    if let value = try? container.decode(Int.self, forKey:.total) {                       
total = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.total) {
 total = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.id) {
 id = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.id) {
id = value 
}
  }

}
