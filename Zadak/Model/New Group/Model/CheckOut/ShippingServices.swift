//
//  ShippingServices.swift
//
//  Created by Ahmed ios on 6/14/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct ShippingServices: Codable {

  enum CodingKeys: String, CodingKey {
    case price
    case serviceId = "service_id"
    case title
  }

  var price: Int?
  var serviceId: Int?
  var title: String?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(String.self, forKey:.price) {
 price = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.price) {
price = value 
}
    if let value = try? container.decode(String.self, forKey:.serviceId) {
 serviceId = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.serviceId) {
serviceId = value 
}
    if let value = try? container.decode(Int.self, forKey:.title) {                       
title = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.title) {
 title = value                                                                                     
}
  }

}
