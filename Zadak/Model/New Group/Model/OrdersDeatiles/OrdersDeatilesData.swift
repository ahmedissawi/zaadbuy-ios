//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 6/15/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct OrdersDeatilesData: Codable {

  enum CodingKeys: String, CodingKey {
    case items
    case financialInfo = "financial_info"
    case itemsRates = "items_rates"
    case basicInfo = "basic_info"
  }

  var items: [Items]?
  var financialInfo: FinancialInfo?
  var itemsRates: [OrderRates]?
  var basicInfo: BasicInfo?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    items = try container.decodeIfPresent([Items].self, forKey: .items)
    financialInfo = try container.decodeIfPresent(FinancialInfo.self, forKey: .financialInfo)
    itemsRates = try container.decodeIfPresent([OrderRates].self, forKey: .itemsRates)
    basicInfo = try container.decodeIfPresent(BasicInfo.self, forKey: .basicInfo)
  }

}
