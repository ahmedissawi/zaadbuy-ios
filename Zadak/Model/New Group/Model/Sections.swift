//
//  Sections.swift
//
//  Created by Ahmed ios on 6/6/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Sections: Codable {
    
    enum CodingKeys: String, CodingKey {
        case id
        case slug
        case title
        case type
        case action
        case item
        case actionTitle = "action_title"
        case items
    }
    
    var id: Int?
    var slug: String?
    var title: String?
    var type: String?
    var action: String?
    var item: Item?
    var actionTitle: String?
    var items: [Items]?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(Int.self, forKey:.slug) {
            slug = String(value)
        }else if let value = try? container.decode(String.self, forKey:.slug) {
            slug = value
        }
        if let value = try? container.decode(Int.self, forKey:.title) {
            title = String(value)
        }else if let value = try? container.decode(String.self, forKey:.title) {
            title = value
        }
        if let value = try? container.decode(Int.self, forKey:.type) {
            type = String(value)
        }else if let value = try? container.decode(String.self, forKey:.type) {
            type = value
        }
        if let value = try? container.decode(Int.self, forKey:.action) {
            action = String(value)
        }else if let value = try? container.decode(String.self, forKey:.action) {
            action = value
        }
        item = try container.decodeIfPresent(Item.self, forKey: .item)
        if let value = try? container.decode(Int.self, forKey:.actionTitle) {
            actionTitle = String(value)
        }else if let value = try? container.decode(String.self, forKey:.actionTitle) {
            actionTitle = value
        }
        items = try container.decodeIfPresent([Items].self, forKey: .items)
    }
    
}
