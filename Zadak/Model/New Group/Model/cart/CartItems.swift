//
//  Items.swift
//
//  Created by Ahmed ios on 6/6/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct CartItems: Codable {

  enum CodingKeys: String, CodingKey {
    case itemId = "item_id"
    case total
    case price
    case title
    case id
    case quantity
  }

  var itemId: Int?
  var total: String?
  var price: String?
  var title: String?
  var id: Int?
  var quantity: String?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(String.self, forKey:.itemId) {
 itemId = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.itemId) {
itemId = value 
}
    if let value = try? container.decode(Int.self, forKey:.total) {                       
total = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.total) {
 total = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.price) {                       
price = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.price) {
 price = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.title) {                       
title = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.title) {
 title = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.id) {
 id = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.id) {
id = value 
}
    if let value = try? container.decode(Int.self, forKey:.quantity) {                       
quantity = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.quantity) {
 quantity = value                                                                                     
}
  }

}
