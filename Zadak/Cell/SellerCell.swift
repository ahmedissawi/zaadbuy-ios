//
//  SellerCell.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 2/20/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class SellerCell: UICollectionViewCell {
    
    @IBOutlet weak var Collection: UICollectionView!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.Collection.delegate = self
        self.Collection.dataSource = self
        Collection.registerCell(id: "SellerproduactCell")

    }
    func getCellSize(_ targetSize: CGSize) -> CGSize {
       return CGSize(width: (UIScreen.main.bounds.width), height: 110)
        }

        // Only this is called on iOS 12 and lower
        override func systemLayoutSizeFitting(_ targetSize: CGSize) -> CGSize {
            return self.getCellSize(targetSize)
        }

        // Only this is called on iOS 13 beta
        override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
            return self.getCellSize(targetSize)
        }

}
extension SellerCell:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SellerproduactCell", for: indexPath) as! SellerproduactCell
                   return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
         let wideth = UIScreen.main.bounds.size.width
        return CGSize(width: wideth, height: 90)
    }
    
   
    
}
