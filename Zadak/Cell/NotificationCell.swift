//
//  NotificationCell.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 2/21/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {
    
    @IBOutlet weak var lblname: UILabel!
    @IBOutlet weak var lbldesc: UILabel!
    @IBOutlet weak var lbltime: UILabel!
    @IBOutlet weak var imgprnotification: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
