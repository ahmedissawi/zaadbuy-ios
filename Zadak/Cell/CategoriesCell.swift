//
//  CategoriesCell.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 2/23/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class CategoriesCell: UITableViewCell {
    
    @IBOutlet weak var viewCategories: UIView!
    @IBOutlet weak var viewline: UIView!
    @IBOutlet weak var lb_categories: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
