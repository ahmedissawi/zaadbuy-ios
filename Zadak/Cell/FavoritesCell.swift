//
//  FavoritesCell.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 2/21/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class FavoritesCell: UITableViewCell {
    
    @IBOutlet weak var lblname: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblprice: UILabel!
    @IBOutlet weak var bt_delete: UIButton!
    @IBOutlet weak var bt_addcart: UIButton!
    @IBOutlet weak var imgproducat: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }


}
