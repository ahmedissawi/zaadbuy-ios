//
//  OrdersCell.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 2/22/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class OrdersCell: UITableViewCell {
    
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var orderIdlbl: UILabel!
    @IBOutlet weak var paymentLbl: UILabel!
    @IBOutlet weak var storlbl: UILabel!
    @IBOutlet weak var lbstatus: UILabel!
    @IBOutlet weak var acountView: UIView!
    @IBOutlet weak var imageLicView: UIView!
    @IBOutlet weak var btdeatiles: UIButton!
    @IBOutlet weak var btseller: UIButton!
    @IBOutlet weak var btacount: UIButton!
    @IBOutlet weak var btpay: UIButton!
    @IBOutlet weak var btPayseconds: UIButton!
    @IBOutlet weak var ViewPayseconds: UIView!
    @IBOutlet weak var btdisputes: UIButton!
    @IBOutlet weak var disputesView: UIView!



        var items : Orders?{
              didSet{
                dateLbl.text = items?.purchaseDate?.prefix(11).description
                orderIdlbl.text = "\(items?.id ?? 0)"
                paymentLbl.text = items?.paymentMethod ?? ""
                if items?.paymentMethodId == "2"{
                    self.ViewPayseconds.isHidden = false
                }else{
                    self.ViewPayseconds.isHidden = true
                }
                storlbl.text = items?.store?.name ?? ""
                lbstatus.text = items?.status?.title ?? ""
                if items?.status?.id == 3{
                    disputesView.isHidden = false
                }else{
                    disputesView.isHidden = true
                }
                if items?.status?.id != 0{
                    acountView.isHidden = true
                    imageLicView.isHidden = true

                }else{
                    acountView.isHidden = false
                    imageLicView.isHidden = false

                }
    //            img_producat.sd_custom(url: items?.quantity)
            }
          }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
