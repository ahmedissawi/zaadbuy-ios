//
//  storeListVC.swift
//  Zadak
//
//  Created by Ahmed ios on 6/21/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class storeListVC: SuperViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    var storesItems = [StoresData]()
    
    var dataStores:DataStores?
    var stores = [Stores]()
    
    var storesCategrouy = [Store]()
    var link = ""
    var titleCat = ""
    var serchQuery = false
    var categoryId:Int?

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.registerCell(id: "StoresCell")
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setTitle(self.titleCat, sender: self)
        navgtion.navigationBar.barTintColor = "FF6A2A".color
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)

        if serchQuery {
            if categoryId == 1{
                getStoresList()
            }
            else{
                getStoresListService()
            }
            
        }
        
        else{
            if(categoryId == 1){
                getStores()
            }
            else{
                getStoresService()
            }
           
        }
        // Do any additional setup after loading the view.
    }
        func getStores(){
            _ = WebRequests.setup(controller: nil).prepare(query: link, method: HTTPMethod.get).start(){ (response, error) in
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200{
    //                    self.showAlert(title: "Error".localized, message:Status.message!)
    //                    return
                    }

                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }


                do {
                    let Status =  try JSONDecoder().decode(storelistClass.self, from: response.data!)
                    
                    self.storesCategrouy = Status.data?.stores ?? []
                    self.collectionView.reloadData()

                } catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }

            }

        }
    
    func getStoresService(){
        _ = WebRequests.setup(controller: nil).prepare(query: link, method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
//                    self.showAlert(title: "Error".localized, message:Status.message!)
//                    return
                }

            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }


            do {
                let Status =  try JSONDecoder().decode(MainStores.self, from: response.data!)
                self.dataStores = Status.data!
                self.stores +=  self.dataStores!.stores ?? []
                self.collectionView.reloadData()

            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }

        }

    }
    func getStoresList(){
             _ = WebRequests.setup(controller: nil).prepare(query: link, method: HTTPMethod.get).start(){ (response, error) in
                 do {
                     let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                     if Status.status != 200{
     //                    self.showAlert(title: "Error".localized, message:Status.message!)
     //                    return
                     }

                 }catch let jsonErr {
                     print("Error serializing  respone json", jsonErr)
                 }


                 do {
                     let Status =  try JSONDecoder().decode(StoresClass.self, from: response.data!)
                     self.storesCategrouy = Status.data!
                     self.collectionView.reloadData()
                    if self.storesCategrouy.count == 0{
                        self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.collectionView, refershSelector: #selector(self.didRefersh))
                        self.emptyView?.firstLabel.text = "No results to display".localized
                        self.storesCategrouy.removeAll()
                        self.collectionView.reloadData()
                    }
                 } catch let jsonErr {
                     print("Error serializing  respone json", jsonErr)
                 }

             }

         }
    func getStoresListService(){
             _ = WebRequests.setup(controller: nil).prepare(query: link, method: HTTPMethod.get).start(){ (response, error) in
                 do {
                     let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                     if Status.status != 200{
     //                    self.showAlert(title: "Error".localized, message:Status.message!)
     //                    return
                     }

                 }catch let jsonErr {
                     print("Error serializing  respone json", jsonErr)
                 }


                 do {
                     let Status =  try JSONDecoder().decode(MainStores.self, from: response.data!)

                     self.dataStores = Status.data!
                    self.stores +=  self.dataStores!.stores ?? []
                    
                     self.collectionView.reloadData()
                    if self.stores.count == 0{
                        self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.collectionView, refershSelector: #selector(self.didRefershs))
                        self.emptyView?.firstLabel.text = "No results to display".localized
                        self.stores.removeAll()
                        self.collectionView.reloadData()
                    }
                 } catch let jsonErr {
                     print("Error serializing  respone json", jsonErr)
                 }

             }

         }
    
    @IBAction func didRefersh(){
           storesCategrouy.removeAll()
           getStoresList()
       }
    @IBAction func didRefershs(){
           stores.removeAll()
           getStoresListService()
       }


}

extension storeListVC:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if categoryId == 2{
            return stores.count
         }
        else{
            return storesCategrouy.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StoresCell", for: indexPath) as! StoresCell
        if categoryId == 2{
            let obj = stores[indexPath.row]
            cell.lblname.text = obj.name
            cell.imgStore.sd_custom(url: obj.logo ?? "")
            return cell
            
        }
        else{
            let obj = storesCategrouy[indexPath.row]
            cell.lblname.text = obj.name
            cell.imgStore.sd_custom(url: obj.logo ?? "")
            return cell
        }
        
    }
    

    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenSize = UIScreen.main.bounds
        return CGSize(width:( (screenSize.width / 3) - 10), height: (screenSize.width / 3) - 10)
    }
    
  
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
          return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
      }
    
//      func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
//        let footermore = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: "FooterMoreStoresCell", for: indexPath)
//
//        return footermore
//    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        if (categoryId == 2 ){
            let obj  = stores[indexPath.row]
            let vc:DetailsStoreVC = DetailsStoreVC.loadFromNib()
            vc.ID = obj.id!
            vc.hidesBottomBarWhenPushed = true
            vc.modalPresentationStyle = .fullScreen
            self.navigationController?.pushViewController(vc, animated: false)
               
        }
        else{
            let obj  = storesCategrouy[indexPath.row]
            let vc:StoreInfoVC = AppDelegate.sb_main.instanceVC()

            vc.id = obj.id!
    //        vc.logoStore = (deatilesproducat?.seller?.store_logo as! String) ?? ""
    //        vc.nameStore = (deatilesproducat?.seller?.name as! String) ?? ""
            vc.hidesBottomBarWhenPushed = true
            vc.modalPresentationStyle = .fullScreen
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize(width: 110, height: 135)
    }
    
}
