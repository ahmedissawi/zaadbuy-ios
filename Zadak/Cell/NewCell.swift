//
//  NewCell.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 2/20/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class NewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgNew: UIImageView!
    @IBOutlet weak var lb_name: UILabel!
    @IBOutlet weak var lb_price: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var bt_like: UIButton!
    @IBOutlet weak var bt_shopping: UIButton!
    @IBOutlet weak var viewDiscount: UIView!
    var obj:Items?{
        didSet {
            lb_name.text = obj?.title
            imgNew.sd_custom(url: obj?.image ?? "")
        }
        
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    
    
    
}
