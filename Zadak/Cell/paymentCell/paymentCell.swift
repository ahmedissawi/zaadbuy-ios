//
//  paymentCell.swift
//  Zadak
//
//  Created by Ahmed ios on 6/21/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class paymentCell: UITableViewCell {
    @IBOutlet weak var lb_orderId: UILabel!
    @IBOutlet weak var lb_nameSaler: UILabel!
    @IBOutlet weak var lb_orderNumber: UILabel!
    @IBOutlet weak var lb_stutse: UILabel!
    var imageTapped: (() -> ())?

    var Object: Resources! {
           didSet {
               lb_orderId.text = "\(Object.orderId ?? 0)"
            lb_nameSaler.text = "\(Object.seller ?? "")"
            lb_orderNumber.text = "\(Object.transactionNo ?? "")"
            lb_stutse.text = "\(Object.status ?? "")"

           }
           
       }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func ShowUrlTapped(_ sender: UIButton) {
            imageTapped?()
        }

}
