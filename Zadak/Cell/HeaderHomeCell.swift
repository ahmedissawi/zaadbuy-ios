//
//  HeaderHomeCell.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 2/20/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class HeaderHomeCell: UICollectionViewCell {
    @IBOutlet weak var lb_name: UILabel!
    @IBOutlet weak var lb_desc: UILabel!
    @IBOutlet weak var btmore: UIButton!
    @IBOutlet weak var Viewmore: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
