//
//  NewAllCell.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 2/20/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit


class NewAllCell: UICollectionViewCell {
    
    @IBOutlet weak var Collection: UICollectionView!
    weak var delegate:HomeVC?
    
    var listItem:[Items]? {
        didSet {
            Collection.reloadData()
        }
        
        
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.Collection.delegate = self
        self.Collection.dataSource = self
        Collection.registerCell(id: "NewCell")
    }
    
    
    func getCellSize(_ targetSize: CGSize) -> CGSize {
        return CGSize(width: (UIScreen.main.bounds.width), height: 500)
    }
    
    // Only this is called on iOS 12 and lower
    override func systemLayoutSizeFitting(_ targetSize: CGSize) -> CGSize {
        return self.getCellSize(targetSize)
    }
    
    // Only this is called on iOS 13 beta
    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
        return self.getCellSize(targetSize)
    }
    
}
extension NewAllCell:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewCell", for: indexPath) as! NewCell
        let obj = listItem![indexPath.row]
        cell.obj = listItem![indexPath.row]
        cell.bt_like.tag = indexPath.row
        cell.bt_shopping.tag = indexPath.row
 
        if obj.salePrice != 0{
            cell.lb_price.text = "\(obj.regularPrice ?? 0.0)" + " "  + "O.R".localized
            cell.lblDesc.text = "\(obj.salePrice ?? 0)" + " " + "O.R".localized
            cell.viewDiscount.isHidden = false
        }else{
            cell.lb_price.text = "\(obj.regularPrice ?? 0.0)" + " "  + "O.R".localized
            cell.viewDiscount.isHidden = true
        }
        if obj.favoriteItem == false{
            cell.bt_like.setImage(UIImage(named: "unlike"), for: .normal)
        }else{
            cell.bt_like.setImage(UIImage(named: "like"), for: .normal)
        }
        cell.bt_like.addTarget(self, action: #selector(self.likeBtn), for: UIControl.Event.touchUpInside)
        cell.bt_shopping.addTarget(self, action: #selector(self.CartBtn), for: UIControl.Event.touchUpInside)
        return cell
    }
    
    @objc func CartBtn(_ sender:UIButton ){
        let obj = self.listItem![sender.tag]
        var parameters: [String: Any] = [:]
        
        parameters["item_id"] = obj.id?.description
        
        parameters["quantity"] = "1"
        
        
        _ = WebRequests.setup(controller: nil).prepare(query: "shopping-cart/items", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200 {
                    
//                    self.showAlert(title: "Error".localized, message:Status.message!)
                    return
                }else{
//                    self.showAlert(title: "Success".localized, message: "Producat Add To Cart".localized)
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
        
    }
    
    @objc func likeBtn(_ sender:UIButton ){
        let obj = self.listItem![sender.tag]
        
        if obj.favoriteItem == false{
            var parameters: [String: Any] = [:]
            parameters["item_id"] = obj.id?.description
            
            _ = WebRequests.setup(controller: nil).prepare(query: "favorite/items", method: HTTPMethod.post,parameters: parameters).start(){ (response, error) in
                
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200 {
                        
                        //                    self.showAlert(title: "Error".localized, message:Status.message!)
                        return
                    }else if Status.status  == 200{
                        NotificationCenter.default.post(name: Notification.Name("UpdateLike"), object: nil)
                        
                        //                    self.showAlert(title: "Success".localized, message:"Produact delete from favorite".localized)
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
                
            }
        }else{
            _ = WebRequests.setup(controller: nil).prepare(query: "favorite/items/\(obj.id ?? 0)", method: HTTPMethod.delete).start(){ (response, error) in
                
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200 {
                        
                        //                    self.showAlert(title: "Error".localized, message:Status.message!)
                        return
                    }else if Status.status  == 200{
                        NotificationCenter.default.post(name: Notification.Name("UpdateLike"), object: nil)
                        
                        //                    self.showAlert(title: "Success".localized, message:"Produact delete from favorite".localized)
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
                
            }
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let wideth = UIScreen.main.bounds.size.width
        return CGSize(width: wideth/2.1, height: 245)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let obj = listItem![indexPath.row]
        let vc:DetailsVC = AppDelegate.sb_main.instanceVC()
        vc.producatID = obj.id
        vc.hidesBottomBarWhenPushed = true
        vc.modalPresentationStyle = .fullScreen
        delegate?.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}
