//
//  HomeSlideCell.swift
//  Zadak
//
//  Created by Ahmed ios on 5/30/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import FSPagerView

class HomeSlideCell: UICollectionViewCell {
    var items : [Items]?{
        
        didSet{
                        self.pagerView.reloadData()
                    }
    }

    @IBOutlet weak var pagerView: FSPagerView!{
             didSet{
                 self.pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
                self.pagerView.delegate = self
                self.pagerView.dataSource = self
                self.pagerView.automaticSlidingInterval = 3.0

             }
         }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func getCellSize(_ targetSize: CGSize) -> CGSize {
        return CGSize(width: (UIScreen.main.bounds.width) - 12, height: 155 )
    }

    // Only this is called on iOS 12 and lower
    override func systemLayoutSizeFitting(_ targetSize: CGSize) -> CGSize {
        return self.getCellSize(targetSize)
    }

    // Only this is called on iOS 13 beta
    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
        return self.getCellSize(targetSize)
    }


}
extension HomeSlideCell:FSPagerViewDelegate,FSPagerViewDataSource{
    
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return items!.count
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        let slider = items

        cell.imageView?.sd_custom(url: slider![index].mobileImage ?? "")
        cell.imageView?.contentMode = .scaleAspectFill
        cell.imageView?.clipsToBounds = true
        return cell
    }
    
    
    
}
