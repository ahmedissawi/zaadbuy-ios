//
//  SellerproduactCell.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 2/20/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class SellerproduactCell: UICollectionViewCell {
    
    @IBOutlet weak var imgcell: UIImageView!
    @IBOutlet weak var lb_desc: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func getCellSize(_ targetSize: CGSize) -> CGSize {
          return CGSize(width: (UIScreen.main.bounds.width), height: 150)
           }

           // Only this is called on iOS 12 and lower
           override func systemLayoutSizeFitting(_ targetSize: CGSize) -> CGSize {
               return self.getCellSize(targetSize)
           }

           // Only this is called on iOS 13 beta
           override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
               return self.getCellSize(targetSize)
           }
}
