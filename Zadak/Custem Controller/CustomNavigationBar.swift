//
//  CustomNavigationBar.swift
//  dabberly
//
//  Created by ahmed on 6/7/18.
//  Copyright © 2018 ahmed. All rights reserved.
//

import UIKit

class CustomNavigationBar: UINavigationController {
    
    
    var ProfileBtn:       UIBarButtonItem?
    var SearchBtn:        UIBarButtonItem?
    var Mapbtn:           UIBarButtonItem?
    var Citybtn:          UIBarButtonItem?
    var CityDropbtn:      UIBarButtonItem?
    var Settingsbtn:      UIBarButtonItem?
    var Donebtn:          UIBarButtonItem?
    var Plusbtn:          UIBarButtonItem?
    var MuneWhitebtn:     UIBarButtonItem?

   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.isDark = true
        self.navigationBar.barTintColor = "FFFFFF".color
        self.navigationBar.tintColor = "FFFFFF".color
//        imageView.image?.imageWithColor(color1: "FF3601".color)
        self.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.isTranslucent = false
        
        
         Mapbtn = UIBarButtonItem(image: UIImage(named:"map")?.withRenderingMode(.alwaysOriginal), style: UIBarButtonItem.Style.plain, target: self, action: #selector(didClickRightButton))
        
        ProfileBtn = UIBarButtonItem(image: UIImage(named:"Profile")?.withRenderingMode(.alwaysOriginal), style: UIBarButtonItem.Style.plain, target: self, action: #selector(didClickRightButton))
        
        SearchBtn = UIBarButtonItem(image: UIImage(named:"Search")?.withRenderingMode(.alwaysOriginal), style: UIBarButtonItem.Style.plain, target: self, action: #selector(didClickRightButton))
        
        
        Citybtn = UIBarButtonItem(title: "Area name".localized, style: UIBarButtonItem.Style.plain, target: self, action: #selector(didClickRightButton))
        
        CityDropbtn = UIBarButtonItem(image: UIImage(named:"dropWithe")?.withRenderingMode(.alwaysOriginal), style: UIBarButtonItem.Style.plain, target: self, action: #selector(didClickRightButton))
        
        Settingsbtn = UIBarButtonItem(image: UIImage(named:"setting")?.withRenderingMode(.alwaysOriginal), style: UIBarButtonItem.Style.plain, target: self, action: #selector(didClickRightButton))
        
        Donebtn = UIBarButtonItem(image: UIImage(named:"Done")?.withRenderingMode(.alwaysOriginal), style: UIBarButtonItem.Style.plain, target: self, action: #selector(didClickRightButton))
        
        Plusbtn = UIBarButtonItem(image: UIImage(named:"pluswhite")?.withRenderingMode(.alwaysOriginal), style: UIBarButtonItem.Style.plain, target: self, action: #selector(didClickRightButton))
        
        MuneWhitebtn = UIBarButtonItem(image: UIImage(named:"menu")?.withRenderingMode(.alwaysOriginal), style: UIBarButtonItem.Style.plain, target: self, action: #selector(didClickRightButton))
        
        Citybtn?.tintColor = UIColor.white
        

        SearchBtn?.tag        = 33
        Mapbtn?.tag           = 44
        ProfileBtn?.tag       = 55
        Citybtn?.tag          = 66
        CityDropbtn?.tag      = 77
        Settingsbtn?.tag      = 88
        Donebtn?.tag          = 99
        Plusbtn?.tag          = 11
        MuneWhitebtn?.tag     = 22

        
    }
    
   
    func setShadowNavBar() {
//        self.navigationBar.layer.masksToBounds = false
//        self.navigationBar.layer.shadowColor = "005A5B".color.cgColor
//        self.navigationBar.layer.shadowOpacity = 0.8
//        self.navigationBar.layer.shadowRadius = 2
//        self.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        
        self.navigationBar.layer.masksToBounds = false
        self.navigationBar.layer.shadowColor = UIColor.clear.cgColor
        self.navigationBar.layer.shadowOpacity = 0.8
        self.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        self.navigationBar.layer.shadowRadius = 2

    }
    
    func setBtnTitle(title :String) -> UIBarButtonItem {
        let BarButton: UIBarButtonItem = UIBarButtonItem.init(title: title, style: .plain, target: self, action: #selector(didClickRightButton))
        BarButton.tintColor = .black
        return BarButton
    }
    
    func setLogotitle(sender :UIViewController){
        let logo = UIImage(named: "logo")
        let imageView = UIImageView(image:logo)
        sender.navigationItem.titleView = imageView
    }
    
    var isDark = false {
         didSet {
             setNeedsStatusBarAppearanceUpdate()
         }
     }

     override var preferredStatusBarStyle: UIStatusBarStyle {
         return isDark ? .lightContent : .default
     }

    
    
    func setCustomBackButtonWithdismiss(sender :UIViewController){
        if MOLHLanguage.isRTLLanguage() {
            let back: UIImage? = UIImage(named:"rightwhite_back")?.withRenderingMode(.alwaysOriginal)
            
            
            sender.navigationItem.leftBarButtonItem = UIBarButtonItem(image: back, style: UIBarButtonItem.Style.plain, target: self, action: #selector(backButtonActionWithdismiss))
            
        } else{
            let back: UIImage? = UIImage(named:"leftwhite_back")?.withRenderingMode(.alwaysOriginal)
            
            sender.navigationItem.leftBarButtonItem = UIBarButtonItem(image: back, style: UIBarButtonItem.Style.plain, target: self, action: #selector(backButtonActionWithdismiss))
            
        }
    }
    func BackButtonWithdismiss(sender :UIViewController)-> UIBarButtonItem{
        if MOLHLanguage.isRTLLanguage() {
            let back: UIImage? = UIImage(named:"right_back")?.withRenderingMode(.alwaysOriginal)
            
            return UIBarButtonItem(image: back, style: UIBarButtonItem.Style.plain, target: self, action: #selector(backButtonAction))
        }
            let back: UIImage? = UIImage(named:"left_back")?.withRenderingMode(.alwaysOriginal)
            
            return UIBarButtonItem(image: back, style: UIBarButtonItem.Style.plain, target: self, action: #selector(backButtonAction))
            
        
    }

    func setCustomBackButtonForViewController(sender :UIViewController){
        if MOLHLanguage.isRTLLanguage() {
            let back: UIImage? = UIImage(named:"right_back")?.withRenderingMode(.alwaysOriginal)
            
            sender.navigationItem.leftBarButtonItem = UIBarButtonItem(image: back, style: UIBarButtonItem.Style.plain, target: self, action: #selector(backButtonAction))
            
        } else{
            let back: UIImage? = UIImage(named:"left_back")?.withRenderingMode(.alwaysOriginal)
            
            sender.navigationItem.leftBarButtonItem = UIBarButtonItem(image: back, style: UIBarButtonItem.Style.plain, target: self, action: #selector(backButtonAction))
        }
        
        
    }
    func setCustomBackButtonWhiteForViewController(sender :UIViewController){
        if MOLHLanguage.isRTLLanguage() {
            let back: UIImage? = UIImage(named:"rightwhite_back")?.withRenderingMode(.alwaysOriginal)
            
            sender.navigationItem.leftBarButtonItem = UIBarButtonItem(image: back, style: UIBarButtonItem.Style.plain, target: self, action: #selector(backButtonAction))
            
        } else{
            let back: UIImage? = UIImage(named:"leftwhite_back")?.withRenderingMode(.alwaysOriginal)
            
            sender.navigationItem.leftBarButtonItem = UIBarButtonItem(image: back, style: UIBarButtonItem.Style.plain, target: self, action: #selector(backButtonAction))
        }
        
        
    }
    
    
    func setCustomBackButtonWhiteForRootViewController(sender :UIViewController){
         if MOLHLanguage.isRTLLanguage() {
             let back: UIImage? = UIImage(named:"rightwhite_back")?.withRenderingMode(.alwaysOriginal)
             
             sender.navigationItem.leftBarButtonItem = UIBarButtonItem(image: back, style: UIBarButtonItem.Style.plain, target: self, action: #selector(backRootButtonAction))
             
         } else{
             let back: UIImage? = UIImage(named:"leftwhite_back")?.withRenderingMode(.alwaysOriginal)
             
             sender.navigationItem.leftBarButtonItem = UIBarButtonItem(image: back, style: UIBarButtonItem.Style.plain, target: self, action: #selector(backRootButtonAction))
         }
         
         
     }
    
    
    
    
    
    
    func setMeunButton(sender :UIViewController){
        
//        sender.navigationItem.leftBarButtonItem = langBtn
        
    }
   
    
    
    @objc func ShowMenuAction (_sender: UIBarButtonItem){
        guard let window = UIApplication.shared.keyWindow else { return }

        let centerNavVC: CustomNavigationBar = AppDelegate.sb_main.instanceVC()
        
//        let vc:LanguageVC = AppDelegate.sb_main.instanceVC()
//
//        centerNavVC.pushViewController(vc, animated: true)
        
        window.rootViewController = centerNavVC

        
    }
    
    func setLeftButtons (_ buttons: NSArray,sender : UIViewController){
        sender.navigationItem.leftBarButtonItems = buttons as? [UIBarButtonItem]
    }
    
    

    func setRightButtons (_ buttons: NSArray,sender : UIViewController){
        sender.navigationItem.rightBarButtonItems = buttons as? [UIBarButtonItem]
    }
    
   
    
    @objc func didClickRightButton(_sender: UIBarButtonItem) {
        let ViewController = self.viewControllers.last as! SuperViewController
       ViewController.didClickRightButton(_sender: _sender)

    }
    
    @objc func backButtonAction(_sender: UIBarButtonItem) {
        if (self.viewControllers.last?.isKind(of: SuperViewController.self))!

        {
          let ViewController = self.viewControllers.last as! SuperViewController
          ViewController.backButtonAction(_sender: _sender)
        }else if (self.viewControllers.last?.isKind(of: BaseViewController.self))!

             {
               let ViewController = self.viewControllers.last as! BaseViewController
               ViewController.backButtonAction(_sender: _sender)

             }
    }
    @objc func backRootButtonAction(_sender: UIBarButtonItem) {
        if (self.viewControllers.last?.isKind(of: SuperViewController.self))!

        {
          let ViewController = self.viewControllers.last as! SuperViewController
          ViewController.backRootButtonAction(_sender: _sender)
        }else if (self.viewControllers.last?.isKind(of: BaseViewController.self))!

             {
               let ViewController = self.viewControllers.last as! BaseViewController
               ViewController.backRootButtonAction(_sender: _sender)

             }
    }
    
    @objc func backButtonActionWithdismiss(_sender: UIBarButtonItem) {
        
        if (self.viewControllers.last?.isKind(of: SuperViewController.self))!

        {
            let ViewController = self.viewControllers.last as! SuperViewController
            ViewController.backButtonActionWithdismiss(_sender: _sender)
            
        }else if (self.viewControllers.last?.isKind(of: BaseViewController.self))!

             {
               let ViewController = self.viewControllers.last as! BaseViewController
            ViewController.dismiss(animated: true, completion: nil)


             }
        
       
        
    }
    
    func setTitle (_ title: String, sender : UIViewController, Srtingcolor:String = "ffffff"){
        let attrs = [
            NSAttributedString.Key.foregroundColor: UIColor.green,
            NSAttributedString.Key.font: UIFont(name: "FFShamelFamilySemiRoundBook-S", size: 17)!
        ]
        sender.navigationController?.navigationBar.titleTextAttributes = attrs
        //sender.navigationController?.navigationBar.topItem?.title = title as String
        sender.navigationItem.title = title as String

        sender.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : Srtingcolor.color]

        sender.navigationItem.titleView = nil
//        let leftSearchBarButtonItem = sender.navigationItem.leftBarButtonItems
//
//        let longTitleLabel = UILabel()
//        longTitleLabel.text = title
//        longTitleLabel.attributedText = NSAttributedString(string: title, attributes: attrs)
//        longTitleLabel.sizeToFit()
//
//        let leftItem = UIBarButtonItem(customView: longTitleLabel)
//        if (sender.navigationController?.viewControllers.count)! > 1 {
//            sender.navigationItem.leftBarButtonItems = [BackButtonWithdismiss(sender: sender),leftItem]
//
//        }else{
//            sender.navigationItem.leftBarButtonItems = [leftItem]
//
//        }


    }
    
    func UIColorFromRGB(rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
    
}
extension UIImage {
    
    func maskWithColor(color: UIColor) -> UIImage? {
        let maskImage = cgImage!
        
        let width = size.width
        let height = size.height
        let bounds = CGRect(x: 0, y: 0, width: width, height: height)
        
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
        let context = CGContext(data: nil, width: Int(width), height: Int(height), bitsPerComponent: 8, bytesPerRow: 0, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)!
        
        context.clip(to: bounds, mask: maskImage)
        context.setFillColor(color.cgColor)
        context.fill(bounds)
        
        if let cgImage = context.makeImage() {
            let coloredImage = UIImage(cgImage: cgImage)
            return coloredImage
        } else {
            return nil
        }
    }
    
}
