
//
//  SuperItem.swift
//
//  Created by ahmed on 6/25/19.
//  Copyright © 2019 ahmed. All rights reserved.
//

import UIKit
import Alamofire

//protocol  superItemDataSource : NSObjectProtocol {
//
////    func ItemSetupNavigation(viewPager: SuperItem)
//    func ItemSetupNavigation(view: SuperItem, prepareNavigationBar: CustomNavigationBar)
//
//}
class SuperItem :NSObject  {
    
    var viewController: SuperViewController!
    var withIdentifierCell: [String]!
    var tableView: UITableView!
    var countItem = 0
    var RequestUrl = ""
    var emptyImage = ""
    var emptyText = ""
    var emptyBtnText = ""

    
    var typeData:Any!
    var parameters = [String : Any]()

    func prepareTable() {
        if tableView != nil{
            for id in withIdentifierCell{
                tableView.registerCell(id: id)
            }
            tableView.reloadData()
        }
    }
    
    
    func LoadItem()  {
        
    }

    /// UITableViewDelegate
    func getCount() -> Int
    {
        return 0
    }
    
    func prepareCellForData(in tableView: UITableView, Data: Any ,indexPath : IndexPath) -> UITableViewCell
    {
        return UITableViewCell()
    }
    
    func prepareHeader(in tableView: UITableView, Data: Any ,viewForHeaderInSection section: Int) -> UIView?
    {
        return UITableViewCell()
    }
    
    func heightForHeaderInSection(section: Int) -> CGFloat
    {
        return 0.0
        
    }
    
    func prepareCellheight(indexPath : IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
        
    }
    func cellDidSelected(indexPath : IndexPath)
    {
        // do sometheing
        
    }
    
    
    
    
    func ItemSetupNavigation(forViewController: SuperViewController, prepareNavigationBar: CustomNavigationBar,title : String){
        
        self.viewController = forViewController
//        prepareNavigationBar.setTitle(title, sender: forViewController)
        prepareNavigationBar.setTitle(title, sender: forViewController,Srtingcolor :"AECB1B")
        
    }
    

}
