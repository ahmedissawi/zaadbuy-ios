//
//  StartVC.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 6/16/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class StartVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startVC()
        
    }
    func startVC(){
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if !CurrentUser.SelectfirtTime!{
                let nav:CustomNavigationBar = AppDelegate.sb_main.instanceVC()
                let vc = LanguageVC.loadFromNib()
                nav.pushViewController(vc, animated: true)
                nav.modalPresentationStyle = .fullScreen
                self.present(nav, animated: true, completion: nil)
            }else if CurrentUser.firtTime!{
                let vc:OnBordingVC = AppDelegate.sb_main.instanceVC()
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
            }else{
                if CurrentUser.userInfo != nil{
                    let vc:TTabBarViewController = AppDelegate.sb_main.instanceVC()
                    vc.modalPresentationStyle = .fullScreen
                    self.present(vc, animated: true, completion: nil)
                }else{
                    let nav:CustomNavigationBar = AppDelegate.sb_main.instanceVC()
                    let vc:LoginVC = AppDelegate.sb_main.instanceVC()
                    nav.pushViewController(vc, animated: true)
                    nav.modalPresentationStyle = .fullScreen
                    self.present(nav, animated: true, completion: nil)
                    
                }
                
            }
//            if !UserDefaults.standard.bool(forKey: "didSee") {
//                UserDefaults.standard.set(true, forKey: "didSee")
//                let vc:LanguageVC = AppDelegate.sb_main.instanceVC()
//                vc.modalPresentationStyle = .fullScreen
//                self.present(vc, animated: true, completion: nil)
//
//            }else{
//
//            }
        }
        
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
}
