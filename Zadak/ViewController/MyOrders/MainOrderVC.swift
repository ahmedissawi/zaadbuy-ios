//
//  MainOrderDeatilesVC.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 6/13/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import MXSegmentedPager

class MainOrderVC: BaseViewController {
    
    @IBOutlet weak var view_header: UIView!

    
    var arrayView = [String]()
    var arrayViewController = [UIViewController]()


    override func viewDidLoad() {
        super.viewDidLoad()
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = "FF6A2A".color
        arrayView = ["في انتظار الدفع","تم الدفع"," تم الشحن","تم التسليم","ملغي" ]

        if MOLHLanguage.isRTLLanguage() {
            arrayView = arrayView.reversed()
            arrayViewController = arrayViewController.reversed()
        }
        setUpSegmentation()
        // Do any additional setup after loading the view.
    }
    
   

    
}

extension MainOrderVC {
    func setUpSegmentation (){
        
        if MOLHLanguage.isRTLLanguage(){
              self.segmentedPager.pager.showPage(at:self.arrayView.count - 1, animated: false)
           }
        DispatchQueue.main.async {
        if MOLHLanguage.isRTLLanguage(){
           self.segmentedPager.pager.showPage(at:self.arrayView.count - 1, animated: false)
           self.segmentedPager.segmentedControl.selectedSegmentIndex = self.arrayView.count - 1
          // segmentedPager.pager.reloadData()
                              }
            }
        
        segmentedPager.backgroundColor = UIColor.white
        segmentedPager.segmentedControl.backgroundColor = UIColor.white
        segmentedPager.parallaxHeader.view = nil
        segmentedPager.parallaxHeader.mode = .bottom
        segmentedPager.parallaxHeader.height = 0
        segmentedPager.parallaxHeader.minimumHeight = 0
        
        
         // Segmented Control customization
            self.segmentedPager.segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocation.down
           segmentedPager.segmentedControl.selectedTitleTextAttributes = [NSAttributedString.Key.foregroundColor: "014A97".color]
        
        segmentedPager.segmentedControl.selectionIndicatorColor = "FF6A2A".color
        segmentedPager.segmentedControl.selectionIndicatorHeight = 1
        
        self.segmentedPager.segmentedControl.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "FFShamelFamilySemiRoundBook-S", size: 17)!, NSAttributedString.Key.foregroundColor :  "014A97".color]

        
    }
    
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, titleForSectionAt index: Int) -> String {
           
        switch index {
            case 0:
             return "Cancelled".localized

            case 1:
             return "Delivered".localized

            case 2:
                return "Shipped".localized
            case 3:
                return "Paid".localized
            case 4:
             return "Pending payment".localized

            default:
                break
            }
         return "Delivered".localized


       }
    
       override func segmentedPager(_ segmentedPager: MXSegmentedPager, didScrollWith parallaxHeader: MXParallaxHeader) {
       }
       
       
       override func numberOfPages(in segmentedPager: MXSegmentedPager) -> Int {
           return 5
       }
    
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, viewControllerForPageAt index: Int) -> UIViewController {
        switch index {
            case 4:
                return W0VC()
            case 3:
                return W1VC()
            case 2:
                return W2VC()
            case 1:
                return W3VC()
            case 0:
                return W4VC()
            default:
                break
            }
         return W0VC()

        
    }
    

    fileprivate  func W0VC() ->WaitingOrdersVC{
             let vc = self.instantiate(id: "WaitingOrdersVC") as! WaitingOrdersVC
            vc.Link = "status=0"
             addChild(vc)
             vc.didMove(toParent: self)
             return vc
         }
     fileprivate  func W1VC() ->WaitingOrdersVC{
                  let vc = self.instantiate(id: "WaitingOrdersVC") as! WaitingOrdersVC
                 vc.Link = "status=1"
                  addChild(vc)
                  vc.didMove(toParent: self)
                  return vc
              }
    fileprivate  func W2VC() ->WaitingOrdersVC{
                let vc = self.instantiate(id: "WaitingOrdersVC") as! WaitingOrdersVC
               vc.Link = "status=2"
                addChild(vc)
                vc.didMove(toParent: self)
                return vc
            }
    
    
    fileprivate  func W3VC() ->WaitingOrdersVC{
                let vc = self.instantiate(id: "WaitingOrdersVC") as! WaitingOrdersVC
               vc.Link = "status=3"
                addChild(vc)
                vc.didMove(toParent: self)
                return vc
            }
    fileprivate  func W4VC() ->WaitingOrdersVC{
                let vc = self.instantiate(id: "WaitingOrdersVC") as! WaitingOrdersVC
               vc.Link = "status=4"
                addChild(vc)
                vc.didMove(toParent: self)
                return vc
            }
    fileprivate  func W5VC() ->WaitingOrdersVC{
                let vc = self.instantiate(id: "WaitingOrdersVC") as! WaitingOrdersVC
               vc.Link = "status=5"
                addChild(vc)
                vc.didMove(toParent: self)
                return vc
            }
}
