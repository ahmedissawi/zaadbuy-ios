//
//  PaymentReceiptVC.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 6/13/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class PaymentReceiptVC: SuperViewController {
    
    @IBOutlet weak var tfnumber: UITextField!
    @IBOutlet weak var txtNote: UITextView!
    @IBOutlet weak var image: UIImageView!

    var orderID:Int?
    var imagePicker: ImagePicker!
    var SelectImage = false

    


    override func viewDidLoad() {
        super.viewDidLoad()
        self.imagePicker = ImagePicker(presentationController: self, delegate: self)

        // Do any additional setup after loading the view.
    }
    
    @IBAction func bt_send(_ sender: Any) {
        
        var parameters: [String: String] = [:]
        parameters["transaction_no"] = self.tfnumber.text ?? ""
        parameters["user_notes"] = self.txtNote.text ?? ""
        parameters["order_id"] = orderID?.description

        
                        self.showIndicator()
        let imageData = (image.image) ?? UIImage()

        _ = WebRequests.sendPostMultipartRequestWithImgParam(url: TAConstant.APIBaseURL + "orders/bank-pay", parameters:parameters ,img: imageData, withName: "document", completion: { (response, error) in
            self.hideIndicator()

                            do {
                                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                                if Status.status != 200 {
                                    
                                    self.showAlert(title: "error".localized, message:Status.message!)
                                    return
                                }else{
                                    self.navigationController?.popViewController(animated: true)
            
                                }
        
                            }catch let jsonErr {
                                print("Error serializing  respone json", jsonErr)
                            }
        
                        }
                    )

    }
   

}
extension PaymentReceiptVC: ImagePickerDelegate {

    @IBAction func showImagePicker(_ sender: UIButton) {
        self.imagePicker.present(from: sender)
    }
    func didSelect(image: UIImage?) {
        self.image.image = image
    }
}
