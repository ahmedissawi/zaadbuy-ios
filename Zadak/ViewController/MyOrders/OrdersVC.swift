//
//  OrdersVC.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 2/21/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import BmoViewPager

private let mainColor = "FF6A2A".color

class OrdersVC: SuperViewController {
    
    @IBOutlet weak var viewPagerNavigationBar: BmoViewPagerNavigationBar!
       
    @IBOutlet weak var viewPager: BmoViewPager!
       

    override func viewDidLoad() {
        super.viewDidLoad()
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = "FF6A2A".color
        viewPager.dataSource = self
        viewPagerNavigationBar.autoFocus = false
        viewPagerNavigationBar.viewPager = viewPager
        
        viewPager.layer.borderWidth = 5.0
        viewPager.layer.cornerRadius = 5.0
        viewPager.layer.masksToBounds = true
        viewPager.layer.borderColor = UIColor.white.cgColor

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension OrdersVC: BmoViewPagerDataSource {
    // Optional
    func bmoViewPagerDataSourceNaviagtionBarItemNormalAttributed(_ viewPager: BmoViewPager, navigationBar: BmoViewPagerNavigationBar, forPageListAt page: Int) -> [NSAttributedString.Key : Any]? {
        return [
            NSAttributedString.Key.font : UIFont(name: "FFShamelFamilySemiRoundBook-S", size: 15)!,
            NSAttributedString.Key.foregroundColor : "014A97".color
        ]
    }
    func bmoViewPagerDataSourceNaviagtionBarItemHighlightedAttributed(_ viewPager: BmoViewPager, navigationBar: BmoViewPagerNavigationBar, forPageListAt page: Int) -> [NSAttributedString.Key : Any]? {
        return [
            NSAttributedString.Key.font : UIFont(name: "FFShamelFamilySemiRoundBook-S", size: 15)!,
            NSAttributedString.Key.foregroundColor : "014A97".color
        ]
    }
    func bmoViewPagerDataSourceNaviagtionBarItemHighlightedBackgroundView(_ viewPager: BmoViewPager, navigationBar: BmoViewPagerNavigationBar, forPageListAt page: Int) -> UIView? {
        let view = UnderLineView()
        view.marginX = 0.0
        view.lineWidth = 5
        view.strokeColor = mainColor
        return view
    }
    func bmoViewPagerDataSourceNaviagtionBarItemNormalBackgroundView(_ viewPager: BmoViewPager, navigationBar: BmoViewPagerNavigationBar, forPageListAt page: Int) -> UIView? {
        let view = UnderLineView()
        view.marginX = 0.0
        view.lineWidth = 5
        view.strokeColor = "E2E2E2".color
        return view
    }
    func bmoViewPagerDataSourceNaviagtionBarItemTitle(_ viewPager: BmoViewPager, navigationBar: BmoViewPagerNavigationBar, forPageListAt page: Int) -> String? {
        switch page {
        case 0:
            return "Being processed".localized
        case 1:
            return "Shipping".localized
        case 2:
            return "Delivered".localized
        case 3:
            return "Rate".localized
        default:
            break
        }
        return "Demo \(page)"

    }
    
    // Required
    func bmoViewSplitFull(in viewPager: BmoViewPager) -> Bool {
       return true
    }
    func bmoViewPagerDataSourceNumberOfPage(in viewPager: BmoViewPager) -> Int {
        return 4
        
    }
    func bmoViewPagerDataSource(_ viewPager: BmoViewPager, viewControllerForPageAt page: Int) -> UIViewController {
        switch page {
//        case 0:
//            if let vc:OpenOrders = AppDelegate.sb_main.instanceVC() as? OpenOrders {
//                return vc
//            }
//        case 1:
//            if let vc:CompletedOrders = AppDelegate.sb_main.instanceVC() as? CompletedOrders {
//                return vc
//            }
            case 0:
            if let vc = storyboard?.instantiateViewController(withIdentifier: "WaitingOrdersVC") as? WaitingOrdersVC {
               
            return vc
            }
        case 1:
            if let vc = storyboard?.instantiateViewController(withIdentifier: "UnderShipmentVC") as? UnderShipmentVC {
               
                return vc
            }
        case 2:
            if let vc = storyboard?.instantiateViewController(withIdentifier: "DeliveryOrdersVC") as? DeliveryOrdersVC {
                          
                    return vc
            }
        case 3:
            if let vc = storyboard?.instantiateViewController(withIdentifier: "RateVC") as? RateVC {
                                     
                return vc
            }
       
        default:
            break
        }
        return UIViewController()
    }
}



@IBDesignable
class UnderLineView: UIView {
    @IBInspectable var strokeColor: UIColor = UIColor.black {
        didSet {
            self.setNeedsDisplay()
        }
    }
    @IBInspectable var lineWidth: CGFloat = 2.0 {
        didSet {
            self.setNeedsDisplay()
        }
    }
    @IBInspectable var marginX: CGFloat = 0.0 {
        didSet {
            self.setNeedsDisplay()
        }
    }
    init() {
        super.init(frame: CGRect.zero)
        self.backgroundColor = UIColor.clear
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }
        strokeColor.setStroke()
        context.setLineWidth(lineWidth)
        context.move(to: CGPoint(x: rect.minX + marginX, y: rect.maxY))
        context.addLine(to: CGPoint(x: rect.maxX - marginX, y: rect.maxY))
        context.strokePath()
    }
}





