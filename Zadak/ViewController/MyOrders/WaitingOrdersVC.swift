//
//  WaitingOrdersVC.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 2/22/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import ESPullToRefresh
class WaitingOrdersVC: SuperViewController {
    var OrdersItems = [Orders]()
    var currentpage = 1

    var dataClass: DataClass?
    var paymentRequest: PaymentRequest?
    
    var Link = "status=0"
    @IBOutlet weak var Table: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        Table.registerCell(id: "OrdersCell")
        getOrders()
        
        self.Table.es.addPullToRefresh {
            self.currentpage = 1
            self.OrdersItems.removeAll()
            self.getOrders()
            self.hideIndicator()
        }
        
        self.Table.es.addInfiniteScrolling {
            self.currentpage += 1
            self.getOrders() // next page
//            self.Table.estimatedRowHeight = 0
            self.hideIndicator()
        }
        
        NotificationCenter.default.addObserver(self, selector:#selector(UpdateOrderPay(notification:)), name: Notification.Name("UpdateOrderPay"), object: nil)
    }
    
    func getOrders(){
        _ = WebRequests.setup(controller: nil).prepare(query: "orders?page=\(currentpage)&" + Link, method: HTTPMethod.get).start(){ (response, error) in
            
            self.Table.es.stopLoadingMore()
            self.Table.es.stopPullToRefresh()
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            do {
                let Status =  try JSONDecoder().decode(OrderClass.self, from: response.data!)
                
                self.OrdersItems += Status.data?.orders as! [Orders]
                if self.OrdersItems.count < 10 {
                    self.Table.es.stopLoadingMore()
                    self.Table.es.noticeNoMoreData()
                }
                if self.OrdersItems.count == 0{
                    self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.Table, refershSelector: #selector(self.didRefersh))
                    if self.Link == "status=0"{
                        self.emptyView?.firstLabel.text = "No orders waiting for payment".localized
                    }
                    if self.Link == "status=1"{
                        self.emptyView?.firstLabel.text = "There are no order in paid".localized
                    }
                    if self.Link == "status=2"{
                        self.emptyView?.firstLabel.text = "No orders shipped".localized
                    }
                    if self.Link == "status=3"{
                        self.emptyView?.firstLabel.text = "No Orders Delivered".localized
                    }
                    if self.Link == "status=4"{
                        self.emptyView?.firstLabel.text = "There are no order in the Canceled".localized
                    }
                    self.OrdersItems.removeAll()
                    self.Table.reloadData()
                    
                }else{
                    self.Table.tableFooterView = nil
                }
                self.Table.reloadData()
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
    }
    @objc func UpdateOrderPay(notification: NSNotification)  {
        OrdersItems.removeAll()
        getOrders()
    }
    @IBAction func didRefersh(){
        OrdersItems.removeAll()
        getOrders()
    }
    

}
extension WaitingOrdersVC:UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return OrdersItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrdersCell", for: indexPath) as! OrdersCell
        cell.items = OrdersItems[indexPath.row]
        cell.btdeatiles.tag = indexPath.row
        cell.btseller.tag = indexPath.row
        cell.btpay.tag = indexPath.row
        cell.btPayseconds.tag = indexPath.row
        cell.btacount.tag = indexPath.row
        cell.btdisputes.tag = indexPath.row
        cell.btdeatiles.addTarget(self, action: #selector(self.orderdeatiles), for: UIControl.Event.touchUpInside)
        cell.btseller.addTarget(self, action: #selector(self.orderseller), for: UIControl.Event.touchUpInside)
        cell.btacount.addTarget(self, action: #selector(self.orderaacount), for: UIControl.Event.touchUpInside)
        cell.btpay.addTarget(self, action: #selector(self.orderpay), for: UIControl.Event.touchUpInside)
        cell.btPayseconds.addTarget(self, action: #selector(self.orderbtPayseconds), for: UIControl.Event.touchUpInside)
        cell.btdisputes.addTarget(self, action: #selector(self.orderdisputes), for: UIControl.Event.touchUpInside)

        return cell
    }
    
    @objc func orderdeatiles(_ sender:UIButton ){
        let vc:MainOrderDeatilesVC = AppDelegate.sb_main.instanceVC()
        vc.OrdersItem = OrdersItems[sender.tag]
        vc.hidesBottomBarWhenPushed = true
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc func orderdisputes(_ sender:UIButton ){
        let obj = self.OrdersItems[sender.tag]
        let vc:MakeDisputesVC = MakeDisputesVC.loadFromNib()
        vc.orderID = obj.id ?? 0
        vc.Orderitems = obj.items!
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    @objc func orderseller(_ sender:UIButton ){
        
       DispatchQueue.main.async {
        let obj = self.OrdersItems[sender.tag]
        let vc:MessagesAllVC = AppDelegate.sb_main.instanceVC()
        vc.IDMessgae = obj.seller?.id
        vc.Storename = obj.seller?.name
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)

        }

    }
    @objc func orderpay(_ sender:UIButton ){
        let obj = OrdersItems[sender.tag]
        let vc:PaymentReceiptVC = AppDelegate.sb_main.instanceVC()
        vc.orderID  = obj.id
        vc.hidesBottomBarWhenPushed = true
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)

    }
    @objc func orderaacount(_ sender:UIButton ){
        let obj = OrdersItems[sender.tag]
        let vc:SellerDeatilesVC = AppDelegate.sb_main.instanceVC()
        vc.hidesBottomBarWhenPushed = true
        vc.OrdersItems = obj
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: false, completion: nil)

        
    }
    @objc func orderbtPayseconds(_ sender:UIButton ){
        let obj = OrdersItems[sender.tag]

        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        
        
        var parameters: [String: Any] = [:]
            parameters["order_id"] = obj.id ?? 0
        
        
        _ = WebRequests.setup(controller: self).prepare(query: "orders/thawani-pay", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200 {
                    self.showAlert(title: "Error".localized, message:Status.message!)
                    return
                }
                else if Status.status == 200{
                    let Status =  try JSONDecoder().decode(ServiceOrdersPay.self, from: response.data!)
                    self.dataClass = Status.data
                    self.paymentRequest =  self.dataClass?.paymentRequest
                    if self.paymentRequest!.success && !self.paymentRequest!.returnurl.isEmpty{
                        if let url = URL(string: self.paymentRequest!.returnurl.description ) {
                            
                            self.tabBarController?.selectedIndex = 2
                            if UIApplication.shared.canOpenURL(url) {
                               if #available(iOS 10.0, *) {
                                   UIApplication.shared.open(url, options: [:]) { (sucess) in
                                       UserDefaults.standard.set(sucess, forKey: "OpenUrlPayOrder")
                                   }
                               } else {
                                   UIApplication.shared.openURL(url)
                               }
                           }
                            
                          }
                    }
                }
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
          
        }
        

    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
    
    
}
