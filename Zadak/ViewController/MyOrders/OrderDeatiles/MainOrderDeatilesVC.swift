//
//  MainOrderDeatilesVC.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 6/13/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import MXSegmentedPager

class MainOrderDeatilesVC: BaseViewController {
    
    @IBOutlet weak var view_header: UIView!
    var OrdersItem:Orders?
    
    
    var arrayView = [String]()
    var arrayViewController = [UIViewController]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let navgtion = self.navigationController as! CustomNavigationBar
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(descriptor: UIFontDescriptor(name: "FFShamelFamilySemiRoundBook-S", size: 17), size: 17),NSAttributedString.Key.foregroundColor: UIColor.white]
               self.title = "Details order".localized
        navigationController?.navigationBar.barTintColor = "FF6A2A".color
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        arrayView = ["Basic Data".localized, "Product Data".localized]
        arrayViewController = [InformationOrder(),DeatilesInformation()]
        if MOLHLanguage.isRTLLanguage() {
            arrayView = arrayView.reversed()
            arrayViewController = arrayViewController.reversed()
        }
        
        setUpSegmentation()
    }
    
    
    
    
}

extension MainOrderDeatilesVC {
    func setUpSegmentation (){
        
        if MOLHLanguage.isRTLLanguage(){
            self.segmentedPager.pager.showPage(at:self.arrayView.count - 1, animated: false)
        }
        DispatchQueue.main.async {
            if MOLHLanguage.isRTLLanguage(){
                self.segmentedPager.pager.showPage(at:self.arrayView.count - 1, animated: false)
                self.segmentedPager.segmentedControl.selectedSegmentIndex = self.arrayView.count - 1
                //                         segmentedPager.pager.reloadData()
            }
        }
        
        segmentedPager.backgroundColor = UIColor.white
        segmentedPager.segmentedControl.backgroundColor = UIColor.white
        segmentedPager.parallaxHeader.view = view_header
        segmentedPager.parallaxHeader.mode = .bottom
        segmentedPager.parallaxHeader.height = 45
        segmentedPager.parallaxHeader.minimumHeight = 45
        
        
        // Segmented Control customization
        self.segmentedPager.segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocation.down
        segmentedPager.segmentedControl.selectedTitleTextAttributes = [NSAttributedString.Key.foregroundColor: "014A97".color]
        
        segmentedPager.segmentedControl.selectionIndicatorColor = "FF6A2A".color
        segmentedPager.segmentedControl.selectionIndicatorHeight = 3
        
        self.segmentedPager.segmentedControl.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "FFShamelFamilySemiRoundBook-S", size: 17)!, NSAttributedString.Key.foregroundColor :  "014A97".color]
        
        
    }
    
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, titleForSectionAt index: Int) -> String {
        
        
        return arrayView[index].localized
        
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, didScrollWith parallaxHeader: MXParallaxHeader) {
    }
    
    
    override func numberOfPages(in segmentedPager: MXSegmentedPager) -> Int {
        return arrayView.count
    }
    
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, viewControllerForPageAt index: Int) -> UIViewController {
        return arrayViewController[index]
        
        
    }
    
    fileprivate  func InformationOrder() ->InformationOrderDeatilesVC{
        let vc = self.instantiate(id: "InformationOrderDeatilesVC") as! InformationOrderDeatilesVC
        vc.orderID = OrdersItem?.id
        addChild(vc)
        vc.didMove(toParent: self)
        return vc
    }
    fileprivate  func DeatilesInformation() ->InformationProducatVC{
        let vc = self.instantiate(id: "InformationProducatVC") as! InformationProducatVC
        vc.orderID = OrdersItem?.id
        addChild(vc)
        vc.didMove(toParent: self)
        return vc
    }
    
}
