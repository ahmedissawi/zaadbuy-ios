//
//  InformationOrderDeatilesVC.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 6/13/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class InformationOrderDeatilesVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var orderID:Int?
    var orderDeatiles:OrdersDeatilesData?
    var orderDeatileInfo:BasicInfo?
    


    override func viewDidLoad() {
        super.viewDidLoad()
        getOrdersDeatiles()
        tableView.registerCell(id: "InformationOrderCell")
        // Do any additional setup after loading the view.
    }
    
    func getOrdersDeatiles(){

            _ = WebRequests.setup(controller: self).prepare(query: "orders/\(orderID ?? 0)", method: HTTPMethod.get).start(){ (response, error) in
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200{
                        self.showAlert(title: "error".localized, message:Status.message!)
                        return
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
                
                do {
                    let Status =  try JSONDecoder().decode(OrdersDeatilesInfo.self, from: response.data!)
                    self.orderDeatiles = Status.data!
                    self.orderDeatileInfo = self.orderDeatiles?.basicInfo
                    self.tableView.reloadData()
                } catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
                
            }
            
    }
    

}

extension InformationOrderDeatilesVC:UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InformationOrderCell", for: indexPath) as! InformationOrderCell
        cell.lblOrdernumber.text = "\(self.orderDeatileInfo?.code ?? "0")"
        cell.lblDate.text = self.orderDeatileInfo?.orderDate
        cell.lblStatus.text = self.orderDeatileInfo?.status
        cell.lblPaument.text = self.orderDeatileInfo?.paymentTitle
        cell.lblCodeDiscount.text = self.orderDeatileInfo?.coupon
        cell.lblTotal.text = "\(self.orderDeatileInfo?.total ?? 0)"
        cell.lblCapounDiscount.text = "\(self.orderDeatileInfo?.couponAmount ?? 0)"
        cell.lblOffers.text = "\(self.orderDeatileInfo?.orderDiscount ?? 0)"

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 334
    }
    
    
    
}
