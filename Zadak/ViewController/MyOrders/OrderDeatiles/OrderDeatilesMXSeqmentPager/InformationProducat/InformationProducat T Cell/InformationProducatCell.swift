//
//  InformationProducatCell.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 6/13/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class InformationProducatCell: UITableViewCell {
    
    
    @IBOutlet weak var lblNameProducat: UILabel!
    @IBOutlet weak var lblPriceUnit: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblService: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var imgProducat: UIImageView!

    var Orders : ItemsOrderDeatiles?{
    didSet{
            lblNameProducat.text = Orders?.title
            lblPriceUnit.text = "\(Orders?.unitPrice ?? 0)"  + " " + "O.R".localized
            lblAmount.text = "\(Orders?.quantity ?? 0)"
            lblService.text = Orders?.shippingService
            lblPrice.text = "\(Orders?.shippingPrice ?? 0 )"  + " " + "O.R".localized
            lblTotal.text = "\(Orders?.total ?? 0)"  + " " + "O.R".localized
            imgProducat.sd_custom(url: Orders?.image ?? "")
        }
        
    }


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    
}

