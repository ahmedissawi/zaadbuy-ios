//
//  InformationProducatVC.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 6/13/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class InformationProducatVC: UIViewController {
    var OrdersItem = [ItemsOrderDeatiles]()
    var orderID:Int?
    var orderDeatiles:OrdersDeatilesData?
    
    
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getOrdersDeatiles()
        tableView.registerCell(id: "InformationProducatCell")
    }
    
    
    
    func getOrdersDeatiles(){
        
        _ = WebRequests.setup(controller: self).prepare(query: "orders/\(orderID ?? 0)", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            do {
                let Status =  try JSONDecoder().decode(OrdersDeatilesInfo.self, from: response.data!)
                self.orderDeatiles = Status.data!
                self.OrdersItem = self.orderDeatiles!.items!
                self.tableView.reloadData()
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            
        }
        
    }
    
    
    
}

extension InformationProducatVC:UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return OrdersItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InformationProducatCell", for: indexPath) as! InformationProducatCell
        cell.Orders = OrdersItem[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 205
    }
    
    
    
}
