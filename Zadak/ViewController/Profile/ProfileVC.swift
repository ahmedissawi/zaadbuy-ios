//
//  ProfileVC.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 2/21/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class ProfileVC: SuperViewController {
    
    @IBOutlet weak var img_profile: UIImageView!
    @IBOutlet weak var lb_name: UILabel!
    @IBOutlet weak var btlogout: UIButton!
    @IBOutlet weak var lblversion: UILabel!

    
    var items = [InfoData]()
    var ID:Int?
    
    
    enum SocialNetwork {
        case Facebook, Twitter, Instagram
        func url() -> SocialNetworkUrl {
            switch self {
            case .Facebook: return SocialNetworkUrl(scheme: "fb://profile?id=ZaadOman", page: "https://www.facebook.com/ZaadOman")
            case .Twitter: return SocialNetworkUrl(scheme: "twitter:///user?screen_name=ZaadOman", page: "https://twitter.com/ZaadOman")
            case .Instagram: return SocialNetworkUrl(scheme: "instagram://user?username=ZaadOman", page:"https://www.instagram.com/ZaadOman")

          }
        }
        func openPage() {
            self.url().openPage()
        }
     }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let text = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            print(text)
            self.lblversion.text =  "App Version".localized + " " + "(\(text))"
        }
        getInfo()
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(descriptor: UIFontDescriptor(name: "FFShamelFamilySemiRoundBook-S", size: 17), size: 17),NSAttributedString.Key.foregroundColor: UIColor.white]
        self.title = "Profile".localized
        navigationController?.navigationBar.barTintColor = "FF6A2A".color
        self.lb_name.text = CurrentUser.userInfo?.user?.name
        self.img_profile.sd_custom(url: CurrentUser.userInfo?.user?.profile?.image ?? "")
        if CurrentUser.userInfo == nil{
            self.btlogout.setTitle("Login".localized, for: .normal)
        }else{
            self.btlogout.setTitle("Logout".localized, for: .normal)
        }
        
    }
    
    func getToken(){
        let token =  UserDefaults.standard.string(forKey: "token")
        var parameters: [String: Any] = [:]
        parameters["token"] = token
        
        _ = WebRequests.setup(controller: self).prepare(query: "fcm/token", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200 {
                    
                    self.showAlert(title: "Error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
    }
    
    func getInfo(){
        _ = WebRequests.setup(controller: self).prepare(query: "site/pages", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            do {
                let Status =  try JSONDecoder().decode(Info.self, from: response.data!)
                self.items = Status.data!
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
    }
    
    
    @IBAction func bts_SocialMedia(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            SocialNetwork.Facebook.openPage()
        case 1:
            SocialNetwork.Twitter.openPage()
        case 2:
            SocialNetwork.Instagram.openPage()
        default:
            break
        }
    }
    
    
    @IBAction func bts_Profile(_ sender: UIButton) {
        if CurrentUser.userInfo != nil{
            if sender.tag == 0{
                let vc:AcoountSettingsVC = AppDelegate.sb_main.instanceVC()
                vc.hidesBottomBarWhenPushed = true
                vc.modalPresentationStyle = .fullScreen
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }else{
            let nav : CustomNavigationBar = AppDelegate.sb_main.instanceVC()
            let vc : LoginVC = AppDelegate.sb_main.instanceVC()
            nav.modalPresentationStyle = .fullScreen
            nav.viewControllers = [vc]
            UIApplication.shared.keyWindow?.rootViewController = nav
        }
        if CurrentUser.userInfo != nil{
            if sender.tag == 1{
                let vc:paymentVC = AppDelegate.sb_main.instanceVC()
                vc.hidesBottomBarWhenPushed = true
                vc.modalPresentationStyle = .fullScreen
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }else{
            let nav : CustomNavigationBar = AppDelegate.sb_main.instanceVC()
            let vc : LoginVC = AppDelegate.sb_main.instanceVC()
            nav.modalPresentationStyle = .fullScreen
            nav.viewControllers = [vc]
            UIApplication.shared.keyWindow?.rootViewController = nav
        }
        if CurrentUser.userInfo != nil{
            if sender.tag == 2{
                let vc:FavoriteVC = AppDelegate.sb_main.instanceVC()
                vc.hidesBottomBarWhenPushed = true
                vc.modalPresentationStyle = .fullScreen
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }else{
            let nav : CustomNavigationBar = AppDelegate.sb_main.instanceVC()
            let vc : LoginVC = AppDelegate.sb_main.instanceVC()
            nav.modalPresentationStyle = .fullScreen
            nav.viewControllers = [vc]
            UIApplication.shared.keyWindow?.rootViewController = nav
        }
        if CurrentUser.userInfo != nil{
            if sender.tag == 3{
                let vc:MSStores = AppDelegate.sb_main.instanceVC()
                
               // let vc:StoreVC = StoreVC.loadFromNib()
              //  vc.is_Store = true
                vc.hidesBottomBarWhenPushed = true
                vc.modalPresentationStyle = .fullScreen
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }else{
            let nav : CustomNavigationBar = AppDelegate.sb_main.instanceVC()
            let vc : LoginVC = AppDelegate.sb_main.instanceVC()
            nav.modalPresentationStyle = .fullScreen
            nav.viewControllers = [vc]
            UIApplication.shared.keyWindow?.rootViewController = nav

        }
        if CurrentUser.userInfo != nil{
            if sender.tag == 4{
                let vc:CallUs = AppDelegate.sb_main.instanceVC()
                vc.hidesBottomBarWhenPushed = true
                vc.modalPresentationStyle = .fullScreen
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }else{
            
        }
        if CurrentUser.userInfo != nil{
            if sender.tag == 5{
                let vc:NotificationVC = AppDelegate.sb_main.instanceVC()
                vc.hidesBottomBarWhenPushed = true
                vc.modalPresentationStyle = .fullScreen
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }else{
            let nav : CustomNavigationBar = AppDelegate.sb_main.instanceVC()
            let vc : LoginVC = AppDelegate.sb_main.instanceVC()
            nav.modalPresentationStyle = .fullScreen
            nav.viewControllers = [vc]
            UIApplication.shared.keyWindow?.rootViewController = nav

        }
        if CurrentUser.userInfo != nil{
            if sender.tag == 6{
                let vc:MessageListVC = AppDelegate.sb_main.instanceVC()
                vc.hidesBottomBarWhenPushed = true
                vc.modalPresentationStyle = .fullScreen
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }else{
            let nav : CustomNavigationBar = AppDelegate.sb_main.instanceVC()
            let vc : LoginVC = AppDelegate.sb_main.instanceVC()
            nav.modalPresentationStyle = .fullScreen
            nav.viewControllers = [vc]
            UIApplication.shared.keyWindow?.rootViewController = nav

        }
        
        if CurrentUser.userInfo != nil{
            if sender.tag == 11{
                let vc:DisputesVC = DisputesVC.loadFromNib()
                vc.hidesBottomBarWhenPushed = true
                vc.modalPresentationStyle = .fullScreen
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }else{
            let nav : CustomNavigationBar = AppDelegate.sb_main.instanceVC()
            let vc : LoginVC = AppDelegate.sb_main.instanceVC()
            nav.modalPresentationStyle = .fullScreen
            nav.viewControllers = [vc]
            UIApplication.shared.keyWindow?.rootViewController = nav

        }
     
        if sender.tag == 20{
            
            var actions: [(String, UIAlertAction.Style)] = []
            actions.append(("العربية", UIAlertAction.Style.default))
            actions.append(("English", UIAlertAction.Style.default))
            actions.append(("Cancel", UIAlertAction.Style.cancel))
            
            showActionsheet(viewController: self, title: "Alert!".localized, message: "The application will close to change the language settings of the application.".localized, actions: actions) { (index) in
                print("call action \(index)")
                
                switch index{
                case 0:
                    self.askForQuit(title: "Alert!".localized, message:"The application will close to change the language settings of the application.".localized ) { (canQuit) in
                        if canQuit {
                            MOLH.setLanguageTo("ar")
                            MOLH.reset(transition: .transitionCrossDissolve)
                            Helper.quit()
                        }
                    }
                    
                case 1:
                    self.askForQuit(title: "Alert!".localized, message:"The application will close to change the language settings of the application.".localized ) { (canQuit) in
                        if canQuit {
                            MOLH.setLanguageTo("en")
                            MOLH.reset(transition: .transitionCrossDissolve)
                            Helper.quit()
                        }
                    }
                default:
                    break
                }
                
            }

        }
        
    }
    
    @IBAction func bt_terms(_ sender: Any) {
        let vc:InfoVC = AppDelegate.sb_main.instanceVC()
        vc.idInfo = 201
        vc.hidesBottomBarWhenPushed = true
        vc.modalPresentationStyle = .fullScreen
        vc.isFromTerms = true
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func bt_Logout(_ sender: Any) {
        CurrentUser.userInfo = nil
        self.getToken()
        let nav : CustomNavigationBar = AppDelegate.sb_main.instanceVC()
        let vc : LoginVC = AppDelegate.sb_main.instanceVC()
        nav.modalPresentationStyle = .fullScreen
        nav.viewControllers = [vc]
        UIApplication.shared.keyWindow?.rootViewController = nav
        
    }
    @IBAction func bt_about(_ sender: Any) {
        let vc:InfoVC = AppDelegate.sb_main.instanceVC()
        vc.idInfo = 200
        vc.hidesBottomBarWhenPushed = true
        vc.modalPresentationStyle = .fullScreen
        vc.isFromAboutus = true
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func bt_poalicy(_ sender: Any) {
        let vc:InfoVC = AppDelegate.sb_main.instanceVC()
        vc.idInfo = 202
        vc.hidesBottomBarWhenPushed = true
        vc.modalPresentationStyle = .fullScreen
        vc.isFromPoilcy = true
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func bt_poalicyreturn(_ sender: Any) {
        let vc:InfoVC = AppDelegate.sb_main.instanceVC()
        vc.idInfo = 202
        vc.hidesBottomBarWhenPushed = true
        vc.modalPresentationStyle = .fullScreen
        vc.isFromPoilcyReturn = true
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
}
