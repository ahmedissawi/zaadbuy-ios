//
//  AcoountSettingsVC.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 2/23/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class AcoountSettingsVC: SuperViewController {
    
    @IBOutlet weak var tf_phone: UITextField!
    @IBOutlet weak var tf_code: UITextField!
    @IBOutlet weak var tf_name: UITextField!
    @IBOutlet weak var bt_male: RadioButton!
    @IBOutlet weak var bt_female: RadioButton!
    
    var gander = ""
    var profiledata:ProfileData?
    var profile:Profile?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let navgtion = self.navigationController as! CustomNavigationBar
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(descriptor: UIFontDescriptor(name: "FFShamelFamilySemiRoundBook-S", size: 17), size: 17),NSAttributedString.Key.foregroundColor: UIColor.white]
        self.title = "Account Settings".localized
        navigationController?.navigationBar.barTintColor = "FF6A2A".color
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        self.tf_phone.keyboardType = .asciiCapable
        bt_male?.alternateButton = [bt_female!]
        bt_female?.alternateButton = [bt_male!]
        self.tf_phone.text = CurrentUser.userInfo?.user?.mobile
        self.tf_name.text = CurrentUser.userInfo?.user?.name
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.getProfile()
    }
    
    func getProfile(){
        _ = WebRequests.setup(controller: self).prepare(query: "profile", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            do {
                let Status =  try JSONDecoder().decode(ProfileInfo.self, from: response.data!)
                self.profiledata = Status.data
                self.profile = self.profiledata?.profile
                if self.profiledata?.profile?.gender == "male"{
                    self.bt_male.isSelected = true
                    self.bt_female.isSelected = false
                }else{
                    self.bt_male.isSelected = false
                    self.bt_female.isSelected = true
                    
                }
                
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
    }
    
    
//    override func didClickRightButton(_sender: UIBarButtonItem) {
//        let vc:AddAddressVC = AppDelegate.sb_main.instanceVC()
//        vc.hidesBottomBarWhenPushed = true
//        vc.modalPresentationStyle = .fullScreen
//        self.navigationController?.pushViewController(vc, animated: true)
//
//    }
    
    @IBAction func bt_AddAddress(_ sender: Any) {
        let vc:AddAddressVC = AppDelegate.sb_main.instanceVC()
        vc.hidesBottomBarWhenPushed = true
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    @IBAction func bt_AllAddress(_ sender: UIButton) {
        let vc:AllAddressVC = AppDelegate.sb_main.instanceVC()
        vc.hidesBottomBarWhenPushed = true
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func bt_save(_ sender: UIButton) {
        CurrentUser.userInfo?.user?.name = self.tf_name.text
        CurrentUser.userInfo?.user?.mobile = self.tf_phone.text

        var parameters: [String: Any] = [:] 
        parameters["language"] = "ar"
        parameters["country"] = "1"
        parameters["region"] = "OM"
        parameters["name"] = CurrentUser.userInfo?.user?.name
        parameters["gender"] = self.gander
        parameters["mobile"] = CurrentUser.userInfo?.user?.mobile

        _ = WebRequests.setup(controller: self).prepare(query: "profile", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200 {
                    self.showAlert(title: "error".localized, message:Status.message ?? "")
                    return
                }else if Status.status == 200{
                    self.showAlert(title: "Done successfully".localized, message:"")
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
    }
    
    @IBAction func bt_edite(_ sender: UIButton) {
        
    }
    @IBAction func bt_delete(_ sender: UIButton) {
        
    }
    
    @IBAction func bt_add(_ sender: UIButton) {
        
    }
    
    @IBAction func bts_gender(_ sender: UIButton) {
        if sender.tag == 1{
            self.gander = "male"
        }else{
            self.gander = "female"
        }
    }
    
    
}
