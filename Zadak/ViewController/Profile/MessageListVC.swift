//
//  MessageListVC.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 6/15/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class MessageListVC: SuperViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var messageItems = [MessageList]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.registerCell(id: "MessageCell")
        let navgtion = self.navigationController as! CustomNavigationBar
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(descriptor: UIFontDescriptor(name: "FFShamelFamilySemiRoundBook-S", size: 17), size: 17),NSAttributedString.Key.foregroundColor: UIColor.white]
        self.title = "Messages".localized
        navigationController?.navigationBar.barTintColor = "FF6A2A".color
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        setMessageRequest()
        tableView.tableFooterView = UIView()
        
    }
    
    func setMessageRequest(){
        _ = WebRequests.setup(controller: self).prepare(query: "messages", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            do {
                let Status =  try JSONDecoder().decode(MessageInfo.self, from: response.data!)
                self.messageItems = Status.data!
                if self.messageItems.count == 0{
                    self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.tableView, refershSelector: #selector(self.didRefersh))
                    self.emptyView?.firstLabel.text = "No messages to display".localized
                    self.messageItems.removeAll()
                    self.tableView.reloadData()
                }

                self.tableView.reloadData()
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
    }
    
     @IBAction func didRefersh(){
           messageItems.removeAll()
           setMessageRequest()
       }
     
    
    
}

extension MessageListVC:UITableViewDelegate,UITableViewDataSource{
    
   
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        
        return messageItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MessageCell", for: indexPath) as! MessageCell
        let obj = messageItems[indexPath.row]
        cell.lblMessage.text = obj.store?.name
        cell.imgMessage.sd_custom(url: obj.store?.logo ?? "")
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = messageItems[indexPath.row]
        let vc:MessagesAllVC = AppDelegate.sb_main.instanceVC()
        vc.IDMessgae = obj.sellerId
        vc.Storename = obj.store?.name
        vc.hidesBottomBarWhenPushed = true
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 73
        
    }
    
    
}
