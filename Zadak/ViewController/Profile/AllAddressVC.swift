//
//  AllAddressVC.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 6/16/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class AllAddressVC: SuperViewController {
    
    @IBOutlet weak var tableView: UITableView!

    var addrssItems = [AddressData]()


    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.registerCell(id: "AddressPayCell")
        let navgtion = self.navigationController as! CustomNavigationBar
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(descriptor: UIFontDescriptor(name: "FFShamelFamilySemiRoundBook-S", size: 17), size: 17),NSAttributedString.Key.foregroundColor: UIColor.white]
        self.title = "Addresses".localized
        navigationController?.navigationBar.barTintColor = "FF6A2A".color
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        getAddress()

    }
    
    func getAddress(){
          _ = WebRequests.setup(controller: self).prepare(query: "addresses", method: HTTPMethod.get).start(){ (response, error) in
              do {
                  let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                  if Status.status != 200{
                      self.showAlert(title: "error".localized, message:Status.message!)
                      return
                  }
                  
              }catch let jsonErr {
                  print("Error serializing  respone json", jsonErr)
              }
              do {
                  let Status =  try JSONDecoder().decode(AddressInfo.self, from: response.data!)
                  
                  self.addrssItems = Status.data!
                  self.tableView.reloadData()
              } catch let jsonErr {
                  print("Error serializing  respone json", jsonErr)
              }
          }
      }

}


extension AllAddressVC:UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addrssItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddressPayCell", for: indexPath) as! AddressPayCell
        let obj = addrssItems[indexPath.row]
        cell.btedite.tag = indexPath.row
        cell.btdelete.tag = indexPath.row
        cell.btdelete.addTarget(self, action: #selector(self.btdelete), for: UIControl.Event.touchUpInside)
        cell.btedite.addTarget(self, action: #selector(self.btedite), for: UIControl.Event.touchUpInside)
        cell.lbllocation.text  = "\(obj.name ?? "")"
        cell.lblcity.text = "\(obj.country ?? "")" + " " + "-" + " " + "\(obj.city ?? "")"
        cell.lblStreet.text = "\(obj.country ?? "")" + " " + "-" + " " + "\(obj.city ?? "")" + " " + "-" + " " + "\(obj.addressLine1 ?? "" )"
        
        return cell
    }
    
    @objc func btdelete(_ sender:UIButton ){
        let obj = addrssItems[sender.tag]

        _ = WebRequests.setup(controller: self).prepare(query: "user-profile/address/\(obj.id ?? 0)", method: HTTPMethod.delete).start(){ (response, error) in
           
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200 {
                    
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }else if Status.status == 200{
                    self.getAddress()
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
    }
    
    @objc func btedite(_ sender:UIButton ){
        let obj = addrssItems[sender.tag]
        let vc:AddAddressVC = AppDelegate.sb_main.instanceVC()
        vc.hidesBottomBarWhenPushed = true
        vc.isFromEditeAddress = true
        vc.addrssItems = obj
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 118
    }
    
    
    
}

