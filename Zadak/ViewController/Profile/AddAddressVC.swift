//
//  AddAddressVC.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 2/23/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class AddAddressVC: SuperViewController,MapCoordinatesDelgate {
    
    var isFromEditeAddress = false
    var addrssItems:AddressData?
    
    
    func SelectedDone(Lat: Double, Lon: Double, address: String) {
        self.lat = Lat
         self.lon = Lon
         self.address = address
        if address != ""{
            self.tf_address.text = address
        }else{
            self.tf_address.text = "\(lat)" + "\(lon)"
        }
    }
    
    
    
    @IBOutlet weak var tf_name: UITextField!
    @IBOutlet weak var tf_phone: UITextField!
    @IBOutlet weak var tf_couuntry: UITextField!
    @IBOutlet weak var tf_city: UITextField!
    @IBOutlet weak var tf_reagion: UITextField!
    @IBOutlet weak var tf_organization: UITextField!
    @IBOutlet weak var tf_zipNumber: UITextField!
    @IBOutlet weak var tf_prefix: UITextField!
    @IBOutlet weak var tf_address: UITextField!
    
    var countries = [Countriess]()
    var city = [City]()
    var countryID:Int?
    var selectedCountryID:Int?
    var selectedCityID:Int?
    var lat = 0.0
    var lon = 0.0
    var address = ""
    var addressID:Int?

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupAddres()
        let navgtion = self.navigationController as! CustomNavigationBar
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(descriptor: UIFontDescriptor(name: "FFShamelFamilySemiRoundBook-S", size: 17), size: 17),NSAttributedString.Key.foregroundColor: UIColor.white]
        self.title = "Add Address".localized
        navigationController?.navigationBar.barTintColor = "FF6A2A".color
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        getCountres()
        // Do any additional setup after loading the view.
    }
    
    
    func setupAddres(){
        if isFromEditeAddress{
            self.addressID = self.addrssItems?.id
            self.tf_name.text = self.addrssItems?.name
            self.tf_phone.text = self.addrssItems?.phone
            self.tf_couuntry.text = self.addrssItems?.country
            self.tf_city.text = self.addrssItems?.city
            self.tf_reagion.text = self.addrssItems?.state
            self.tf_organization.text = self.addrssItems?.organization
            self.tf_zipNumber.text = self.addrssItems?.addressLine1
            self.tf_prefix.text = self.addrssItems?.prefix
            self.tf_address.text = (self.addrssItems!.addressLine1 ?? "") +  (self.addrssItems?.addressLine2 ?? "") + (self.addrssItems?.addressLine3 ?? "")
            self.selectedCityID = Int(self.addrssItems?.cityId ?? "0")
            self.selectedCountryID = Int(self.addrssItems?.countryId ?? "0")
            if selectedCountryID != 0{
                self.getCity()
            }

        }
    }
    
    func getCountres(){
        _ = WebRequests.setup(controller: self).prepare(query: "site/countries", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            do {
                let Status =  try JSONDecoder().decode(CountriesInfo.self, from: response.data!)
                self.countries = Status.data!
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
    }
    
    func getCity(){
        _ = WebRequests.setup(controller: self).prepare(query: "countries/\(selectedCountryID ?? 0)/cities", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            do {
                let Status =  try JSONDecoder().decode(CityInfo.self, from: response.data!)
                self.city = Status.data!
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
    }
    
    @IBAction func bt_country(_ sender: UIButton) {
        ActionSheetStringPicker.show(withTitle:"Country".localized, rows: countries.map { $0.title as Any }
            , initialSelection: 0, doneBlock: {
                picker, value, index in
                if let Value = index {
                    self.tf_couuntry.text = Value as? String
                    self.selectedCountryID = self.countries[value].id ?? 0
                    self.getCity()
                }
                return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
    }
    
    @IBAction func bt_city(_ sender: UIButton) {
        if selectedCountryID != nil{
            
            ActionSheetStringPicker.show(withTitle:"City".localized, rows: city.map { $0.name as Any }
                , initialSelection: 0, doneBlock: {
                    picker, value, index in
                    if let Value = index {
                        self.tf_city.text = Value as? String
                        self.selectedCityID = self.city[value].id ?? 0
                    }
                    return
            }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        }else{
            self.showAlert(title: "Selection".localized, message:"Please select a country first.".localized)
        }
        
        
    }
    @IBAction func bt_address(_ sender: Any) {
        let vc:MapVC = AppDelegate.sb_main.instanceVC()
        vc.MapDelegate = self
        self.present(vc, animated: true, completion: nil)

    }
    


    
    @IBAction func bt_save(_ sender: Any) {
        
        
        guard let name = self.tf_name.text, !name.isEmpty else{
            self.showAlert(title: "error".localized, message: "Name cannot be empty".localized)
                  return
              }
              
//              guard let prefix = self.tf_prefix.text, !prefix.isEmpty else{
//                  self.showAlert(title: "error".localized, message: "لا يمكن أن تكون اللقب فارغة".localized)
//                  return
//              }
              
//              guard let organization = self.tf_organization.text, !organization.isEmpty else{
//                         self.showAlert(title: "error".localized, message: "منظمة لا يمكن أن تكون فارغة".localized)
//                         return
//                     }
              
        guard let mobile = self.tf_phone.text, !mobile.isEmpty else{
            self.showAlert(title: "error".localized, message: "The phone cannot be empty.".localized)
            return
        }
        
        guard let country = self.tf_couuntry.text, !country.isEmpty else{
            self.showAlert(title: "error".localized, message: "The country can't be empty".localized)
            return
        }
        
        guard let city = self.tf_city.text, !city.isEmpty else{
            self.showAlert(title: "error".localized, message: "The area cannot be empty".localized)
            return
        }
              
//              guard let region = self.tf_reagion.text, !region.isEmpty else{
//                  self.showAlert(title: "error".localized, message: "لا يمكن أن تكون المنطقة فارغة".localized)
//                  return
//              }
              
        
        guard let address = self.tf_address.text, !address.isEmpty else{
            self.showAlert(title: "error".localized, message: "Title cannot be empty.".localized)
            return
        }
              
//              guard let zipnumber = self.tf_zipNumber.text, !zipnumber.isEmpty else{
//                  self.showAlert(title: "error".localized, message: "لا يمكن أن يكون الرمز البريدي فارغًا".localized)
//                  return
//              }
        
        var parameters: [String: Any] = [:]
        parameters["phone"] = mobile
        parameters["name"] = name
        parameters["country"] = country
        parameters["country_id"] = selectedCountryID?.description
        parameters["city"] = city
        parameters["city_id"] = selectedCityID?.description
        // parameters["state"] = region
             // parameters["postcode"] = zipnumber
              parameters["address_line_1"] = address
//              parameters["address_line_2"] = "\(country)" + "\(city)"
//              parameters["address_line_3"] = "\(country)" + "\(city)" + "\(address)"
             // parameters["prefix"] = prefix
             // parameters["organization"] = organization
             // parameters["lat"] = self.lat.description
              //parameters["lat"] = self.lon.description
        
        
        if isFromEditeAddress{
            _ = WebRequests.setup(controller: self).prepare(query: "user-profile/address/\(addressID ?? 0)", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
                     
                     do {
                         let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                         if Status.status != 200 {
                             
                             self.showAlert(title: "error".localized, message:Status.message!)
                             return
                         }else if Status.status == 200 {
                             self.showAlert(title: "Done successfully".localized, message:"")
                             self.navigationController?.popViewController(animated: true)
                             
                         }
                         
                     }catch let jsonErr {
                         print("Error serializing  respone json", jsonErr)
                     }
                     
                     
                 }
            
            
            
        }else{
            
        
        _ = WebRequests.setup(controller: self).prepare(query: "user-profile/address", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200 {
                    
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }else if Status.status == 200 {
                    self.showAlert(title: "Done successfully".localized, message:"")
                    self.navigationController?.popViewController(animated: true)
                    
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            
        }
            
         }
        
    }
    
    
    
}
