//
//  FavoriteVC.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 2/21/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class FavoriteVC: SuperViewController {
    
    @IBOutlet weak var Table: UITableView!
    
    var favoritesItems = [FavoritesData]()
    var refreshControl = UIRefreshControl()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setFavoriteRequest()
        setRefrechData()
        Table.registerCell(id: "FavoritesCell")
        let navgtion = self.navigationController as! CustomNavigationBar
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(descriptor: UIFontDescriptor(name: "FFShamelFamilySemiRoundBook-S", size: 17), size: 17),NSAttributedString.Key.foregroundColor: UIColor.white]
        self.title = "Favorite".localized
        navigationController?.navigationBar.barTintColor = "FF6A2A".color
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        // Do any additional setup after loading the view.
    }
    
    
    func setFavoriteRequest(){
        _ = WebRequests.setup(controller: self).prepare(query: "favorite/items", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            do {
                let Status =  try JSONDecoder().decode(FavoritesInfo.self, from: response.data!)
                self.favoritesItems = Status.data!
                if self.favoritesItems.count == 0{
                    self.Table.showEmptyListMessage("There are no products to display as favourites.".localized)
                }

                self.Table.reloadData()
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
    }
    
    func setRefrechData(){
        refreshControl.addTarget(self, action: #selector(refreshListData(_:)), for: .valueChanged)
        if #available(iOS 10.0, *) {
            Table.refreshControl = refreshControl
        } else {
            Table.addSubview(refreshControl)
        }
    }
    
    @objc private func refreshListData(_ sender: Any) {
        setFavoriteRequest()
        self.refreshControl.endRefreshing() // You can stop after API Call
        // Call API
    }
    
    
}

extension FavoriteVC:UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favoritesItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FavoritesCell", for: indexPath) as!  FavoritesCell
        let obj = favoritesItems[indexPath.row]
        cell.lblname.text = obj.title
        cell.lblDesc.text = obj.shortDescription
        cell.lblprice.text  = "\(obj.price ?? 0)" + " " + "O.R".localized
        cell.imgproducat.sd_custom(url: obj.image ?? "")
        cell.bt_delete.tag = indexPath.row
        cell.bt_addcart.tag = indexPath.row
        cell.bt_delete.addTarget(self, action: #selector(DeleteFavorite), for: UIControl.Event.touchUpInside)
        cell.bt_addcart.addTarget(self, action: #selector(addCartFavorite), for: UIControl.Event.touchUpInside)
        return cell
    }
    
    @objc func addCartFavorite(sender:UIButton) {
        
        let idProduct = favoritesItems[sender.tag].id
        
        var parameters: [String: Any] = [:]
        parameters["product_id"] = idProduct?.description
        
        _ = WebRequests.setup(controller: self).prepare(query: "shopping-cart/items", method: HTTPMethod.post,parameters:parameters ).start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200 {
                    
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }else if Status.status  == 200{
                    self.showAlert(title: "Done successfully".localized, message:"Add product to cart".localized)
                    self.setFavoriteRequest()
                    
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
    }
    
    @objc func DeleteFavorite(sender:UIButton) {
        
        let idFavorite = favoritesItems[sender.tag].itemId
        
        _ = WebRequests.setup(controller: self).prepare(query: "favorite/items/\(idFavorite ?? 0)", method: HTTPMethod.delete).start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200 {
                    
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }else if Status.status  == 200{
                    self.showAlert(title: "Done successfully".localized, message:"Product has been removed from favourites".localized)
                    self.setFavoriteRequest()
                    
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    
    
}
