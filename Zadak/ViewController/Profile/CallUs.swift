//
//  CallUs.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 2/21/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class CallUs: SuperViewController {
    
    @IBOutlet weak var tf_address: UITextField!
    @IBOutlet weak var tf_Email: UITextField!
    @IBOutlet weak var txt_text: UITextView!
    @IBOutlet weak var txt_name: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        let navgtion = self.navigationController as! CustomNavigationBar
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(descriptor: UIFontDescriptor(name: "FFShamelFamilySemiRoundBook-S", size: 17), size: 17),NSAttributedString.Key.foregroundColor: UIColor.white]
              self.title = "Call us".localized
        navigationController?.navigationBar.barTintColor = "FF6A2A".color
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func bt_send(_ sender: Any) {
        
        var parameters: [String: Any] = [:]
        parameters["title"] = self.tf_address.text ?? ""
        parameters["message"] = self.txt_text.text ?? ""
        parameters["email"] =  self.tf_Email.text ?? ""
        parameters["name"] = self.txt_name.text ?? ""

        _ = WebRequests.setup(controller: self).prepare(query: "contact-us", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200 {
                    
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }else if Status.status == 200{
                    self.showAlert(title:"Done successfully".localized, message: "Data has been sent".localized)
                    self.tf_address.text = ""
                    self.txt_text.text = ""
                    self.tf_Email.text = ""
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
          
            
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
