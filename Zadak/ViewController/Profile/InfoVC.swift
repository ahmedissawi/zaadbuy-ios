//
//  InfoVC.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 2/21/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class InfoVC: SuperViewController {
    
     var isFromAboutus = false
     var isFromTerms = false
     var isFromPoilcy = false
     var isFromPoilcyReturn = false
     var isFromTermsLogin = false
     var isDismis = false
     var idInfo:Int?
     var infoDeatiles :InfoDeatilesData?

    @IBOutlet weak var lb_title: UILabel!
    @IBOutlet weak var lb_desc: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.lb_desc.text = ""
        self.getInfo()
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = "FF6A2A".color
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        
        
        if isFromAboutus{
            self.lb_title.text = "About Zad Amman" .localized
        }
        if isFromTerms{
            self.lb_title.text = "Terms and Conditions".localized
        }
        if isFromPoilcy{
            self.lb_title.text = "Privacy policy".localized
        }
        if isFromPoilcyReturn{
            self.lb_title.text = "Return Policy".localized
        }
        if isFromTermsLogin{
            self.lb_title.text = "Terms and Conditions".localized
        }
        if isDismis{
            let navgtion = self.navigationController as! CustomNavigationBar
            navgtion.setLogotitle(sender: self)
            navigationController?.navigationBar.barTintColor = "FF6A2A".color
            navgtion.setCustomBackButtonWithdismiss(sender: self)
        }
        
        // Do any additional setup after loading the view.
    }
    
    func getInfo(){
             _ = WebRequests.setup(controller: self).prepare(query: "site/pages/\(idInfo ?? 0)", method: HTTPMethod.get).start(){ (response, error) in
                      do {
                          let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                          if Status.status != 200{
                              self.showAlert(title: "error".localized, message:Status.message!)
                              return
                          }
                          
                      }catch let jsonErr {
                          print("Error serializing  respone json", jsonErr)
                      }
                      do {
                          let Status =  try JSONDecoder().decode(InfoDeatiles.self, from: response.data!)
                         self.infoDeatiles = Status.data
                        self.lb_desc.text = self.infoDeatiles?.html?.htmlToString
                      } catch let jsonErr {
                          print("Error serializing  respone json", jsonErr)
                      }
                  }
         }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
