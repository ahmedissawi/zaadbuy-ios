//
//  WalletVC.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 2/21/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class WalletVC: SuperViewController {
    
    
    @IBOutlet weak var bt_payed: UIButton!
    @IBOutlet weak var bt_deserved: UIButton!
    
    var pay:PayDeatiles?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getPay()
        let navgtion = self.navigationController as! CustomNavigationBar
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(descriptor: UIFontDescriptor(name: "FFShamelFamilySemiRoundBook-S", size: 17), size: 17),NSAttributedString.Key.foregroundColor: UIColor.white]
        self.title = "Wallet".localized
        navigationController?.navigationBar.barTintColor = "FF6A2A".color
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        
    }
    
    func getPay(){
        _ = WebRequests.setup(controller: self).prepare(query: "finance-details", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            do {
                let Status =  try JSONDecoder().decode(PayInfo.self, from: response.data!)
                self.pay = Status.data!
                self.bt_payed.setTitle("\(self.pay?.payed ?? 0.0)"  + "O.R".localized, for: .normal)
                self.bt_deserved.setTitle("\(self.pay?.deserved ?? 0)" + "O.R".localized, for: .normal)
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
    }
    
    
}
