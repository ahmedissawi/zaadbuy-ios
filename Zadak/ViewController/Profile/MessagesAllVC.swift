//
//  MessagesAllVC.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 6/15/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class MessagesAllVC: SuperViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tf_Message: UITextView!
    @IBOutlet weak var height: NSLayoutConstraint!

    
    var message = [Messages]()
    var messages:MessageItems?
    var IDMessgae:Int?
    var Storename:String?
    var isFromDisputes  = false
    var idDisputes:Int?
    var disputesitems :Dispute?
    var comments = [DisputesDeatilesComments]()
    var selectedMessgeID:Int?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tf_Message.placeholder = "Message".localized
        tableView.registerCell(id: "MessageSenderCell")
        tableView.registerCell(id: "MessageReciveCell")
        setMessageRequest()
        let navgtion = self.navigationController as! CustomNavigationBar
        navigationController?.navigationBar.barTintColor = "FF6A2A".color
        navgtion.setTitle(Storename ?? "", sender: self)
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        if UIDevice.current.hasBottomNotch{
            self.height.constant = 100
        }else{
            self.height.constant = 60
        }
        NotificationCenter.default.addObserver(self, selector: #selector(UpdateChat(notification:)), name: Notification.Name("UpdateChat"), object: nil)

    }
    
    func setMessageRequest(){
        if isFromDisputes{
            _ = WebRequests.setup(controller: self).prepare(query: "disputes/\(idDisputes ?? 0)", method: HTTPMethod.get).start(){ (response, error) in
                 do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200{
                        self.showAlert(title: "error".localized, message:Status.message!)
                        return
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
                do {
                    let Status =  try JSONDecoder().decode(DisputesDeatilesObject.self, from: response.data!)
                    self.disputesitems = Status.data?.dispute
                    self.comments = self.disputesitems!.comments!
                    self.tableView.reloadData()
                } catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
                
            }
        }else{
            _ = WebRequests.setup(controller: self).prepare(query: "messages/\(IDMessgae ?? 0)", method: HTTPMethod.get).start(){ (response, error) in
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200{
                        self.showAlert(title: "error".localized, message:Status.message!)
                        return
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                do {
                    let Status =  try JSONDecoder().decode(MessageItemsInfo.self, from: response.data!)
                    self.messages = Status.data!
                    self.message = self.messages!.messages!
                    self.tableView.reloadData()
                } catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
            }
        }
        
    }
    
    @IBAction func bt_Send(_ sender: Any) {
        if isFromDisputes{
            if tf_Message.text != ""{
                
                let id = disputesitems?.id ?? 0
                
                var parameters: [String: Any] = [:]
                parameters["comment"] = self.tf_Message.text
                
                _ = WebRequests.setup(controller: self).prepare(query: "disputes/\(id)/actions/comment", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
                    
                    do {
                        let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                        if Status.status != 200 {
                            
                            self.showAlert(title: "error".localized, message:Status.message!)
                            return
                        }else if Status.status == 200{
                            self.tf_Message.text = ""
                            NotificationCenter.default.post(name: Notification.Name("UpdateChat"), object: nil)
                            
                        }
                        
                    }catch let jsonErr {
                        print("Error serializing  respone json", jsonErr)
                    }
                    
                }
            }else{
                self.showAlert(title: "".localized, message:"Message is empty!".localized)
            }
            
        }else{
            if tf_Message.text != ""{
                var parameters: [String: Any] = [:]
                parameters["seller_id"] = IDMessgae?.description
                parameters["message"] = self.tf_Message.text
                
                _ = WebRequests.setup(controller: self).prepare(query: "messages", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
                    
                    do {
                        let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                        if Status.status != 200 {
                            
                            self.showAlert(title: "error".localized, message:Status.message!)
                            return
                        }else if Status.status == 200{
                            self.tf_Message.text = ""
                            NotificationCenter.default.post(name: Notification.Name("UpdateChat"), object: nil)
                        }
                        
                    }catch let jsonErr {
                        print("Error serializing  respone json", jsonErr)
                    }
                    
                }
            }else{
                self.showAlert(title: "".localized, message:"Enter Message!".localized)
            }
        }
    }
    
    
    @objc func UpdateChat(notification: NSNotification)  {
          if isFromDisputes{
              setMessageRequest()
          }else{
              message.removeAll()
              setMessageRequest()
          }
      }
    
}



extension MessagesAllVC:UITableViewDelegate,UITableViewDataSource{
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFromDisputes{
            return comments.count
        }else{
            return message.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isFromDisputes{
            let obj = comments[indexPath.row]
            if obj.senderType == "user"{
                let cell = tableView.dequeueReusableCell(withIdentifier: "MessageSenderCell", for: indexPath) as! MessageSenderCell
                cell.lblMessage.text = obj.comment ?? ""
                cell.lblDate.text = obj.date ?? ""
                cell.imgUser.isHidden = false
                cell.lblname.isHidden = false
                cell.imgUser.sd_custom(url: disputesitems?.user?.image ?? "")
                cell.lblname.text = disputesitems?.user?.name ?? ""
                if selectedMessgeID == indexPath.row{
                    cell.lblDate.isHidden = false
                }else{
                    cell.lblDate.isHidden = true
                }
                
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "MessageReciveCell", for: indexPath) as! MessageReciveCell
                cell.lblMessage.text = obj.comment ?? ""
                cell.lblDate.text = obj.date ?? ""
                cell.imgUser.isHidden = false
                cell.lblname.isHidden = false
                cell.imgUser.sd_custom(url: disputesitems?.seller?.image ?? "")
                cell.lblname.text = disputesitems?.user?.name ?? ""
                if selectedMessgeID == indexPath.row{
                    cell.lblDate.isHidden = false
                }else{
                    cell.lblDate.isHidden = true
                }
                return cell
                
            }
        }else{
            let obj = message[indexPath.row]
            if obj.type == 1{
                let cell = tableView.dequeueReusableCell(withIdentifier: "MessageSenderCell", for: indexPath) as! MessageSenderCell
                cell.lblMessage.text = obj.message
                return cell
                
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "MessageReciveCell", for: indexPath) as! MessageReciveCell
                cell.lblMessage.text = obj.message
                return cell
                
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
          if self.selectedMessgeID == indexPath.row{
              self.selectedMessgeID = -99
              self.tableView.reloadData()
          }else{
              self.selectedMessgeID = indexPath.row
              self.tableView.reloadData()
          }
      }
      
      func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
          if isFromDisputes{
              if self.selectedMessgeID != indexPath.row{
                  UIView.animate(withDuration: 0.2) {
                      cell.transform = CGAffineTransform.identity
                  }
              }
          }else{
              if self.selectedMessgeID != indexPath.row{
                  cell.transform = CGAffineTransform(scaleX: 0.8, y: 0)
                  UIView.animate(withDuration: 0.2) {
                      cell.transform = CGAffineTransform.identity
                  }
              }
          }
          
      }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        
    }
    
    
}
