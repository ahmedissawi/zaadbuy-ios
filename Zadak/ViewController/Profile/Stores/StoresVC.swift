//
//  StoresVC.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 6/13/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class StoresVC: SuperViewController,UITextFieldDelegate{
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchText: UITextField!

    var storesItems = [StoresData]()
    var storID : Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.searchText.delegate = self
        searchText.returnKeyType = .search
        let navgtion = self.navigationController as! CustomNavigationBar
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(descriptor: UIFontDescriptor(name: "FFShamelFamilySemiRoundBook-S", size: 17), size: 17),NSAttributedString.Key.foregroundColor: UIColor.white]
        self.title = "Stores".localized
        navigationController?.navigationBar.barTintColor = "FF6A2A".color
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        getStores()
        tableView.registerCell(id: "TypeStoreCell")
        
    }
    
    func getStores(){
        _ = WebRequests.setup(controller: self).prepare(query: "stores/activity/\(storID!)", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            do {
                let Status =  try JSONDecoder().decode(StoresInfo.self, from: response.data!)
                self.storesItems = Status.data!
                self.tableView.reloadData()
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            
        }
        
    }
    
    @IBAction func bt_search(_ sender: Any) {
        if  searchText.text == nil{
            return
        }
        
        
        let vc:storeListVC = AppDelegate.sb_main.instanceVC()
        
        if(storID == 1){
            let encodedLink = "search/stores?search=\(searchText.text ?? "")".addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)
            vc.link = encodedLink!
        }
        else if (storID == 2){
            let encodedLink = "stores?search=\(searchText.text ?? "")".addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)
            vc.link = encodedLink!
        }
     
        vc.categoryId = storID
        vc.titleCat = searchText.text ?? ""
        vc.serchQuery = true
        vc.hidesBottomBarWhenPushed = true
        self.searchText.text = ""
        hideKeyboardWhenTappedAround()
        hideKeyboard()
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)

    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if  searchText.text != nil{
        let vc:storeListVC = AppDelegate.sb_main.instanceVC()
            if(storID == 1){
                let encodedLink = "search/stores?search=\(searchText.text ?? "")".addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)
                vc.link = encodedLink!
            }
            else if (storID == 2){
                let encodedLink = "stores?search=\(searchText.text ?? "")".addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)
                vc.link = encodedLink!
            }
            
        vc.categoryId = storID
        vc.titleCat = searchText.text ?? ""
        vc.serchQuery = true
        vc.hidesBottomBarWhenPushed = true
        self.searchText.text = ""
        hideKeyboardWhenTappedAround()
            hideKeyboard()
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
        }
        return true

    }
    
}
extension String {
var fixedArabicURL: String?  {
       return self.addingPercentEncoding(withAllowedCharacters: CharacterSet.alphanumerics
           .union(CharacterSet.urlPathAllowed)
           .union(CharacterSet.urlHostAllowed))
   } }


extension StoresVC:UITableViewDataSource,UITableViewDelegate{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return storesItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TypeStoreCell", for: indexPath) as! TypeStoreCell
        let obj  = storesItems[indexPath.row]
        cell.lblType.text  = obj.title
        cell.storesCategrouy = obj.stores!
        cell.storesItems = obj
       

        cell.view = self
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 205
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
    
    
}
