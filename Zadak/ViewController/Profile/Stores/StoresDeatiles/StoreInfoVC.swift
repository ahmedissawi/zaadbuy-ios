//
//  StoreInfoVC.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 6/16/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class StoreInfoVC: SuperViewController {
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var lblProductCategory: UILabel!
    @IBOutlet weak var lblAddress: UILabel!

    @IBOutlet weak var collectionView: UICollectionView!
    var items = [Items]()
    var id = -99
    var nameStore = ""
    var logoStore = ""
    var currentpage = 1
    


    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.registerCell(id: "StoreProducatCell")
        let navgtion = self.navigationController as! CustomNavigationBar
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(descriptor: UIFontDescriptor(name: "FFShamelFamilySemiRoundBook-S", size: 17), size: 17),NSAttributedString.Key.foregroundColor: UIColor.white]
        self.title = "Store details".localized
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navigationController?.navigationBar.barTintColor = "FF6A2A".color
        getFilter()
        self.collectionView.es.addPullToRefresh {
            self.currentpage = 1
            self.items.removeAll()
            self.getFilter()
            self.hideIndicator()
        }
        
        self.collectionView.es.addInfiniteScrolling {
            self.currentpage += 1
            self.getFilter() // next page
            self.hideIndicator()
        }
    }
    
    func getFilter(){
        
        _ = WebRequests.setup(controller: self).prepare(query: "stores/\(id)/with-items?page=\(currentpage)", method: HTTPMethod.get).start(){ (response, error) in
            
            self.collectionView.es.stopLoadingMore()
            self.collectionView.es.stopPullToRefresh()

            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            do {
                let Status =  try JSONDecoder().decode(ItemsRespon.self, from: response.data!)
                
                self.items += Status.data?.items! ?? []
                let category = Status.data?.seller?.storeCategory ?? ""
                self.lblAddress.text = Status.data?.seller?.storeAddress ?? ""
                self.name.text = Status.data?.seller?.store_name ?? ""

                self.logo.sd_custom(url: (Status.data?.seller?.store_logo ?? ""))
                
                if self.items.count > 0{
                    let categoryObj = self.items[0].category
                    self.lblProductCategory.text =  (categoryObj?.title ?? category)!
                    
                }
                if Status.data?.nextPageUrl == nil {
                    self.collectionView.es.stopLoadingMore()
                    self.collectionView.es.noticeNoMoreData()
                }
                if self.items.count == 0{
                    
                    self.lblProductCategory.text = Status.data?.seller?.storeCategory ?? ""
    
                    self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.collectionView, refershSelector: #selector(self.didRefersh))
                    self.emptyView?.firstLabel.text = "No products to display".localized
                    self.items.removeAll()
                    self.collectionView.reloadData()
                    
                }
                
                self.collectionView.reloadData()
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
    }
    @IBAction func didRefersh(){
        items.removeAll()
        getFilter()
    }
    
}

extension StoreInfoVC:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StoreProducatCell", for: indexPath) as! StoreProducatCell
        let obj = items[indexPath.row]
        cell.lb_name.text = obj.title
        if obj.discount != 0{
            cell.viewDiscount.isHidden = false
            cell.lblDesc.text = "Discount".localized + "\(obj.discount ?? 0)"
        }else{
            cell.viewDiscount.isHidden = true

        }
        cell.lb_price.text = "\(obj.regularPrice ?? 0)" + "O.R".localized
        cell.imgNew.sd_custom(url: obj.image ?? "")
        cell.bt_like.isHidden = true
        cell.bt_shopping.isHidden = true
        if obj.discount == 0{
            cell.viewDiscount.isHidden = true
        }else{
            cell.lblDesc.text = "\(obj.discount ?? 0)"
            cell.viewDiscount.isHidden = false
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let obj = items[indexPath.row]
        let vc:DetailsVC = AppDelegate.sb_main.instanceVC()
        vc.producatID = obj.id
        vc.hidesBottomBarWhenPushed = true
        vc.modalPresentationStyle = .fullScreen
        navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let wideth = UIScreen.main.bounds.size.width
        return CGSize(width: wideth/2.1, height: 245)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
    
    
    
    
    
    
}


