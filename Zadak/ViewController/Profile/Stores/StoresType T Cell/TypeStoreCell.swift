//
//  TypeStoreCell.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 6/13/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class TypeStoreCell: UITableViewCell {
    
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var storesItems : StoresData!
    //    var storesCategrouy = [Store]()
    var view :UIViewController?
    var storesCategrouy: [Store]! {
        didSet {
            collectionView.reloadData()
        }
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //        getStores()
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.registerCell(id: "StoresCell")
        let moreHeader = UINib(nibName: "FooterMoreStoresCell", bundle: nil)
        self.collectionView.register(moreHeader, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: "FooterMoreStoresCell")
        
        // Initialization code
    }
    
    
}

extension TypeStoreCell:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return storesCategrouy.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StoresCell", for: indexPath) as! StoresCell
        let obj = storesCategrouy[indexPath.row]
        cell.lblname.text = obj.name
        cell.imgStore.sd_custom(url: obj.logo ?? "")
        return cell
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 110, height: 135)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let footermore = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: "FooterMoreStoresCell", for: indexPath) as! FooterMoreStoresCell
        footermore.btnMore.tag = indexPath.row
        footermore.btnMore.addTarget(self, action: #selector(self.btMore), for: UIControl.Event.touchUpInside)
        
        return footermore
    }
    @objc func btMore(_ sender:UIButton ){
        let vc:storeListVC = AppDelegate.sb_main.instanceVC()
        vc.link = "categories/\(storesItems.id ?? 0)/stores?page=1&perPage=100"
       vc.categoryId = storesItems.categoryId as! Int
        vc.titleCat = storesItems.title ?? ""
        vc.hidesBottomBarWhenPushed = true
        vc.modalPresentationStyle = .fullScreen
        view?.navigationController?.pushViewController(vc, animated: true)
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let obj  = storesCategrouy[indexPath.row]
   
        if (storesItems.categoryId == 1 ){
            let vc:StoreInfoVC  = AppDelegate.sb_main.instanceVC()
            vc.id = obj.id! as! Int
            vc.hidesBottomBarWhenPushed = true
            vc.modalPresentationStyle = .fullScreen
            view?.navigationController?.pushViewController(vc, animated: true)
            //        vc.logoStore = (deatilesproducat?.seller?.store_logo as! String) ?? ""
            //        vc.nameStore = (deatilesproducat?.seller?.name as! String) ?? ""
        }
        else{
            let vc:DetailsStoreVC = DetailsStoreVC.loadFromNib()
            vc.ID = obj.id! as! Int
            vc.hidesBottomBarWhenPushed = true
            vc.modalPresentationStyle = .fullScreen
            view?.navigationController?.pushViewController(vc, animated: false)
            
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize(width: 110, height: 135)
    }
    
}
