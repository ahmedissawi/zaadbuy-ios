//
//  paymentVC.swift
//  Zadak
//
//  Created by Ahmed ios on 6/21/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import SKPhotoBrowser

class paymentVC: SuperViewController {
    @IBOutlet weak var Table: UITableView!
    var paymentItems = [Resources]()
    var images = [SKPhoto]()

    override func viewDidLoad() {
        super.viewDidLoad()
        Table.registerCell(id: "paymentCell")
        let navgtion = self.navigationController as! CustomNavigationBar
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(descriptor: UIFontDescriptor(name: "FFShamelFamilySemiRoundBook-S", size: 17), size: 17),NSAttributedString.Key.foregroundColor: UIColor.white]
               self.title = "Pay".localized
        navigationController?.navigationBar.barTintColor = "FF6A2A".color
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        getOrders()

    }
    
    
    func getOrders(){
        _ = WebRequests.setup(controller: self).prepare(query: "bank-payments", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            do {
                let Status =  try JSONDecoder().decode(myPaumentClass.self, from: response.data!)
                
                self.paymentItems = Status.data!
                self.Table.reloadData()
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension paymentVC:UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return paymentItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "paymentCell", for: indexPath) as! paymentCell
        let obj = paymentItems[indexPath.row]
        cell.Object = paymentItems[indexPath.row]
        cell.imageTapped = { [weak self] in
            let photo = SKPhoto.photoWithImageURL(obj.document!)
            photo.shouldCachePhotoURLImage = true // you can use image cache by true(NSCache)
            self!.images.append(photo)
            let browser = SKPhotoBrowser(photos: self!.images)
            browser.initializePageIndex(indexPath.row)
            SKPhotoBrowserOptions.displayAction = false    // action button will be hidden
            self!.present(browser, animated: true, completion: nil)
            self?.images.removeAll()

        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 286
    }
    
    
    
    
}
