//
//  MakeDisputesVC.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 9/6/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0


class MakeDisputesVC: SuperViewController {
    
    
    @IBOutlet weak var tfproblame: UITextField!
    @IBOutlet weak var tfproducat: UITextField!
    @IBOutlet weak var txtDeatiles: UITextView!
    @IBOutlet weak var tfvideo: UITextField!
    @IBOutlet weak var imgDisputes: UIImageView!
    @IBOutlet weak var viewProduct: UIView!
    
    var imagePicker: ImagePicker!
    var SelectImage = false
    var Problems = [ProblemsData]()
    var selectedProblemsID:Int?
    var orderID:Int?
    var selectedorderItemID:Int?
    var Orderitems = [ItemsOrder]()
    
    var isFromService:Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(descriptor: UIFontDescriptor(name: "FFShamelFamilySemiRoundBook-S", size: 17), size: 17),NSAttributedString.Key.foregroundColor: UIColor.white]
        self.title = "Make dispute".localized
        navigationController?.navigationBar.barTintColor = "FF6A2A".color
        self.imagePicker = ImagePicker(presentationController: self, delegate: self)
        getproblems()
        if isFromService {
            viewProduct.isHidden = true
        }
        
    }
    
    func getproblems(){
        _ = WebRequests.setup(controller: self).prepare(query: "disputes/problems", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            do {
                let Status =  try JSONDecoder().decode(DisputesProblems.self, from: response.data!)
                self.Problems = Status.data!
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
    }
    
    
    
    
    @IBAction func bt_problame(_ sender: UIButton) {
        ActionSheetStringPicker.show(withTitle:"Problems".localized, rows: Problems.map { $0.value as Any }
                                     , initialSelection: 0, doneBlock: {
                                        picker, value, index in
                                        if let Value = index {
                                            self.tfproblame.text = Value as? String
                                            self.selectedProblemsID = self.Problems[value].id ?? 0
                                        }
                                        return
                                     }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        
    }
    
    @IBAction func bt_producat(_ sender: UIButton) {
        ActionSheetStringPicker.show(withTitle:"Products".localized, rows: Orderitems.map { $0.title as Any }
                                     , initialSelection: 0, doneBlock: {
                                        picker, value, index in
                                        if let Value = index {
                                            self.tfproducat.text = Value as? String
                                            self.selectedorderItemID = self.Orderitems[value].itemId ?? 0
                                        }
                                        return
                                     }, cancel: { ActionStringCancelBlock in return }, origin:sender)
        
    }
    
    @IBAction func bt_send(_ sender: Any) {
        
        guard  self.selectedProblemsID != nil else{
            self.showAlert(title: "".localized, message: "Please select the type of problem.".localized)
            return
        }
        
       
        var parameters: [String: String] = [:]
        if(self.isFromService){
            parameters["order_item_id"] = "0"
            parameters["order_id"] =  self.orderID?.description
            parameters["type"] =  "2"
        }
        else{
            guard  self.selectedorderItemID != nil else{
                self.showAlert(title: "".localized, message:"Please select a product.".localized)
                return
            }
            parameters["order_item_id"] = self.selectedorderItemID?.description
            parameters["order_id"] =  self.orderID?.description
            
        }
        parameters["problem_id"] = self.selectedProblemsID?.description
        parameters["video_url"] =  self.tfvideo.text ?? ""
        parameters["comment"] =  self.txtDeatiles.text ?? ""
        self.showIndicator()
        let imageData = (imgDisputes.image) ?? UIImage()
        
        WebRequests.sendPostMultipartRequestWithImgParam(url: TAConstant.APIBaseURL + "disputes", parameters:parameters ,img: imageData, withName: "image", completion: { (response, error) in
            self.hideIndicator()
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200 {
                    
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }else{
                    self.navigationController?.popViewController(animated: true)
                    
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
        )
        
    }
    
    
    
}
extension MakeDisputesVC: ImagePickerDelegate {
    
    @IBAction func showImagePicker(_ sender: UIButton) {
        self.imagePicker.present(from: sender)
    }
    func didSelect(image: UIImage?) {
        self.imgDisputes.image = image
        self.SelectImage = true
    }
}
