//
//  DisputesDeatilesVC.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 9/5/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class DisputesDeatilesVC: SuperViewController {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblCode: UILabel!
    @IBOutlet weak var lblNameproducat: UILabel!
    @IBOutlet weak var lblproblame: UILabel!
    @IBOutlet weak var lblstatus: UILabel!
    @IBOutlet weak var lbldate: UILabel!
    @IBOutlet weak var imgdisputes: UIImageView!
    @IBOutlet weak var btSendAdmin: UIButton!
    @IBOutlet weak var btCloseDisputes: UIButton!
    @IBOutlet weak var btDontCloseDisputes: UIButton!
    @IBOutlet weak var lblvideo: UILabel!

    
    var disputesitems:Dispute?
    var idDisputes:Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        let navgtion = self.navigationController as! CustomNavigationBar
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(descriptor: UIFontDescriptor(name: "FFShamelFamilySemiRoundBook-S", size: 17), size: 17),NSAttributedString.Key.foregroundColor: UIColor.white]
        self.title = "Dispute details".localized
        navigationController?.navigationBar.barTintColor = "FF6A2A".color
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        getDisputestDeatiles()
        
    }
    
    
    
    func getDisputestDeatiles(){
        
        
        _ = WebRequests.setup(controller: self).prepare(query: "disputes/\(idDisputes ?? 0)", method: HTTPMethod.get).start(){ (response, error) in
            
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            do {
                let Status =  try JSONDecoder().decode(DisputesDeatilesObject.self, from: response.data!)
                self.disputesitems = Status.data?.dispute
                
                self.lblNameproducat.text = self.disputesitems?.item ?? ""
                self.lblstatus.text = self.disputesitems?.status?.name ?? ""
                self.lblName.text = self.disputesitems?.user?.name ?? ""
                self.lblCode.text = self.disputesitems?.disputeCode ?? ""
                self.lblproblame.text = self.disputesitems?.problem ?? ""
                self.lbldate.text = self.disputesitems?.createdAt ?? ""
                self.lblvideo.text = self.disputesitems?.videoUrl ?? ""

                self.imgdisputes.sd_custom(url: self.disputesitems?.attachment ?? "")
                
                if self.disputesitems?.sellerAskedForClose == true,self.disputesitems?.customerConfirmedClose == false{
                    self.btCloseDisputes.isHidden = false
                    self.btDontCloseDisputes.isHidden = false

                }else{
                    self.btCloseDisputes.isHidden = true
                    self.btDontCloseDisputes.isHidden = true
                }
                if self.disputesitems?.adminInformed == false{
                    self.btSendAdmin.isHidden = false
                }else{
                    self.btSendAdmin.isHidden = true
                }
                
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            
        }
        
    }
    
    
    @IBAction func didTab_SendAdmin(_ sender: Any) {
        
        _ = WebRequests.setup(controller: self).prepare(query: "disputes/\(idDisputes ?? 0)/actions/inform-admin", method: HTTPMethod.post).start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200 {
                    
                    self.showAlert(title: "Error".localized, message:Status.message!)
                    return
                }else{
                    self.navigationController?.popViewController(animated: true)
                    NotificationCenter.default.post(name: Notification.Name("UpdateDisputes"), object: nil)
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
        
        
    }
    @IBAction func didTab_Close(_ sender: UIButton) {
        
        var parameters: [String: Any] = [:]
        if sender.tag == 1{
            parameters["status"] = "1"
            
        }else{
            parameters["status"] = "2"
        }
        
        
        _ = WebRequests.setup(controller: self).prepare(query: "disputes/\(idDisputes ?? 0))/actions/close", method: HTTPMethod.post,parameters:parameters).start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200 {
                    
                    self.showAlert(title: "Error".localized, message:Status.message!)
                    return
                }else{
                    self.navigationController?.popViewController(animated: true)
                    NotificationCenter.default.post(name: Notification.Name("UpdateDisputes"), object: nil)
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
        
        
    }
    @IBAction func didTab_Chat(_ sender: Any) {
        
      let vc:MessagesAllVC = AppDelegate.sb_main.instanceVC()
      vc.modalPresentationStyle = .fullScreen
      vc.Storename = self.disputesitems?.seller?.name ?? ""
      vc.idDisputes = self.disputesitems?.id ?? 0
      vc.isFromDisputes = true
      self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    
}
