//
//  DisputesVC.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 9/6/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class DisputesVC: SuperViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var currentpage = 1
    var disputesData:DisputesData?
    var disputesitems = [DisputesResources]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let navgtion = self.navigationController as! CustomNavigationBar
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(descriptor: UIFontDescriptor(name: "FFShamelFamilySemiRoundBook-S", size: 17), size: 17),NSAttributedString.Key.foregroundColor: UIColor.white]
        self.title = "Disputes".localized
        navigationController?.navigationBar.barTintColor = "FF6A2A".color
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        self.tableView.tableFooterView = UIView()
        tableView.registerCell(id: "DisputesCell")
        getDisputest()
        self.tableView.es.addPullToRefresh {
            self.currentpage = 1
            self.disputesitems.removeAll()
            self.getDisputest()
            self.hideIndicator()
        }
        
        self.tableView.es.addInfiniteScrolling {
            self.currentpage += 1
            self.getDisputest() // next page
            self.tableView.estimatedRowHeight = 0
            self.hideIndicator()
        }
        NotificationCenter.default.addObserver(self, selector: #selector(UpdateDisputes(notification:)), name: Notification.Name("UpdateDisputes"), object: nil)
        
    }
    

    func getDisputest(){
        
        
        _ = WebRequests.setup(controller: self).prepare(query: "disputes?page=\(currentpage)", method: HTTPMethod.get).start(){ (response, error) in
            
            self.tableView.es.stopLoadingMore()
            self.tableView.es.stopPullToRefresh()
            
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            do {
                let Status =  try JSONDecoder().decode(DisputesObject.self, from: response.data!)
                self.disputesData = Status.data
                self.disputesitems += self.disputesData!.disputes!
                if self.disputesData?.nextPageUrl == nil {
                    self.tableView.es.stopLoadingMore()
                    self.tableView.es.noticeNoMoreData()
                }
                if self.disputesitems.count == 0{
                    self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.tableView, refershSelector: #selector(self.didRefersh))
                    self.emptyView?.firstLabel.text = "No Disputes to Show".localized
                    self.disputesitems.removeAll()
                    self.tableView.reloadData()
                    
                }else{
                    self.tableView.tableFooterView = nil
                }
                self.tableView.reloadData()
                
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            
        }
        
    }
    
    @IBAction func didRefersh(){
        disputesitems.removeAll()
        getDisputest()
    }
    
    @objc func UpdateDisputes(notification: NSNotification)  {
        disputesitems.removeAll()
        getDisputest()
    }
    
}
extension DisputesVC:UITableViewDataSource,UITableViewDelegate{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return disputesitems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DisputesCell", for: indexPath) as! DisputesCell
        cell.disputesitems =  disputesitems[indexPath.row]
        cell.DeatilesTapped = { [weak self] in
            let  obj  = self?.disputesitems[indexPath.row]
            let vc:DisputesDeatilesVC = DisputesDeatilesVC.loadFromNib()
            vc.idDisputes = obj?.id
            vc.modalPresentationStyle = .fullScreen
            self?.navigationController?.pushViewController(vc, animated: true)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let  obj  = disputesitems[indexPath.row]
        let vc:DisputesDeatilesVC = DisputesDeatilesVC.loadFromNib()
        vc.idDisputes = obj.id
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 188
    }
    
    
    
    
}
