//
//  PaySelection.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 2/22/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import BmoViewPager

private let mainColor = "FF6A2A".color

class PaySelection: SuperViewController {
    
    @IBOutlet weak var viewPagerNavigationBar: BmoViewPagerNavigationBar!
       
    @IBOutlet weak var viewPager: BmoViewPager!

    override func viewDidLoad() {
        super.viewDidLoad()
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setLogotitle(sender: self)
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navigationController?.navigationBar.barTintColor = "FF6A2A".color
        viewPager.dataSource = self
        viewPagerNavigationBar.autoFocus = false
        viewPagerNavigationBar.viewPager = viewPager
        
        viewPager.layer.borderWidth = 5.0
        viewPager.layer.cornerRadius = 5.0
        viewPager.layer.masksToBounds = true
        viewPager.layer.borderColor = UIColor.white.cgColor
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension PaySelection: BmoViewPagerDataSource {
    // Optional
    func bmoViewPagerDataSourceNaviagtionBarItemNormalAttributed(_ viewPager: BmoViewPager, navigationBar: BmoViewPagerNavigationBar, forPageListAt page: Int) -> [NSAttributedString.Key : Any]? {
        return [
            NSAttributedString.Key.font : UIFont(name: "FFShamelFamilySemiRoundBook-S", size: 15)!,
            NSAttributedString.Key.foregroundColor : "014A97".color
        ]
    }
    func bmoViewPagerDataSourceNaviagtionBarItemHighlightedAttributed(_ viewPager: BmoViewPager, navigationBar: BmoViewPagerNavigationBar, forPageListAt page: Int) -> [NSAttributedString.Key : Any]? {
        return [
            NSAttributedString.Key.font : UIFont(name: "FFShamelFamilySemiRoundBook-S", size: 15)!,
            NSAttributedString.Key.foregroundColor : "014A97".color
        ]
    }
    func bmoViewPagerDataSourceNaviagtionBarItemHighlightedBackgroundView(_ viewPager: BmoViewPager, navigationBar: BmoViewPagerNavigationBar, forPageListAt page: Int) -> UIView? {
        let view = UnderLineView()
        view.marginX = 0.0
        view.lineWidth = 5
        view.strokeColor = mainColor
        return view
    }
    func bmoViewPagerDataSourceNaviagtionBarItemNormalBackgroundView(_ viewPager: BmoViewPager, navigationBar: BmoViewPagerNavigationBar, forPageListAt page: Int) -> UIView? {
        let view = UnderLineView()
        view.marginX = 0.0
        view.lineWidth = 5
        view.strokeColor = "E2E2E2".color
        return view
    }
    func bmoViewPagerDataSourceNaviagtionBarItemTitle(_ viewPager: BmoViewPager, navigationBar: BmoViewPagerNavigationBar, forPageListAt page: Int) -> String? {
        switch page {
        case 0:
            return "Pay on receipt".localized
        case 1:
            return "Credit card".localized
        case 2:
            return "PayPal".localized
       
        default:
            break
        }
        return "Demo \(page)"
    }
    
    // Required
    func bmoViewSplitFull(in viewPager: BmoViewPager) -> Bool {
       return true
    }
    func bmoViewPagerDataSourceNumberOfPage(in viewPager: BmoViewPager) -> Int {
        return 3
        
    }
    func bmoViewPagerDataSource(_ viewPager: BmoViewPager, viewControllerForPageAt page: Int) -> UIViewController {
        switch page {

            case 0:
            if let vc = storyboard?.instantiateViewController(withIdentifier: "CashVC") as? CashVC {
               
            return vc
            }
        case 1:
            if let vc = storyboard?.instantiateViewController(withIdentifier: "VisaVC") as? VisaVC {
               
                return vc
            }
        case 2:
            if let vc = storyboard?.instantiateViewController(withIdentifier: "PayPalVC") as? PayPalVC {
                          
                    return vc
            }
    
       
        default:
            break
        }
        return UIViewController()
    }
}
