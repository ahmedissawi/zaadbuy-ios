//
//  AddressPayCell.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 6/12/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class AddressPayCell: UITableViewCell {
    
    @IBOutlet weak var imglocation: UIImageView!
    @IBOutlet weak var lbllocation: UILabel!
    @IBOutlet weak var lblStreet: UILabel!
    @IBOutlet weak var lblcity: UILabel!
    @IBOutlet weak var btdelete: UIButton!
    @IBOutlet weak var btedite: UIButton!
    @IBOutlet weak var  viewDelete: UIView!
    @IBOutlet weak var  viewEdite: UIView!


    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

  
}
