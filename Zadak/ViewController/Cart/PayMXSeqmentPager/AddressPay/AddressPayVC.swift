//
//  AddressPayVC.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 6/12/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
    protocol AddDoneDelgate : class {
         func SelectedDone(sendr:Any?)
     }

class AddressPayVC: SuperViewController {
    var Delegete:AddDoneDelgate?

    @IBOutlet weak var tableView: UITableView!
   
    var addrssItems = [AddressData]()
    
    var selectIndex = -99
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.registerCell(id: "AddressPayCell")
        getAddress()
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
                getAddress()

    }
    @IBAction func addnew(_ sender: Any) {
        let vc:AddAddressVC = AppDelegate.sb_main.instanceVC()
        vc.hidesBottomBarWhenPushed = true
        vc.isFromEditeAddress = false
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)

    }
    func getAddress(){
        _ = WebRequests.setup(controller: self).prepare(query: "addresses", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            do {
                let Status =  try JSONDecoder().decode(AddressInfo.self, from: response.data!)
                
                self.addrssItems = Status.data!
                self.tableView.reloadData()
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
    }
    
    @IBAction func bt_Confirm(_ sender: Any) {
        if selectIndex == -99 {
            self.showAlert(title: "error".localized, message: "Please choose a title".localized)
            return
        }
        self.Delegete!.SelectedDone(sendr: selectIndex)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "checkOut"), object: nil)

       }
    
    
}

extension AddressPayVC:UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addrssItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddressPayCell", for: indexPath) as! AddressPayCell
        let obj = addrssItems[indexPath.row]
        
        cell.lbllocation.text  = "\(obj.name ?? "")"
        cell.lblcity.text = "\(obj.country ?? "")" + " " + "-" + " " + "\(obj.city ?? "")"
        cell.lblStreet.text = "\(obj.country ?? "")" + " " + "-" + " " + "\(obj.city ?? "")" + " " + "-" + " " + "\(obj.addressLine1 ?? "" )"

        if obj.id == selectIndex {
            cell.imglocation.image = UIImage(named: "addresscheck")
        }
        else{
            cell.imglocation.image = UIImage(named: "addressUnCheck")

        }
        cell.viewEdite.isHidden = true
        cell.viewDelete.isHidden = true

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = addrssItems[indexPath.row]

        selectIndex = obj.id!
        tableView.reloadData()
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 118
    }
    
    
    
}
