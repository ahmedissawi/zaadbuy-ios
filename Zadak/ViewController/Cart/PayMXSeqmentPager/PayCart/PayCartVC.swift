//
//  PayCartVC.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 6/12/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class PayCartVC: SuperViewController {
    var listItem = [cartData]()
    var CartItemsList = [CartItems]()
    
    @IBOutlet weak var tableView: UITableView!
    
    var listMethod = [PayMethodsStruct]()
    var selectSerivce :PayMethodsStruct?
    var summeryObject:summeryData?
    
    var discountCapuon = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.registerCell(id: "PayCell")
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.reloadData(notification:)),
            name: NSNotification.Name(rawValue: "selectSerivce"),
            object: nil)
        
    }

    
    @IBAction func bt_Confirm(_ sender: Any) {
        if selectSerivce == nil {
            self.showAlert(title: "error".localized, message: "Please choose a payment method".localized)
            return
        }
        let  returnValue: Int? = UserDefaults.standard.object(forKey: "selectSerivce") as? Int
        let  seller_id: Int? = UserDefaults.standard.object(forKey: "seller_id") as? Int
        
        let  returnAddress: Int? = UserDefaults.standard.object(forKey: "addresId") as? Int

        
        var parameters: [String: Any] = [:]
        var i = 0

        for obj  in  listItem  {
            CartItemsList  = obj.items!
            parameters["seller_id"] = seller_id
            parameters["payment_method_id"] = selectSerivce?.id
            parameters["address_id"] = returnAddress
            parameters["coupon"] = discountCapuon

            
            
            UserDefaults.standard.set(seller_id, forKey:"seller_id")
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "selectSerivce"), object: nil)
            
            for item  in CartItemsList {
                
                parameters["items[\(i)][item_id]"] = item.itemId
                parameters["items[\(i)][quantity]"] = item.quantity
                parameters["items[\(i)][service_id]"] = returnValue
                i = i + 1
            }
        }
        _ = WebRequests.setup(controller: self).prepare(query: "orders", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
            self.showIndicator()
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                self.hideIndicator()
                print("Error serializing  respone json", jsonErr)
            }
            
            
            do {
                self.hideIndicator()
                
                let Status =  try JSONDecoder().decode(newOrderClass.self, from: response.data!)
                let orderData = Status.data
                if orderData?.payment != nil{
                    if let url = URL(string: orderData?.payment?.returnurl ?? "") {
                        self.tabBarController?.selectedIndex = 2
                        
                        UIApplication.shared.open(url)
                        
                    }
                        
                        
                        
                    else{
                        self.hideIndicator()
                        
                        CATransaction.begin()
                        //                            self.navigationController?.popViewController(animated: true)
                        CATransaction.setCompletionBlock({ [weak self] in
                            //                            self?.tabBarController?.selectedIndex = 2
                            let vc:AleartCartVC = AppDelegate.sb_main.instanceVC()
                            vc.modalPresentationStyle = .fullScreen
                            self!.navigationController?.pushViewController(vc, animated: true)
                            
                            
                        })
                        CATransaction.commit()
                        
                        
                        
                    }
                }
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
        
        
    }
    
    @objc func reloadData(notification: NSNotification) {
        let  returnValue: Int? = UserDefaults.standard.object(forKey: "seller_id") as? Int
        //        UserDefaults.standard.set(seller_id, forKey:"seller_id")
        
        _ = WebRequests.setup(controller: nil).prepare(query: "seller/\(returnValue ?? 0)/payment-methods", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            do {
                let Status =  try JSONDecoder().decode(PayMethodsClass.self, from: response.data!)
                
                self.listMethod = Status.data!
                self.tableView.reloadData()
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
    }
}

extension PayCartVC:UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PayCell", for: indexPath) as! PayCell
        if selectSerivce != nil{
            cell.textSerivce.text = selectSerivce?.name
            
            
        }
        if summeryObject != nil{
            cell.discountOther.text = "\(summeryObject?.otherDiscount ?? 0)" + " " + "O.R".localized
            cell.discountChargPrice.text = "\(summeryObject?.shippingDiscount ?? 0)" + " " + "O.R".localized
            cell.discountCapuon.text = "\(summeryObject?.couponDiscount ?? 0)" + " " + "O.R".localized
            
            cell.proudctPrice.text = "\(summeryObject?.totalPrice ?? 0)" + " " + "O.R".localized
            cell.discountTotal.text = "\(summeryObject?.netPrice ?? 0)" + " " + "O.R".localized
        }
        cell.addTapped = { [weak self] in
            ActionSheetStringPicker.show(withTitle:  "Choose payment method".localized, rows: self?.listMethod.map { $0.name as Any }
                , initialSelection: 0, doneBlock: {
                    picker, value, index in
                    if let Value = index {
                        self!.selectSerivce   = self!.listMethod[value]
                        //                        self!.textSerivce.text = Value as? String
                        tableView.reloadData()
                    }
                    self!.orderSummery(sender: "")
                    
                    self!.tableView.reloadData()
                    return
            }, cancel: { ActionStringCancelBlock in return }, origin: self?.tableView .cellForRow(at: indexPath))
            
        }
        
        cell.capuonTapped = { [weak self] in
            self?.couponValidate(sender: cell.textCode.text ?? "")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 450
    }
    
    func couponValidate(sender: String) {
        if selectSerivce == nil {
            self.showAlert(title: "error".localized, message: "Please choose a payment method".localized)
            return
        }
        
        
        var parameters: [String: Any] = [:]
        var i = 0
        
        for obj  in  listItem  {
            CartItemsList  = obj.items!
            parameters["code"] = sender
            self.discountCapuon = sender
            for item  in CartItemsList {
                
                parameters["items[\(i)][item_id]"] = item.itemId
                parameters["items[\(i)][quantity]"] = item.quantity
                
                i = i + 1
            }
        }
        _ = WebRequests.setup(controller: self).prepare(query: "coupons/validate", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
                self.orderSummery(sender: sender)
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            
            
            
        }
        
        
    }
    func orderSummery(sender: String) {
        if selectSerivce == nil {
            self.showAlert(title: "error".localized, message: "Please choose a payment method".localized)
            return
        }
        let  returnValue: Int? = UserDefaults.standard.object(forKey: "selectSerivce") as? Int
        let  seller_id: Int? = UserDefaults.standard.object(forKey: "seller_id") as? Int
        let  returnAddress: Int? = UserDefaults.standard.object(forKey: "addresId") as? Int

        
        var parameters: [String: Any] = [:]
        var i = 0
        
        for obj  in  listItem  {
            CartItemsList  = obj.items!
            parameters["seller_id"] = seller_id
            parameters["payment_method_id"] = selectSerivce?.id
            parameters["coupon"] = discountCapuon
            parameters["address_id"] = returnAddress

            UserDefaults.standard.set(seller_id, forKey:"seller_id")
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "selectSerivce"), object: nil)
            
            for item  in CartItemsList {
                
                parameters["items[\(i)][item_id]"] = item.itemId
                parameters["items[\(i)][quantity]"] = item.quantity
                parameters["items[\(i)][service_id]"] = returnValue
                i = i + 1
            }
        }
        _ = WebRequests.setup(controller: self).prepare(query: "orders/summery", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
            self.showIndicator()
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                self.hideIndicator()
                print("Error serializing  respone json", jsonErr)
            }
            
            
            do {
                self.hideIndicator()
                
                let Status =  try JSONDecoder().decode(summeryClass.self, from: response.data!)
                self.summeryObject = Status.data
                self.tableView.reloadData()
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
        
        
    }
}
