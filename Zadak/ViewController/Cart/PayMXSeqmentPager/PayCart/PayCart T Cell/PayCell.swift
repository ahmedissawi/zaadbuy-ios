//
//  PayCell.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 6/12/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class PayCell: UITableViewCell {
    @IBOutlet weak var textSerivce: UILabel!
    @IBOutlet weak var proudctPrice: UILabel!
    @IBOutlet weak var discountChargPrice: UILabel!
    @IBOutlet weak var discountCapuon: UILabel!
    @IBOutlet weak var discountOther: UILabel!
    @IBOutlet weak var discountTotal: UILabel!
    @IBOutlet weak var textCode: UITextField!

    var addTapped: (() -> ())?
    var capuonTapped: (() -> ())?

    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

   @IBAction func addTappedTapped(_ sender: UIButton) {
                   addTapped?()
              }
    @IBAction func capuonTappedTapped(_ sender: UIButton) {
                      capuonTapped?()
                 }
}
