//
//  MainPayAllVC.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 6/12/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import MXSegmentedPager


class MainPayAllVC: BaseViewController, AddDoneDelgate,PayInfoDoneDelgate {
  
    
  
    var listItem = [cartData]()
    var selectSerivce :ShippingServices?

    @IBOutlet weak var view_header: UIView!
    
     var arrayView = [String]()
     var arrayViewController = [UIViewController]()
  var addresId = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = "FF6A2A".color
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        arrayView = ["العنوان","البيانات الاساسية","الدفع"]
        arrayViewController = [Address(),DeatilesInformation(),Pay()]
        if MOLHLanguage.isRTLLanguage() {
            arrayView = arrayView.reversed()
            arrayViewController = arrayViewController.reversed()
        }
        setUpSegmentation()
    }
    


}

extension MainPayAllVC {
    func setUpSegmentation (){
        
        if MOLHLanguage.isRTLLanguage(){
              self.segmentedPager.pager.showPage(at:self.arrayView.count - 1, animated: false)
           }
        DispatchQueue.main.async {
        if MOLHLanguage.isRTLLanguage(){
           self.segmentedPager.pager.showPage(at:self.arrayView.count - 1, animated: false)
           self.segmentedPager.segmentedControl.selectedSegmentIndex = self.arrayView.count - 1
          //                         segmentedPager.pager.reloadData()
                              }
            }
        segmentedPager.segmentedControl.isUserInteractionEnabled = false
        segmentedPager.backgroundColor = UIColor.white
        segmentedPager.segmentedControl.backgroundColor = UIColor.white
        segmentedPager.parallaxHeader.view = view_header
        segmentedPager.parallaxHeader.mode = .bottom
        segmentedPager.parallaxHeader.height = 20
        segmentedPager.parallaxHeader.minimumHeight = 20
        
        segmentedPager.pager.isScrollEnabled = false
         // Segmented Control customization
            self.segmentedPager.segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocation.down
           segmentedPager.segmentedControl.selectedTitleTextAttributes = [NSAttributedString.Key.foregroundColor: "014A97".color]
        
        segmentedPager.segmentedControl.selectionIndicatorColor = "FF6A2A".color
        segmentedPager.segmentedControl.selectionIndicatorHeight = 3
        
        self.segmentedPager.segmentedControl.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "FFShamelFamilySemiRoundBook-S", size: 17)!, NSAttributedString.Key.foregroundColor :  "014A97".color]

        
    }
    
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, titleForSectionAt index: Int) -> String {
           

           return arrayView[index].localized
       
       }
    
       override func segmentedPager(_ segmentedPager: MXSegmentedPager, didScrollWith parallaxHeader: MXParallaxHeader) {
       }
       
       
       override func numberOfPages(in segmentedPager: MXSegmentedPager) -> Int {
           return arrayView.count
       }
    
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, viewControllerForPageAt index: Int) -> UIViewController {
       return arrayViewController[index]
     
        
    }
    
    fileprivate  func Address() ->AddressPayVC{
             let vc = self.instantiate(id: "AddressPayVC") as! AddressPayVC
                vc.Delegete = self
             addChild(vc)
             vc.didMove(toParent: self)
             return vc
         }
    func SelectedDone(sendr: Any?) {
          if MOLHLanguage.isRTLLanguage(){
             self.segmentedPager.pager.showPage(at:1, animated: false)
          }
        self.addresId = sendr as? Int ?? 0
        UserDefaults.standard.set(addresId, forKey:"addresId")

//        self.segmentedPager.pager.showPage(at:1, animated: false)

      }
      func SelectedPayDone(sendr: Any?) {
        if sendr == nil{
            self.showAlert(title: "error".localized, message:"Please select a shipping company.".localized)

            return
        }
        selectSerivce = sendr as! ShippingServices
//        UserDefaults.standard.set(selectSerivce, forKey:"selectSerivce")

               if MOLHLanguage.isRTLLanguage(){
                      self.segmentedPager.pager.showPage(at:0, animated: false)
                   }
//        UserDefaults.standard.set(addresId, forKey:"selectSerivce")
      

        }
      fileprivate  func DeatilesInformation() ->PayInformationVC{
          let vc = self.instantiate(id: "PayInformationVC") as! PayInformationVC
        vc.Delegete = self

          addChild(vc)
        
          vc.listItem = self.listItem
          vc.didMove(toParent: self)
          return vc
      }
    
    fileprivate  func Pay() ->PayCartVC{
             let vc = self.instantiate(id: "PayCartVC") as! PayCartVC
        vc.listItem = self.listItem

             addChild(vc)
             vc.didMove(toParent: self)
             return vc
         }
    
    
}
