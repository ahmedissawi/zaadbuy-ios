//
//  PayInformationVC.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 6/12/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
protocol PayInfoDoneDelgate : class {
    func SelectedPayDone(sendr:Any?)
}

class PayInformationVC: UIViewController {
    var listItem = [cartData]()
    var listItemCheckout = [CheckoutStruct]()
    var CartItemsList = [CartItems]()
    var ShippingServicesList = [ShippingServices]()
    var selectSerivce :ShippingServices?
    var Delegete:PayInfoDoneDelgate?
    
    var seller_id = 0
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.registerCell(id: "CartCell")
    }
    override func viewWillAppear(_ animated: Bool) {
        //        getCart()
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.reloadData(notification:)),
            name: NSNotification.Name(rawValue: "checkOut"),
            object: nil)
        
    }
    @objc func reloadData(notification: NSNotification) {
        let  returnValue: Int? = UserDefaults.standard.object(forKey: "addresId") as? Int
        
        var parameters: [String: Any] = [:]
        parameters["address_id"] = returnValue
        var i = 0
        
        for obj  in  listItem  {
            CartItemsList  = obj.items!
            seller_id = obj.seller?.id as! Int
            parameters["seller_id"] = seller_id
            UserDefaults.standard.set(seller_id, forKey:"seller_id")
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "selectSerivce"), object: nil)
            
            for item  in CartItemsList {
                parameters["items[\(i)][item_id]"] = item.itemId
                parameters["items[\(i)][quantity]"] = item.quantity
                i = i + 1
            }
        }
        _ = WebRequests.setup(controller: self).prepare(query: "shopping-cart/checkout", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            
            do {
                let Status =  try JSONDecoder().decode(CheckoutClass.self, from: response.data!)
                self.listItemCheckout = Status.data!
                self.ShippingServicesList = self.listItemCheckout[0].shippingServices!
                
                self.tableView.reloadData()
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            
        }
        
    }
//    func languageAction() {
//        ActionSheetStringPicker.show(withTitle:  "اختر شركة الشحن".localized, rows: ShippingServicesList.map { $0.title as Any }
//            , initialSelection: 0, doneBlock: {
//                picker, value, index in
//                if index != nil {
//                    self.selectSerivce   = self.ShippingServicesList[value]
//                    UserDefaults.standard.set(self.selectSerivce?.serviceId, forKey:"selectSerivce")
//
//                }
//
//                self.tableView.reloadData()
//                return
//        }, cancel: { ActionStringCancelBlock in return }, origin: self.view)
//
//    }
    func sumStrAndDouble(str: String?, num: Int) -> String {
        guard let str = str else {
            return String(num)
        }
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        let transferStr = formatter.number(from: str)
        let sum = (transferStr as? Int) ?? 0 + num
        let _res =  formatter.string(from: NSNumber(value: sum))
        return _res ?? ""
    }
   
    @IBAction func bt_Confirm(_ sender: Any) {
        self.Delegete!.SelectedPayDone(sendr: selectSerivce)
        
    }
    
    
}

extension PayInformationVC:UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listItemCheckout.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CartCell", for: indexPath) as! CartCell
        let obj = self.listItemCheckout[indexPath.row]

        cell.CheckoutStructem = self.listItemCheckout[indexPath.row]
        cell.lbTotal.text = obj.total

        cell.addSercse = { [weak self] in
            ActionSheetStringPicker.show(withTitle:  "Choose a shipping company".localized, rows: self?.ShippingServicesList.map { $0.title as Any }
                , initialSelection: 0, doneBlock: {
                    picker, value, index in
                    if index != nil {
                        self?.selectSerivce   = self?.ShippingServicesList[value]
                        
                        UserDefaults.standard.set(self?.selectSerivce?.serviceId, forKey:"selectSerivce")
                        
                    }
                    
                    self?.tableView.reloadData()
                    return
            }, cancel: { ActionStringCancelBlock in return }, origin: self?.tableView .cellForRow(at: indexPath))
        }
        cell.removeFromCard = { [weak self] in
            _ = WebRequests.setup(controller: nil).prepare(query: "shopping-cart/items/\(self!.listItemCheckout[indexPath.row].id ?? 0)", method: HTTPMethod.delete).start(){ (response, error) in
                
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200 {
                        
                        self?.showAlert(title: "error".localized, message:Status.message!)
                        return
                    }else if Status.status  == 200{
                        if self!.listItemCheckout.count == 0{
                            self?.navigationController?.popViewController(animated: true)
                        }else{
                            NotificationCenter.default.post(name: Notification.Name("checkOut"), object: nil)
                            self?.showAlert(title: "Done successfully".localized, message:Status.message!)
                        }
                        
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
                
            }
        }
        cell.buyOnlySercse = { [weak self] in
            self!.Delegete!.SelectedPayDone(sendr: self!.selectSerivce)

        }
        
        if selectSerivce != nil {
            cell.compName.text = selectSerivce?.title
            cell.companPrice.text = "\(selectSerivce?.price ?? 0)"
            let price = selectSerivce?.price
            if let amountValue = obj.total, let am = Double(amountValue) {
                let daa = am + Double(price!)
                cell.lbTotal.text = "\(daa)"
            }
        }
        return cell
        
    }
    

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    
}
