//
//  CartCell.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 2/22/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class CartCell: UITableViewCell {
    
    @IBOutlet weak var lbname: UILabel!
    @IBOutlet weak var lbdesc: UILabel!
    @IBOutlet weak var lbprice: UILabel!
    @IBOutlet weak var lbTotal: UILabel!
    @IBOutlet weak var lbamount: UILabel!
    @IBOutlet weak var img_producat: UIImageView!

    @IBOutlet weak var btDelete: UIButton!
    @IBOutlet weak var btlike: UIButton!
    @IBOutlet weak var lbsize: UILabel!
    @IBOutlet weak var lbcolore: UILabel!
    @IBOutlet weak var btsize: UIButton!
    @IBOutlet weak var btcolore: UIButton!
    @IBOutlet weak var shopingView: UIView!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var decBrn: UIButton!
    @IBOutlet weak var lbldelete: UILabel!
    @IBOutlet weak var lblBuy: UILabel!


    
    @IBOutlet weak var companPrice: UILabel!
    @IBOutlet weak var compName: UILabel!

    var addTapped: (() -> ())?
    var decresTapped: (() -> ())?
    var addSercse: (() -> ())?
    var buyOnlySercse: (() -> ())?
    var removeFromCard: (() -> ())?

    
    var items : CartItems?{
          didSet{
            shopingView.isHidden = true
            lbname.text = items?.title ?? ""
            lbprice.text = "\(items?.price ?? 0.0)"
            lbdesc.text = items?.title ?? ""
            lbTotal.text = items?.total ?? "" + " " + "O.R".localized
            lbamount.text = items?.quantity ?? ""
            img_producat.sd_custom(url: (items?.image)!)
        }
      }



      var CheckoutStructem : CheckoutStruct?{
              didSet{
                
                addBtn.isHidden = true
                decBrn.isHidden = true
                shopingView.isHidden = false
                lbname.text = CheckoutStructem?.title ?? ""
                lbdesc.text = CheckoutStructem?.title ?? ""
                lbprice.text = "\(CheckoutStructem?.price ?? 0.0)"
//                lbTotal.text = CheckoutStructem?.total ?? ""
                lbamount.text = CheckoutStructem?.quantity ?? ""
                img_producat.sd_custom(url: CheckoutStructem?.image ?? "")
            }
          }
    @IBAction func serrvceTapped(_ sender: UIButton) {
                   addSercse?()
              }

    @IBAction func addTappedTapped(_ sender: UIButton) {
                   addTapped?()
              }
    @IBAction func decresTapped(_ sender: UIButton) {
                   decresTapped?()
              }
    @IBAction func buyOnlySercseTappeed(_ sender: UIButton) {
                   buyOnlySercse?()
              }
    @IBAction func removeFromCardTappeed(_ sender: UIButton) {
                      removeFromCard?()
                 }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
