//
//  CartConfirmVC.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 2/22/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class CartConfirmVC: SuperViewController {
    
    @IBOutlet weak var Table: UITableView!
    @IBOutlet weak var tf_code: UITextField!
    @IBOutlet weak var lbprice: UILabel!


    override func viewDidLoad() {
        super.viewDidLoad()
        Table.registerCell(id: "CartCell")
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = "FF6A2A".color
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
    }
    
    @IBAction func bt_confirmation(_ sender: Any) {
        
    }
    @IBAction func bt_Continue(_ sender: Any) {
           let vc:PayTypeVC = AppDelegate.sb_main.instanceVC()
           vc.hidesBottomBarWhenPushed = true
           vc.modalPresentationStyle = .fullScreen
           self.navigationController?.pushViewController(vc, animated: true)
    }


}
extension CartConfirmVC:UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CartCell", for: indexPath) as! CartCell
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }
    
    
    
    
}
