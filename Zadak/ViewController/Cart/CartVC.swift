//
//  CartVC.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 2/21/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class CartVC: SuperViewController {
    
    @IBOutlet weak var Table: UITableView!
    var listItem = [cartData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Table.registerCell(id: "headerCartCell")
        Table.registerCell(id: "CartCell")
        Table.registerCell(id: "FooterCartCell")
        
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = "FF6A2A".color

    }
    override func viewWillAppear(_ animated: Bool) {
        getCart()
    }
    func getCart(){
        _ = WebRequests.setup(controller: self).prepare(query: "shopping-cart/items", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            
            do {
                let Status =  try JSONDecoder().decode(CartRespone.self, from: response.data!)
                self.listItem = Status.data!
                if self.listItem.count == 0{
                    self.Table.showEmptyListMessage("There are no products to display in cart".localized)
                }
                self.Table.reloadData()
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
        
    }
    @IBAction func bt_confirm(_ sender: Any) {
        let vc:CartConfirmVC = AppDelegate.sb_main.instanceVC()
        vc.hidesBottomBarWhenPushed = true
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}
extension CartVC:UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return listItem.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let obj = listItem[section]
        return obj.items!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let obj = listItem[indexPath.section]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CartCell", for: indexPath) as! CartCell
        cell.items = obj.items![indexPath.row]
        cell.btlike.isHidden = false
        cell.btDelete.isHidden = false
        cell.lblBuy.isHidden = false
        cell.lbldelete.isHidden = false

        let price =  Double(obj.items![indexPath.row].price ?? 0.0)
        
        cell.addTapped = { [weak self] in
            var objNew = obj
            let quantatity =  Double(objNew.items![indexPath.row].quantity!)
            objNew.items![indexPath.row].quantity = "\(quantatity! + 1)"
            objNew.items![indexPath.row].total = "\((quantatity! + 1) * price)"
            
            self!.listItem.remove(at: indexPath.section)
            self!.listItem.insert(objNew, at: indexPath.section)
            tableView.reloadData()
        }

        cell.decresTapped = { [weak self] in
            var objNew = obj
            let quantatity =  Double(objNew.items![indexPath.row].quantity!)
            objNew.items![indexPath.row].quantity = "\(quantatity! + -1)"
            objNew.items![indexPath.row].total = "\((quantatity! - 1) * price)"
            
            self!.listItem.remove(at: indexPath.section)
            self!.listItem.insert(objNew, at: indexPath.section)
            tableView.reloadData()
        }
        cell.buyOnlySercse = { [weak self] in
            let vc:MainPayAllVC = AppDelegate.sb_main.instanceVC()
            //        var listItem = [cartData]()
            vc.listItem.append(obj)
            vc.hidesBottomBarWhenPushed = true
            vc.modalPresentationStyle = .fullScreen
            self?.navigationController?.pushViewController(vc, animated: true)
            
        }
        cell.removeFromCard = { [weak self] in
            _ = WebRequests.setup(controller: nil).prepare(query: "shopping-cart/items/\(obj.items![indexPath.row].itemId ?? 0)", method: HTTPMethod.delete).start(){ (response, error) in
                
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200 {
                        
                        self?.showAlert(title: "error".localized, message:Status.message!)
                        return
                    }else if Status.status  == 200{
                        self!.getCart()
                        
                        self?.showAlert(title: "Done successfully".localized, message:Status.message!)
                        
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
                
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = listItem[indexPath.section]
        let vc:DetailsVC = AppDelegate.sb_main.instanceVC()
        vc.producatID = obj.items![indexPath.row].itemId
        vc.hidesBottomBarWhenPushed = true
        vc.modalPresentationStyle = .fullScreen
        navigationController?.pushViewController(vc, animated: true)

    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 90
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let obj = listItem[section]
        
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "headerCartCell") as! headerCartCell
        headerCell.lb_title.text = obj.seller?.name
        
        
        return headerCell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let obj = listItem[section]
        
        let FooterCell = tableView.dequeueReusableCell(withIdentifier: "FooterCartCell") as! FooterCartCell
        FooterCell.btnShop.setTitle( "Buy all from the seller".localized + "\(obj.seller?.name ?? "")", for: .normal)
        
        FooterCell.btnShop.tag = section
        FooterCell.btnShop.addTarget(self, action:#selector(self.ByuAllFromSellerButton), for: UIControl.Event.touchUpInside)
        
        return FooterCell
        
    }
    
    @objc func ByuAllFromSellerButton(sender:UIButton){
        let vc:MainPayAllVC = AppDelegate.sb_main.instanceVC()
        vc.listItem.append(self.listItem[sender.tag])
        vc.hidesBottomBarWhenPushed = true
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}


