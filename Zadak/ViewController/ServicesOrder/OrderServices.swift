//
//  OrderServices.swift
//  Zadak
//
//  Created by osamaaassi on 15/03/2021.
//  Copyright © 2021  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import MXSegmentedPager

class OrderServices: BaseViewController {

    var arrayView = [String]()
    var arrayViewController = [UIViewController]()
    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        arrayView = ["انتظار القبول","الطلب مقبول","تم عرض سعر","السعر مقبول","قيد التنفيذ","تم التنفيذ","انتظار قبول الدفعة","مدفوع","الدفعة مرفوضة","ملغي","مرفوضة","السعر مقبول"]
        
        
        if MOLHLanguage.isRTLLanguage() {
            arrayView = arrayView.reversed()
            arrayViewController = arrayViewController.reversed()
        }
        setUpSegmentation()
        
    }
    
    
//    override func viewWillDisappear(_ animated: Bool) {
//        let navgtion = self.navigationController as! CustomNavigationBar
//        navgtion.setLogotitle(sender: self)
//        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        let navgtion = self.navigationController as! CustomNavigationBar
//        navgtion.setLogotitle(sender: self)
//        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 0.4156862745, blue: 0.1647058824, alpha: 1)
//    }

}

extension OrderServices {
    
    func setUpSegmentation (){
        
        if MOLHLanguage.isRTLLanguage(){
            self.segmentedPager.pager.showPage(at:self.arrayView.count - 1, animated: false)
        }
        DispatchQueue.main.async {
            if MOLHLanguage.isRTLLanguage(){
                self.segmentedPager.pager.showPage(at:self.arrayView.count - 1, animated: false)
                self.segmentedPager.segmentedControl.selectedSegmentIndex = self.arrayView.count - 1
                // segmentedPager.pager.reloadData()
            }
        }
        
        segmentedPager.backgroundColor = UIColor.white
        segmentedPager.segmentedControl.backgroundColor = UIColor.white
        segmentedPager.parallaxHeader.view = nil
        segmentedPager.parallaxHeader.mode = .bottom
        segmentedPager.parallaxHeader.height = 0
        segmentedPager.parallaxHeader.minimumHeight = 0
        
        
        
        // Segmented Control customization
        self.segmentedPager.segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocation.down
        segmentedPager.segmentedControl.selectedTitleTextAttributes = [NSAttributedString.Key.foregroundColor: "014A97".color]
        
        segmentedPager.segmentedControl.selectionIndicatorColor = "FF6A2A".color
        segmentedPager.segmentedControl.selectionIndicatorHeight = 1
        
        self.segmentedPager.segmentedControl.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "FFShamelFamilySemiRoundBook-S", size: 17)!, NSAttributedString.Key.foregroundColor :  "014A97".color]
        
        
    }
    
    
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, titleForSectionAt index: Int) -> String {
        
        
        switch index {
        case 11:
            return "Waiting Acceptance".localized
        case 10:
            return "Order Accepted".localized
        case 9:
            return "Price has been shown".localized
        case 8:
            return "Price acceptable".localized
        case 7:
            return "Underway".localized
        case 6:
            return "Done".localized
            
        case 5:
            return "Waiting to accept the price".localized
        case 4:
            return "Paid".localized
        case 3:
            return "Payment Rejected".localized
           
        case 2:
            return "Price Denied".localized
        case 1:
            return "rejected".localized
        case 0:
            return "Cancelled".localized
        default:
            break
        }
        return "Waiting Acceptance".localized
        
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, didScrollWith parallaxHeader: MXParallaxHeader) {
    }
    
    
    override func numberOfPages(in segmentedPager: MXSegmentedPager) -> Int {
        return arrayView.count
    }
    
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, viewControllerForPageAt index: Int) -> UIViewController {
        switch index {
        
        case 0:
            return W11VC()
        case 1:
            return W10VC()
        case 2:
            return W9VC()
            
        case 3:
            return W8VC()
        case 4:
            return W7VC()
        case 5:
            return W6VC()
               
        case 6:
            return W5VC()
        case 7:
            return W4VC()
        case 8:
            return W3VC()
        case 9:
            return W2VC()
        case 10:
            return W1VC()
        case 11:
            return W0VC()
       
            
        default:
            break
        }
        return W0VC()
        
        
    }
    
    
    fileprivate  func W0VC() -> OrdersServicesVC{
        let vc:OrdersServicesVC = OrdersServicesVC.loadFromNib()
        addChild(vc)
        vc.Link = "status=0"
        defaults.set(0, forKey: "status")
        vc.didMove(toParent: self)
        return vc
    }
    fileprivate  func W1VC() ->OrdersServicesVC{
        let vc:OrdersServicesVC = OrdersServicesVC.loadFromNib()
        vc.Link = "status=1"
        defaults.set(1, forKey: "status")
        addChild(vc)
        vc.didMove(toParent: self)
        return vc
    }
    fileprivate  func W2VC() ->OrdersServicesVC{
        let vc:OrdersServicesVC = OrdersServicesVC.loadFromNib()
        vc.Link = "status=2"
        defaults.set(2, forKey: "status")
        NotificationCenter.default.post(name: Notification.Name("UpdateOrder"), object: nil)
        addChild(vc)
        vc.didMove(toParent: self)
        return vc
    }
    fileprivate  func W3VC() ->OrdersServicesVC{
        let vc:OrdersServicesVC = OrdersServicesVC.loadFromNib()
        vc.Link = "status=3"
        defaults.set(3, forKey: "status")
        addChild(vc)
        vc.didMove(toParent: self)
        return vc
    }
    fileprivate  func W4VC() ->OrdersServicesVC{

        let vc:OrdersServicesVC = OrdersServicesVC.loadFromNib()
        vc.Link = "status=4"
        defaults.set(4, forKey: "status")
        addChild(vc)
        vc.didMove(toParent: self)
        return vc
    }
    fileprivate  func W5VC() ->OrdersServicesVC{
        let vc:OrdersServicesVC = OrdersServicesVC.loadFromNib()
        vc.Link = "status=5"
        defaults.set(3, forKey: "status")
        addChild(vc)
        vc.didMove(toParent: self)
        return vc
    }
    
    
    fileprivate  func W6VC() ->OrdersServicesVC{
        let vc:OrdersServicesVC = OrdersServicesVC.loadFromNib()
        vc.Link = "payment_status=1"
        defaults.set(1, forKey: "payment_status")
        addChild(vc)
        vc.didMove(toParent: self)
        return vc
    }
    fileprivate  func W7VC() ->OrdersServicesVC{
        let vc:OrdersServicesVC = OrdersServicesVC.loadFromNib()
        vc.Link = "payment_status=2"
        defaults.set(2, forKey: "payment_status")
        addChild(vc)
        vc.didMove(toParent: self)
        return vc
    }
    fileprivate  func W8VC() ->OrdersServicesVC{
        let vc:OrdersServicesVC = OrdersServicesVC.loadFromNib()
        vc.Link = "payment_status=11"
        defaults.set(11, forKey: "payment_status")
        addChild(vc)
        vc.didMove(toParent: self)
        return vc
    }
    
    
    
    fileprivate  func W9VC() ->OrdersServicesVC{
        let vc:OrdersServicesVC = OrdersServicesVC.loadFromNib()
        vc.Link = "status=11"
        defaults.set(11, forKey: "status")
        addChild(vc)
        vc.didMove(toParent: self)
        return vc
    }
    fileprivate  func W10VC() ->OrdersServicesVC{
        let vc:OrdersServicesVC = OrdersServicesVC.loadFromNib()
        vc.Link = "status=12"
        defaults.set(12, forKey: "status")
        addChild(vc)
        vc.didMove(toParent: self)
        return vc
    }
    fileprivate  func W11VC() ->OrdersServicesVC{
        let vc:OrdersServicesVC = OrdersServicesVC.loadFromNib()
        vc.Link = "status=13"
        defaults.set(13, forKey: "status")
        addChild(vc)
        vc.didMove(toParent: self)
        return vc
    }
    
}
