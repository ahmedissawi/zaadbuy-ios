//
//  MxSeqmentCategoriesVC.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 12/22/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import MXSegmentedPager



class MainServices: BaseViewController {
    
    var arrayView = [String]()
    var arrayViewController = [UIViewController]()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = "FF6A2A".color
        
        arrayView = ["الخدمات","الطلبيات"]
        
        if MOLHLanguage.isRTLLanguage() {
            arrayView = arrayView.reversed()
            arrayViewController = arrayViewController.reversed()
        }
        
        setUpSegmentation()
    }
  
    
    override func viewWillAppear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 0.4156862745, blue: 0.1647058824, alpha: 1)

    }
   
}

extension MainServices {
    func setUpSegmentation (){
        
        if MOLHLanguage.isRTLLanguage(){
            self.segmentedPager.pager.showPage(at:self.arrayView.count - 1, animated: false)
        }
        DispatchQueue.main.async {
            if MOLHLanguage.isRTLLanguage(){
                self.segmentedPager.pager.showPage(at:self.arrayView.count - 1, animated: false)
                self.segmentedPager.segmentedControl.selectedSegmentIndex = self.arrayView.count - 1
                // segmentedPager.pager.reloadData()
            }
        }
 

        segmentedPager.backgroundColor = UIColor.white
        segmentedPager.segmentedControl.backgroundColor = UIColor.white
        // segmentedPager.parallaxHeader.view = HeadrView
        segmentedPager.parallaxHeader.mode = .bottom
        //   segmentedPager.parallaxHeader.height = 50
     //   segmentedPager.parallaxHeader.minimumHeight = 44
        
       
       
     //   self.segmentedPager.segmentedControl.verticalDividerColor = UIColor.red
       // self.segmentedPager.segmentedControl.isVerticalDividerEnabled = true
        
       
        // Segmented Control customization
        self.segmentedPager.segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocation.down
        segmentedPager.segmentedControl.selectedTitleTextAttributes = [NSAttributedString.Key.foregroundColor: "014A97".color]
       

        
        //color seleded background change
        self.segmentedPager.segmentedControl.selectionStyle = .box
        self.segmentedPager.segmentedControl.selectionIndicatorBoxOpacity = CGFloat(0.05)
        segmentedPager.segmentedControl.selectionIndicatorColor = "014A97".color
        
        segmentedPager.segmentedControl.selectionIndicatorHeight = 0.5
        
        self.segmentedPager.segmentedControl.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "FFShamelFamilySemiRoundBook-S", size: 17)!, NSAttributedString.Key.foregroundColor :  "014A97".color]
        
       
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, titleForSectionAt index: Int) -> String {
        
        
        switch index {
        case 0:
            return "Service order".localized
        case 1:
            return "Product orders".localized
        default:
            break
        }
        return "Service order".localized
        
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, didScrollWith parallaxHeader: MXParallaxHeader) {
    }
    
    
    override func numberOfPages(in segmentedPager: MXSegmentedPager) -> Int {
        return arrayView.count
    }
    
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, viewControllerForPageAt index: Int) -> UIViewController {
        switch index {
        case 1:
            return W0VC()
        case 0:
            return W1VC()
        default:
            break
        }
        return W0VC()
        
        
    }
    
    
//    fileprivate  func W0VC() ->ServicesVC{
//        let vc:ServicesVC = ServicesVC.loadFromNib()
//        addChild(vc)
//        vc.didMove(toParent: self)
//        return vc
//    }
    
    fileprivate  func W0VC() ->MainOrderVC{
           let vc = self.instantiate(id: "MainOrderVC") as! MainOrderVC
    
            addChild(vc)
            vc.didMove(toParent: self)
            return vc
        }
    
    fileprivate  func W1VC() ->OrderServices{
        let vc = self.instantiate(id: "OrderServices") as! OrderServices
        addChild(vc)
        vc.didMove(toParent: self)
        return vc
    }
    


}

