//
//  DeatilesServiceOrderVC.swift
//  Zadak
//
//  Created by osamaaassi on 15/03/2021.
//  Copyright © 2021  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class DeatilesServiceOrderVC: SuperViewController {
    
    
    //OrderDetails
    @IBOutlet weak var tfOrder: UILabel!
    @IBOutlet weak var tfDescription: UILabel!
    @IBOutlet weak var tfStatus: UILabel!
    @IBOutlet weak var tfpaymentStatus: UILabel!
    @IBOutlet weak var tfCreatedAt: UILabel!
    @IBOutlet weak var tfRequiredDate: UILabel!
    @IBOutlet weak var tfFromTime: UILabel!
    @IBOutlet weak var tfToTime: UILabel!
    @IBOutlet weak var viewOrderDetails: UIView!
    
    
    //Seller
    @IBOutlet weak var tfName: UILabel!
    @IBOutlet weak var tfMobileSeller: UILabel!
    @IBOutlet weak var tfNameStore: UILabel!
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var viewSeller: UIView!
    
    
    //bankAccount
    @IBOutlet weak var tfNameBank: UILabel!
    @IBOutlet weak var tfBranch: UILabel!
    @IBOutlet weak var tfAccountNo: UILabel!
    @IBOutlet weak var tfNotes: UILabel!
    @IBOutlet weak var tfStatusBank: UILabel!
    @IBOutlet weak var tfPaymentMethods: UILabel!
    @IBOutlet weak var viewBankAccount: UIView!
    
    //Address
    
    @IBOutlet weak var tfAddressOne: UILabel!
    @IBOutlet weak var tfAddressTwo: UILabel!
    @IBOutlet weak var tfAddressThree: UILabel!
    @IBOutlet weak var tfCounty: UILabel!
    @IBOutlet weak var tfCity: UILabel!
    @IBOutlet weak var tfPostCode: UILabel!
    @IBOutlet weak var tfPhone: UILabel!
    @IBOutlet weak var tfMobile: UILabel!
    @IBOutlet weak var tfEmail: UILabel!
    @IBOutlet weak var tfSubtotal: UILabel!
    @IBOutlet weak var tfTotal: UILabel!
    @IBOutlet weak var viewAddress: UIView!
    
    
    
    
    @IBOutlet weak var tableViewServices: UITableView!
    @IBOutlet weak var heighttableViewServices: NSLayoutConstraint!
    @IBOutlet weak var viewItemOrder: UIView!
    
    var isFromDeatileOrder:Bool = false
    var isFromItemOrder:Bool = false
    var isFromDeatileSeller:Bool = false
    var isFromBanck:Bool = false
    var isFromAddressOrder:Bool = false
    
    var id:Int?
    var dataServiceOrders : DataServiceOrders?
    var seller: SellerServiceOrder?
    var bankAccount: BankAccountServiec?
    var itemsOrder = [ItemsOrderModel]()
    var paymentMethods = [PaymentMethods]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getOrder()
        tableViewServices.registerCell(id: "ServiceCell")
        tableViewServices.tableFooterView = UIView.init(frame: .zero)
        
        if(isFromDeatileOrder){
            viewOrderDetails.isHidden = false
            viewItemOrder.isHidden = true
            viewSeller.isHidden = true
            viewBankAccount.isHidden = true
            viewAddress.isHidden = true
        }
        if(isFromItemOrder){
            viewOrderDetails.isHidden = true
            viewItemOrder.isHidden = false
            viewSeller.isHidden = true
            viewBankAccount.isHidden = true
            viewAddress.isHidden = true
        }
        if(isFromDeatileSeller){
            viewOrderDetails.isHidden = true
            viewItemOrder.isHidden = true
            viewSeller.isHidden = false
            viewBankAccount.isHidden = true
            viewAddress.isHidden = true
        }
        if(isFromBanck){
            viewOrderDetails.isHidden = true
            viewItemOrder.isHidden = true
            viewSeller.isHidden = true
            viewBankAccount.isHidden = false
            viewAddress.isHidden = true
        }
        if(isFromAddressOrder){
            viewOrderDetails.isHidden = true
            viewItemOrder.isHidden = true
            viewSeller.isHidden = true
            viewBankAccount.isHidden = true
            viewAddress.isHidden = false
        }
        
        NotificationCenter.default.addObserver(self, selector:#selector(UpdateCellItemService(notification:)), name: Notification.Name("UpdateCellItemService"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector:#selector(UpdateItemService(notification:)), name: Notification.Name("UpdateItemService"), object: nil)
    }
    
    @objc func UpdateItemService(notification: NSNotification)  {
        itemsOrder.removeAll()
        paymentMethods.removeAll()
        getOrder()
    }
    
    @objc func UpdateCellItemService(notification: NSNotification)  {
        
        if itemsOrder.count == 0{
           
            heighttableViewServices.constant = 300
        }else{
            heighttableViewServices.constant = 968
            tableViewServices.reloadData()
        }
    }
    
    
    func getOrder(){
        
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        
        _ = WebRequests.setup(controller: self).prepare(query: "service-orders/\(id!)", method: HTTPMethod.get,isAuthRequired: false).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            do {
                let Status =  try JSONDecoder().decode(ServiceOrders.self, from: response.data!)
                
                self.dataServiceOrders = Status.data
                self.seller = self.dataServiceOrders!.seller
                self.bankAccount = self.dataServiceOrders!.bankAccount
                self.itemsOrder += self.dataServiceOrders!.items!
                self.paymentMethods += self.dataServiceOrders!.paymentMethods!
                self.showData()
                
                if self.itemsOrder.count == 0{
                    self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.tableViewServices, refershSelector: #selector(self.UpdateItemService))
                    self.emptyView?.firstLabel.text = "No service".localized
                    self.emptyView?.imgEmpty.isHidden = false
                    self.emptyView?.imgEmpty.image = UIImage(named: "refreshment")
                    if self.isFromItemOrder {
                        self.emptyView?.firstLabel.text = "No service".localized
                        
                    }
                }
                
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
        
    }
    
    
    func showData() {
        if (isFromDeatileOrder){
            self.tfOrder.text = self.dataServiceOrders?.orderNo ?? ""
            self.tfDescription.text = self.dataServiceOrders?.descriptionValue ?? ""
            self.tfStatus.text = self.dataServiceOrders?.status?.name ?? ""
            self.tfpaymentStatus.text = self.dataServiceOrders?.paymentStatus?.name ?? ""
            self.tfCreatedAt.text = self.dataServiceOrders?.createdAt ?? ""
            self.tfRequiredDate.text = self.dataServiceOrders?.requiredDate ?? ""
            self.tfFromTime.text = self.dataServiceOrders?.fromTime ?? ""
            self.tfToTime.text = self.dataServiceOrders?.toTime ?? ""
            self.tfSubtotal.text = self.dataServiceOrders?.subtotal?.description ?? "0"
            self.tfTotal.text = self.dataServiceOrders?.total?.description ?? "0"
        }
        
        
        if (isFromDeatileSeller){
            self.tfName.text = self.seller?.name ?? ""
            self.tfMobileSeller.text = self.seller?.mobile ?? ""
            self.tfNameStore.text = self.seller?.store?.name ?? ""
            self.imgLogo.sd_custom(url: self.seller?.store?.logo ?? "")
            
        }
        if (isFromItemOrder){
            
            NotificationCenter.default.post(name: Notification.Name("UpdateCellItemService"), object: nil)
        }
        if (isFromBanck){
            self.tfNameBank.text = self.bankAccount?.name ?? ""
            self.tfBranch.text = self.bankAccount?.branch ?? ""
            self.tfAccountNo.text = self.bankAccount?.accountNo ?? ""
            self.tfNotes.text = self.bankAccount?.notes ?? ""
            self.tfStatusBank.text = self.bankAccount?.status ?? ""
            self.tfPaymentMethods.text = self.paymentMethods.first?.name ?? ""
        }
        
        if (isFromAddressOrder){
            self.tfAddressOne.text = self.dataServiceOrders?.addressLine1 ?? ""
            self.tfAddressTwo.text = self.dataServiceOrders?.addressLine2 ?? ""
            self.tfAddressThree.text = self.dataServiceOrders?.addressLine3 ?? ""
            self.tfCounty.text = self.dataServiceOrders?.county ?? ""
            self.tfCity.text = self.dataServiceOrders?.city ?? ""
            self.tfPostCode.text = self.dataServiceOrders?.postalCode ?? ""
            self.tfPhone.text = self.dataServiceOrders?.phone ?? ""
            self.tfMobile.text = self.dataServiceOrders?.mobile ?? ""
            self.tfEmail.text = self.dataServiceOrders?.email ?? ""
           
        }
    }
    
}


extension DeatilesServiceOrderVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemsOrder.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableViewServices.dequeueReusableCell(withIdentifier: "ServiceCell", for: indexPath) as! ServiceCell
      
        cell.obj = itemsOrder[indexPath.row]
        cell.objdata = self.dataServiceOrders
        let capun:Bool = UserDefaults.standard.bool(forKey: "isCoupn")
        let addCoupn:Bool = UserDefaults.standard.bool(forKey: "isAddCoupn")
        
        if(capun){
            cell.viewCoupon.isHidden = false
            cell.butPrice.isHidden = false
            cell.butCoupon.isHidden = true
            cell.viewCodeCoupon.isHidden = true
            
        }
        else if(addCoupn){
            cell.viewCoupon.isHidden = false
            cell.viewCodeCoupon.isHidden = false
            cell.butCoupon.isHidden = false
            cell.butPrice.isHidden = true
        }
        else{
            cell.viewCoupon.isHidden = true
            cell.viewCodeCoupon.isHidden = true
            cell.butPrice.isHidden = true
            cell.butCoupon.isHidden = true
            
        }
        
        cell.acceptPriceOffer = {[weak self] in
            
            let vc:AcceptPriceVC = AcceptPriceVC.loadFromNib()
            let obj = self!.itemsOrder[indexPath.row]
            vc.isFromePrice = true
            vc.itemsOrderModel = obj
            vc.dataServiceOrders = self!.dataServiceOrders
            vc.idOrder = self!.id ?? 0
            vc.modalPresentationStyle = .overCurrentContext
            self!.present(vc, animated: false, completion: nil)
        }
        cell.validateCouponSeller = {[weak self] in
            
            let obj = self!.itemsOrder[indexPath.row]
           
            guard let coupon =  cell.tfCoupon.text , !coupon.isEmpty else {
                self?.showAlert(title: "".localized, message:"No capon".localized)
                return
            }
            
            guard Helper.isConnectedToNetwork() else {
                self!.showAlert(title: "".localized, message: "There is no internet connection".localized)
                return }
            
            var parameters: [String: Any] = [:]
            parameters["order_id"] = self!.id ?? 0
            parameters["item_id"] = obj.id ?? 0
            parameters["coupon"] = coupon
            
         
            
            _ = WebRequests.setup(controller: self).prepare(query: "service-orders/apply-coupon", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200 {
                        self?.showAlert(title: "Error".localized, message:Status.message!)
                        return
                    }
                    else if Status.status == 200{
                        NotificationCenter.default.post(name: Notification.Name("UpdateItemService"), object: nil)
                     
                        
                        
                    }
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
            }
            
//            _ = WebRequests.setup(controller: self).prepare(query: "service-orders/validate-coupon", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
//                do {
//                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
//                    if Status.status != 200 {
//                        self!.showAlert(title: "Error".localized, message:Status.message!)
//                        return
//                    }
//                    else if Status.status == 200{
//                        self!.dismiss(animated: false) {
//                            
//                            let vc:AcceptPriceVC = AcceptPriceVC.loadFromNib()
//                            vc.isFromeCouopn = true
//                            vc.itemsOrderModel = obj
//                            vc.idOrder = self!.id ?? 0
//                            vc.coupon = coupon
//                            vc.modalPresentationStyle = .overCurrentContext
//                            self!.present(vc, animated: false, completion: nil)
//                            
//                        }
//                        self!.dismiss(animated: false, completion: nil)
//                    }
//                }catch let jsonErr {
//                    print("Error serializing  respone json", jsonErr)
//                }
//            }
            
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.heighttableViewServices.constant = self.tableViewServices.contentSize.height }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 968
    }
    
}
