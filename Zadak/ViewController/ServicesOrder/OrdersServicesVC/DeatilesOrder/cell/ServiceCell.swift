//
//  ServiceCell.swift
//  ZaadSeller
//
//  Created by osamaaassi on 1/31/21.
//  Copyright © 2021  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class ServiceCell: UITableViewCell {
    
    @IBOutlet weak var tfTitle: UITextField!
    @IBOutlet weak var tfQuantity: UITextField!
    @IBOutlet weak var tfStatus: UITextField!
    @IBOutlet weak var tfPaymentStatus: UITextField!
    @IBOutlet weak var tfSellerDeliveryDate: UITextField!
    @IBOutlet weak var tfRegularPrice: UITextField!
    @IBOutlet weak var tfDiscount: UITextField!
    @IBOutlet weak var tfPayable: UITextField!
    @IBOutlet weak var tfCoupon: UITextField!
    @IBOutlet weak var tfNote: UITextField!
    
    @IBOutlet weak var viewCoupon: UIView!
    @IBOutlet weak var viewCodeCoupon: UIView!
    @IBOutlet weak var butPrice: UIButton!
    @IBOutlet weak var butCoupon: UIButton!
    
    var acceptPriceOffer :(() -> ())?
    var validateCouponSeller:(() -> ())?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    var obj : ItemsOrderModel?{
        didSet{
            tfTitle.text = obj?.title ?? ""
            tfQuantity.text = obj?.quantity?.description ?? ""
            tfStatus.text = obj?.status?.name?.description ?? ""
          
            tfCoupon.text = obj?.coupon?.description ?? ""
            
            tfPaymentStatus.text = obj?.paymentStatus?.name?.description ?? ""
            //tfSellerDeliveryDate.text = obj?.sellerDeliveryDate?.description ?? ""
              
           
        }
    }
    
    var objdata :  DataServiceOrders? {
        didSet{
            tfNote.text = objdata?.sellerNotes ?? ""
            tfRegularPrice.text = objdata?.subtotal?.description ?? "0"
            tfDiscount.text = objdata?.couponValue?.description ?? "0"
            tfPayable.text = objdata?.total?.description ?? "0"
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func acceptPrice(_ sender: UIButton) {
        acceptPriceOffer?()
    }
    @IBAction func validateCoupon(_ sender: UIButton) {
        validateCouponSeller?()
    }
}
