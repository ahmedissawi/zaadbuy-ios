//
//  MainOrderDeatiles.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/13/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import MXSegmentedPager

class MainServicesDeatiles: BaseViewController {

    @IBOutlet weak var HeadrView: UIView!
    
    
    var arrayView = [String]()
    var arrayViewController = [UIViewController]()
    var ID:Int?
    var isCoupn:Bool = false
    var isDetalisSellar:Bool = false
    var countPage:Int?
    var isAddCoupn:Bool = false
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        arrayView = ["بيانات الطلبية","الطلبيات","بيانات التاجر","بيانات البنك","العنوان"]
        
        if MOLHLanguage.isRTLLanguage() {
            arrayView = arrayView.reversed()
            arrayViewController = arrayViewController.reversed()
        }
        setUpSegmentation()
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
            let navgtion = self.navigationController as! CustomNavigationBar
        if (isDetalisSellar){
            navgtion.setCustomBackButtonWithdismiss(sender: self)
        }else{
            navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        }
            navgtion.setLogotitle(sender: self)
            navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 0.4156862745, blue: 0.1647058824, alpha: 1)
            
        }
  

    
   
    
}
extension MainServicesDeatiles {
    
    func setUpSegmentation (){
        if (isCoupn){
            countPage = 3
        }
        
        else if (isAddCoupn){
            countPage = 3
        }
        else if (isDetalisSellar){
            countPage = 1
            UserDefaults.standard.set(false, forKey: "isCoupn")
            UserDefaults.standard.set(false, forKey: "isAddCoupn")
        }
        else{
            UserDefaults.standard.set(false, forKey: "isCoupn")
            UserDefaults.standard.set(false, forKey: "isAddCoupn")
            countPage = self.arrayView.count - 1
        }
        
        if MOLHLanguage.isRTLLanguage(){
            self.segmentedPager.pager.showPage(at:countPage!, animated: false)
          
        }
        DispatchQueue.main.async {
            if MOLHLanguage.isRTLLanguage(){
                self.segmentedPager.pager.showPage(at:self.countPage!, animated: false)
                self.segmentedPager.segmentedControl.selectedSegmentIndex = self.countPage!
                self.segmentedPager.pager.reloadData()
            }
        }
        
        segmentedPager.backgroundColor = UIColor.white
        segmentedPager.segmentedControl.backgroundColor = UIColor.white
        segmentedPager.parallaxHeader.view = nil
        segmentedPager.parallaxHeader.mode = .bottom
        segmentedPager.parallaxHeader.height = 0
        segmentedPager.parallaxHeader.minimumHeight = 0
      //  segmentedPager.segmentedControl.selectedSegmentIndex = 3
        // Segmented Control customization
        self.segmentedPager.segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocation.down
        segmentedPager.segmentedControl.selectedTitleTextAttributes = [NSAttributedString.Key.foregroundColor: "014A97".color]
        
        segmentedPager.segmentedControl.selectionIndicatorColor = "FF6A2A".color
        segmentedPager.segmentedControl.selectionIndicatorHeight = 1
        
        self.segmentedPager.segmentedControl.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "FFShamelFamilySemiRoundBook-S", size: 17)!, NSAttributedString.Key.foregroundColor :  "014A97".color]
        
        
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, titleForSectionAt index: Int) -> String {
        
        
        switch index {
        
        
        case 0:
            return "Address".localized
        case 1:
            return "Bank data".localized
        case 2:
            return "Seller Data".localized
        case 3:
            return "Orders".localized
        case 4:
            return "Order data".localized
        default:
            break
        }
        if (isCoupn){
            return "Orders".localized
        }else{
            return "Order data".localized
        }
        
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, didScrollWith parallaxHeader: MXParallaxHeader) {
    }
    
    
    override func numberOfPages(in segmentedPager: MXSegmentedPager) -> Int {
        return arrayView.count
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, viewControllerForPageAt index: Int) -> UIViewController {
        switch index {
        case 4:
            return W0VC()
        case 3:
            return W1VC()
        case 2:
            return W2VC()
        case 1:
            return W3VC()
        case 0:
            return W4VC()
        default:
            break
        }
        if (isCoupn){
            return W1VC()
        }else{
            return W0VC()
        }
        
        
    }
    
    
    
    fileprivate  func W0VC() ->DeatilesServiceOrderVC{
        let vc:DeatilesServiceOrderVC = DeatilesServiceOrderVC.loadFromNib()
        vc.isFromDeatileOrder = true
        vc.id = self.ID
        addChild(vc)
        vc.didMove(toParent: self)
        return vc
    }
    fileprivate  func W1VC() ->DeatilesServiceOrderVC{
        let vc:DeatilesServiceOrderVC = DeatilesServiceOrderVC.loadFromNib()
        vc.isFromItemOrder = true
        vc.id = self.ID
        addChild(vc)
        vc.didMove(toParent: self)
        return vc
    }
    fileprivate  func W2VC() ->DeatilesServiceOrderVC{
        let vc:DeatilesServiceOrderVC = DeatilesServiceOrderVC.loadFromNib()
        vc.isFromDeatileSeller = true
        vc.id = self.ID
        addChild(vc)
        vc.didMove(toParent: self)
        return vc
    }
    fileprivate  func W3VC() ->DeatilesServiceOrderVC{
        let vc:DeatilesServiceOrderVC = DeatilesServiceOrderVC.loadFromNib()
        vc.isFromBanck = true
        vc.id = self.ID
        addChild(vc)
        vc.didMove(toParent: self)
        return vc
    }
    
    fileprivate  func W4VC() ->DeatilesServiceOrderVC{
        let vc:DeatilesServiceOrderVC = DeatilesServiceOrderVC.loadFromNib()
        vc.isFromAddressOrder = true
        vc.id = self.ID
        addChild(vc)
        vc.didMove(toParent: self)
        return vc
    }
    
}
