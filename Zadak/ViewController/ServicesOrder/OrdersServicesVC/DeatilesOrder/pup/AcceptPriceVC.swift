//
//  EditPupVC.swift
//  ZaadSeller
//
//  Created by osamaaassi on 1/31/21.
//  Copyright © 2021  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class AcceptPriceVC: SuperViewController {
    
    @IBOutlet weak var tfPayable: UITextField!
    @IBOutlet weak var tfDiscount: UITextField!
    @IBOutlet weak var tfRegularPrice: UITextField!
    @IBOutlet weak var tfCoupon: UITextField!
    
    @IBOutlet weak var viewCoupon: UIView!
    @IBOutlet weak var viewPrice: UIView!
    
    var isFromeCouopn:Bool = false
    var isFromePrice:Bool = false
   
    
    var itemsOrderModel: ItemsOrderModel?
    var dataServiceOrders: DataServiceOrders?
    var idOrder:Int!
    var coupon:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if (isFromePrice){
            viewPrice.isHidden = false
            viewCoupon.isHidden = true
        
            tfRegularPrice.text = dataServiceOrders?.subtotal?.description ?? "0"
            tfDiscount.text = dataServiceOrders?.couponValue?.description ?? "0"
            tfPayable.text = dataServiceOrders?.total?.description ?? "0"
        }
        if(isFromeCouopn){
            viewPrice.isHidden = true
            viewCoupon.isHidden = false
            tfCoupon.text = coupon ?? ""
        }
       
        
    }
    
    
    @IBAction func didValedCoupon(_ sender:UIButton){
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        
        var parameters: [String: Any] = [:]
        parameters["order_id"] = idOrder.description
        parameters["item_id"] = itemsOrderModel?.id ?? 0
        parameters["coupon"] = coupon ?? 0

        
        _ = WebRequests.setup(controller: self).prepare(query: "service-orders/apply-coupon", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200 {
                    self.showAlert(title: "Error".localized, message:Status.message!)
                    return
                }
                else if Status.status == 200{
                    self.dismiss(animated: false) {
                        NotificationCenter.default.post(name: Notification.Name("UpdateItemService"), object: nil)
                    }
                    self.dismiss(animated: false, completion: nil)
                }
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
        
        
    }
    @IBAction func didSeave(_ sender:UIButton){
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        
        
        var parameters: [String: Any] = [:]
        parameters["order_id"] = idOrder.description
        parameters["item_id"] = itemsOrderModel?.id ?? 0
        
        
        _ = WebRequests.setup(controller: self).prepare(query: "service-orders/accept-price-offer", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200 {
                    self.showAlert(title: "Error".localized, message:Status.message!)
                    return
                }
                else if Status.status == 200{
                    self.dismiss(animated: false) {
                        NotificationCenter.default.post(name: Notification.Name("UpdateItemService"), object: nil)
                    }
                    self.dismiss(animated: false, completion: nil)
                }
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
    }
    
    @IBAction func Cancel(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }  
}
