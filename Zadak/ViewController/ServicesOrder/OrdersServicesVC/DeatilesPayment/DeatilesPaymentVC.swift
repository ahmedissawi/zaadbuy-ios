//
//  DeatilesPaymentVC.swift
//  Zadak
//
//  Created by osamaaassi on 18/03/2021.
//  Copyright © 2021  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class DeatilesPaymentVC: SuperViewController {
    
    @IBOutlet weak var tfname: UITextField!
    @IBOutlet weak var tfbranch: UITextField!
    @IBOutlet weak var tfaccountNo: UITextField!
    @IBOutlet weak var tfaccountNoSeller: UITextField!
    @IBOutlet weak var tftransactionNo: UITextField!
    @IBOutlet weak var tfamount: UITextField!
    @IBOutlet weak var tfuserNotes: UITextView!
    @IBOutlet weak var imgDisputes: UIImageView!
    var imagePicker: ImagePicker!
    var selectedimage = false
    
    var itemsOrder: ResourcesServiseOrder?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imagePicker = ImagePicker(presentationController: self, delegate: self)
        
//        tfname.text = itemsOrder?.bankAccount?.name ?? ""
//        tfbranch.text = itemsOrder?.bankAccount?.branch ?? ""
        //tfaccountNoSeller.text = itemsOrder?.bankAccount?.accountNo ?? ""
    }
    override func viewWillAppear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWithdismiss(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 0.4156862745, blue: 0.1647058824, alpha: 1)
        
    }
 
    
    
    @IBAction func didbBankPay(_ sender:UIButton){
        
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        
        guard let name = self.tfname.text, !name.isEmpty else{
            self.showAlert(title: "error".localized, message: "Name cannot be empty".localized)
            return
        }
       
        guard let branch = self.tfbranch.text, !branch.isEmpty else{
            self.showAlert(title: "error".localized, message: "Branch name cannot be empty".localized)
            return
        }
//        guard let transactionNo = self.tftransactionNo.text, !transactionNo.isEmpty else{
//            self.showAlert(title: "error".localized, message: "لا يمكن أن يكون رقم القيد فارغًا".localized)
//            return
//        }
        guard let accountNo = self.tfaccountNo.text, !accountNo.isEmpty else{
            self.showAlert(title: "error".localized, message: "Account number cannot be empty".localized)
            return
        }
        
       
        guard let amount = self.tfamount.text, !amount.isEmpty else{
            self.showAlert(title: "error".localized, message: "The amount cannot be empty.".localized)
            return
        }
       
      
      
        
        
        if self.selectedimage {
            self.showIndicator()
            let imageData = (imgDisputes.image) ?? UIImage()
            
            
              var parameters: [String: String] = [:]
              parameters["order_id"] = itemsOrder?.id?.description ?? "0"
              parameters["name"] = name
              parameters["branch"] = branch
              parameters["account_no"] = accountNo
              parameters["transaction_no"] = "0"
              parameters["amount"] = amount
              parameters["user_notes"] = tfuserNotes.text ?? ""
            
            WebRequests.sendPostMultipartRequestWithImgParam(url: TAConstant.APIBaseURL + "service-orders/bank-pay", parameters:parameters ,img: imageData, withName: "document", completion: { (response, error)
                in
            self.hideIndicator()
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)

                    if Status.status != 200 {
                        self.showAlert(title: "Error".localized, message:Status.message!)
                        return
                    }
                    else if Status.status == 200{
                        self.dismiss(animated: false) {
                            self.showAlert(title: "Done successfully".localized, message:Status.message!)
                            NotificationCenter.default.post(name: Notification.Name("UpdateServiceOrder"), object: nil)
                        }
                        self.dismiss(animated: false, completion: nil)
                    }
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
            })
            
       
            
        }else{
            self.showAlert(title: "error".localized, message: "select image".localized)
            return
        }
        
    }
    
    
}


extension DeatilesPaymentVC: ImagePickerDelegate {
    
    @IBAction func showImagePicker(_ sender: UIButton) {
        self.imagePicker.present(from: sender)
    }
    func didSelect(image: UIImage?) {
        self.imgDisputes.image = image
        self.selectedimage = true
    }
}
