//
//  PaymentOrderServiceVC.swift
//  Zadak
//
//  Created by osamaaassi on 17/03/2021.
//  Copyright © 2021  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class PaymentOrderServiceVC: SuperViewController {
    
    var itemsOrder: ResourcesServiseOrder?
    var listMethod = [PayMethodsStruct]()
    var selectPaymentID:Int?
    var selectPayment:Int?
    
    var dataClass: DataClass?
    var paymentRequest: PaymentRequest?
    var textTotal : String?
    
    @IBOutlet weak var tableView:UITableView?
    @IBOutlet weak var totole :UILabel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // getPaymentMothed()
        tableView?.registerCell(id: "CellPayment")
        
        totole?.text = "اجمالي الطلبية : \(textTotal ?? "0") ر.غٌ" 
        
        
        //   paymentMethod += itemsOrder?.paymentMethods! ?? [PaymentMethods]()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //    func getPaymentMothed() {
    //        _ = WebRequests.setup(controller: nil).prepare(query: "seller/\(itemsOrder?.seller?.id ?? 0)/payment-methods", method: HTTPMethod.get).start(){ (response, error) in
    //            do {
    //                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
    //                if Status.status != 200{
    //                    self.showAlert(title: "error".localized, message:Status.message!)
    //                    return
    //                }
    //
    //            }catch let jsonErr {
    //                print("Error serializing  respone json", jsonErr)
    //            }
    //            do {
    //                let Status =  try JSONDecoder().decode(PayMethodsClass.self, from: response.data!)
    //                self.listMethod = Status.data!
    //                self.tableView?.reloadData()
    //            } catch let jsonErr {
    //                print("Error serializing  respone json", jsonErr)
    //            }
    //        }
    //    }
    
    
    
    @IBAction func didPayment(_ sender:UIButton){
        
        guard selectPayment != nil else {
            self.showAlert(title: "".localized, message:"Payment method must be selected".localized)
            return }
        
        if selectPayment == 3 {
            weak var pvc = self.presentingViewController
            self.dismiss(animated: true) {
                let navVC: CustomNavigationBar = AppDelegate.sb_main.instanceVC()
                let vc:DeatilesPaymentVC = DeatilesPaymentVC.loadFromNib()
                vc.itemsOrder = self.itemsOrder
                navVC.viewControllers = [vc]
                navVC.modalPresentationStyle = .fullScreen
                pvc!.present(navVC, animated: false, completion: nil)
            }
        }
        else{
            guard Helper.isConnectedToNetwork() else {
                self.showAlert(title: "".localized, message: "There is no internet connection".localized)
                return }
            
            
            var parameters: [String: Any] = [:]
            parameters["order_id"] = itemsOrder?.id ?? 0
            
            
            _ = WebRequests.setup(controller: self).prepare(query: "service-orders/pay", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200 {
                        self.showAlert(title: "Error".localized, message:Status.message!)
                        return
                    }
                    else if Status.status == 200{
                        self.dismiss(animated: false) {
                            //self.showAlert(title: "Success".localized, message:Status.message!)
                        }
                        self.dismiss(animated: false, completion: nil)
                    }
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
                do {
                    let Status =  try JSONDecoder().decode(ServiceOrdersPay.self, from: response.data!)
                    
                    self.dataClass = Status.data
                    self.paymentRequest =  self.dataClass?.paymentRequest
                    if self.paymentRequest!.success && !self.paymentRequest!.returnurl.isEmpty{
                        if let url = URL(string: self.paymentRequest!.returnurl.description ) {
                            self.tabBarController?.selectedIndex = 2
                             if UIApplication.shared.canOpenURL(url) {
                                if #available(iOS 10.0, *) {
                                    UIApplication.shared.open(url, options: [:]) { (sucess) in
                                        UserDefaults.standard.set(sucess, forKey: "OpenUrlPay")
                                    }
                                } else {
                                    UIApplication.shared.openURL(url)
                                }
                            }
                      
                            
                        }
                        
                        
                    }
                    
                } catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
            }
        }
        
        
        
    }
    
    @IBAction func didbDetalisSellerPay(_ sender:UIButton){
        
        weak var pvc = self.presentingViewController
        let navVC: CustomNavigationBar = AppDelegate.sb_main.instanceVC()
        let vc:MainServicesDeatiles = AppDelegate.sb_main.instanceVC()
        vc.isDetalisSellar = true
        vc.ID = self.itemsOrder?.id
        navVC.viewControllers = [vc]
        navVC.modalPresentationStyle = .fullScreen
        pvc!.present(navVC, animated: false, completion: nil)
        
        //  self.dismiss(animated: true) {  }
        
    }
    
    
    @IBAction func Cancel(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    } 
}

extension  PaymentOrderServiceVC: UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return listMethod.count
        return itemsOrder!.paymentMethods!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellPayment", for: indexPath)  as! CellPayment
        
        //cell.select?.tag = indexPath.row
        
        if self.itemsOrder!.paymentMethods![indexPath.row].id == selectPayment{
            //  cell.select?.isSelected = true
            cell.imglocation.image = UIImage(named: "addresscheck")
            
        }
        else{
            //cell.select?.isSelected = false
            cell.imglocation.image = UIImage(named: "addressUnCheck")
            
        }
        
        //  cell.lblname!.text = listMethod[indexPath.row].name ?? ""
        cell.lblname!.text = self.itemsOrder!.paymentMethods![indexPath.row].name ?? ""
        cell.selectionStyle = .none
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectPayment = self.itemsOrder!.paymentMethods![indexPath.row].id
        tableView.reloadData()
    }
    
    
    
}
