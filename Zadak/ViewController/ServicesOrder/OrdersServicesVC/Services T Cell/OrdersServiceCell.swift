//
//  ServicesCell.swift
//  ZaadSeller
//
//  Created by osamaaassi on 1/16/21.
//  Copyright © 2021  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class OrdersServiceCell: UITableViewCell {
    
    @IBOutlet weak var lblCodeOrder: UILabel!
    @IBOutlet weak var lblDateOrder: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblPaymentStatus: UILabel!
    @IBOutlet weak var lblChangeStatus: UILabel!
    
    @IBOutlet weak var btDeatiles: UIButton!
    @IBOutlet weak var btChangeStatus: UIButton!
    @IBOutlet weak var btPayment: UIButton!
    @IBOutlet weak var btChangeTheOrder: UIButton!
    @IBOutlet weak var btMessage: UIButton!
    
    @IBOutlet weak var viewDeatiles: UIView!
    @IBOutlet weak var viewPayment: UIView!
    @IBOutlet weak var viewMessage: UIView!
    @IBOutlet weak var viewCopone: UIView!
    @IBOutlet weak var viewDispute: UIView!
    @IBOutlet weak var viewAddCopone: UIView!
   
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       
    }
    
    var deatilesTapped: (() -> ())?
    var messageTapped : (() -> ())?
    var PaymentTapped: (() -> ())?
    var cuponeTapped: (() -> ())?
    var disputeTapped:(() -> ())?
    var addCuponeTapped: (() -> ())?
//    payment_status
    
    
    let isStatus = UserDefaults.standard.integer(forKey: "status")
    var order:ResourcesServiseOrder?{
           didSet{
            self.lblCodeOrder.text = order?.orderNo ?? ""
            self.lblDateOrder.text = order?.createdAt ?? ""
            self.lblName.text = order?.orderNo ?? ""
            self.lblStatus.text = order?.status?.name ?? ""
            self.lblPaymentStatus.text = order?.paymentStatus?.name ?? ""
            }
           
       }
    
    
    @IBAction func Deatiles(_ sender: UIButton) {
        deatilesTapped?()
    }
    
   
    @IBAction func Message(_ sender: UIButton) {
        messageTapped?()
    }
    
    @IBAction func Payment(_ sender: UIButton) {
        PaymentTapped?()
    }
   
    @IBAction func Coupon(_ sender: UIButton) {
        cuponeTapped?()
    }
    
    @IBAction func Dispute(_ sender: UIButton) {
        disputeTapped?()
    }
    @IBAction func AddCoupon(_ sender: UIButton) {
        addCuponeTapped?()
    }
    
    
}
