//
//  OrdersServicesVC.swift
//  Zadak
//
//  Created by osamaaassi on 15/03/2021.
//  Copyright © 2021  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class OrdersServicesVC: SuperViewController {
    
    
    @IBOutlet weak var tableView: UITableView!
    
    var servicesOrders = [ResourcesServiseOrder]()
   
    var servicesData:DatasMainServise?
    
    var currentpage = 1
    var Link = "status=0"
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.tableFooterView = UIView()
        tableView.registerCell(id: "OrdersServiceCell")
        getOrdersServies()
        
        
        self.tableView.es.addPullToRefresh {
            self.currentpage = 1
            self.servicesOrders.removeAll()
            self.getOrdersServies()
            self.hideIndicator()
        }
        
        self.tableView.es.addInfiniteScrolling {
            self.currentpage += 1
            self.getOrdersServies() // next page
            self.tableView.estimatedRowHeight = 0
            self.hideIndicator()
        }
        
        NotificationCenter.default.addObserver(self, selector:#selector(UpdateServiceOrder(notification:)), name: Notification.Name("UpdateServiceOrder"), object: nil)
        
        
    }
    @objc func UpdateServiceOrder(notification: NSNotification)  {
        servicesOrders.removeAll()
        getOrdersServies()
    }
    
    @IBAction func didRefersh(){
        servicesOrders.removeAll()
        getOrdersServies()
    }
    
    
    
}
extension OrdersServicesVC{
    
    func getOrdersServies(){
        guard Helper.isConnectedToNetwork() else {
            self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.tableView, refershSelector: #selector(self.didRefersh))
            self.emptyView?.imgEmpty.image = UIImage(named: "refreshment")
            self.emptyView?.firstLabel.text = "There is no internet connection".localized
            return }
        self.servicesOrders.removeAll()
        self.showIndicator()
        
        
        _ = WebRequests.setup(controller: self).prepare(query: "service-orders?perPage=10&page=\(self.currentpage)&" + Link, method: HTTPMethod.get).start(){ (response, error) in
            
            self.hideIndicator()
            
            self.tableView.es.stopLoadingMore()
            self.tableView.es.stopPullToRefresh()
            
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            do {
                let Status =  try JSONDecoder().decode(MainServisOrders.self, from: response.data!)
                self.servicesData = Status.data
                self.servicesOrders += self.servicesData!.resources!
              
                
                
                
                if self.servicesData?.nextPageUrl == nil {
                    self.tableView.es.stopLoadingMore()
                    self.tableView.es.noticeNoMoreData()
                }
                if self.servicesOrders.count == 0{
                    
                    self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.tableView, refershSelector: #selector(self.didRefersh))
                    self.emptyView?.firstLabel.text = "No Order Show".localized
                    self.emptyView?.imgEmpty.isHidden = true
                    
                    if self.Link == "status=0"{
                        self.emptyView?.firstLabel.text = "There are no order awaiting acceptance".localized
                    }
                    if self.Link == "status=1"{
                        self.emptyView?.firstLabel.text = "No paid orders".localized
                    }
                    if self.Link == "status=2"{
                        self.emptyView?.firstLabel.text = "There are no priced orders".localized
                    }
                    if self.Link == "status=4"{
                        self.emptyView?.firstLabel.text = "No Order in progress".localized
                    }
                    if self.Link == "status=5"{
                        self.emptyView?.firstLabel.text = "No Order fulfilled".localized
                    }
                    
                    if self.Link == "payment-status=1"{
                        self.emptyView?.firstLabel.text = "No waiting orders".localized
                    }
                    if self.Link == "payment-status=2"{
                        self.emptyView?.firstLabel.text = "No paid orders".localized
                    }
                    if self.Link == "payment-status=11"{
                        self.emptyView?.firstLabel.text = "No Rejected".localized
                    }
                    
                    if self.Link == "status=11"{
                        self.emptyView?.firstLabel.text = "There are no rejected order".localized
                    }
                    if self.Link == "status=12"{
                        self.emptyView?.firstLabel.text = "There are no rejected order".localized
                    }
                    if self.Link == "status=13"{
                        self.emptyView?.firstLabel.text = "No canceled orders".localized
                    }
                    
                    self.servicesOrders.removeAll()
                    self.tableView.reloadData()
                    
                }else{
                    self.tableView.tableFooterView = nil
                }
                self.tableView.reloadData()
                
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
    }
    
}

extension OrdersServicesVC:UITableViewDataSource,UITableViewDelegate{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return servicesOrders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrdersServiceCell", for: indexPath) as! OrdersServiceCell
        let obj = servicesOrders[indexPath.row]
        cell.order = obj
    
        if(Link == "status=13" || Link == "status=12" || Link == "status=11" || Link == "payment_status=2"){
            cell.viewPayment.isHidden = true
        }
        
        if(Link == "status=2" ){
            cell.viewCopone.isHidden = false
         
        }
        if(Link == "status=3" ){
            cell.viewPayment.isHidden = false
            cell.viewAddCopone.isHidden = false
        }
       
        if(Link == "status=4" || Link == "status=5" || Link == "status=13"  ){
            cell.viewDispute.isHidden = false
        }
        
        
        
        cell.deatilesTapped = { [weak self] in
            let vc:MainServicesDeatiles = AppDelegate.sb_main.instanceVC()
            vc.ID = self!.servicesOrders[indexPath.row].id
            
            vc.hidesBottomBarWhenPushed = true
            vc.modalPresentationStyle = .fullScreen
            self!.navigationController?.pushViewController(vc, animated: true)
        }
        
        cell.PaymentTapped = { [weak self] in
            let payment:PaymentOrderServiceVC = PaymentOrderServiceVC.loadFromNib()
            payment.itemsOrder = self!.servicesOrders[indexPath.row]
            let total = self!.servicesOrders[indexPath.row].total ?? 0
            payment.textTotal =  total.description
//            payment.listMethod
            payment.modalPresentationStyle = .overCurrentContext
            self!.present(payment, animated: false, completion: nil)
        }
        
        cell.cuponeTapped = { [weak self] in
            let vc:MainServicesDeatiles = AppDelegate.sb_main.instanceVC()
            vc.ID = self!.servicesOrders[indexPath.row].id
            vc.isCoupn = true
            UserDefaults.standard.set(true, forKey: "isCoupn")
            UserDefaults.standard.set(false, forKey: "isAddCoupn")
            vc.hidesBottomBarWhenPushed = true
            vc.modalPresentationStyle = .fullScreen
            self!.navigationController?.pushViewController(vc, animated: true)
        
        }
        cell.addCuponeTapped = { [weak self] in
            let vc:MainServicesDeatiles = AppDelegate.sb_main.instanceVC()
            vc.ID = self!.servicesOrders[indexPath.row].id
            vc.isAddCoupn = true
            UserDefaults.standard.set(true, forKey: "isAddCoupn")
            UserDefaults.standard.set(false, forKey: "isCoupn")
            vc.hidesBottomBarWhenPushed = true
            vc.modalPresentationStyle = .fullScreen
            self!.navigationController?.pushViewController(vc, animated: true)
        
        }
        
        cell.disputeTapped = { [weak self] in
            let vc:MakeDisputesVC = MakeDisputesVC.loadFromNib()
            vc.orderID = self!.servicesOrders[indexPath.row].id
            vc.isFromService = true
            vc.hidesBottomBarWhenPushed = true
            vc.modalPresentationStyle = .fullScreen
            self!.navigationController?.pushViewController(vc, animated: true)
            
        
        }
        
        
        cell.messageTapped = { [weak self] in
            
            let vcChat:CommentChatVC = CommentChatVC.loadFromNib()
            vcChat.id = self!.servicesOrders[indexPath.row].id
            vcChat.hidesBottomBarWhenPushed = true
            self!.navigationController?.pushViewController(vcChat, animated: true)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  222
    }
    
    
}
