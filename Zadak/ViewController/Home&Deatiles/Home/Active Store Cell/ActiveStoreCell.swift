//
//  ActiveStoreCell.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 8/17/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class ActiveStoreCell: UICollectionViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    weak var delegate:HomeVC?

    
    var listItem:[Items]? {
           didSet {
               collectionView.reloadData()
           }
           
           
       }
    
    func getCellSize(_ targetSize: CGSize) -> CGSize {
          return CGSize(width: (UIScreen.main.bounds.width), height: 145)
      }
      
      // Only this is called on iOS 12 and lower
      override func systemLayoutSizeFitting(_ targetSize: CGSize) -> CGSize {
          return self.getCellSize(targetSize)
      }
      
      // Only this is called on iOS 13 beta
      override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
          return self.getCellSize(targetSize)
      }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        collectionView.registerCell(id: "StoresCell")

    }

}

extension ActiveStoreCell:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listItem!.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StoresCell", for: indexPath) as! StoresCell
        let obj = listItem![indexPath.row]
        cell.lblname.text = obj.name ?? ""
        cell.imgStore.sd_custom(url: obj.logo ?? "")

        return cell
    }
   
    
   
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 110, height: 140)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let obj = listItem![indexPath.row]
        let vc:StoreInfoVC = AppDelegate.sb_main.instanceVC()
        vc.id = obj.id!
        vc.hidesBottomBarWhenPushed = true
        vc.modalPresentationStyle = .fullScreen
        delegate?.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}
