//
//  HomeVC.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 2/19/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import FSPagerView
import ActionSheetPicker_3_0
import FirebaseCrashlytics


class HomeVC: SuperViewController,UITextFieldDelegate{
    
    @IBOutlet weak var tf_search: UITextField!
    @IBOutlet weak var Colelction: UICollectionView!
    @IBOutlet weak var pagerView: FSPagerView!{
        didSet{
            self.pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
            self.pagerView.automaticSlidingInterval = 0.3
            self.pagerView.interitemSpacing = 10
            self.pagerView.transformer = FSPagerViewTransformer(type: .coverFlow)
        }
    }
    var listItem = [Sections]()
    var searchValue:String?
    var categories = [categoriesData]()
    var categoriesproducats = [categoriesitems]()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tf_search.delegate = self
        Colelction.registerCell(id: "HomeSlideCell")
        Colelction.registerCell(id: "NewAllCell")
        Colelction.registerCell(id: "ShoppingAllCell")
        Colelction.registerCell(id: "ActiveStoreCell")
        Colelction.registerCell(id: "SellerproduactCell")
        Colelction.registerCell(id: "LatestoffersAllCell")
        let Producat = UINib(nibName: "HeaderHomeCell", bundle: nil)
        self.Colelction.register(Producat, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "HeaderHomeCell")
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = "FF6A2A".color
        tf_search.returnKeyType = UIReturnKeyType.search
        NotificationCenter.default.addObserver(self, selector: #selector(UpdateLike(notification:)), name: Notification.Name("UpdateLike"), object: nil)
        getHoem()
        getCategories()
    }
    func getCategories(){
        _ = WebRequests.setup(controller: self).prepare(query: "categories", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            
            do {
                let Status =  try JSONDecoder().decode(categoriesInfo.self, from: response.data!)
                self.categories = Status.data!
                self.categoriesproducats = self.categories[0].childrens!
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            
        }
        
    }
 
    
    func getHoem(){
        _ = WebRequests.setup(controller: self).prepare(query: "site/home", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            do {
                let Status =  try JSONDecoder().decode(homeResbon.self, from: response.data!)
                self.listItem = Status.data?.sections as! [Sections]
                self.Colelction.reloadData()
                self.Colelction.contentOffset = .zero
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
    }
    
    
    private func fetch_search_results_data(_title: String){
        
        
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.searchValue = tf_search.text
        if searchValue != ""{
            let vc:SearchVC = AppDelegate.sb_main.instanceVC()
            vc.hidesBottomBarWhenPushed = true
            vc.searchValue  = self.searchValue
            vc.modalPresentationStyle = .fullScreen
            navigationController?.pushViewController(vc, animated: true)
            self.tf_search.text = ""
            
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.searchValue = tf_search.text
        if searchValue != ""{
            let vc:SearchVC = AppDelegate.sb_main.instanceVC()
            vc.hidesBottomBarWhenPushed = true
            vc.searchValue  = self.searchValue
            vc.modalPresentationStyle = .fullScreen
            navigationController?.pushViewController(vc, animated: true)
            self.tf_search.text = ""
            
        }
        return true
    }
    
    
    @objc func UpdateLike(notification: NSNotification)  {
        getHoem()
    }
    
    @IBAction func bt_filter(_ sender: UIButton) {

        ActionSheetStringPicker.show(withTitle: "Categories".localized, rows: self.categoriesproducats.map { $0.title as Any }
            , initialSelection: 0, doneBlock: {
                picker, value, index in
                if let Value = index {
                    let vc:SearchVC = AppDelegate.sb_main.instanceVC()
                    vc.hidesBottomBarWhenPushed = true
                    vc.catogryValue  = self.categoriesproducats[value].id?.description
                    vc.modalPresentationStyle = .fullScreen
                    self.navigationController?.pushViewController(vc, animated: true)

                }
                
                return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)

    }
    
    
}
extension HomeVC:FSPagerViewDelegate,FSPagerViewDataSource{
    
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        if self.listItem.count == 0 {
            return 0
        }
        let slider = self.listItem[0].items
        return slider!.count
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        let slider = self.listItem[0].items
        
        cell.imageView?.sd_custom(url: slider![index].mobileImage ?? "")
        cell.imageView?.contentMode = .scaleAspectFill
        cell.imageView?.clipsToBounds = true
        return cell
    }
    
}
extension HomeVC:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        let arra  = self.listItem.filter {(  $0.type == "stores" || $0.type == "category-product" || $0.type == "category-products" || $0.type == "promotions" || $0.type == "carousel" ) }
        //        let arra  = self.listItem
        return arra.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let sectionData = self.listItem[section]
        
        if sectionData.type == "carousel"{
            return 1
        }
        if sectionData.type == "category-products" || sectionData.type == "category-product"{
            return 1
        }
        if sectionData.type == "promotions"{
            return 1
            
        }
        if sectionData.type == "stores"{
            return 1
            
        }
        
        //        if self.listItem.count == 0 {
        return 0
        //        }
        //
        //        if section ==  0{
        //            return section.items?.count
        //        }
        //        if section == 1{
        //            return 1
        //        }
        //        if section == 2{
        //            return 1
        //        }
        //        if section == 3{
        //            return 1
        //        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let sectionData = self.listItem[indexPath.section]
   
        if sectionData.type == "carousel"{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeSlideCell", for: indexPath) as! HomeSlideCell
            cell.items = sectionData.items
            return cell
            
        }
        if sectionData.type == "category-products" || sectionData.type == "category-product"{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewAllCell", for: indexPath) as! NewAllCell
            cell.delegate = self
            cell.listItem = sectionData.items!
            
            return cell
            
        }
        //    if sectionData.slug == "category-products"{
        //
        
        //        if indexPath.section == 1{
        //              let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ShoppingAllCell", for: indexPath) as! ShoppingAllCell
        //            return cell
        //
        //        }
        if sectionData.type == "promotions"{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SellerproduactCell", for: indexPath) as! SellerproduactCell
            cell.imgcell.sd_custom(url: sectionData.item?.image ?? "")
            
            return cell
            
        }
        
        if sectionData.type == "stores"{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ActiveStoreCell", for: indexPath) as! ActiveStoreCell
            cell.delegate = self
            cell.listItem = sectionData.items!
            
            return cell
            
        }
        
      
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewAllCell", for: indexPath) as! NewAllCell
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let sectionData = self.listItem[indexPath.section]
        
        if  sectionData.type == "category-products" || sectionData.type == "category-product"{
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HeaderHomeCell", for: indexPath) as! HeaderHomeCell
            header.lb_name.text = sectionData.title
            header.lb_desc.text = "Show more".localized
            header.lb_name.isHidden  = false
            header.Viewmore.isHidden  = false
            header.btmore.tag = indexPath.section
            header.btmore.addTarget(self, action: #selector(self.MoreBtn), for: UIControl.Event.touchUpInside)
            
            return header
        }else if sectionData.type ==  "stores"{
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HeaderHomeCell", for: indexPath) as! HeaderHomeCell
            header.lb_name.text = sectionData.title
            header.lb_desc.text = "Show more".localized
            header.lb_name.isHidden  = false
            header.Viewmore.isHidden  = true
            header.btmore.tag = indexPath.section
            header.btmore.addTarget(self, action: #selector(self.MoreBtn), for: UIControl.Event.touchUpInside)
              return header

        }

        return UICollectionReusableView()
        
    }
    
    @objc func MoreBtn(_ sender:UIButton ){
        let sectionData = self.listItem[sender.tag]
        let vc:SearchVC = AppDelegate.sb_main.instanceVC()
        vc.hidesBottomBarWhenPushed = true
        vc.catogryValue  = sectionData.items?[0].category?.id?.description ?? "0"
        vc.modalPresentationStyle = .fullScreen
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        let sectionData = self.listItem[section]
        
        if sectionData.type == "carousel"{
            return CGSize(width: UIScreen.main.bounds.width, height: 0)
            
        }
        if sectionData.type == "category-products" || sectionData.type == "category-product"{
            
            return CGSize(width: UIScreen.main.bounds.width, height: 55)
        }
        if sectionData.type == "promotions"{
            
            return CGSize(width: UIScreen.main.bounds.width, height: 0)
        }
        if sectionData.type == "stores"{
            
             return CGSize(width: UIScreen.main.bounds.width, height: 55)
        }
        return CGSize(width: UIScreen.main.bounds.width, height: 0)
        
    }
    
    
    
    
}


