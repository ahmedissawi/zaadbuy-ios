//
//  SearchVC.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 6/16/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import ESPullToRefresh

class SearchVC: SuperViewController {
    
    
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var searchValue:String?
    var catogryValue:String?
    
    var dataitems : itemData?
    var currentpage = 1
    var items = [Items]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let navgtion = self.navigationController as! CustomNavigationBar
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(descriptor: UIFontDescriptor(name: "FFShamelFamilySemiRoundBook-S", size: 17), size: 17),NSAttributedString.Key.foregroundColor: UIColor.white]
        self.title = "search".localized
        navigationController?.navigationBar.barTintColor = "FF6A2A".color
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        getFilter()
        collectionView.registerCell(id: "StoreProducatCell")
        self.collectionView.es.addPullToRefresh {
            self.currentpage = 1
            self.items.removeAll()
            self.getFilter()
            self.hideIndicator()
        }
        
        self.collectionView.es.addInfiniteScrolling {
            self.currentpage += 1
            self.getFilter() // next page
            self.hideIndicator()
        }
        
    }
    
    
    
    
    func getFilter(){
        
        var parameters: [String: Any] = [:]
        if searchValue != nil{
            parameters["search"] = searchValue
        }
        if catogryValue != nil{
            parameters["category_id"] = catogryValue
        }
        
        
        _ = WebRequests.setup(controller: self).prepare(query: "items?page=\(currentpage)", method: HTTPMethod.get,parameters:parameters).start(){ (response, error) in
            
            self.collectionView.es.stopLoadingMore()
            self.collectionView.es.stopPullToRefresh()
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            do {
                let Status =  try JSONDecoder().decode(ItemsInfo.self, from: response.data!)
                self.dataitems = Status.data
                self.items += self.dataitems!.items!
                
                if self.dataitems?.nextPageUrl == nil  {
                    self.collectionView.es.stopLoadingMore()
                    self.collectionView.es.noticeNoMoreData()
                }
                if self.items.count == 0{
                    self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.collectionView, refershSelector: #selector(self.didRefersh))
                    self.emptyView?.firstLabel.text = "No results to display".localized
                    self.items.removeAll()
                    self.collectionView.reloadData()
                }
                self.collectionView.reloadData()
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
    }
    
    @IBAction func didRefersh(){
          items.removeAll()
          getFilter()
      }
    
    
}



extension SearchVC:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StoreProducatCell", for: indexPath) as! StoreProducatCell
        let obj = items[indexPath.row]
        cell.lb_name.text = obj.title ?? ""
        
        cell.imgNew.sd_custom(url: obj.image ?? "")
        cell.bt_like.isHidden = true
        cell.bt_shopping.isHidden = true
        if obj.salePrice != 0{
            cell.lb_price.text = "\(obj.regularPrice ?? 0.0)" + " "  + "O.R".localized
            cell.lblDesc.text = "\(obj.salePrice ?? 0)" + " " + "O.R".localized
            cell.viewDiscount.isHidden = false
        }else{
            cell.lb_price.text = "\(obj.regularPrice ?? 0.0)" + " "  + "O.R".localized
            cell.viewDiscount.isHidden = true
        }

        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let obj = items[indexPath.row]
        let vc:DetailsVC = AppDelegate.sb_main.instanceVC()
        vc.producatID = obj.id
        vc.hidesBottomBarWhenPushed = true
        vc.modalPresentationStyle = .fullScreen
        navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let wideth = UIScreen.main.bounds.size.width
        return CGSize(width: wideth/2.1, height: 245)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
    
    
    
    
    
    
}

