//
//  ProductSpecificationCell.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 6/10/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class ProductSpecificationCell: UITableViewCell {

    @IBOutlet weak var lblname: UILabel!
    @IBOutlet weak var lblSize: UILabel!
    @IBOutlet weak var lblColor: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblCode: UILabel!
    @IBOutlet weak var lblContry: UILabel!
    @IBOutlet weak var lblAmount: UILabel!


    
    override func awakeFromNib() {
        super.awakeFromNib()

    }

   
    
}
