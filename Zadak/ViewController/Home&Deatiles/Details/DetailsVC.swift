//
//  DetailsVC.swift
//  Zadak
//
//  Created by Ahmed ios on 5/30/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import FSPagerView

class DetailsVC: SuperViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var lblamount: UILabel!
    @IBOutlet weak var lblacolor: UILabel!
    @IBOutlet weak var lblsize: UILabel!
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lbldesc: UILabel!
    @IBOutlet weak var lblprice: UILabel!
    @IBOutlet weak var lblsaleprice: UILabel!
    @IBOutlet weak var Viewprice: UIView!
    @IBOutlet weak var Viewsaleprice: UIView!
    @IBOutlet weak var lineprice: UIView!
    @IBOutlet weak var imgseller: UIImageView!
    @IBOutlet weak var nameseller: UILabel!
    @IBOutlet weak var amountpieces: UILabel!
    @IBOutlet weak var amountrate: UILabel!
    @IBOutlet weak var amountSale: UILabel!
    @IBOutlet weak var btlike: UIButton!
    @IBOutlet weak var imgCart: UIImageView!
    @IBOutlet weak var btCart: UIButton!
    
    
    
    var deatilesproducat:DeatilesProducatData?
    var produacatImages = [Images]()
    var realtedItems = [Related]()
    var commentsItem = [Details]()
    var rate:Double?
    var producatID:Int?
    
    @IBOutlet weak var pagerView: FSPagerView!{
        didSet{
            self.pagerView.isScrollEnabled = true
            self.pagerView.register(UINib(nibName: "SliderProducatCell", bundle: Bundle.main), forCellWithReuseIdentifier: "SliderProducatCell")
        }
    }
    
    @IBOutlet weak var pageControlle: FSPageControl!{
        didSet{
            self.pageControlle.numberOfPages = self.produacatImages.count
            self.pageControlle.contentHorizontalAlignment = .center
            pageControlle.setImage(UIImage(named: "slideroff"), for: .normal)
            pageControlle.setImage(UIImage(named: "sliderin"), for: .selected)
        }
        
    }
    
    var ExpandInt:Int?
    var ProducatTitle = ["Product details".localized,"Product specification".localized,"Advantages".localized,"Rate".localized]

    
    override func viewDidLoad() {
        super.viewDidLoad()
        getDeatiles()
        tableView.registerCell(id: "DeatileProducatCell")
        tableView.registerCell(id: "FeaturesProducatCell")
        tableView.registerCell(id: "ProductSpecificationCell")
        tableView.registerCell(id: "CommentsCell")
        collectionView.registerCell(id: "NewCell")
        let nibHeader = UINib(nibName: "HeaderDeatilesCell", bundle: nil)
        tableView.register(nibHeader, forCellReuseIdentifier: "HeaderDeatilesCell")
        let navgtion = self.navigationController as! CustomNavigationBar
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(descriptor: UIFontDescriptor(name: "FFShamelFamilySemiRoundBook-S", size: 17), size: 17),NSAttributedString.Key.foregroundColor: UIColor.white]
        self.title = "Details".localized
        navigationController?.navigationBar.barTintColor = "FF6A2A".color
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        
        NotificationCenter.default.addObserver(self, selector: #selector(UpdateLike(notification:)), name: Notification.Name("UpdateLike"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(UpdateCart(notification:)), name: Notification.Name("UpdateCart"), object: nil)
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.getDeatiles()
        
    }
    
    
    func getDeatiles(){
        _ = WebRequests.setup(controller: self).prepare(query: "items/\(producatID ?? 0)", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            do {
                let Status =  try JSONDecoder().decode(DeatilesProducatInfo.self, from: response.data!)
                self.deatilesproducat = Status.data!
                self.lbltitle.text = self.deatilesproducat?.title
                self.lbldesc.text = self.deatilesproducat?.shortDescription
                if self.deatilesproducat?.salePrice != 0{
                    self.Viewsaleprice.isHidden = false
                  self.lblprice.text = "\(self.deatilesproducat?.regularPrice ?? 0.0)" + " " + "O.R".localized
                 self.lblsaleprice.text = "\(self.deatilesproducat?.salePrice ?? 0)" + " " + "O.R".localized
                    self.lineprice.isHidden = false
                }else{
                    self.Viewsaleprice.isHidden = true
                    self.lblprice.text = "\(self.deatilesproducat?.regularPrice ?? 0.0)" + " " + "O.R".localized
                    self.lineprice.isHidden = true
                }
                self.nameseller.text = self.deatilesproducat?.seller?.store_name
                self.imgseller.sd_custom(url: self.deatilesproducat?.seller?.store_logo ?? "")
                self.produacatImages = self.deatilesproducat!.images!
                self.realtedItems = self.deatilesproducat!.related!
                self.commentsItem = (self.deatilesproducat?.rates!.details)!
                if self.deatilesproducat?.favoriteItem == false{
                    self.btlike.setImage(UIImage(named: "Heart"), for: .normal)
                }else{
                    self.btlike.setImage(UIImage(named: "like"), for: .normal)
                }
                if self.deatilesproducat?.cartItem == true{
                    self.imgCart.image = UIImage(named: "deletefavorite")
                    self.btCart.setTitle("Remove from cart".localized, for: .normal)
                }else{
                    self.imgCart.image = UIImage(named: "cartSelcted")
                    self.btCart.setTitle("Add to cart".localized, for: .normal)
                }
                
                let d = self.deatilesproducat?.rate
                UserDefaults.standard.set(d, forKey: "rate")
                self.tableView.reloadData()
                self.collectionView.reloadData()
                self.pagerView.reloadData()
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
    }
    
    
    @objc func UpdateLike(notification: NSNotification)  {
        getDeatiles()
    }
    @objc func UpdateCart(notification: NSNotification)  {
        getDeatiles()
    }
    
    @IBAction func bt_amountPlus(_ sender: Any) {
        
    }
    @IBAction func bt_amountMinus(_ sender: Any) {
        
    }
    @IBAction func bt_color(_ sender: Any) {
        
    }
    @IBAction func bt_size(_ sender: Any) {
        
    }
    
    @IBAction func bt_seller(_ sender: Any) {
        let vc:MessagesAllVC = AppDelegate.sb_main.instanceVC()
        vc.IDMessgae = self.deatilesproducat?.seller?.id
        vc.Storename = self.deatilesproducat?.seller?.name
        vc.hidesBottomBarWhenPushed = true
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func bt_store(_ sender: Any) {
        let vc:StoreInfoVC = AppDelegate.sb_main.instanceVC()
        vc.id = deatilesproducat?.seller?.store_id ?? 0
        vc.hidesBottomBarWhenPushed = true
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func bt_share(_ sender: Any) {
        
        let link = deatilesproducat?.link
        
        let activityViewController = UIActivityViewController(
            activityItems: [link!],
            applicationActivities: nil)
        
        if activityViewController.popoverPresentationController != nil {
            activityViewController.popoverPresentationController?.sourceView = (sender as! UIButton)
            
        }
        
        activityViewController.excludedActivityTypes = [
            UIActivity.ActivityType.print,
            UIActivity.ActivityType.assignToContact,
            UIActivity.ActivityType.saveToCameraRoll
        ]
        present(activityViewController, animated: true, completion: nil)
        
    }
    
    @IBAction func bt_decress(_ sender: Any) {
        let ammount = Int(self.lblamount.text!)
        self.lblamount.text =  "\(ammount! + 1)"
    }
    @IBAction func bt_increes(_ sender: Any) {
        let ammount = Int(self.lblamount.text!)
        if ammount == 1 {
            return
        }
        self.lblamount.text =  "\(ammount! - 1)"
        
    }
    @IBAction func bt_cartshopping(_ sender: Any) {
        if deatilesproducat?.cartItem == true{
            _ = WebRequests.setup(controller: self).prepare(query: "shopping-cart/items/\(deatilesproducat?.id ?? 0)", method: HTTPMethod.delete).start(){ (response, error) in
                
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200 {
                        
                        self.showAlert(title: "error".localized, message:Status.message!)
                        return
                    }else if Status.status  == 200{
                        NotificationCenter.default.post(name: Notification.Name("UpdateCart"), object: nil)
                        
                        self.showAlert(title: "Done successfully".localized, message:Status.message!)
                        
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
                
            }
        }else{
            let ammount = self.lblamount.text
            
            var parameters: [String: Any] = [:]
            
            parameters["item_id"] = producatID?.description
            parameters["quantity"] = ammount
            
            
            _ = WebRequests.setup(controller: self).prepare(query: "shopping-cart/items", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
                
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200 {
                        
                        self.showAlert(title: "error".localized, message:Status.message!)
                        return
                    }else{
                        NotificationCenter.default.post(name: Notification.Name("UpdateCart"), object: nil)
                        self.showAlert(title: "Done successfully".localized, message: "Product has been added to cart!".localized)
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
            }
        }
        
    }
    
    @IBAction func bt_like(_ sender: Any) {
        
        if self.deatilesproducat?.favoriteItem == false{
            var parameters: [String: Any] = [:]
            parameters["item_id"] = self.deatilesproducat?.id?.description
            
            _ = WebRequests.setup(controller: nil).prepare(query: "favorite/items", method: HTTPMethod.post,parameters: parameters).start(){ (response, error) in
                
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200 {
                        
                        self.showAlert(title: "error".localized, message:Status.message!)
                        return
                    }else if Status.status  == 200{
                        NotificationCenter.default.post(name: Notification.Name("UpdateLike"), object: nil)
                        
                        self.showAlert(title: "Done successfully".localized, message:"Product has been added to favourites".localized)                      }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
                
            }
        }else{
            let id = self.deatilesproducat?.id
            _ = WebRequests.setup(controller: nil).prepare(query: "favorite/items/\(id ?? 0)", method: HTTPMethod.delete).start(){ (response, error) in
                
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200 {
                        
                        self.showAlert(title: "error".localized, message:Status.message!)
                        return
                    }else if Status.status  == 200{
                        NotificationCenter.default.post(name: Notification.Name("UpdateLike"), object: nil)
                        self.showAlert(title: "Done successfully".localized, message:"Product has been removed from favourites".localized)
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
                
            }
        }
    }
    
    @IBAction func bt_buy(_ sender: Any) {
        
        
        let ammount = self.lblamount.text
        
        var parameters: [String: Any] = [:]
        
        parameters["item_id"] = producatID?.description
        parameters["quantity"] = ammount
        
        
        _ = WebRequests.setup(controller: self).prepare(query: "shopping-cart/items", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200 {
                    
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }else{
                    self.tabBarController?.selectedIndex = 3
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
        
    }
    @IBAction func bt_addTocart(_ sender: Any) {
        if deatilesproducat?.cartItem == true{
            _ = WebRequests.setup(controller: self).prepare(query: "shopping-cart/items/\(deatilesproducat?.id ?? 0)", method: HTTPMethod.delete).start(){ (response, error) in
                
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200 {
                        
                        self.showAlert(title: "error".localized, message:Status.message!)
                        return
                    }else if Status.status  == 200{
                        NotificationCenter.default.post(name: Notification.Name("UpdateCart"), object: nil)
                        
                        self.showAlert(title:"Done successfully" .localized, message:Status.message!)
                        
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
                
            }
        }else{
            let ammount = self.lblamount.text
            
            var parameters: [String: Any] = [:]
            
            parameters["item_id"] = producatID?.description
            parameters["quantity"] = ammount
            
            
            _ = WebRequests.setup(controller: self).prepare(query: "shopping-cart/items", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
                
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200 {
                        
                        self.showAlert(title: "error".localized, message:Status.message!)
                        return
                    }else{
                        self.showAlert(title: "Done successfully" .localized, message: "Product has been added to cart!".localized)
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
            }
        }
        
        
        
    }
}
extension DetailsVC:UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return ProducatTitle.count
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if  section == 0,ExpandInt == section {
            return 1
        }
        if section == 1,ExpandInt == section {
            return 1
        }
        if section == 2,ExpandInt == section {
            return 1
        }
        if section == 3,ExpandInt == section {
            return commentsItem.count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DeatileProducatCell", for: indexPath) as! DeatileProducatCell
            let obj = deatilesproducat?.descriptionValue
            cell.lblDecrbtion.text = obj?.htmlToString
            return cell
        }
        
        if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProductSpecificationCell", for: indexPath) as! ProductSpecificationCell
            return cell
        }
        
        if indexPath.section == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FeaturesProducatCell", for: indexPath) as! FeaturesProducatCell
            return cell
        }
        
        if indexPath.section == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CommentsCell", for: indexPath) as! CommentsCell
            let obj = commentsItem[indexPath.row]
            cell.imgUser.sd_custom(url: obj.user?.image ?? "")
            cell.lblname.text = obj.user?.name
            cell.lblnote.text = obj.note
            cell.lbldate.text  = obj.date
            cell.ViewRate.rating = Double(obj.rate ?? 0)
            return cell
        }
        return UITableViewCell()
        
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let obj = ProducatTitle[section].localized
        let headerView = tableView.dequeueReusableCell(withIdentifier: "HeaderDeatilesCell") as! HeaderDeatilesCell
        headerView.lbltitle.text = obj
        headerView.bt_Header.tag =  section
        if section == 0{
            if obj != ""{
                headerView.icDrop.isHidden = false
            }else{
                headerView.icDrop.isHidden = true
            }
        }
          
        
        if section == 3{
            let rating = UserDefaults.standard.string(forKey: "rate")
                      DispatchQueue.main.async{
                          if rating != nil{
                              headerView.ViewRate.rating = Double(rating!)!
                              headerView.icDrop.isHidden = false
                          }else{
                              headerView.icDrop.isHidden = true
                          }
                          
                      }
            headerView.ViewRate.isHidden = false
        }else{
            headerView.ViewRate.isHidden = true
        }
        
        headerView.bt_Header.addTarget(self, action: #selector(self.ExpandCell), for: UIControl.Event.touchUpInside)
        
        return headerView
    }
    
    
    @objc func ExpandCell(_ sender:UIButton ){
        ExpandInt =  sender.tag
        tableView.reloadData()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return UITableView.automaticDimension
        }
        
        if indexPath.section == 1 {
            return 300
        }
        
        if indexPath.section == 2 {
            return 44
        }
        
        if indexPath.section == 3 {
            
            return 125
        }
        return CGFloat()
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1{
            return 0
        }else if section == 2{
            return 0
        }else{
            return 55
        }
    }
    
    
}


extension DetailsVC:FSPagerViewDelegate,FSPagerViewDataSource{
    
    
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return produacatImages.count
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "SliderProducatCell", at: index) as! SliderProducatCell
        
        cell.imgProducta.sd_custom(url: produacatImages[index].src!)
        self.pageControlle.currentPage = pagerView.currentIndex
        
        cell.imgProducta?.contentMode = .scaleToFill
        
        
        return cell
    }
    func pagerViewDidEndScrollAnimation(_ pagerView: FSPagerView) {
        self.pageControlle.currentPage = pagerView.currentIndex
    }
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        self.pageControlle.currentPage = targetIndex
        
    }
    
}


extension DetailsVC:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return realtedItems.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewCell", for: indexPath) as! NewCell
        let obj = realtedItems[indexPath.row]
        cell.lb_name.text = obj.title
        cell.imgNew.sd_custom(url: obj.image ?? "")
        if obj.salePrice != 0{
            cell.lb_price.text = "\(obj.regularPrice ?? 0.0)" + " "  + "O.R".localized
            cell.lblDesc.text = "\(obj.salePrice ?? 0)" + " " + "O.R".localized
            cell.viewDiscount.isHidden = false
        }else{
            cell.lb_price.text = "\(obj.regularPrice ?? 0.0)" + " "  + "O.R".localized
            cell.viewDiscount.isHidden = true
        }

        if obj.favoriteItem == false{
            cell.bt_like.setImage(UIImage(named: "unlike"), for: .normal)
        }else{
            cell.bt_like.setImage(UIImage(named: "like"), for: .normal)
        }
        cell.bt_like.tag = indexPath.row
        cell.bt_like.addTarget(self, action: #selector(self.likeReated), for: UIControl.Event.touchUpInside)
        return cell
    }
    
    
    @objc func likeReated(_ sender:UIButton ){
        if self.deatilesproducat?.favoriteItem == false{
            var parameters: [String: Any] = [:]
            parameters["item_id"] = self.deatilesproducat?.id?.description
            
            _ = WebRequests.setup(controller: nil).prepare(query: "favorite/items", method: HTTPMethod.post,parameters: parameters).start(){ (response, error) in
                
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200 {
                        
                        self.showAlert(title: "error".localized, message:Status.message!)
                        return
                    }else if Status.status  == 200{
                        NotificationCenter.default.post(name: Notification.Name("UpdateLike"), object: nil)
                        
                        self.showAlert(title: "Done successfully".localized, message:"Product has been added to favourites".localized)                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
                
            }
        }else{
            let id = self.deatilesproducat?.id
            _ = WebRequests.setup(controller: nil).prepare(query: "favorite/items/\(id ?? 0)", method: HTTPMethod.delete).start(){ (response, error) in
                
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200 {
                        
                        self.showAlert(title: "error".localized, message:Status.message!)
                        return
                    }else if Status.status  == 200{
                        NotificationCenter.default.post(name: Notification.Name("UpdateLike"), object: nil)
                        
                        self.showAlert(title:"Done successfully".localized, message:"Product has been removed from favourites".localized)
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
                
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let obj = realtedItems[indexPath.row]
        let vc:DetailsVC = AppDelegate.sb_main.instanceVC()
        vc.producatID = obj.id
        vc.hidesBottomBarWhenPushed = true
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 200, height: 250)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
    
}
