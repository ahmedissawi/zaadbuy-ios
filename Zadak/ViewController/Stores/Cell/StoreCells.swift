//
//  ServicesCell.swift
//  Zadak
//
//  Created by osamaaassi on 12/03/2021.
//  Copyright © 2021  Osamaaassi MacBook Pro. All rights reserved.
//

import UIKit

class StoreCells: UITableViewCell {

    
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lblCreated: UILabel!
    @IBOutlet weak var imgStore: UIImageView!
    
    var DetailesShowTapped: (() -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
        
    }

    var storesObj : Stores? {
        didSet{
            
            lbltitle.text = self.storesObj?.name ?? ""
            lblCreated.text = "\(storesObj?.createdAt ?? "0000-00-00")"
            self.imgStore.sd_custom(url: storesObj?.logo ?? "")
            
        }
        
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func detailesShow(_ sender: UISwitch) {
           DetailesShowTapped?()
       }
    
}
