//
//  StoreVC.swift
//  Zadak
//
//  Created by osamaaassi on 12/03/2021.
//  Copyright © 2021  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import ESPullToRefresh
import ActionSheetPicker_3_0

protocol ServiecDelegat {
    func selectedDoneStore(id: Int , name : String)
}

class StoreVC: SuperViewController,UITextFieldDelegate {
    
    @IBOutlet weak var tableView:UITableView!
    @IBOutlet var search: UITextField!
    @IBOutlet var cancel: UIButton!
    
   var lblStatus:String?
    
    var dataStores:DataStores?
    var stores = [Stores]()
    var delegate:ServiecDelegat?
    var currentpage = 1
    
    var searchText:String!
    
    var dataCategories:DataCategories?
    var dataCategorie = [Childrens]()
    var selectedTypeActivityID:Int?
    
    var is_Store:Bool = false
    var is_filter:Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        getStore()
        getStatus()
        cancel.isHidden = true
        tableView.registerCell(id: "StoreCell")
        self.tableView.es.addPullToRefresh {
            
            self.currentpage = 1
            self.stores.removeAll()
            self.getStore()
            self.hideIndicator()
        }
        
        self.tableView.es.addInfiniteScrolling {
            self.currentpage += 1
            self.getStore() // next page
            self.tableView.estimatedRowHeight = UITableView.automaticDimension
            self.hideIndicator()
        }
        
        let navgtion = self.navigationController as! CustomNavigationBar
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(descriptor: UIFontDescriptor(name: "FFShamelFamilySemiRoundBook-S", size: 17), size: 17),NSAttributedString.Key.foregroundColor: UIColor.white]
        self.title = "Stores".localized
        navigationController?.navigationBar.barTintColor = "FF6A2A".color
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
    }
   
   
    
    @IBAction func didRefersh(){
        stores.removeAll()
        getStore()
    }
 
   
    @IBAction func didTap_Cancel(_ sender: UIButton) {
        searchText = ""
        search.text = ""
        selectedTypeActivityID = nil
        stores.removeAll()
        getStore()
        cancel.isHidden = true
        
        
    }
    @IBAction func didTap_EndEditing(_ sender: UIButton) {
        if(search.text != nil && search.text != "" || selectedTypeActivityID != nil){
            cancel.isHidden = false
            stores.removeAll()
            getStore()
        }
       
        
    }
    @IBAction func didTap_Category(_ sender: UIButton) {
        
        ActionSheetStringPicker.show(withTitle: self.lblStatus ?? "Classification of services".localized, rows: self.dataCategorie.map { $0.title as Any }
            , initialSelection: 0, doneBlock: {
                picker, value, index in
                if let Value = index {
                    self.lblStatus = Value as? String
                    self.selectedTypeActivityID = self.dataCategorie[value].id ?? 0
                    if(self.selectedTypeActivityID != nil){
                        
                        self.cancel.isHidden = false
                        self.stores.removeAll()
                        self.getStore()
                    }
                }
                
                return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        
    }
    
    
   
    func getStore(){
        
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        
        
        var parameters: [String: Any] = [:]
        if (!search.text!.isEmpty && search.text! != ""){
            parameters["search"] = search.text!.description
        }
        if (selectedTypeActivityID != nil){
            parameters["category_id"] = self.selectedTypeActivityID?.description
        }
        
        
        _ = WebRequests.setup(controller: self).prepare(query: "stores?page=\(currentpage)", method: HTTPMethod.get,parameters: parameters).start(){ (response, error) in
            
            self.tableView.es.stopLoadingMore()
            self.tableView.es.stopPullToRefresh()
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            
            do {
                let Status =  try JSONDecoder().decode(MainStores.self, from: response.data!)
                self.dataStores = Status.data!
                self.stores +=  self.dataStores!.stores ?? []
               
                
                if self.dataStores?.nextPageUrl == nil {
                    self.tableView.es.stopLoadingMore()
                    self.tableView.es.noticeNoMoreData()
                }
                
                if self.stores.count == 0{
                    self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.tableView, refershSelector: #selector(self.didRefersh))
                    self.emptyView?.firstLabel.text = "لا يوجد متاجر".localized
                    self.stores.removeAll()
                    self.tableView.reloadData()
                    
                }else{
                    self.tableView.tableFooterView = nil
                }
                self.tableView.reloadData()
                
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
           
    }
    
    
    func getStatus(){
        
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        
        _ = WebRequests.setup(controller: self).prepare(query: "categories/2", method: HTTPMethod.get,isAuthRequired: false).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            do {
                let Status =  try JSONDecoder().decode(CategoriesServices.self, from: response.data!)
                
                self.dataCategories = Status.data!
                self.dataCategorie += self.dataCategories!.childrens!
           
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
        
    }
    
}

extension StoreVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stores.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "StoreCell", for: indexPath) as! StoreCell
        cell.storesObj = self.stores[indexPath.row]
        
        if (is_Store){
            cell.veiwDetalis.isHidden = false
        }
        
        cell.DetailesShowTapped = { [ weak self] in
            let vc:DetailsStoreVC = DetailsStoreVC.loadFromNib()
            vc.ID = self?.stores[indexPath.row].id ?? 0
            self!.navigationController?.pushViewController(vc, animated: false)
            
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(is_filter){
            let id = self.stores[indexPath.row].id ?? 0
            let name = self.stores[indexPath.row].name ?? ""
            let vc:ServicesVC = ServicesVC.loadFromNib()
            self.delegate?.selectedDoneStore(id: id, name: name)
            navigationController?.popViewController(animated: true)
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 135
    }
    
}
