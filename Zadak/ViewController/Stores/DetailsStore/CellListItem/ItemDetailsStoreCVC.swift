//
//  ItemDetailsStoreCVC.swift
//  Zadak
//
//  Created by osamaaassi on 13/03/2021.
//  Copyright © 2021  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class ItemDetailsStoreCVC: UITableViewCell {

    
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lblSeller: UILabel!
    @IBOutlet weak var lblprice: UILabel!
    @IBOutlet weak var lblcategory: UILabel!
    @IBOutlet weak var imgServices: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
          
    }
    
    var itemsObj:ItemsStore?{
            didSet{
                
                lbltitle.text = itemsObj?.title ?? ""
                lblSeller.text = itemsObj?.seller?.name ?? ""
                lblcategory.text = itemsObj?.category?.name ?? ""
                lblprice.text = itemsObj?.price?.description ?? "0"
                imgServices.sd_custom(url: itemsObj?.image ?? "")
                
            }
        }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
