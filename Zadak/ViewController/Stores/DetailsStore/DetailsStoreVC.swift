//
//  DetailsStoreVC.swift
//  Zadak
//
//  Created by osamaaassi on 13/03/2021.
//  Copyright © 2021  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class DetailsStoreVC: SuperViewController {

    
    @IBOutlet weak var lblSellrName: UILabel!
    @IBOutlet weak var lblStoreName: UILabel!
    @IBOutlet weak var lblstoreCategory: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var imgStore: UIImageView!
    @IBOutlet weak var tableView: UITableView!
  
    
    var storeDetails:DataStoreDetails?
    
    var dataItemStore: DataItemStore?
    var itemsStore = [ItemsStore]()
    var isFromServec:Bool = false
    
    var ID : Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.registerCell(id: "ItemDetailsStoreCVC")
        
        let navgtion = self.navigationController as! CustomNavigationBar
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(descriptor: UIFontDescriptor(name: "FFShamelFamilySemiRoundBook-S", size: 17), size: 17),NSAttributedString.Key.foregroundColor: UIColor.white]
        self.title = "Store details".localized
        navigationController?.navigationBar.barTintColor = "FF6A2A".color
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        if(isFromServec){
            getItemStore()
        }else{
            getStore()
            getItemStore()
        }
    }
    @IBAction func didRefersh(){
        itemsStore.removeAll()
        getItemStore()
    }
//    override func viewWillAppear(_ animated: Bool) {
//        let navgtion = self.navigationController as! CustomNavigationBar
//        navgtion.setLogotitle(sender: self)
//        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 0.4156862745, blue: 0.1647058824, alpha: 1)
//
//    }
  

    func getStore(){
        
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        
        _ = WebRequests.setup(controller: self).prepare(query: "stores/\(ID!)", method: HTTPMethod.get,isAuthRequired: false).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            do {
                let Status =  try JSONDecoder().decode(StoreDetails.self, from: response.data!)
                
                self.storeDetails = Status.data
                
               // self.lblSellrName.text = self.storeDetails?.username ?? ""
                self.lblStoreName.text = self.storeDetails?.storeName ?? ""
                self.lblstoreCategory.text = self.storeDetails?.storeCategory ?? ""
                self.lblAddress.text = self.storeDetails?.storeAddress ?? ""
                self.imgStore.sd_custom(url:self.storeDetails?.storeLogo ?? "")
           
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
        
        
      
    }
    
    
    func getItemStore(){
        
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        
        _ = WebRequests.setup(controller: self).prepare(query: "stores/\(ID!)/with-services", method: HTTPMethod.get,isAuthRequired: false).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            do {
                let Status =  try JSONDecoder().decode(StoreItem.self, from: response.data!)
                
                self.dataItemStore = Status.data
                self.itemsStore += self.dataItemStore!.items!
                
                if self.isFromServec{
                   // self.lblSellrName.text = self.dataItemStore?.seller?.username ?? ""
                    self.lblStoreName.text = self.dataItemStore?.seller?.storeName ?? ""
                    self.lblstoreCategory.text = self.dataItemStore?.seller?.storeCategory ?? ""
                    self.lblAddress.text = self.dataItemStore?.seller?.storeAddress ?? ""
                    self.imgStore.sd_custom(url:self.dataItemStore?.seller?.storeLogo ?? "")
                }
                
                if self.itemsStore.count == 0{
                    self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.tableView, refershSelector: #selector(self.didRefersh))
                    self.emptyView?.firstLabel.text = "No services to display".localized
                    self.itemsStore.removeAll()
                    self.tableView.reloadData()
                    
                }else{
                    self.tableView.tableFooterView = nil
                }
                self.tableView.reloadData()
           
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
        
        
    }
    
//    @IBAction func itemStore(_ sender: UIButton) {
//        guard !self.itemsStore.isEmpty else {
//            self.showAlert(title: "".localized, message: "لا يوجد عناصر للعرض".localized)
//            return
//        }
//        let vc:ServiceStoreItemsVC = ServiceStoreItemsVC.loadFromNib()
//        vc.itemsStore = self.itemsStore
//        self.navigationController?.pushViewController(vc, animated: false)
//    }
    

}


extension  DetailsStoreVC : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return itemsStore.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemDetailsStoreCVC", for: indexPath) as! ItemDetailsStoreCVC
        cell.itemsObj =  self.itemsStore[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
           return 130
       }
    
    
    
}
