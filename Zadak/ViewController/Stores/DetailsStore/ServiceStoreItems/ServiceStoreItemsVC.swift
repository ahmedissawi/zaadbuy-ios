//
//  ServiceStoreItemsVC.swift
//  Zadak
//
//  Created by osamaaassi on 13/03/2021.
//  Copyright © 2021  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class ServiceStoreItemsVC: UIViewController {

    
    var itemsStore = [ItemsStore]()
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.registerCell(id: "ItemDetailsStoreCVC")


    }

}

extension  ServiceStoreItemsVC : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return itemsStore.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemDetailsStoreCVC", for: indexPath) as! ItemDetailsStoreCVC
        cell.itemsObj =  self.itemsStore[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
           return 130
    
        
        
       }
    
    
    
}
