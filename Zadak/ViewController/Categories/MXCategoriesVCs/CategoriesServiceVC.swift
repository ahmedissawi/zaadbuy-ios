//
//  CategoriesServiceVC.swift
//  Zadak
//
//  Created by osamaaassi on 22/03/2021.
//  Copyright © 2021  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class CategoriesServiceVC: SuperViewController {
    
    @IBOutlet weak var Table: UITableView!
    @IBOutlet weak var Colelction: UICollectionView!
    @IBOutlet weak var ViewTable: UIView!
    @IBOutlet weak var lblStatus:UILabel!
    var selctedIndex:Int?
    
    var dataCategories:DataCategories?
    var dataCategorie = [Childrens]()
    
    var selectedTypeActivityID:Int?
    var childrens = [Childrens]()
    
    var dataCategoriesServices:DataCategoriesServices?
    var itemsServices = [ItemsServices]()
    var isShow = false
    override func viewDidLoad() {
        super.viewDidLoad()
        getCategories()
      //  self.tableView.section = UIView()
        Table.registerCell(id: "CategoriesCell")
        Colelction.registerCell(id: "ProducatCell")
        let Producat = UINib(nibName: "HeaderProducatCell", bundle: nil)
        self.Colelction.register(Producat, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "HeaderProducatCell")
        
        selctedIndex = 0
    }
    
    @IBAction func didRefersh(){
            getcategoriesDeatiles()
       }

    
    func getCategories(){
        _ = WebRequests.setup(controller: self).prepare(query: "categories/2", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            do {
                
                let Status =  try JSONDecoder().decode(CategoriesServices.self, from: response.data!)
                self.dataCategories = Status.data!
                self.dataCategorie += self.dataCategories!.childrens!
                let object = self.dataCategorie.compactMap({$0.childrens}).first!
                
                if object.count == 0{
                    self.Colelction.showEmptyListMessage("No results to display".localized)
                   
                    self.Colelction.reloadData()
                }
                else{
                    self.Colelction.showEmptyListMessage("")

                }
                if object.count != 0{
                    self.childrens = object
                }
               
                
                self.Table.reloadData()
                //                self.dataCategories = Status.data!
                //                self.categoriesItems = self.categories.compactMap({$0.childrens}).first!
                //                self.getcategoriesDeatiles()
                //                self.Table.reloadData()
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
        
    }
    
    func getcategoriesDeatiles(){
        self.itemsServices.removeAll()
        _ = WebRequests.setup(controller: self).prepare(query: "categories/\(selectedTypeActivityID ?? 0)/services", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            do {
                let Status =  try JSONDecoder().decode(CategoriesService.self, from: response.data!)
                self.dataCategoriesServices = Status.data!
                self.itemsServices += self.dataCategoriesServices!.items!
                self.Colelction.reloadData()
                if self.itemsServices.count == 0{
                    self.Colelction.showEmptyListMessage("No results to display".localized)
                   
                    self.Colelction.reloadData()
                }
                else{
                    self.Colelction.showEmptyListMessage("")

                }
                
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
    }
    
    @IBAction func didTab_typeCategorie(_ sender: UIButton) {
        
        ActionSheetStringPicker.show(withTitle: "Service Type".localized, rows: self.childrens.map { $0.title as Any }
                                     , initialSelection: 0, doneBlock: {
                                        picker, value, index in
                                        if let Value = index {
                                            self.lblStatus.text = Value as? String
                                            self.selectedTypeActivityID = self.childrens[value].id ?? 0
                                            self.getcategoriesDeatiles()
                                            //NotificationCenter.default.post(name: Notification.Name("UpdateServices"), object: nil)
                                            
                                        }
                                        return
                                     }, cancel: { ActionStringCancelBlock in return }, origin: sender)
    }
    
    
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat {
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        
        label.sizeToFit()
        return label.frame.height
    }
    
}

extension CategoriesServiceVC:UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataCategorie.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoriesCell", for: indexPath) as! CategoriesCell
        
        let obj = dataCategorie[indexPath.row]
        cell.lb_categories.text = obj.title
        if selctedIndex == indexPath.row {
            cell.viewCategories.backgroundColor =  UIColor.white
            cell.viewline.isHidden = false
        }else{
            cell.viewCategories.backgroundColor =  "F8F8FB".color
            cell.viewline.isHidden = true
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let categorie = dataCategorie[indexPath.row].childrens?.first
        if categorie != nil{
            self.itemsServices.removeAll()
            self.lblStatus.text = categorie?.title ?? ""
            self.childrens = dataCategorie[indexPath.row].childrens!
            
            _ = WebRequests.setup(controller: self).prepare(query: "categories/\( self.childrens.first?.id ?? 0)/services", method: HTTPMethod.get).start(){ (response, error) in
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200{
                        self.showAlert(title: "error".localized, message:Status.message!)
                        return
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
                do {
                    let Status =  try JSONDecoder().decode(CategoriesService.self, from: response.data!)
                    self.dataCategoriesServices = Status.data!
                    self.itemsServices += self.dataCategoriesServices!.items!
                    self.Colelction.reloadData()
                    
                    if( self.itemsServices.count == 0){
//                        self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.Colelction, refershSelector: #selector(self.didRefersh))
//                        self.emptyView?.firstLabel.text = "لا توجد نتائج للعرض".localized
                        self.Colelction.showEmptyListMessage("No results to display".localized)

                        self.Colelction.reloadData()
                    }
                    else{
                        self.Colelction.showEmptyListMessage("")

                    }
                } catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
            }
            self.selctedIndex = indexPath.row
            self.Table.reloadData()
        }
        else {
            self.showAlert(title: "".localized, message: "No results to display".localized)
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
}


extension CategoriesServiceVC:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return itemsServices.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProducatCell", for: indexPath) as! ProducatCell
        
            let obj = itemsServices[indexPath.row]
            cell.lb_name.text = obj.title
            cell.imgproducat.sd_custom(url: obj.image ?? "")
            
            return cell
        
    }
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            let obj = itemsServices[indexPath.row].id ?? 0
            let vc:DetalesServiceVC = DetalesServiceVC.loadFromNib()
            vc.ID = obj
            vc.hidesBottomBarWhenPushed = true
            vc.modalPresentationStyle = .fullScreen
            navigationController?.pushViewController(vc, animated: true)
    
        }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if !isShow{
            let name = itemsServices[indexPath.row].title
            let  hight  = self.heightForView(text: name!, font: UIFont.init(name: "FFShamelFamilySemiRoundBook-S", size: 15)!, width: (UIScreen.main.bounds.width * 0.7 / 2.2) - 16)
            return CGSize(width: (UIScreen.main.bounds.width * 0.7 / 2.2), height: hight + 150)
        }else{
            
            
            let name = itemsServices[indexPath.row].title
            let  hight  = self.heightForView(text: name!, font: UIFont.init(name: "FFShamelFamilySemiRoundBook-S", size: 15)!, width: (UIScreen.main.bounds.width * 0.7 / 3) - 16)
            return CGSize(width: (UIScreen.main.bounds.width * 0.7 / 3), height: hight + 150)
        }
        
        
    }
    
    
    
}


