//
//  CategoriesVC.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 2/21/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class CategoriesVC:SuperViewController {
    
    var selctedIndex:Int?
    var ExpandInt:Int?
    var categories = [categoriesData]()
    var categoriesItems = [categoriesitems]()
    
    var categoriesDeatiles:CategoriesDeatilesData?
    var categoriesDeatilesItems = [CategoriesProducat]()
    
    var isShow = false
    
    @IBOutlet weak var Table: UITableView!
    @IBOutlet weak var Colelction: UICollectionView!
    @IBOutlet weak var ViewTable: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        getCategories()
        Table.registerCell(id: "CategoriesCell")
        Colelction.registerCell(id: "ProducatCell")
        let Producat = UINib(nibName: "HeaderProducatCell", bundle: nil)
        self.Colelction.register(Producat, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "HeaderProducatCell")
        selctedIndex = 0
        ExpandInt = 0
    }

    
    override func didClickRightButton(_sender: UIBarButtonItem) {
        if isShow{
            self.isShow = false
            self.ViewTable.isHidden = false
        }else{
            self.isShow = true
            self.ViewTable.isHidden = true
        }
        self.Colelction.reloadData()
    }
    
    func getCategories(){
        _ = WebRequests.setup(controller: self).prepare(query: "categories", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            
            do {
                let Status =  try JSONDecoder().decode(categoriesInfo.self, from: response.data!)
                self.categories = Status.data!
                self.categoriesItems = self.categories.compactMap({$0.childrens}).first!
                self.getcategoriesDeatiles()
                self.Table.reloadData()
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            
        }
        
    }
    func getcategoriesDeatiles(){
        let id =  self.categoriesItems.compactMap({$0.id}).first
        _ = WebRequests.setup(controller: self).prepare(query: "categories/\(id ?? 0)/items", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            do {
                let Status =  try JSONDecoder().decode(CategoriesDeatilesInfo.self, from: response.data!)
                self.categoriesDeatiles = Status.data
                self.categoriesDeatilesItems = self.categoriesDeatiles!.items!
                self.Colelction.reloadData()
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
    }
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat {
          let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
          label.numberOfLines = 0
          label.lineBreakMode = NSLineBreakMode.byWordWrapping
          label.font = font
          label.text = text

          label.sizeToFit()
          return label.frame.height
      }
    
}
extension CategoriesVC:UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categoriesItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoriesCell", for: indexPath) as! CategoriesCell
        let obj = categoriesItems[indexPath.row]
        cell.lb_categories.text = obj.title
        if selctedIndex == indexPath.row {
            cell.viewCategories.backgroundColor =  UIColor.white
            cell.viewline.isHidden = false
        }else{
            cell.viewCategories.backgroundColor =  "F8F8FB".color
            cell.viewline.isHidden = true
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let categorieID = categoriesItems[indexPath.row].id
        _ = WebRequests.setup(controller: self).prepare(query: "categories/\(categorieID ?? 0)/items", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            do {
                let Status =  try JSONDecoder().decode(CategoriesDeatilesInfo.self, from: response.data!)
                self.categoriesDeatiles = Status.data
                self.categoriesDeatilesItems = self.categoriesDeatiles!.items!
                self.Colelction.reloadData()
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            
        }
        self.selctedIndex = indexPath.row
        self.Table.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    
    
}


extension CategoriesVC:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if ExpandInt == section{
            return categoriesDeatilesItems.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProducatCell", for: indexPath) as! ProducatCell
        let obj = categoriesDeatilesItems[indexPath.row]
        cell.lb_name.text = obj.title
        cell.imgproducat.sd_custom(url: obj.image ?? "")
        
        return cell
        
        
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let obj = categoriesDeatilesItems[indexPath.row]
        let vc:DetailsVC = AppDelegate.sb_main.instanceVC()
        vc.producatID = obj.id
        vc.hidesBottomBarWhenPushed = true
        vc.modalPresentationStyle = .fullScreen
        navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if !isShow{
            let name = categoriesDeatilesItems[indexPath.row].title
            let  hight  = self.heightForView(text: name!, font: UIFont.init(name: "FFShamelFamilySemiRoundBook-S", size: 15)!, width: (UIScreen.main.bounds.width * 0.7 / 2.2) - 16)
            return CGSize(width: (UIScreen.main.bounds.width * 0.7 / 2.2), height: hight + 150)
        }else{
            
            
            let name = categoriesDeatilesItems[indexPath.row].title
            let  hight  = self.heightForView(text: name!, font: UIFont.init(name: "FFShamelFamilySemiRoundBook-S", size: 15)!, width: (UIScreen.main.bounds.width * 0.7 / 3) - 16)
            return CGSize(width: (UIScreen.main.bounds.width * 0.7 / 3), height: hight + 150)
        }
      
        
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HeaderProducatCell", for: indexPath) as! HeaderProducatCell
        header.lb_name.text = categoriesDeatiles?.title
        header.bt_producat.tag = indexPath.section
        header.bt_producat.addTarget(self, action: #selector(self.ExpandCell), for: UIControl.Event.touchUpInside)
        
        return header
    }
    
    @objc func ExpandCell(_ sender:UIButton ){
        ExpandInt = sender.tag
        Colelction.reloadData()
    }

}

