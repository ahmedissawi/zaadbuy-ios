//
//  MxSeqmentCategoriesVC.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 12/22/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import MXSegmentedPager

class MxSeqmentCategoriesVC: BaseViewController {
    
    var arrayView = [String]()
    var arrayViewController = [UIViewController]()


    override func viewDidLoad() {
        super.viewDidLoad()
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setLogotitle(sender: self)
//        navgtion.setLeftButtons([navgtion.MuneWhitebtn!], sender: self)
        navigationController?.navigationBar.barTintColor = "FF6A2A".color
        
        arrayView = ["المنتجات","الخدمات"]
        
        if MOLHLanguage.isRTLLanguage() {
            arrayView = arrayView.reversed()
            arrayViewController = arrayViewController.reversed()
        }
        
        setUpSegmentation()
    }
    override func viewWillAppear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.9960784314, green: 0.4196078431, blue: 0.1647058824, alpha: 1)

    }
  
    
}

extension MxSeqmentCategoriesVC {
    func setUpSegmentation (){
        
        if MOLHLanguage.isRTLLanguage(){
            self.segmentedPager.pager.showPage(at:self.arrayView.count - 1, animated: false)
        }
        DispatchQueue.main.async {
            if MOLHLanguage.isRTLLanguage(){
                self.segmentedPager.pager.showPage(at:self.arrayView.count - 1, animated: false)
                self.segmentedPager.segmentedControl.selectedSegmentIndex = self.arrayView.count - 1
                // segmentedPager.pager.reloadData()
            }
        }
        
        segmentedPager.backgroundColor = UIColor.white
        segmentedPager.segmentedControl.backgroundColor = UIColor.white
        segmentedPager.parallaxHeader.view = nil
        segmentedPager.parallaxHeader.mode = .bottom
        segmentedPager.parallaxHeader.height = 0
        segmentedPager.parallaxHeader.minimumHeight = 0
        
        
        // Segmented Control customization
        self.segmentedPager.segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocation.down
        segmentedPager.segmentedControl.selectedTitleTextAttributes = [NSAttributedString.Key.foregroundColor: "014A97".color]
        
        segmentedPager.segmentedControl.selectionIndicatorColor = "FF6A2A".color
        segmentedPager.segmentedControl.selectionIndicatorHeight = 3
        
        self.segmentedPager.segmentedControl.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "FFShamelFamilySemiRoundBook-S", size: 17)!, NSAttributedString.Key.foregroundColor :  "014A97".color]
        
        
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, titleForSectionAt index: Int) -> String {
        
        
        switch index {
        case 0:
            return "Services".localized
           
        case 1:
            return "Products".localized
        default:
            break
        }
        return "Services".localized
        
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, didScrollWith parallaxHeader: MXParallaxHeader) {
    }
    
    
    override func numberOfPages(in segmentedPager: MXSegmentedPager) -> Int {
        return 2
    }
    
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, viewControllerForPageAt index: Int) -> UIViewController {
        switch index {
        case 1:
            return W0VC()
        case 0:
            return W1VC()
        default:
            break
        }
        return W0VC()
        
        
    }
    
    
    fileprivate  func W0VC() ->CategoriesVC{
        let vc = self.instantiate(id: "CategoriesVC") as! CategoriesVC
        addChild(vc)
        vc.didMove(toParent: self)
        return vc
    }
    fileprivate  func W1VC() ->CategoriesServiceVC{
        let vc = self.instantiate(id: "CategoriesServiceVC") as! CategoriesServiceVC
        addChild(vc)
        vc.didMove(toParent: self)
        return vc
    }

}
