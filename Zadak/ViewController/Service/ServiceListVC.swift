//
//  ServiceListVC.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 12/22/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class ServiceListVC: SuperViewController,UITextFieldDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchText: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.searchText.delegate = self
        searchText.returnKeyType = .search
        let navgtion = self.navigationController as! CustomNavigationBar
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(descriptor: UIFontDescriptor(name: "FFShamelFamilySemiRoundBook-S", size: 17), size: 17),NSAttributedString.Key.foregroundColor: UIColor.white]
        self.title = "المتاجر".localized
        navigationController?.navigationBar.barTintColor = "FF6A2A".color
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        tableView.registerCell(id: "TypeStoreC")
    }


}
