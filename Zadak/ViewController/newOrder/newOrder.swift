//
//  Data.swift
//
//  Created by Ahmed ios on 6/21/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct newOrder: Codable {

  enum CodingKeys: String, CodingKey {
    case status
    case orderId = "order_id"
//    case order
    case payment
  }

  var status: Bool?
  var orderId: Int?
//  var order: Order?
  var payment: Payment?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    status = try container.decodeIfPresent(Bool.self, forKey: .status)
    if let value = try? container.decode(String.self, forKey:.orderId) {
 orderId = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.orderId) {
orderId = value 
}
//    order = try container.decodeIfPresent(Order.self, forKey: .order)
    payment = try container.decodeIfPresent(Payment.self, forKey: .payment)
  }

}
