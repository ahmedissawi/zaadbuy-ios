//
//  BaseClass.swift
//
//  Created by Ahmed ios on 6/21/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct newOrderClass: Codable {

  enum CodingKeys: String, CodingKey {
    case locale
    case data
    case code
    case message
    case status
  }

  var locale: String?
  var data: newOrder?
  var code: Int?
  var message: String?
  var status: Int?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(Int.self, forKey:.locale) {                       
locale = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.locale) {
 locale = value                                                                                     
}
    data = try container.decodeIfPresent(newOrder.self, forKey: .data)
    if let value = try? container.decode(String.self, forKey:.code) {
 code = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.code) {
code = value 
}
    if let value = try? container.decode(Int.self, forKey:.message) {                       
message = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.message) {
 message = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.status) {
 status = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.status) {
status = value 
}
  }

}
