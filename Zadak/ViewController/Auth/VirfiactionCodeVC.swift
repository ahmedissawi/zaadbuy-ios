//
//  VirfiactionCodeVC.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 2/19/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class VirfiactionCodeVC: SuperViewController {
    
    @IBOutlet weak var tf_virfiaction: UITextField!
    @IBOutlet weak var lb_time: UILabel!
    @IBOutlet weak var lblMobile: UILabel!
    
    var username:String?
    var timer = Timer()
    var seconds = 10
    
    override func viewDidLoad() {
        super.viewDidLoad()
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(self.updateTimer)), userInfo: nil, repeats: true)

        self.lblMobile.text  = "\(username ?? "")تحقق من حسابك عن طريق إدخال الرمز المكون من 4 ارقام الى الرقم "
   
        let navagtion = navigationController as! CustomNavigationBar
        navagtion.isDark = false
        navagtion.setCustomBackButtonForViewController(sender: self)
    }
    
    
    @objc func updateTimer() {
         seconds -= 1     //This will decrement(count down)the seconds.
         if seconds <= 0 {
//             RsendView.isHidden = false
             lb_time.text = "00:00" //This will update the label.
             timer.invalidate()
             
         }else{
             lb_time.text = "00:\(seconds)" //This will update the label.
             
         }
     }
    
    @IBAction func bt_send(_ sender: Any) {
        
        var parameters: [String: Any] = [:]
        parameters["username"] = username
        parameters["code"] = self.tf_virfiaction.text ?? ""
        
        _ = WebRequests.setup(controller: self).prepare(query: "auth/verify", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200 {
                    
                    self.showAlert(title: "Error".localized, message:Status.message!)
                    return
                }else if  Status.status == 200{
                    let vc:TTabBarViewController = AppDelegate.sb_main.instanceVC()
                    vc.modalPresentationStyle = .fullScreen
                    self.present(vc, animated: true, completion: nil)
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            
        }
        
        
    }
    
    @IBAction func bt_resend(_ sender: Any) {
        var parameters: [String: Any] = [:]
        parameters["username"] = username
        
        _ = WebRequests.setup(controller: self).prepare(query: "auth/resend-code", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200 {
                    
                    self.showAlert(title: "Error".localized, message:Status.message!)
                    return
                }else if  Status.status == 200{
                    let vc:TTabBarViewController = AppDelegate.sb_main.instanceVC()
                    vc.modalPresentationStyle = .fullScreen
                    self.present(vc, animated: true, completion: nil)
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            
        }
        
    }
    
    
    
}
