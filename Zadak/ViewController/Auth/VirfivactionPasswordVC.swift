//
//  VirfivactionPasswordVC.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 2/21/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class VirfivactionPasswordVC: SuperViewController {
    
    @IBOutlet weak var tf_code: UITextField!
    var userName:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let navagtion = navigationController as! CustomNavigationBar
        navagtion.isDark = false
        navagtion.setCustomBackButtonForViewController(sender: self)
    }
    
    @IBAction func bt_send(_ sender: Any) {
        
        var parameters: [String: Any] = [:]
        parameters["username"] = userName
        parameters["code"] = self.tf_code.text ?? ""
        
        _ = WebRequests.setup(controller: self).prepare(query: "auth/verify-reset-password-code", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200 {
                    
                    self.showAlert(title: "Error".localized, message:Status.message!)
                    return
                }else if  Status.status == 200{
                    let vc:ResetPasswordVC = AppDelegate.sb_main.instanceVC()
                    vc.modalPresentationStyle = .fullScreen
                    vc.name = self.userName
                    vc.codeReset = self.tf_code.text ?? ""
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            
        }
        
        
        
    }
    
    
}
