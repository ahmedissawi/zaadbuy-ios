//
//  RegisterVC.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 2/19/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class RegisterVC: SuperViewController {
    
    @IBOutlet weak var tf_phone: UITextField!
    @IBOutlet weak var tf_password: UITextField!
    @IBOutlet weak var tf_Conformpassword: UITextField!
    @IBOutlet weak var tf_name: UITextField!
    @IBOutlet weak var tf_email: UITextField!
    @IBOutlet weak var imgShowpassword: UIImageView!
    @IBOutlet weak var imgShowConfirmpassword: UIImageView!

    
    var isShowPassword = true
    var isShowConfirmPassword = true

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let navagtion = navigationController as! CustomNavigationBar
        navagtion.isDark = false
        navagtion.setCustomBackButtonForViewController(sender: self)
        tf_password.isSecureTextEntry = true
        imgShowpassword.image = UIImage(named: "unshow")
        tf_Conformpassword.isSecureTextEntry = true
        imgShowConfirmpassword.image = UIImage(named: "unshow")
    }
    func getToken(){
        let token =  UserDefaults.standard.string(forKey: "token")
        var parameters: [String: Any] = [:]
        parameters["token"] = token
        
        _ = WebRequests.setup(controller: self).prepare(query: "fcm/token", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200 {
                    
                    self.showAlert(title: "Error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
    }
    
    @IBAction func bt_checkpass(_ sender:UIButton) {
        if sender.tag == 0{
            
            if(isShowPassword == true) {
                tf_password.isSecureTextEntry = false
                imgShowpassword.image = UIImage(named: "pass")
            } else {
                tf_password.isSecureTextEntry = true
                imgShowpassword.image = UIImage(named: "unshow")
            }
            
            isShowPassword = !isShowPassword
        }else{
            if(isShowConfirmPassword == true) {
                tf_Conformpassword.isSecureTextEntry = false
                imgShowConfirmpassword.image = UIImage(named: "pass")
            } else {
                tf_Conformpassword.isSecureTextEntry = true
                imgShowConfirmpassword.image = UIImage(named: "unshow")
            }
            
            isShowConfirmPassword = !isShowConfirmPassword
        }
    }
    
    @IBAction func bt_register(_ sender: Any) {
        
        guard let mobile = self.tf_phone.text, !mobile.isEmpty else{
            self.showAlert(title: "error".localized, message: "The phone cannot be empty.".localized)
            return
        }
        guard let password = self.tf_password.text, !password.isEmpty else{
                   self.showAlert(title: "error".localized, message: "Password cannot be empty.".localized)
                   return
        }
               
        if password.count < 6 {
            showAlert(title: "error".localized, message: "Password must be at least 6 characters long".localized)
                   return
        }
        
        
        guard let confrimpassword = self.tf_Conformpassword.text, !confrimpassword.isEmpty else{
            self.showAlert(title: "error".localized, message: "Confirm password cannot be empty".localized)
            return
        }
        
        guard tf_Conformpassword.text == tf_password.text  else {
            self.showAlert(title: "error".localized, message: "Password does not match confirm password".localized)
            return
        }
        
        var MobileCheck = mobile
        
        var parameters: [String: Any] = [:]
        parameters["mobile"] =  MobileCheck
        parameters["name"] =  tf_name.text ?? ""
        parameters["email"] =  tf_email.text ?? ""
        parameters["password"] = password
        parameters["password_confirmation"] = confrimpassword
        
        
        _ = WebRequests.setup(controller: self).prepare(query: "auth/register", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200 {
                    
                    self.showAlert(title: "Error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            do {
                let Status =  try JSONDecoder().decode(UserObject.self, from: response.data!)
                CurrentUser.userInfo = Status.data
                print("SessionManager.shared.session", CurrentUser.userInfo as Any)
                if CurrentUser.userInfo != nil{
                    self.getToken()
                    let vc:VirfiactionCodeVC = AppDelegate.sb_main.instanceVC()
                    vc.username = self.tf_email.text
                    vc.modalPresentationStyle = .fullScreen
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                }
                
            } catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
        }
        
    }
    @IBAction func bt_login(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func bt_terms(_ sender: Any) {
        let nav:CustomNavigationBar = AppDelegate.sb_main.instanceVC()
        let vc:InfoVC = AppDelegate.sb_main.instanceVC()
        vc.idInfo = 201
        nav.pushViewController(vc, animated: true)
        vc.modalPresentationStyle = .fullScreen
        vc.isFromTermsLogin = true
        vc.isDismis = true
        self.present(nav, animated: true, completion: nil)
    }
    
}
