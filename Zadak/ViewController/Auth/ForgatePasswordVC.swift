//
//  ForgatePasswordVC.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 2/21/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class ForgatePasswordVC: SuperViewController {
    
    @IBOutlet weak var tf_phone: UITextField!

    var username:String?

    override func viewDidLoad() {
        super.viewDidLoad()
        let navagtion = navigationController as! CustomNavigationBar
        navagtion.isDark = false
        navagtion.setCustomBackButtonForViewController(sender: self)
    }
    
    
    @IBAction func bt_forgatepassword(_ sender: Any) {
        
        guard let mobile = self.tf_phone.text, !mobile.isEmpty else{
            self.showAlert(title: "error".localized, message: "The phone cannot be empty.".localized)
            return
        }
        
        var parameters: [String: Any] = [:]
        parameters["username"] = self.tf_phone.text ?? ""

        _ = WebRequests.setup(controller: self).prepare(query: "auth/reset-password-request", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200 {
                    self.showAlert(title: "Error".localized, message:Status.message!)
                    return
                }else if Status.status == 200{
                    let vc:VirfivactionPasswordVC = AppDelegate.sb_main.instanceVC()
                    vc.userName = mobile
                    vc.modalPresentationStyle = .fullScreen
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }

            
        }
    }
   

}
