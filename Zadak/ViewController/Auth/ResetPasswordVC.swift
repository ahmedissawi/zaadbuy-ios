//
//  ResetPasswordVC.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 2/21/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class ResetPasswordVC: SuperViewController {
    
    @IBOutlet weak var tf_password: UITextField!
    @IBOutlet weak var tf_Conformpassword: UITextField!
    @IBOutlet weak var imgShowpassword: UIImageView!
    @IBOutlet weak var imgShowConfirmpassword: UIImageView!

    
    var isShowPassword = true
    var isShowConfirmPassword = true
    var name:String?
    var codeReset:String?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        let navagtion = navigationController as! CustomNavigationBar
        navagtion.isDark = false
        navagtion.setCustomBackButtonForViewController(sender: self)
        tf_password.isSecureTextEntry = true
        imgShowpassword.image = UIImage(named: "unshow")
        tf_Conformpassword.isSecureTextEntry = true
        imgShowConfirmpassword.image = UIImage(named: "unshow")

    }
    
    @IBAction func bt_checkpass(_ sender: UIButton) {
        if sender.tag == 0{
            
            if(isShowPassword == true) {
                tf_password.isSecureTextEntry = false
                imgShowpassword.image = UIImage(named: "pass")
            } else {
                tf_password.isSecureTextEntry = true
                imgShowpassword.image = UIImage(named: "unshow")
            }
            
            isShowPassword = !isShowPassword
        }else{
            if(isShowConfirmPassword == true) {
                tf_Conformpassword.isSecureTextEntry = false
                imgShowConfirmpassword.image = UIImage(named: "pass")
            } else {
                tf_Conformpassword.isSecureTextEntry = true
                imgShowConfirmpassword.image = UIImage(named: "unshow")
            }
            
            isShowConfirmPassword = !isShowConfirmPassword
        }

    }
    
    @IBAction func bt_save(_ sender: Any) {
        var parameters: [String: Any] = [:]
        parameters["password"] = self.tf_password.text ?? ""
        parameters["password_confirmation"] = self.tf_Conformpassword.text ?? ""
        parameters["username"] = self.name
        parameters["code"] = self.codeReset
        
        _ = WebRequests.setup(controller: self).prepare(query: "auth/reset-password", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200 {
                    
                    self.showAlert(title: "Error".localized, message:Status.message!)
                    return
                }else if  Status.status == 200{
//                    let vc:TTabBarViewController = AppDelegate.sb_main.instanceVC()
//                    vc.modalPresentationStyle = .fullScreen
//                    self.present(vc, animated: true, completion: nil)
                    let nav:CustomNavigationBar = AppDelegate.sb_main.instanceVC()
                    let vc:LoginVC = AppDelegate.sb_main.instanceVC()
                    nav.pushViewController(vc, animated: true)
                    nav.modalPresentationStyle = .fullScreen
                    self.present(nav, animated: true, completion: nil)

                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            
        }
        
    }
    
    
    
    
}
