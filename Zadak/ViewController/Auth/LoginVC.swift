//
//  LoginVC.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 2/19/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class LoginVC: SuperViewController {
    
    @IBOutlet weak var tf_phone: UITextField!
    @IBOutlet weak var tf_password: UITextField!
    @IBOutlet weak var imgShow: UIImageView!

    
    var isShowPassword = true

    override func viewDidLoad() {
        super.viewDidLoad()
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.isDark = false
        tf_password.isSecureTextEntry = true
        imgShow.image = UIImage(named: "unshow")
        #if DEBUG
        //hasan5_007@hotmail.com
        //123456789
//            self.tf_phone.text  = "98787654"
//            self.tf_password.text = "123123123"
        self.tf_phone.text  = "hasan5_007@hotmail.com"
        self.tf_password.text = "123456789"
//         self.tf_phone.text  = "zaadtest2020@zaad.om"
//         self.tf_password.text = "Zaadtest@2020"
        #endif

    }
    
    func getToken(){
        let token =  UserDefaults.standard.string(forKey: "token")
        var parameters: [String: Any] = [:]
        parameters["token"] = token
        
        _ = WebRequests.setup(controller: self).prepare(query: "fcm/token", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
           
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200 {
                    
                    self.showAlert(title: "Error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
    }
    
    @IBAction func bt_Skip(_ sender: Any) {
        let vc:TTabBarViewController = AppDelegate.sb_main.instanceVC()
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)

    }
    
    @IBAction func didTab_SeePassword(_ sender: Any) {
         
         if(isShowPassword == true) {
             tf_password.isSecureTextEntry = false
             imgShow.image = UIImage(named: "pass")
         } else {
             tf_password.isSecureTextEntry = true
             imgShow.image = UIImage(named: "unshow")
         }
         
         isShowPassword = !isShowPassword
         
     }
    
    @IBAction func bt_login(_ sender: Any) {
        
         view.endEditing(true)
        
        guard let mobile = self.tf_phone.text, !mobile.isEmpty else{
            self.showAlert(title: "error".localized, message: "The phone cannot be empty.".localized)
            return
        }
        guard let password = self.tf_password.text, !password.isEmpty else{
                   self.showAlert(title: "error".localized, message: "Password cannot be empty.".localized)
                   return
        }
               
        if password.count < 6 {
            showAlert(title: "error".localized, message: "Password must be at least 6 characters long".localized)
                   return
        }
//
//        if mobile.count < 9 ||  mobile.count > 10  {
//                     self.showAlert(title: "Error".localized, message: "Mobile Not Vaild".localized)
//                     return
//
//        }
//            var MobileCheck = mobile
//            if mobile.count > 9 && mobile.prefix(1) == "0" {
//                    let start = String.Index(utf16Offset: 1, in: mobile)
//                    let end = String.Index(utf16Offset: 10, in: mobile)
//                    MobileCheck = String(mobile[start..<end])
//
//                }else if mobile.count > 9 && mobile.prefix(1) != "0" {
//                     self.showAlert(title: "Error".localized, message: "Mobile Not Vaild".localized)
//                }
        
        var parameters: [String: Any] = [:]
        parameters["username"] = self.tf_phone.text ?? ""
        parameters["password"] = self.tf_password.text ?? ""
        
        _ = WebRequests.setup(controller: self).prepare(query: "auth/login", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
                 
                  do {
                      let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                      if Status.status != 200 {
                          
                          self.showAlert(title: "Error".localized, message:Status.message!)
                          return
                      }else{
                        self.showAlert(title: "".localized, message:Status.message!)

                    }
                      
                  }catch let jsonErr {
                      print("Error serializing  respone json", jsonErr)
                  }
                  do {
                      let Status =  try JSONDecoder().decode(UserObject.self, from: response.data!)
                     CurrentUser.userInfo = Status.data
                      if CurrentUser.userInfo != nil{
                         self.getToken()
                         let vc:TTabBarViewController = AppDelegate.sb_main.instanceVC()
                         vc.modalPresentationStyle = .fullScreen
                         self.present(vc, animated: true, completion: nil)
                      }
                      
                  } catch let jsonErr {
                      print("Error serializing  respone json", jsonErr)
                  }
                  
              }
              
        
      
    }
    
    @IBAction func bt_forgatepassword(_ sender: Any) {
        let vc:ForgatePasswordVC = AppDelegate.sb_main.instanceVC()
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func bt_register(_ sender: Any) {
        let vc:RegisterVC = AppDelegate.sb_main.instanceVC()
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
        
    }

   

}
