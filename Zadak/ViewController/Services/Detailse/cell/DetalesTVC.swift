//
//  DetalesTVC.swift
//  Zadak
//
//  Created by osamaaassi on 17/03/2021.
//  Copyright © 2021  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class DetalesTVC: UITableViewCell {

    
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblprice: UILabel!
    @IBOutlet weak var lblFree: UILabel!
    @IBOutlet weak var lblOptional: UILabel!
    
   

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var featuresData:FeaturesService? {
        didSet{
            lblDescription.text = featuresData?.descriptionValue ?? ""
            lblprice.text = featuresData?.price ?? "0"
            if(featuresData?.free == 1){
                lblFree.text =  "Free".localized
            }
            else{
                lblFree.text =  "not free".localized
                
            }
            
            if(featuresData?.optional == 1){
                lblOptional.text = "optional".localized
            }
            else{
                lblOptional.text =  "non-choice".localized
            }
           
        }
        
    }
    
}
