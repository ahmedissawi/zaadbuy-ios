//
//  DetalesStroerVC.swift
//  Zadak
//
//  Created by osamaaassi on 13/03/2021.
//  Copyright © 2021  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class DetalesServiceVC: SuperViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var imgseller: UIImageView!
    @IBOutlet weak var imgCart: UIImageView!
    @IBOutlet weak var lblNameSellr: UILabel!
    @IBOutlet weak var lblNameServies: UILabel!
    @IBOutlet weak var lblTypeServies: UILabel!
    @IBOutlet weak var lblStoreName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblRegularPrice: UILabel!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblShortDescription: UILabel!
    @IBOutlet weak var lblQuantity: UILabel!
    @IBOutlet weak var viewFooter: UIView!

    var ExpandInt:Int?
    var ProducatTitle = ["تفاصيل الخدمة","المميزات","التقييمات"]
    
    var ID : Int?
    var servicesData:DataItemServiceDetails?
    var featuresService = [FeaturesService]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getDetales()
        tableView.tableFooterView = viewFooter
        tableView.registerCell(id: "DetalesTVC")
        tableView.registerCell(id: "DeatileProducatCell")
        tableView.registerCell(id: "FeaturesProducatCell")
        tableView.registerCell(id: "ProductSpecificationCell")
        let nibHeader = UINib(nibName: "HeaderDeatilesCell", bundle: nil)
        tableView.register(nibHeader, forCellReuseIdentifier: "HeaderDeatilesCell")
        
        //tableView.tableFooterView = UIView.init(frame: .zero)

        NotificationCenter.default.addObserver(self, selector:#selector(UpdateCellService(notification:)), name: Notification.Name("UpdateCellService"), object: nil)
        
        
        let navgtion = self.navigationController as! CustomNavigationBar
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(descriptor: UIFontDescriptor(name: "FFShamelFamilySemiRoundBook-S", size: 17), size: 17),NSAttributedString.Key.foregroundColor: UIColor.white]
        self.title = "Service Details".localized
        navigationController?.navigationBar.barTintColor = "FF6A2A".color
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        
    }
    override func awakeFromNib() {
            super.awakeFromNib()
        }
    
    @objc func UpdateCellService(notification: NSNotification)  {
        self.tableView.reloadData()
    }
    
    @IBAction func bt_store(_ sender: Any) {
        let vc:DetailsStoreVC = DetailsStoreVC.loadFromNib()
        vc.ID = servicesData?.store?.id ?? 0
        vc.isFromServec = true
        vc.hidesBottomBarWhenPushed = true
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func bt_seller(_ sender: Any) {
        let vc:MessagesAllVC = AppDelegate.sb_main.instanceVC()
        vc.IDMessgae = self.servicesData?.seller?.id
        vc.Storename = self.servicesData?.seller?.name
        vc.hidesBottomBarWhenPushed = true
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func bt_payment(_ sender: Any) {
        let vc:PaymentServiceVC = PaymentServiceVC.loadFromNib()
        vc.servicesData = servicesData
        vc.hidesBottomBarWhenPushed = true
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func getDetales()
    {
        
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
       
        _ = WebRequests.setup(controller: self).prepare(query: "services/\(ID ?? 0)", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(ServiceItemDetails.self, from: response.data!)
                if Status.status != 200 {
                    self.showAlert(title: "Error".localized, message:Status.message!)
                    return
                }
                else if Status.status == 200{
                    
                    self.servicesData = Status.data!
                    self.featuresService += self.servicesData!.features!
                   
                    NotificationCenter.default.post(name: Notification.Name("UpdateCellService"), object: nil)
                    
                    self.showData()
                }
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
        
    }
    
    
    func showData() {
        self.imgseller.sd_custom(url: self.servicesData?.src ?? "")
        self.imgCart.sd_custom(url: self.servicesData?.src ?? "")
        self.lblNameServies.text = self.servicesData?.title ?? ""
        self.lblStoreName.text = self.servicesData?.store?.name ?? ""
        self.lblNameSellr.text = self.servicesData?.seller?.name ?? ""
        self.lblTypeServies.text = self.servicesData?.category?.name ?? ""
        self.lblShortDescription.text = self.servicesData?.shortDescription ?? ""
        self.lblPrice.text = "\(servicesData?.prices?.description ?? "0")" + " " + "O.R".localized

    }

}

extension DetalesServiceVC:UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return ProducatTitle.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
      
        if section == 0 ,ExpandInt == section {
            return 1
        }
        if section == 1 ,ExpandInt == section {
            return featuresService.count
        }
        if section == 2 ,ExpandInt == section {
            return 1
        }
        
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "DeatileProducatCell", for: indexPath) as! DeatileProducatCell
            let obj = servicesData?.descriptionValue
            cell.lblDecrbtion.text = obj?.htmlToString
            return cell
            
        }
        if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetalesTVC", for: indexPath) as! DetalesTVC
            cell.featuresData = featuresService[indexPath.row]

            return cell
        }
        if indexPath.section == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProductSpecificationCell", for: indexPath) as! ProductSpecificationCell
            return cell
        }
        return UITableViewCell()
    }

    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let obj = ProducatTitle[section]
        let headerView = tableView.dequeueReusableCell(withIdentifier: "HeaderDeatilesCell") as! HeaderDeatilesCell
        
        headerView.lbltitle.text = obj
        headerView.bt_Header.tag =  section
        if section == 2{
            if obj != ""{
                headerView.icDrop.isHidden = true
            }else{
                headerView.icDrop.isHidden = false
            }
        }
          
        
        if section == 2{
            let rating = servicesData?.rate
                      DispatchQueue.main.async{
                          if rating != nil{
                            headerView.ViewRate.rating = Double(rating!)!
                          }

                      }
            headerView.ViewRate.isHidden = false
        }else{
            headerView.ViewRate.isHidden = true
        }
        
        headerView.bt_Header.addTarget(self, action: #selector(self.ExpandCell), for: UIControl.Event.touchUpInside)
        
        return headerView
    }
    @objc func ExpandCell(_ sender:UIButton ){
        ExpandInt =  sender.tag
        tableView.reloadData()
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
            return UITableView.automaticDimension
        }
        
        if indexPath.section == 1 {
            return 130
        }
        
        if indexPath.section == 2 {
            
            return 0
        }
        return CGFloat()
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 2{
           return 55
        }else{
            return 55
            
        }
    }

    
}
