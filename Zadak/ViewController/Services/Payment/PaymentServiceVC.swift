//
//  PaymentServiceVC.swift
//  Zadak
//
//  Created by osamaaassi on 24/03/2021.
//  Copyright © 2021  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
class PaymentServiceVC: SuperViewController {
    
    
    @IBOutlet weak var lblNameServie:UILabel!
    @IBOutlet weak var lbldescrptionServie: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblPaymentMethod:UITextField!
    @IBOutlet weak var lblRequiredDate: UITextField!
    @IBOutlet weak var lblAddress: UITextField!
    @IBOutlet weak var lblFromTime: UITextField!
    @IBOutlet weak var lblToTime: UITextField!
    @IBOutlet weak var lblDescription: UITextField!
    
   
   
    @IBOutlet var viewOne: NSLayoutConstraint!
    @IBOutlet var viewtwo: NSLayoutConstraint!
    @IBOutlet var viewThree: NSLayoutConstraint!
    
    var servicesData:DataItemServiceDetails?
    
    var sellerID = 0
    
    var selectPaymentID:Int?
    var selectAdressID:Int?
    var date:String?
    var fromTime:String?
    var toTime:String?
    
    
    var addrssItems = [AddressData]()
    
    //Payment
    var listMethod = [PayMethodsStruct]()
    var selectSerivce :PayMethodsStruct?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getAddress()
        getPaymentMothed()
        
        viewtwo.constant = 0
        viewThree.constant = 0
        
        lblNameServie.text = servicesData?.title ?? ""
        lbldescrptionServie.text = servicesData?.shortDescription ?? ""
        lblPrice.text = servicesData?.regularPrice ?? ""
        img.sd_custom(url: servicesData?.src ?? "")
    }
    
    
    func getAddress(){
        _ = WebRequests.setup(controller: nil).prepare(query: "addresses", method: HTTPMethod.get).start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            do {
                let Status =  try JSONDecoder().decode(AddressInfo.self, from: response.data!)
                self.addrssItems = Status.data!
                
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
    }
    
    
    
    @IBAction func addTappedAddress(_ sender:UIButton)
    {
        ActionSheetStringPicker.show(withTitle:  "Choose address".localized, rows: self.addrssItems.map { $0.addressLine1 as Any } , initialSelection: 0, doneBlock: {
              picker, value, index in
            if let Value = index {
                self.lblAddress.text = Value as? String
                self.selectAdressID = self.addrssItems[value].id ?? 0
                }
                    return
            
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        
    }
    
    
    @IBAction func addTappedPay(_ sender:UIButton)
    {
        ActionSheetStringPicker.show(withTitle:  "Choose payment method".localized, rows: self.listMethod.map { $0.name as Any } , initialSelection: 0, doneBlock: {
              picker, value, index in
            if let Value = index {
                self.lblPaymentMethod.text = Value as? String
                self.selectPaymentID = self.listMethod[value].id ?? 0
                self.viewtwo.constant = 345
                }
                    return
            
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        
    }
    
    
    
    func getPaymentMothed() {
        _ = WebRequests.setup(controller: nil).prepare(query: "seller/\(servicesData?.sellerId ?? 0)/payment-methods", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            do {
                let Status =  try JSONDecoder().decode(PayMethodsClass.self, from: response.data!)
                self.listMethod = Status.data!
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
    }
    
    
    @IBAction func dp(_ sender: Any) {
        self.view.endEditing(true)
        let alert = UIAlertController(style: .actionSheet, title: "Please select date".localized)
        alert.addDatePicker(mode: .date, date: Date(), minimumDate: Date(), maximumDate: nil) {[weak self] date in
            
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "yyyy-M-dd"
            let myString = formatter.string(from: date)
            self?.date = myString
            self?.lblRequiredDate.text = self?.date

        }
        alert.addAction(title: "Done".localized, style: .cancel)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func timePicker(_ sender: Any) {
        self.view.endEditing(true)
        let alert = UIAlertController(style: .actionSheet, title: "Please select time".localized)
        alert.addDatePicker(mode: .time, date: Date(), minimumDate: Date(), maximumDate: nil) {[weak self] date in
            
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "hh:mm"
            let myString = formatter.string(from: date) // string purpose I add here
            
            print(myString)
            
            self?.fromTime = myString
            self?.lblFromTime.text = self?.fromTime
        }
        alert.addAction(title: "Done".localized, style: .cancel)
        //alert.show()
        self.present(alert, animated: true, completion: nil)
        
    }
    @IBAction func timePickerTo(_ sender: Any) {
        self.view.endEditing(true)
        let alert = UIAlertController(style: .actionSheet, title: "Please select time".localized)
        alert.addDatePicker(mode: .time, date: Date(), minimumDate: Date(), maximumDate: nil) {[weak self] date in
            
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "hh:mm"
            let myString = formatter.string(from: date) // string purpose I add here
            self?.toTime = myString
            self?.lblToTime.text = myString
            self?.viewThree.constant = 230
        }
        alert.addAction(title: "Done".localized, style: .cancel)
        //alert.show()
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @objc func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        lblRequiredDate.text = dateFormatter.string(from: sender.date)
    }
    
    
    @IBAction func paymentSend(_ sender: Any) {
        
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        
       
        guard let address = self.lblAddress.text, !address.isEmpty else{
            self.showAlert(title: "error".localized, message: "Choose address".localized)
            return
        }
        
        guard let paymentMethod = self.lblPaymentMethod.text, !paymentMethod.isEmpty else{
            self.showAlert(title: "error".localized, message: "Choose payment method" .localized)
            return
        }
        
        guard let dates = self.date, !dates.isEmpty else{
            self.showAlert(title: "error".localized, message: "Please select date".localized)
            return
        }
        guard let fromTimes = self.fromTime, !fromTimes.isEmpty else{
            self.showAlert(title: "error".localized, message: "Please select time".localized)
            return
        }
        guard let time = self.toTime, !time.isEmpty else{
            self.showAlert(title: "error".localized, message: "Please select time".localized)
            return
        }
       
        var parameters: [String: Any] = [:]
        parameters["service_id"] = servicesData?.id ?? 0
        parameters["payment_method_id"] = selectPaymentID ?? 0
        parameters["address_id"] = selectAdressID ?? 0
        parameters["description"] = servicesData?.descriptionValue ?? ""
        parameters["required_date"] =  date
        parameters["from_time"] = fromTimes
        parameters["to_time"] = time
        
        _ = WebRequests.setup(controller: self).prepare(query: "service-orders", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200 {
                    self.showAlert(title: "Error".localized, message:Status.message!)
                    return
                }
                else if Status.status == 200{
                    self.showAlert(title: "Done successfully".localized, message:Status.message!)
                    self.navigationController?.popViewController(animated: true)
                }
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }

        
        
    }
    
}
