//
//  ServicesCell.swift
//  Zadak
//
//  Created by osamaaassi on 12/03/2021.
//  Copyright © 2021  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class ServicesCell: UITableViewCell {

    
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lblstore: UILabel!
    @IBOutlet weak var lblprice: UILabel!
    @IBOutlet weak var imgServices: UIImageView!
    
    var DetailesShowTapped: (() -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
        
    }

    var servicesitem : ItemsServices? {
        didSet{
            
            lbltitle.text = self.servicesitem?.title ?? ""
            lblstore.text = self.servicesitem?.store?.name ?? ""
            lblprice.text = "\(servicesitem?.regularPrice ?? 0)" + " " + "O.R".localized
            self.imgServices.sd_custom(url: servicesitem?.image ?? "")
            
        }
        
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func detailesShow(_ sender: UISwitch) {
           DetailesShowTapped?()
       }
    
}
