//
//  ServicesVC.swift
//  Zadak
//
//  Created by osamaaassi on 12/03/2021.
//  Copyright © 2021  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import ESPullToRefresh
import ActionSheetPicker_3_0



class ServicesVC: SuperViewController ,ServiecDelegat{
    func selectedDoneStore(id: Int, name: String) {
        selectedStoreID = id
        lblFilterStore.text = name
        viewFilterStore.isHidden = false
        viewCancelStore.isHidden = false
        servicesitems.removeAll()
        getServices()
    }
    
    
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var lblStatus:UILabel!
    @IBOutlet weak var lblFilterStore:UILabel!
    
    @IBOutlet weak var tfTypeCategory:UILabel!
    
    @IBOutlet weak var TypeService: UIView!
    @IBOutlet weak var viewFilterStore: UIView!
    @IBOutlet weak var viewCancelStore: UIButton!
  
    var servicesData:DataServices?
    var servicesitems = [ItemsServices]()
    
    var dataCategories:DataCategories?
    
    var dataCategorie = [Childrens]()
    var childrens = [Childrens]()
    
    var dataStores:DataStores?
    var stores = [Stores]()
    
    
    var currentpage = 1
   
    var selectedTypeActivityID:Int?
    var selectedCategoryID:Int?
    
    var selectedStoreID:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.tableFooterView = UIView()
        tableView.registerCell(id: "ServicesCell")
        viewFilterStore.isHidden = true
        viewCancelStore.isHidden = true
        
        getServices()
        getStatus()
        
        
        TypeService.isHidden = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(UpdateServices(notification:)), name: NSNotification.Name("UpdateServices"), object: nil)
        
        self.tableView.es.addPullToRefresh {
            
            self.currentpage = 1
            self.servicesitems.removeAll()
            self.getServices()
            self.hideIndicator()
        }
        
        self.tableView.es.addInfiniteScrolling {
            self.currentpage += 1
            self.getServices() // next page
            self.tableView.estimatedRowHeight = UITableView.automaticDimension
            self.hideIndicator()
        }
        
        let navgtion = self.navigationController as! CustomNavigationBar
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(descriptor: UIFontDescriptor(name: "FFShamelFamilySemiRoundBook-S", size: 17), size: 17),NSAttributedString.Key.foregroundColor: UIColor.white]
        self.title = "Services".localized
        navigationController?.navigationBar.barTintColor = "FF6A2A".color
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
    }
    
    @objc func UpdateServices(notification: NSNotification)  {
        
        servicesitems.removeAll()
        getServices()
        
        
    }
    
    func getServices(){
       
        
        guard Helper.isConnectedToNetwork() else {
            self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.tableView, refershSelector: #selector(self.didRefersh))
            self.emptyView?.firstLabel.text = "There is no internet connection".localized
            return }
        
        var parameters: [String: Any] = [:]
        if selectedCategoryID != nil{
            parameters["category_id"] = selectedCategoryID?.description
        }
        if selectedStoreID != nil{
            parameters["store_id"] = selectedStoreID?.description
        }
        
        _ = WebRequests.setup(controller: self).prepare(query: "services?page=\(currentpage)", method: HTTPMethod.get,parameters: parameters).start(){ (response, error) in
            
            
            self.tableView.es.stopLoadingMore()
            self.tableView.es.stopPullToRefresh()
            
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            
            do {
                let Status =  try JSONDecoder().decode(Services.self, from: response.data!)
                self.servicesData = Status.data
                self.servicesitems += self.servicesData!.items!
                if self.servicesData?.nextPageUrl == nil {
                    self.tableView.es.stopLoadingMore()
                    self.tableView.es.noticeNoMoreData()
                }
                
                if self.servicesitems.count == 0{
                    self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.tableView, refershSelector: #selector(self.didRefersh))
                    self.emptyView?.firstLabel.text = "No services to display".localized
                    self.servicesitems.removeAll()
                    self.tableView.reloadData()
                    
                }else{
                    self.tableView.tableFooterView = nil
                    
                }
                self.tableInit()
                self.tableView.reloadData()
                
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
    }

    @IBAction func didRefersh(){
        servicesitems.removeAll()
        getServices()
    }
  

}

extension ServicesVC {
 
    func getStatus(){
        
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        
        _ = WebRequests.setup(controller: self).prepare(query: "categories/2", method: HTTPMethod.get,isAuthRequired: false).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            do {
                let Status =  try JSONDecoder().decode(CategoriesServices.self, from: response.data!)
                
                self.dataCategories = Status.data!
                self.dataCategorie += self.dataCategories!.childrens!
           
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
        
    }
    
    @IBAction func didTab_CancelStore(_ sender: UIButton) {
        sender.isHidden = true
        sender.isEnabled = true
        self.viewFilterStore.isHidden = true
      //  self.viewCancelStore.isHidden = true
        self.selectedStoreID = nil
        self.lblFilterStore.text = ""
        self.selectedTypeActivityID = nil
        self.selectedCategoryID = nil
        self.getServices()
       

        }
        
    @IBAction func didTab_Status(_ sender: UIButton) {
        self.lblStatus.text = ""
        self.childrens.removeAll()
        self.selectedCategoryID = nil
        if selectedCategoryID  == nil {
            self.TypeService.isHidden = true
        }
        
        ActionSheetStringPicker.show(withTitle: "Classification of services".localized, rows: self.dataCategorie.map { $0.title as Any }
            , initialSelection: 0, doneBlock: {
                picker, value, index in
                if let Value = index {
                    self.lblStatus.text = Value as? String
                    self.selectedTypeActivityID = self.dataCategorie[value].id ?? 0
      
                    if let object = self.dataCategorie.filter({ $0.id == self.selectedTypeActivityID }).first{
                        if object.childrens != nil && object.childrens?.count != 0{
                            self.childrens = object.childrens!
                            self.TypeService.isHidden = false
                        }
                        else{
                            self.tableView.reloadData()
//                            NotificationCenter.default.post(name: Notification.Name("UpdateServices"), object: nil)
                            self.TypeService.isHidden = true
                        }
                           
                    }
                    
                }
                
                return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        
    }
    
    @IBAction func didTab_typeCategorie(_ sender: UIButton) {
         
        ActionSheetStringPicker.show(withTitle: "Service type".localized, rows: self.childrens.map { $0.title as Any }
            , initialSelection: 0, doneBlock: {
                picker, value, index in
                if let Value = index {
                    self.tfTypeCategory.text = Value as? String
                    self.selectedCategoryID = self.childrens[value].id ?? 0
                    NotificationCenter.default.post(name: Notification.Name("UpdateServices"), object: nil)

                }
                return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
    }
    
    @IBAction func didTab_Store(_ sender: UIButton) {
        
        let vc:StoreVC = StoreVC.loadFromNib()
        vc.delegate = self
        vc.is_filter = true
        vc.modalPresentationStyle = .fullScreen
        servicesitems.removeAll()
        navigationController?.pushViewController(vc, animated: true)
        

    }
    
}
extension ServicesVC :UITableViewDelegate,UITableViewDataSource{
    
    func tableInit() {
        tableView.dataSource = self
        tableView.delegate = self
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (servicesitems.count != 0){
            return servicesitems.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ServicesCell", for: indexPath) as! ServicesCell
        
          
      cell.servicesitem = servicesitems[indexPath.row]
        
        cell.DetailesShowTapped =  { [ weak self] in
       
        let detalesServiceVC:DetalesServiceVC = DetalesServiceVC.loadFromNib()
          detalesServiceVC.ID = self!.servicesitems[indexPath.row].id ?? 0
          self!.navigationController?.pushViewController(detalesServiceVC, animated: true)
            
        }
            return cell

       
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
           return 135
       }
  
}
