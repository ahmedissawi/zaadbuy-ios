//
//  LanguageVC.swift
//  ZaadSeller
//
//  Created by osamaaassi on 27/07/2021.
//  Copyright © 2021  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class LanguageVC: SuperViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setLogotitle(sender: self)
        self.navigationItem.setHidesBackButton(true, animated: false)
        navigationController?.navigationBar.barTintColor = "FF6A2A".color
        CurrentUser.firtTime = true
        // Do any additional setup after loading the view.
    }


    
    @IBAction func tapAr(_ sender: Any) {
      CurrentUser.SelectfirtTime = true
      MOLH.setLanguageTo("ar")
      MOLH.reset(transition: .transitionCrossDissolve, duration: 0.25)

    }
    @IBAction func tapEn(_ sender: Any) {
        CurrentUser.SelectfirtTime = true
        MOLH.setLanguageTo("en")
        MOLH.reset(transition: .transitionCrossDissolve, duration: 0.25)

    }

}
