//
//  OnBordingVC.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 2/19/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import FSPagerView

class OnBordingVC: UIViewController {
    
    
    @IBOutlet weak var pageControlle: FSPageControl!{
        didSet{
            pageControlle.numberOfPages =  3
            pageControlle.contentHorizontalAlignment = .center
            pageControlle.setImage(UIImage(named: "unselectedZadak"), for: .normal)
            pageControlle.setImage(UIImage(named: "selectedzadak"), for: .selected)
        }
        
    }
    
    @IBOutlet weak var pagerView: FSPagerView!{
        didSet{
            self.pagerView.isScrollEnabled = true
            self.pagerView.register(UINib(nibName: "OnBordingCell", bundle: Bundle.main), forCellWithReuseIdentifier: "OnBordingCell")
            
        }
    }
    
    
    var listArray = [String]()
    var listTextArray = [String]()
    var listdisArray = [String]()
    var indexnext:Int?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupdata()
        CurrentUser.firtTime = false
    }
    
    
    
    func setupdata(){
        listArray = ["img1","img2","img3"]
        listTextArray = ["Welcome to Zad Oman".localized,"Welcome to Zad Oman".localized,"Welcome to Zad Oman".localized]
        listdisArray = ["Opening a window for small and medium enterprises, productive families and home business owners to market their products within the Sultanate of Oman.".localized,"Provides electronic payment, delivery and product evaluation service by the buyer.".localized,"Ensuring the satisfaction and comfort of our customers while shopping.".localized]
        
    }
     
    
    @IBAction func bt_Next(_ sender: Any) {
        let nextIndex = pagerView.currentIndex + 1 < numberOfItems(in:self.pagerView) ? pagerView.currentIndex + 1 : 0
        indexnext = nextIndex
        pagerView.scrollToItem(at: nextIndex, animated: true)
        if nextIndex == 0{
            let vc:LoginVC = AppDelegate.sb_main.instanceVC()
            let nav:CustomNavigationBar = AppDelegate.sb_main.instanceVC()
            nav.viewControllers = [vc]
            nav.modalPresentationStyle = .fullScreen
            self.present(nav, animated: true, completion: nil)
        }
    }
    
    
    
    
    
}
extension OnBordingVC:FSPagerViewDelegate,FSPagerViewDataSource{
    
    
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return listArray.count
    }
    
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "OnBordingCell", at: index) as! OnBordingCell
        
        cell.lbname.text = listTextArray[index]
        cell.lblDesc.text  = listdisArray[index]
        cell.img.image = UIImage(named: listArray[index])
        
        return cell
    }
    
    
    func pagerViewDidEndScrollAnimation(_ pagerView: FSPagerView) {
        self.pageControlle.currentPage = pagerView.currentIndex
    }
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        self.pageControlle.currentPage = targetIndex
        if targetIndex == 0{
            let vc:LoginVC = AppDelegate.sb_main.instanceVC()
            let nav:CustomNavigationBar = AppDelegate.sb_main.instanceVC()
            nav.viewControllers = [vc]
            nav.modalPresentationStyle = .fullScreen
            self.present(nav, animated: true, completion: nil)
        }
        
    }

    
}
