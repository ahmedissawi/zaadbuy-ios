//
//  CheckoutClass.swift
//
//  Created by Ahmed ios on 6/14/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct CheckoutClass: Codable {

  enum CodingKeys: String, CodingKey {
    case code
    case locale
    case message
    case data
    case status
  }

  var code: Int?
  var locale: String?
  var message: String?
  var data: [CheckoutStruct]?
  var status: Int?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(String.self, forKey:.code) {
        code = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.code) {
        code = value
    }
    if let value = try? container.decode(Int.self, forKey:.locale) {                       
        locale = String(value)
        }else if let value = try? container.decode(String.self, forKey:.locale) {
        locale = value
    }
    if let value = try? container.decode(Int.self, forKey:.message) {                       
        message = String(value)
    }else if let value = try? container.decode(String.self, forKey:.message) {
        message = value
    }
    data = try container.decodeIfPresent([CheckoutStruct].self, forKey: .data)
    if let value = try? container.decode(String.self, forKey:.status) {
        status = Int(value)
    } else if let value = try? container.decode(Int.self, forKey:.status) {
        status = value 
    }
  }

}
