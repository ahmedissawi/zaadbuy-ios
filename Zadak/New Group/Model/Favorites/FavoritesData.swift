//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 6/15/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct FavoritesData: Codable {
    
    enum CodingKeys: String, CodingKey {
        case price
        case image
        case id
        case title
        case itemId = "item_id"
        case shortDescription = "short_description"
    }
    
    var price: Int?
    var image: String?
    var id: Int?
    var title: String?
    var itemId: Int?
    var shortDescription: String?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(String.self, forKey:.price) {
            price = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.price) {
            price = value
        }
        if let value = try? container.decode(Int.self, forKey:.image) {
            image = String(value)
        }else if let value = try? container.decode(String.self, forKey:.image) {
            image = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(Int.self, forKey:.title) {
            title = String(value)
        }else if let value = try? container.decode(String.self, forKey:.title) {
            title = value
        }
        if let value = try? container.decode(String.self, forKey:.itemId) {
            itemId = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.itemId) {
            itemId = value
        }
        if let value = try? container.decode(Int.self, forKey:.shortDescription) {
            shortDescription = String(value)
        }else if let value = try? container.decode(String.self, forKey:.shortDescription) {
            shortDescription = value                                                                                     
        }
    }
    
}
