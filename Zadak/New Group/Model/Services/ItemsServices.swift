//
//  Items.swift
//
//  Created by osamaaassi on 12/03/2021
//  Copyright (c) . All rights reserved.
//

import Foundation

struct ItemsServices: Codable {

  enum CodingKeys: String, CodingKey {
    case title
    case price
    case category
    case seller
    case id
    case store
    case image
    case regularPrice = "regular_price"
  }

  var title: String?
  var price: Int?
  var category: CategoryServices?
  var seller: SellerServices?
  var id: Int?
  var store: StoreServices?
  var image: String?
  var regularPrice: Int?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(Int.self, forKey:.title) {                       
title = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.title) {
 title = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.price) {
 price = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.price) {
price = value 
}
    category = try container.decodeIfPresent(CategoryServices.self, forKey: .category)
    seller = try container.decodeIfPresent(SellerServices.self, forKey: .seller)
    if let value = try? container.decode(String.self, forKey:.id) {
 id = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.id) {
id = value 
}
    store = try container.decodeIfPresent(StoreServices.self, forKey: .store)
    if let value = try? container.decode(Int.self, forKey:.image) {                       
image = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.image) {
 image = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.regularPrice) {
 regularPrice = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.regularPrice) {
regularPrice = value 
}
  }

}
