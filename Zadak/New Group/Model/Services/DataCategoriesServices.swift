//
//  DataCategoriesServices.swift
//  Zadak
//
//  Created by osamaaassi on 23/03/2021.
//  Copyright © 2021  Ahmed’s MacBook Pro. All rights reserved.
//

import Foundation

struct DataCategoriesServices: Codable {
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case title
        case items
    }
    
    var id:Int?
    var name:String?
    var title:String?
    var items: [ItemsServices]?
    
    
    
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        items = try container.decodeIfPresent([ItemsServices].self, forKey: .items)
        
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        
        if let value = try? container.decode(Int.self, forKey:.name) {
            name = String(value)
        }else if let value = try? container.decode(String.self, forKey:.name) {
        name = value
        }
        if let value = try? container.decode(Int.self, forKey:.title) {
            title = String(value)
        }else if let value = try? container.decode(String.self, forKey:.title) {
            title = value
        }
        
    }
    
}
