//
//  Options.swift
//
//  Created by osamaaassi on 12/03/2021
//  Copyright (c) . All rights reserved.
//

import Foundation

struct OptionsServices: Codable {
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case children
        case title
        case valueData = "value"
        case slug
    }
    
    var id: Int?
    var name: String?
    var children: [OptionsServices]?
    var title: String?
    var valueData: String?
    var slug: String?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(Int.self, forKey:.name) {
            name = String(value)
        }else if let value = try? container.decode(String.self, forKey:.name) {
            name = value
        }
        children = try container.decodeIfPresent([OptionsServices].self, forKey: .children)
        
        if let value = try? container.decode(Int.self, forKey:.valueData) {
            valueData = String(value)
        }else if let value = try? container.decode(String.self, forKey:.valueData) {
            valueData = value
        }
        
        if let value = try? container.decode(Int.self, forKey:.title) {
            title = String(value)
        }else if let value = try? container.decode(String.self, forKey:.title) {
            title = value
        }
        if let value = try? container.decode(Int.self, forKey:.slug) {
            slug = String(value)
        }else if let value = try? container.decode(String.self, forKey:.slug) {
            slug = value
        }
    }
    
}
