//
//  PaymentMethod.swift
//
//  Created by  Ahmed’s MacBook Pro on 7/13/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct ItemsServicess: Codable {

  enum CodingKeys: String, CodingKey {
    case items
   
  }

  var items: [PaymentStatus]?




  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    items = try container.decodeIfPresent([PaymentStatus].self, forKey: .resources)


  }

}
