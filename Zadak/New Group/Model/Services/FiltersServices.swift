//
//  Filters.swift
//
//  Created by osamaaassi on 12/03/2021
//  Copyright (c) . All rights reserved.
//

import Foundation

struct FiltersServices: Codable {

  enum CodingKeys: String, CodingKey {
    case slug
    case title
    case type
    case options
  }

  var slug: String?
  var title: String?
  var type: String?
  var options: [OptionsServices]?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(Int.self, forKey:.slug) {                       
slug = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.slug) {
 slug = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.title) {                       
title = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.title) {
 title = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.type) {                       
type = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.type) {
 type = value                                                                                     
}
    options = try container.decodeIfPresent([OptionsServices].self, forKey: .options)
  }

}
