//
//  ServiceItemDetails.swift
//
//  Created by osamaaassi on 17/03/2021
//  Copyright (c) . All rights reserved.
//

import Foundation

struct ServiceItemDetails: Codable {

  enum CodingKeys: String, CodingKey {
    case message
    case status
    case data
    case locale
    case code
  }

  var message: String?
  var status: Int?
  var data: DataItemServiceDetails?
  var locale: String?
  var code: Int?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(Int.self, forKey:.message) {                       
message = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.message) {
 message = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.status) {
 status = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.status) {
status = value 
}
    data = try container.decodeIfPresent(DataItemServiceDetails.self, forKey: .data)
    if let value = try? container.decode(Int.self, forKey:.locale) {                       
locale = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.locale) {
 locale = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.code) {
 code = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.code) {
code = value 
}
  }

}
