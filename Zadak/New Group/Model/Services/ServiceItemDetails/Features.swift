//
//  Features.swift
//
//  Created by osamaaassi on 17/03/2021
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Features: Codable {

  enum CodingKeys: String, CodingKey {
    case price
    case optional
    case descriptionValue = "description"
    case free
    case id
    case serviceId = "service_id"
  }

  var price: String?
  var optional: Int?
  var descriptionValue: String?
  var free: Int?
  var id: Int?
  var serviceId: Int?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(Int.self, forKey:.price) {                       
price = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.price) {
 price = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.optional) {
 optional = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.optional) {
optional = value 
}
    if let value = try? container.decode(Int.self, forKey:.descriptionValue) {                       
descriptionValue = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.descriptionValue) {
 descriptionValue = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.free) {
 free = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.free) {
free = value 
}
    if let value = try? container.decode(String.self, forKey:.id) {
 id = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.id) {
id = value 
}
    if let value = try? container.decode(String.self, forKey:.serviceId) {
 serviceId = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.serviceId) {
serviceId = value 
}
  }

}
