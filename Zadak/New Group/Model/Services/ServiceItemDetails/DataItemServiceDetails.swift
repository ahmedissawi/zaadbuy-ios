//
//  Data.swift
//
//  Created by osamaaassi on 17/03/2021
//  Copyright (c) . All rights reserved.
//

import Foundation

struct DataItemServiceDetails: Codable {
    
    enum CodingKeys: String, CodingKey {
        case categoryId = "category_id"
        case discount
        case rate
        case subtitle
        case seller
        case regularPrice = "regular_price"
        case prices
        case quantityIsAvailable = "quantity_is_available"
        case shortDescription = "short_description"
        case descriptionValue = "description"
        case sellerId = "seller_id"
        case title
        case cartItem = "cart_item"
        case sellerFull = "seller_full"
        case cartCount = "cart_count"
        case quantity
        case salePrice = "sale_price"
        case features
        case category
        case favoriteItem = "favorite_item"
        case id
        case src
        case canComment = "can_comment"
        case store
        case sold
    }
    
    var seller: SellerServices?
    var category: SellerServices?
    var store: SellerServices?
    var sellerFull: SellerServices?
    
    var categoryId: Int?
    var discount: Int?
    var rate: String?
    var subtitle: String?
    
    var regularPrice: String?
    var prices: String?
    var quantityIsAvailable: Int?
    var shortDescription: String?
    var descriptionValue: String?
    var sellerId: Int?
    var title: String?
    var cartItem: Bool?
    
    var cartCount: Int?
    var quantity: Int?
    var salePrice: Int?
    var features: [FeaturesService]?
    
    var favoriteItem: Bool?
    var id: Int?
    var src: String?
    var canComment: Bool?
    
    var sold: Int?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(String.self, forKey:.categoryId) {
            categoryId = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.categoryId) {
            categoryId = value
        }
        if let value = try? container.decode(String.self, forKey:.discount) {
            discount = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.discount) {
            discount = value
        }
        if let value = try? container.decode(Int.self, forKey:.rate) {
            rate = String(value)
        }else if let value = try? container.decode(String.self, forKey:.rate) {
            rate = value
        }
        if let value = try? container.decode(Int.self, forKey:.subtitle) {
            subtitle = String(value)
        }else if let value = try? container.decode(String.self, forKey:.subtitle) {
            subtitle = value
        }
        seller = try container.decodeIfPresent(SellerServices.self, forKey: .seller)
        if let value = try? container.decode(Int.self, forKey:.regularPrice) {
            regularPrice = String(value)
        }else if let value = try? container.decode(String.self, forKey:.regularPrice) {
            regularPrice = value
        }
        if let value = try? container.decode(Int.self, forKey:.prices) {
            prices = String(value)
        }else if let value = try? container.decode(String.self, forKey:.prices) {
            prices = value
        }
        if let value = try? container.decode(String.self, forKey:.quantityIsAvailable) {
            quantityIsAvailable = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.quantityIsAvailable) {
            quantityIsAvailable = value
        }
        if let value = try? container.decode(Int.self, forKey:.shortDescription) {
            shortDescription = String(value)
        }else if let value = try? container.decode(String.self, forKey:.shortDescription) {
            shortDescription = value
        }
        if let value = try? container.decode(Int.self, forKey:.descriptionValue) {
            descriptionValue = String(value)
        }else if let value = try? container.decode(String.self, forKey:.descriptionValue) {
            descriptionValue = value
        }
        if let value = try? container.decode(String.self, forKey:.sellerId) {
            sellerId = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.sellerId) {
            sellerId = value
        }
        if let value = try? container.decode(Int.self, forKey:.title) {
            title = String(value)
        }else if let value = try? container.decode(String.self, forKey:.title) {
            title = value
        }
        cartItem = try container.decodeIfPresent(Bool.self, forKey: .cartItem)
        sellerFull = try container.decodeIfPresent(SellerServices.self, forKey: .sellerFull)
        if let value = try? container.decode(String.self, forKey:.cartCount) {
            cartCount = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.cartCount) {
            cartCount = value
        }
        if let value = try? container.decode(String.self, forKey:.quantity) {
            quantity = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.quantity) {
            quantity = value
        }
        if let value = try? container.decode(String.self, forKey:.salePrice) {
            salePrice = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.salePrice) {
            salePrice = value
        }
        features = try container.decodeIfPresent([FeaturesService].self, forKey: .features)
        category = try container.decodeIfPresent(SellerServices.self, forKey: .category)
        favoriteItem = try container.decodeIfPresent(Bool.self, forKey: .favoriteItem)
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(Int.self, forKey:.src) {
            src = String(value)
        }else if let value = try? container.decode(String.self, forKey:.src) {
            src = value
        }
        canComment = try container.decodeIfPresent(Bool.self, forKey: .canComment)
        
        store = try container.decodeIfPresent(SellerServices.self, forKey: .store)
        
        if let value = try? container.decode(String.self, forKey:.sold) {
            sold = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.sold) {
            sold = value
        }
    }
    
}
