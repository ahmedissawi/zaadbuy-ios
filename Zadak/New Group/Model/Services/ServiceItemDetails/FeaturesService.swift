//
//  FeaturesService.swift
//  Zadak
//
//  Created by osamaaassi on 26/04/2021.
//  Copyright © 2021  Ahmed’s MacBook Pro. All rights reserved.
//

import Foundation

struct FeaturesService: Codable {
    
    enum CodingKeys: String, CodingKey {
        case id
        case serviceID = "service_id"
        case descriptionValue = "description"
        case free
        case optional = "optional"
        case price
        
    }
    

    
    var id: Int?
    var serviceID: Int?
    var free: Int?
    var optional: Int?
    var price: String?
    var descriptionValue: String?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(String.self, forKey:.serviceID) {
            serviceID = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.serviceID) {
            serviceID = value
        }
        if let value = try? container.decode(String.self, forKey:.free) {
            free = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.free) {
            free = value
        }
        
        if let value = try? container.decode(String.self, forKey:.optional) {
            optional = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.optional) {
            optional = value
        }
        
        if let value = try? container.decode(Int.self, forKey:.price) {
            price = String(value)
        }else if let value = try? container.decode(String.self, forKey:.price) {
            price = value
        }
        
        if let value = try? container.decode(Int.self, forKey:.descriptionValue) {
            descriptionValue = String(value)
        }else if let value = try? container.decode(String.self, forKey:.descriptionValue) {
            descriptionValue = value
        }
        
    }
}


