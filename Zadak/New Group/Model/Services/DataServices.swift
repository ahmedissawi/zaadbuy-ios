//
//  Data.swift
//
//  Created by osamaaassi on 12/03/2021
//  Copyright (c) . All rights reserved.
//

import Foundation

struct DataServices: Codable {

  enum CodingKeys: String, CodingKey {
    case filters
    case nextPageUrl = "next_page_url"
    case perPage = "per_page"
    case prevPageUrl = "prev_page_url"
    case items
  }

  var items: [ItemsServices]?
  var filters: [FiltersServices]?
  var nextPageUrl: Int?
  var perPage: String?
  var prevPageUrl: Int?
  



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    filters = try container.decodeIfPresent([FiltersServices].self, forKey: .filters)
    if let value = try? container.decode(String.self, forKey:.nextPageUrl) {
 nextPageUrl = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.nextPageUrl) {
nextPageUrl = value 
}
    if let value = try? container.decode(Int.self, forKey:.perPage) {                       
perPage = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.perPage) {
 perPage = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.prevPageUrl) {
 prevPageUrl = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.prevPageUrl) {
prevPageUrl = value 
}
    items = try container.decodeIfPresent([ItemsServices].self, forKey: .items)
  }

}
