//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 6/15/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct CategoriesDeatilesData: Codable {
    
    enum CodingKeys: String, CodingKey {
        case title
        case items
        case name
        case id
    }
    
    var title: String?
    var items: [CategoriesProducat]?
    var name: String?
    var id: Int?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.title) {                       
            title = String(value)                                                                                     
        }else if let value = try? container.decode(String.self, forKey:.title) {
            title = value                                                                                     
        }
        items = try container.decodeIfPresent([CategoriesProducat].self, forKey: .items)
        if let value = try? container.decode(Int.self, forKey:.name) {                       
            name = String(value)                                                                                     
        }else if let value = try? container.decode(String.self, forKey:.name) {
            name = value                                                                                     
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)                                                                                     
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value 
        }
    }
    
}
