//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 6/15/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct categoriesData: Codable {
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case title
        case childrens
    }
    
    var id: Int?
    var name: String?
    var title: String?
    var childrens: [categoriesitems]?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(Int.self, forKey:.name) {
            name = String(value)
        }else if let value = try? container.decode(String.self, forKey:.name) {
            name = value
        }
        if let value = try? container.decode(Int.self, forKey:.title) {
            title = String(value)
        }else if let value = try? container.decode(String.self, forKey:.title) {
            title = value
        }
        childrens = try container.decodeIfPresent([categoriesitems].self, forKey: .childrens)
    }
    
}
