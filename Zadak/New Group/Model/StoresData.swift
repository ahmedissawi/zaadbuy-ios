//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 6/15/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct StoresData: Codable {
    
    enum CodingKeys: String, CodingKey {
        case title
        case stores
        case id
        case categoryId = "category_id"
    }
    
    var title: String?
    var stores: [Store]?
    var id: Int?
    var categoryId: Int?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.title) {
            title = String(value)
        }else if let value = try? container.decode(String.self, forKey:.title) {
            title = value
        }
        stores = try container.decodeIfPresent([Store].self, forKey: .stores)
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(String.self, forKey:.categoryId) {
            categoryId = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.categoryId) {
            categoryId = value
        }
    }
    
}
