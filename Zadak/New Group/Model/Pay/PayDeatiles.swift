//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 6/15/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct PayDeatiles: Codable {
    
    enum CodingKeys: String, CodingKey {
        case payed
        case deserved
    }
    
    var payed: Float?
    var deserved: Int?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        payed = try container.decodeIfPresent(Float.self, forKey: .payed)
        if let value = try? container.decode(String.self, forKey:.deserved) {
            deserved = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.deserved) {
            deserved = value
        }
    }
    
}
