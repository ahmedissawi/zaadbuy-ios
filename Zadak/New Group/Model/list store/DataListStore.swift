//
//  Data.swift
//
//  Created by Ahmed ios on 6/21/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct DataListStore: Codable {

  enum CodingKeys: String, CodingKey {
    case perPage = "per_page"
    case stores
    case prevPageUrl = "prev_page_url"
  }

  var perPage: String?
  var stores: [Store]?
  var prevPageUrl: String?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(Int.self, forKey:.perPage) {                       
perPage = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.perPage) {
 perPage = value                                                                                     
}
    stores = try container.decodeIfPresent([Store].self, forKey: .stores)
    if let value = try? container.decode(Int.self, forKey:.prevPageUrl) {                       
prevPageUrl = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.prevPageUrl) {
 prevPageUrl = value                                                                                     
}
  }

}
