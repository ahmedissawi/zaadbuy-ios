//
//  BaseClass.swift
//
//  Created by  Ahmed’s MacBook Pro on 6/16/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct ItemsInfo: Codable {
    
    enum CodingKeys: String, CodingKey {
        case locale
        case code
        case message
        case status
        case data
    }
    
    var locale: String?
    var code: Int?
    var message: String?
    var status: Int?
    var data: itemData?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.locale) {
            locale = String(value)
        }else if let value = try? container.decode(String.self, forKey:.locale) {
            locale = value
        }
        if let value = try? container.decode(String.self, forKey:.code) {
            code = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.code) {
            code = value
        }
        if let value = try? container.decode(Int.self, forKey:.message) {
            message = String(value)
        }else if let value = try? container.decode(String.self, forKey:.message) {
            message = value                                                                                     
        }
        if let value = try? container.decode(String.self, forKey:.status) {
            status = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.status) {
            status = value
        }
        data = try container.decodeIfPresent(itemData.self, forKey: .data)
    }
    
}
