//
//  Options.swift
//
//  Created by  Ahmed’s MacBook Pro on 6/16/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Options: Codable {
    
    enum CodingKeys: String, CodingKey {
        case name
        case slug
        case title
        case id
    }
    
    var name: String?
    var slug: String?
    var title: String?
    var id: Int?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.name) {
            name = String(value)
        }else if let value = try? container.decode(String.self, forKey:.name) {
            name = value
        }
        if let value = try? container.decode(Int.self, forKey:.slug) {
            slug = String(value)
        }else if let value = try? container.decode(String.self, forKey:.slug) {
            slug = value
        }
        if let value = try? container.decode(Int.self, forKey:.title) {
            title = String(value)
        }else if let value = try? container.decode(String.self, forKey:.title) {
            title = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
    }
    
}
