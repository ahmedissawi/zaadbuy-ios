//
//  Filters.swift
//
//  Created by  Ahmed’s MacBook Pro on 6/16/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Filters: Codable {
    
    enum CodingKeys: String, CodingKey {
        case title
        case slug
        case type
        case options
    }
    
    var title: String?
    var slug: String?
    var type: String?
    var options: [Options]?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.title) {
            title = String(value)
        }else if let value = try? container.decode(String.self, forKey:.title) {
            title = value
        }
        if let value = try? container.decode(Int.self, forKey:.slug) {
            slug = String(value)
        }else if let value = try? container.decode(String.self, forKey:.slug) {
            slug = value
        }
        if let value = try? container.decode(Int.self, forKey:.type) {
            type = String(value)
        }else if let value = try? container.decode(String.self, forKey:.type) {
            type = value
        }
        options = try container.decodeIfPresent([Options].self, forKey: .options)
    }
    
}
