//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 6/26/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Notifications: Codable {
    
    enum CodingKeys: String, CodingKey {
        case title
        case createdAt = "created_at"
        case id
        case isRead = "is_read"
        case descriptionValue = "description"
    }
    
    var title: String?
    var createdAt: String?
    var id: Int?
    var isRead: Bool?
    var descriptionValue: String?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.title) {                       
            title = String(value)                                                                                     
        }else if let value = try? container.decode(String.self, forKey:.title) {
            title = value                                                                                     
        }
        if let value = try? container.decode(Int.self, forKey:.createdAt) {                       
            createdAt = String(value)                                                                                     
        }else if let value = try? container.decode(String.self, forKey:.createdAt) {
            createdAt = value                                                                                     
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)                                                                                     
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value 
        }
        isRead = try container.decodeIfPresent(Bool.self, forKey: .isRead)
        if let value = try? container.decode(Int.self, forKey:.descriptionValue) {                       
            descriptionValue = String(value)                                                                                     
        }else if let value = try? container.decode(String.self, forKey:.descriptionValue) {
            descriptionValue = value                                                                                     
        }
    }
    
}
