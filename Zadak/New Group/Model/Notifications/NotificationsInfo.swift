//
//  BaseClass.swift
//
//  Created by  Ahmed’s MacBook Pro on 6/26/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct NotificationsInfo: Codable {
    
    enum CodingKeys: String, CodingKey {
        case code
        case locale
        case status
        case data
        case message
    }
    
    var code: Int?
    var locale: String?
    var status: Int?
    var data: [Notifications]?
    var message: String?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(String.self, forKey:.code) {
            code = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.code) {
            code = value
        }
        if let value = try? container.decode(Int.self, forKey:.locale) {
            locale = String(value)
        }else if let value = try? container.decode(String.self, forKey:.locale) {
            locale = value
        }
        if let value = try? container.decode(String.self, forKey:.status) {
            status = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.status) {
            status = value
        }
        data = try container.decodeIfPresent([Notifications].self, forKey: .data)
        if let value = try? container.decode(Int.self, forKey:.message) {
            message = String(value)
        }else if let value = try? container.decode(String.self, forKey:.message) {
            message = value
        }
    }
    
}
