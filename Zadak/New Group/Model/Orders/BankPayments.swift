//
//  BankPayments.swift
//
//  Created by Ahmed ios on 6/15/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct BankPayments: Codable {

  enum CodingKeys: String, CodingKey {
    case transactionNo = "transaction_no"
    case statusCode = "status_code"
    case status
    case document
    case id
    case orderNo = "order_no"
    case userNotes = "user_notes"
    case orderId = "order_id"
    case seller
    case customer
  }

  var transactionNo: String?
  var statusCode: Int?
  var status: String?
  var document: String?
  var id: Int?
  var orderNo: String?
  var userNotes: String?
  var orderId: Int?
  var seller: String?
  var customer: String?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(Int.self, forKey:.transactionNo) {                       
transactionNo = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.transactionNo) {
 transactionNo = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.statusCode) {
 statusCode = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.statusCode) {
statusCode = value 
}
    if let value = try? container.decode(Int.self, forKey:.status) {                       
status = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.status) {
 status = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.document) {                       
document = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.document) {
 document = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.id) {
 id = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.id) {
id = value 
}
    if let value = try? container.decode(Int.self, forKey:.orderNo) {                       
orderNo = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.orderNo) {
 orderNo = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.userNotes) {                       
userNotes = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.userNotes) {
 userNotes = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.orderId) {
 orderId = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.orderId) {
orderId = value 
}
    if let value = try? container.decode(Int.self, forKey:.seller) {                       
seller = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.seller) {
 seller = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.customer) {                       
customer = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.customer) {
 customer = value                                                                                     
}
  }

}
