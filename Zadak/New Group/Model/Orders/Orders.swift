//
//  Orders.swift
//
//  Created by Ahmed ios on 6/15/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Orders: Codable {

  enum CodingKeys: String, CodingKey {
    case id
    case orderNo = "order_no"
    case purchaseDate = "purchase_date"
    case discount
    case total
    case payable
    case bankAccount = "bank_account"
    case seller
    case store
    case saleDiscount = "sale_discount"
    case paymentMethod = "payment_method"
    case bankPayments = "bank_payments"
    case status
    case paymentMethodId = "payment_method_id"
    case shippingDiscount = "shipping_discount"
    case items
  }

  var id: Int?
  var orderNo: String?
  var status: Status?
  var purchaseDate: String?
  var discount: Int?
  var total: Int?
  var payable: Int?
  var bankAccount: BankAccount?
  var seller: Seller?
  var store: Store?
  var saleDiscount: Int?
  var paymentMethod: String?
  var bankPayments: [BankPayments]?
  
  var paymentMethodId: String?
  var shippingDiscount: Int?
  var items: [ItemsOrder]?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(String.self, forKey:.id) {
 id = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.id) {
id = value 
}
    if let value = try? container.decode(Int.self, forKey:.orderNo) {                       
orderNo = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.orderNo) {
 orderNo = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.purchaseDate) {                       
purchaseDate = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.purchaseDate) {
 purchaseDate = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.discount) {
 discount = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.discount) {
discount = value 
}
    if let value = try? container.decode(String.self, forKey:.total) {
 total = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.total) {
total = value 
}
    if let value = try? container.decode(String.self, forKey:.payable) {
 payable = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.payable) {
payable = value 
}
    bankAccount = try container.decodeIfPresent(BankAccount.self, forKey: .bankAccount)
    seller = try container.decodeIfPresent(Seller.self, forKey: .seller)
    store = try container.decodeIfPresent(Store.self, forKey: .store)
    if let value = try? container.decode(String.self, forKey:.saleDiscount) {
 saleDiscount = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.saleDiscount) {
saleDiscount = value 
}
    if let value = try? container.decode(Int.self, forKey:.paymentMethod) {                       
paymentMethod = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.paymentMethod) {
 paymentMethod = value                                                                                     
}
    bankPayments = try container.decodeIfPresent([BankPayments].self, forKey: .bankPayments)
    status = try container.decodeIfPresent(Status.self, forKey: .status)
    if let value = try? container.decode(Int.self, forKey:.paymentMethodId) {                       
paymentMethodId = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.paymentMethodId) {
 paymentMethodId = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.shippingDiscount) {
 shippingDiscount = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.shippingDiscount) {
shippingDiscount = value 
}
    items = try container.decodeIfPresent([ItemsOrder].self, forKey: .items)
  }

}
