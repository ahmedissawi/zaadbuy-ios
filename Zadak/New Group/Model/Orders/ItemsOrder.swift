//
//  Items.swift
//
//  Created by Ahmed ios on 6/15/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct ItemsOrder: Codable {
    
    enum CodingKeys: String, CodingKey {
        case unitPrice = "unit_price"
        case image
        case quantity
        case total
        case shippingPrice = "shipping_price"
        case itemId = "item_id"
        case shippingCompany = "shipping_company"
        case shippingService = "shipping_service"
        case title
        case id
    }
    
    var unitPrice: Int?
    var image: String?
    var quantity: Int?
    var total: Int?
    var shippingPrice: Int?
    var itemId: Int?
    var shippingCompany: String?
    var shippingService: String?
    var title: String?
    var id: Int?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(String.self, forKey:.unitPrice) {
            unitPrice = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.unitPrice) {
            unitPrice = value
        }
        if let value = try? container.decode(Int.self, forKey:.image) {
            image = String(value)
        }else if let value = try? container.decode(String.self, forKey:.image) {
            image = value
        }
        if let value = try? container.decode(String.self, forKey:.quantity) {
            quantity = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.quantity) {
            quantity = value
        }
        if let value = try? container.decode(String.self, forKey:.total) {
            total = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.total) {
            total = value
        }
        if let value = try? container.decode(String.self, forKey:.shippingPrice) {
            shippingPrice = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.shippingPrice) {
            shippingPrice = value
        }
        if let value = try? container.decode(String.self, forKey:.itemId) {
            itemId = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.itemId) {
            itemId = value
        }
        if let value = try? container.decode(Int.self, forKey:.shippingCompany) {
            shippingCompany = String(value)
        }else if let value = try? container.decode(String.self, forKey:.shippingCompany) {
            shippingCompany = value
        }
        if let value = try? container.decode(Int.self, forKey:.shippingService) {
            shippingService = String(value)
        }else if let value = try? container.decode(String.self, forKey:.shippingService) {
            shippingService = value
        }
        if let value = try? container.decode(Int.self, forKey:.title) {
            title = String(value)
        }else if let value = try? container.decode(String.self, forKey:.title) {
            title = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
    }
    
}
