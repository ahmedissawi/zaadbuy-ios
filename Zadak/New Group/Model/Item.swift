//
//  Item.swift
//
//  Created by Ahmed ios on 6/6/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Item: Codable {

  enum CodingKeys: String, CodingKey {
    case webImage = "web_image"
    case target
    case image
  }

  var webImage: String?
  var target: String?
  var image: String?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(Int.self, forKey:.webImage) {                       
webImage = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.webImage) {
 webImage = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.target) {                       
target = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.target) {
 target = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.image) {                       
image = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.image) {
 image = value                                                                                     
}
  }

}
