//
//  StoreDetails.swift
//  Zadak
//
//  Created by osamaaassi on 13/03/2021.
//  Copyright © 2021  Ahmed’s MacBook Pro. All rights reserved.
//

import Foundation

// MARK: - StoreDetails
struct StoreDetails: Codable {
    let status, code: Int
    let locale, message: String
    let data: DataStoreDetails
}

// MARK: - DataClass
struct DataStoreDetails: Codable {
    let id,storeID : Int
    let username, storeName: String
    let storeLogo: String
    let storeCategory, storeAddress: String

    enum CodingKeys: String, CodingKey {
        case id, username
        case storeID = "store_id"
        case storeName = "store_name"
        case storeLogo = "store_logo"
        case storeCategory = "store_category"
        case storeAddress = "store_address"
    }
}
