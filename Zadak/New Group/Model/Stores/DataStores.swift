//
//  Data.swift
//
//  Created by osamaaassi on 12/03/2021
//  Copyright (c) . All rights reserved.
//

import Foundation

struct DataStores: Codable {
    
    enum CodingKeys: String, CodingKey {
        case perPage = "per_page"
        case nextPageUrl = "next_page_url"
        case prevPageUrl = "prev_page_url"
        
        case stores
    }
    
    var perPage: Int?
    var nextPageUrl: String?
    var prevPageUrl: String?
    var stores: [Stores]?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(String.self, forKey:.perPage) {
            perPage = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.perPage) {
            perPage = value
        }
        if let value = try? container.decode(Int.self, forKey:.nextPageUrl) {
            nextPageUrl = String(value)
        }else if let value = try? container.decode(String.self, forKey:.nextPageUrl) {
            nextPageUrl = value
        }
        if let value = try? container.decode(Int.self, forKey:.prevPageUrl) {
            prevPageUrl = String(value)
        }else if let value = try? container.decode(String.self, forKey:.prevPageUrl) {
            prevPageUrl = value
        }
        
        stores = try container.decodeIfPresent([Stores].self, forKey: .stores)
    }
    
}
