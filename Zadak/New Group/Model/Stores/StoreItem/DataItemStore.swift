//
//  Data.swift
//
//  Created by osamaaassi on 13/03/2021
//  Copyright (c) . All rights reserved.
//

import Foundation

struct DataItemStore: Codable {
    
    enum CodingKeys: String, CodingKey {
        case nextPageUrl = "next_page_url"
        case perPage = "per_page"
        case seller
        case page
        case items
        case prevPageUrl = "prev_page_url"
    }
    
   
    var seller: DataStoreDetails?
    var items: [ItemsStore]?
    
    
  
    var prevPageUrl: String?
    var nextPageUrl: String?
    var perPage: Int?
    var page: Int?
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.nextPageUrl) {
            nextPageUrl = String(value)
        }else if let value = try? container.decode(String.self, forKey:.nextPageUrl) {
            nextPageUrl = value
        }
        if let value = try? container.decode(String.self, forKey:.perPage) {
            perPage = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.perPage) {
            perPage = value
        }
        seller = try container.decodeIfPresent(DataStoreDetails.self, forKey: .seller)
        if let value = try? container.decode(String.self, forKey:.page) {
            page = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.page) {
            page = value
        }
        items = try container.decodeIfPresent([ItemsStore].self, forKey: .items)
        if let value = try? container.decode(Int.self, forKey:.prevPageUrl) {
            prevPageUrl = String(value)
        }else if let value = try? container.decode(String.self, forKey:.prevPageUrl) {
            prevPageUrl = value
        }
    }
    
}
