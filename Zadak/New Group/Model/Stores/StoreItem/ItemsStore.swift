//
//  Items.swift
//
//  Created by osamaaassi on 13/03/2021
//  Copyright (c) . All rights reserved.
//

import Foundation

struct ItemsStore: Codable {
    
    enum CodingKeys: String, CodingKey {
        case category
        case seller
        case link
        case id
        case price
        case title
        case regularPrice = "regular_price"
        case image
        case store
    }
    
    
    
   
    var id: Int?
    
    var title: String?
    var regularPrice: Int?
    var price: String?
    var image: String?
    
    var category: SellerStoreItem?
    var seller: SellerStoreItem?
    var store: SellerStoreItem?
    var link: String?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
       
        if let value = try? container.decode(Int.self, forKey:.link) {
            link = String(value)
        }else if let value = try? container.decode(String.self, forKey:.link) {
            link = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(Int.self, forKey:.price) {
            price = String(value)
        }else if let value = try? container.decode(String.self, forKey:.price) {
            price = value
        }
        if let value = try? container.decode(Int.self, forKey:.title) {
            title = String(value)
        }else if let value = try? container.decode(String.self, forKey:.title) {
            title = value
        }
        if let value = try? container.decode(String.self, forKey:.regularPrice) {
            regularPrice = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.regularPrice) {
            regularPrice = value
        }
        if let value = try? container.decode(Int.self, forKey:.image) {
            image = String(value)
        }else if let value = try? container.decode(String.self, forKey:.image) {
            image = value
        }
        
        category = try container.decodeIfPresent(SellerStoreItem.self, forKey: .category)
        seller = try container.decodeIfPresent(SellerStoreItem.self, forKey: .seller)
        store = try container.decodeIfPresent(SellerStoreItem.self, forKey: .store)
    }
    
}
