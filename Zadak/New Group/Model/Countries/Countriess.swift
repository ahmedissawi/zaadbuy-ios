//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 6/16/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Countriess: Codable {
    
    enum CodingKeys: String, CodingKey {
        case code
        case title
        case id
    }
    
    var code: String?
    var title: String?
    var id: Int?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.code) {
            code = String(value)
        }else if let value = try? container.decode(String.self, forKey:.code) {
            code = value
        }
        if let value = try? container.decode(Int.self, forKey:.title) {
            title = String(value)
        }else if let value = try? container.decode(String.self, forKey:.title) {
            title = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
    }
    
}
