//
//  BasicInfo.swift
//
//  Created by  Ahmed’s MacBook Pro on 6/15/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct BasicInfo: Codable {
    
    enum CodingKeys: String, CodingKey {
        case id
        case paymentId = "payment_id"
        case orderShippingDiscount = "order_shipping_discount"
        case orderDiscount = "order_discount"
        case total
        case orderDate = "order_date"
        case code
        case paymentTitle = "payment_title"
        case status
        case storeId = "store_id"
        case storeName = "store_name"
        case coupon
        case couponAmount = "coupon_amount"
    }
    
    var id: Int?
    var paymentId: String?
    var orderShippingDiscount: Int?
    var orderDiscount: Int?
    var total: Int?
    var orderDate: String?
    var code: String?
    var paymentTitle: String?
    var status: String?
    var storeId: Int?
    var storeName: String?
    var coupon: String?
    var couponAmount: Int?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)                                                                                     
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value 
        }
        if let value = try? container.decode(Int.self, forKey:.paymentId) {                       
            paymentId = String(value)                                                                                     
        }else if let value = try? container.decode(String.self, forKey:.paymentId) {
            paymentId = value                                                                                     
        }
        if let value = try? container.decode(String.self, forKey:.orderShippingDiscount) {
            orderShippingDiscount = Int(value)                                                                                     
        } else if let value = try? container.decode(Int.self, forKey:.orderShippingDiscount) {
            orderShippingDiscount = value 
        }
        if let value = try? container.decode(String.self, forKey:.orderDiscount) {
            orderDiscount = Int(value)                                                                                     
        } else if let value = try? container.decode(Int.self, forKey:.orderDiscount) {
            orderDiscount = value 
        }
        if let value = try? container.decode(String.self, forKey:.total) {
            total = Int(value)                                                                                     
        } else if let value = try? container.decode(Int.self, forKey:.total) {
            total = value 
        }
        if let value = try? container.decode(Int.self, forKey:.orderDate) {                       
            orderDate = String(value)                                                                                     
        }else if let value = try? container.decode(String.self, forKey:.orderDate) {
            orderDate = value                                                                                     
        }
        if let value = try? container.decode(Int.self, forKey:.code) {                       
            code = String(value)                                                                                     
        }else if let value = try? container.decode(String.self, forKey:.code) {
            code = value                                                                                     
        }
        if let value = try? container.decode(Int.self, forKey:.paymentTitle) {                       
            paymentTitle = String(value)                                                                                     
        }else if let value = try? container.decode(String.self, forKey:.paymentTitle) {
            paymentTitle = value                                                                                     
        }
        if let value = try? container.decode(Int.self, forKey:.status) {                       
            status = String(value)                                                                                     
        }else if let value = try? container.decode(String.self, forKey:.status) {
            status = value                                                                                     
        }
        if let value = try? container.decode(String.self, forKey:.storeId) {
            storeId = Int(value)                                                                                     
        } else if let value = try? container.decode(Int.self, forKey:.storeId) {
            storeId = value 
        }
        if let value = try? container.decode(Int.self, forKey:.storeName) {                       
            storeName = String(value)                                                                                     
        }else if let value = try? container.decode(String.self, forKey:.storeName) {
            storeName = value                                                                                     
        }
        if let value = try? container.decode(Int.self, forKey:.coupon) {                       
            coupon = String(value)                                                                                     
        }else if let value = try? container.decode(String.self, forKey:.coupon) {
            coupon = value                                                                                     
        }
        if let value = try? container.decode(String.self, forKey:.couponAmount) {
            couponAmount = Int(value)                                                                                     
        } else if let value = try? container.decode(Int.self, forKey:.couponAmount) {
            couponAmount = value 
        }
    }
    
}
