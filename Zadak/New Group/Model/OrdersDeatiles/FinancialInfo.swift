//
//  FinancialInfo.swift
//
//  Created by  Ahmed’s MacBook Pro on 6/15/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct FinancialInfo: Codable {
    
    enum CodingKeys: String, CodingKey {
        case couponAmount = "coupon_amount"
        case amountForPay = "amount_for_pay"
        case coupon
        case orderShippingDiscount = "order_shipping_discount"
        case total
        case orderDiscount = "order_discount"
    }
    
    var couponAmount: Int?
    var amountForPay: Float?
    var coupon: String?
    var orderShippingDiscount: Int?
    var total: Int?
    var orderDiscount: Int?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(String.self, forKey:.couponAmount) {
            couponAmount = Int(value)                                                                                     
        } else if let value = try? container.decode(Int.self, forKey:.couponAmount) {
            couponAmount = value 
        }
        amountForPay = try container.decodeIfPresent(Float.self, forKey: .amountForPay)
        if let value = try? container.decode(Int.self, forKey:.coupon) {                       
            coupon = String(value)                                                                                     
        }else if let value = try? container.decode(String.self, forKey:.coupon) {
            coupon = value                                                                                     
        }
        if let value = try? container.decode(String.self, forKey:.orderShippingDiscount) {
            orderShippingDiscount = Int(value)                                                                                     
        } else if let value = try? container.decode(Int.self, forKey:.orderShippingDiscount) {
            orderShippingDiscount = value 
        }
        if let value = try? container.decode(String.self, forKey:.total) {
            total = Int(value)                                                                                     
        } else if let value = try? container.decode(Int.self, forKey:.total) {
            total = value 
        }
        if let value = try? container.decode(String.self, forKey:.orderDiscount) {
            orderDiscount = Int(value)                                                                                     
        } else if let value = try? container.decode(Int.self, forKey:.orderDiscount) {
            orderDiscount = value 
        }
    }
    
}
