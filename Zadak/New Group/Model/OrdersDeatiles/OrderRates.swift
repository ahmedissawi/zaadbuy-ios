//
//  ItemsRates.swift
//
//  Created by  Ahmed’s MacBook Pro on 6/15/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct OrderRates: Codable {

  enum CodingKeys: String, CodingKey {
    case image
    case subtitle
    case rate
    case title
    case comment
    case id
  }

  var image: String?
  var subtitle: String?
  var rate: Int?
  var title: String?
  var comment: String?
  var id: Int?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(Int.self, forKey:.image) {                       
image = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.image) {
 image = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.subtitle) {                       
subtitle = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.subtitle) {
 subtitle = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.rate) {
 rate = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.rate) {
rate = value 
}
    if let value = try? container.decode(Int.self, forKey:.title) {                       
title = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.title) {
 title = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.comment) {                       
comment = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.comment) {
 comment = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.id) {
 id = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.id) {
id = value 
}
  }

}
