//
//  Addresses.swift
//
//  Created by Ahmed ios on 6/6/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Addresses: Codable {

  enum CodingKeys: String, CodingKey {
    case countryId = "country_id"
    case id
    case country
    case city
    case organization
    case prefix
    case type
    case name
    case cityId = "city_id"
  }

  var countryId: Int?
  var id: Int?
  var country: String?
  var city: String?
  var organization: String?
  var prefix: String?
  var type: String?
  var name: String?
  var cityId: Int?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(String.self, forKey:.countryId) {
 countryId = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.countryId) {
countryId = value 
}
    if let value = try? container.decode(String.self, forKey:.id) {
 id = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.id) {
id = value 
}
    if let value = try? container.decode(Int.self, forKey:.country) {                       
country = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.country) {
 country = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.city) {                       
city = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.city) {
 city = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.organization) {                       
organization = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.organization) {
 organization = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.prefix) {                       
prefix = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.prefix) {
 prefix = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.type) {                       
type = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.type) {
 type = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.name) {                       
name = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.name) {
 name = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.cityId) {
 cityId = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.cityId) {
cityId = value 
}
  }

}
