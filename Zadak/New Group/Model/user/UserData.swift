//
//  User.swift
//
//  Created by Ahmed ios on 6/6/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct UserData: Codable {

  enum CodingKeys: String, CodingKey {
    case addresses
    case profile
    case email
    case id
    case mobile
    case name
    case mobilePrefix = "mobile_prefix"
    case preferences
  }

  var addresses: [Addresses]?
  var profile: Profile?
  var email: String?
  var id: Int?
  var mobile: String?
  var name: String?
  var mobilePrefix: String?
  var preferences: Preferences?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    addresses = try container.decodeIfPresent([Addresses].self, forKey: .addresses)
    profile = try container.decodeIfPresent(Profile.self, forKey: .profile)
    if let value = try? container.decode(Int.self, forKey:.email) {                       
email = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.email) {
 email = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.id) {
 id = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.id) {
id = value 
}
    if let value = try? container.decode(Int.self, forKey:.mobile) {                       
mobile = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.mobile) {
 mobile = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.name) {                       
name = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.name) {
 name = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.mobilePrefix) {                       
mobilePrefix = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.mobilePrefix) {
 mobilePrefix = value                                                                                     
}
    preferences = try container.decodeIfPresent(Preferences.self, forKey: .preferences)
  }

}
