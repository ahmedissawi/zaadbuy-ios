//
//  summeryData.swift
//
//  Created by Ahmed ios on 6/21/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct summeryData: Codable {
    
    enum CodingKeys: String, CodingKey {
        case shippingDiscount = "shipping_discount"
        case totalPrice = "total_price"
        case otherDiscount = "other_discount"
        case couponDiscount = "coupon_discount"
        case netPrice = "net_price"
    }
    
    var shippingDiscount: Double?
    var totalPrice: Double?
    var otherDiscount: Double?
    var couponDiscount: Double?
    var netPrice: Double?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(String.self, forKey:.shippingDiscount) {
            shippingDiscount = Double(value)
        } else if let value = try? container.decode(Double.self, forKey:.shippingDiscount) {
            shippingDiscount = value
        }else{
            if let value = try? container.decode(Int.self, forKey:.shippingDiscount) {
                shippingDiscount = Double(value)
            }
        }
        if let value = try? container.decode(String.self, forKey:.totalPrice) {
            totalPrice = Double(value)
        } else if let value = try? container.decode(Double.self, forKey:.totalPrice) {
            totalPrice = value
        }else{
            if let value = try? container.decode(Int.self, forKey:.totalPrice) {
            totalPrice = Double(value)
            }
        }
        if let value = try? container.decode(String.self, forKey:.otherDiscount) {
            otherDiscount = Double(value)
        } else if let value = try? container.decode(Double.self, forKey:.otherDiscount) {
            otherDiscount = value
        }else{
            if let value = try? container.decode(Int.self, forKey:.otherDiscount) {
            otherDiscount = Double(value)
            }
        }
        if let value = try? container.decode(String.self, forKey:.couponDiscount) {
            couponDiscount = Double(value)
        } else if let value = try? container.decode(Double.self, forKey:.couponDiscount) {
            couponDiscount = value
        }else{
            if let value = try? container.decode(Int.self, forKey:.couponDiscount) {
            couponDiscount =  Double(value)
            }
        }
        if let value = try? container.decode(String.self, forKey:.netPrice) {
            netPrice = Double(value)
        } else if let value = try? container.decode(Double.self, forKey:.netPrice) {
            netPrice = value
        }else{
           if let value = try? container.decode(Int.self, forKey:.netPrice) {
            netPrice = Double(value)
           }
        }
    }
    
    
}
