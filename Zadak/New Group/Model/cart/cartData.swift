//
//  Data.swift
//
//  Created by Ahmed ios on 6/6/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct cartData: Codable {

  enum CodingKeys: String, CodingKey {
    case seller
    case items
  }

  var seller: Seller?
  var items: [CartItems]?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    seller = try container.decodeIfPresent(Seller.self, forKey: .seller)
    items = try container.decodeIfPresent([CartItems].self, forKey: .items)
  }

}
