//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 10/17/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct DataStoreInfo: Codable {

  enum CodingKeys: String, CodingKey {
    case perPage = "per_page"
    case items
    case page
    case seller
    case nextPageUrl = "next_page_url"

  }

  var perPage: Int?
  var items: [Items]?
  var page: Int?
  var seller: Seller?
  var nextPageUrl: String?


    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(String.self, forKey:.perPage) {
            perPage = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.perPage) {
            perPage = value
        }
        items = try container.decodeIfPresent([Items].self, forKey: .items)
        if let value = try? container.decode(String.self, forKey:.page) {
            page = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.page) {
            page = value
        }
        seller = try container.decodeIfPresent(Seller.self, forKey: .seller)
        if let value = try? container.decode(Int.self, forKey:.nextPageUrl) {
            nextPageUrl = String(value)
        }else if let value = try? container.decode(String.self, forKey:.nextPageUrl) {
            nextPageUrl = value
        }
    }
    
    
}
