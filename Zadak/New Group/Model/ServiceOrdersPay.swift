//
//  ServiceOrdersPay.swift
//  Zadak
//
//  Created by osamaaassi on 07/04/2021.
//  Copyright © 2021  Ahmed’s MacBook Pro. All rights reserved.
//

import Foundation

// MARK: - ServiceOrdersPay
struct ServiceOrdersPay: Codable {
    let status, code: Int
    let locale, message: String
    let data: DataClass
}

// MARK: - DataClass
struct DataClass: Codable {
    let paymentRequest: PaymentRequest

    enum CodingKeys: String, CodingKey {
        case paymentRequest = "payment_request"
    }
}

// MARK: - PaymentRequest
struct PaymentRequest: Codable {
    let success: Bool
    let code: Int
    let paymentRequestDescription: String
    let returnurl: String

    enum CodingKeys: String, CodingKey {
        case success, code
        case paymentRequestDescription = "description"
        case returnurl
    }
}
