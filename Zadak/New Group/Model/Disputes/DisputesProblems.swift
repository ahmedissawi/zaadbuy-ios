//
//  BaseClass.swift
//
//  Created by  Ahmed’s MacBook Pro on 9/6/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct DisputesProblems: Codable {

  enum CodingKeys: String, CodingKey {
    case status
    case code
    case data
    case message
    case locale
  }

  var status: Int?
  var code: Int?
  var data: [ProblemsData]?
  var message: String?
  var locale: String?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(String.self, forKey:.status) {
 status = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.status) {
status = value 
}
    if let value = try? container.decode(String.self, forKey:.code) {
 code = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.code) {
code = value 
}
    data = try container.decodeIfPresent([ProblemsData].self, forKey: .data)
    if let value = try? container.decode(Int.self, forKey:.message) {                       
message = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.message) {
 message = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.locale) {                       
locale = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.locale) {
 locale = value                                                                                     
}
  }

}
