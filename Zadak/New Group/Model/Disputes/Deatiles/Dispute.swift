//
//  Dispute.swift
//
//  Created by  Ahmed’s MacBook Pro on 9/6/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Dispute: Codable {
    
    enum CodingKeys: String, CodingKey {
        case seller
        case user
        case orderNo = "order_no"
        case admin
        case adminInformed
        case createdAt = "created_at"
        case disputeCode = "dispute_code"
        case item
        case id
        case sellerAskedForClose = "seller_asked_for_close"
        case comments
        case status
        case customerConfirmedClose = "customer_confirmed_close"
        case problem
        case attachment
        case videoUrl = "video_url"

    }
    
    var seller: DisputesSeller?
    var user: DisputesDeatilesUser?
    var orderNo: String?
    var admin: Admin?
    var adminInformed: Bool?
    var createdAt: String?
    var disputeCode: String?
    var item: String?
    var id: Int?
    var sellerAskedForClose: Bool?
    var comments: [DisputesDeatilesComments]?
    var status: DisputesDeatilesStatus?
    var customerConfirmedClose: Bool?
    var problem: String?
    var attachment: String?
    var videoUrl: String?

    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        seller = try container.decodeIfPresent(DisputesSeller.self, forKey: .seller)
        user = try container.decodeIfPresent(DisputesDeatilesUser.self, forKey: .user)
        if let value = try? container.decode(Int.self, forKey:.orderNo) {
            orderNo = String(value)
        }else if let value = try? container.decode(String.self, forKey:.orderNo) {
            orderNo = value
        }
        admin = try container.decodeIfPresent(Admin.self, forKey: .admin)
        adminInformed = try container.decodeIfPresent(Bool.self, forKey: .adminInformed)
        if let value = try? container.decode(Int.self, forKey:.createdAt) {
            createdAt = String(value)
        }else if let value = try? container.decode(String.self, forKey:.createdAt) {
            createdAt = value
        }
        if let value = try? container.decode(Int.self, forKey:.disputeCode) {
            disputeCode = String(value)
        }else if let value = try? container.decode(String.self, forKey:.disputeCode) {
            disputeCode = value
        }
        if let value = try? container.decode(Int.self, forKey:.item) {
            item = String(value)
        }else if let value = try? container.decode(String.self, forKey:.item) {
            item = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        sellerAskedForClose = try container.decodeIfPresent(Bool.self, forKey: .sellerAskedForClose)
        comments = try container.decodeIfPresent([DisputesDeatilesComments].self, forKey: .comments)
        status = try container.decodeIfPresent(DisputesDeatilesStatus.self, forKey: .status)
        customerConfirmedClose = try container.decodeIfPresent(Bool.self, forKey: .customerConfirmedClose)
        if let value = try? container.decode(Int.self, forKey:.problem) {
            problem = String(value)
        }else if let value = try? container.decode(String.self, forKey:.problem) {
            problem = value
        }
        if let value = try? container.decode(Int.self, forKey:.attachment) {
            attachment = String(value)
        }else if let value = try? container.decode(String.self, forKey:.attachment) {
            attachment = value
        }
        if let value = try? container.decode(Int.self, forKey:.videoUrl) {
                 videoUrl = String(value)
             }else if let value = try? container.decode(String.self, forKey:.videoUrl) {
                 videoUrl = value
             }
    }
    
}
