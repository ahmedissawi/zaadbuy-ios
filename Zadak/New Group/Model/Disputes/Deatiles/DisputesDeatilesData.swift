//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 9/5/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct DisputesDeatilesData: Codable {
    
    enum CodingKeys: String, CodingKey {
      case dispute
    }

    var dispute: Dispute?



    init(from decoder: Decoder) throws {
      let container = try decoder.container(keyedBy: CodingKeys.self)
      dispute = try container.decodeIfPresent(Dispute.self, forKey: .dispute)
    }


}
