//
//  Resources.swift
//
//  Created by  Ahmed’s MacBook Pro on 9/5/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct DisputesResources: Codable {
    
    enum CodingKeys: String, CodingKey {
        case customerConfirmedClose = "customer_confirmed_close"
        case sellerAskedForClose = "seller_asked_for_close"
        case status
        case createdAt = "created_at"
        case admin
        case problem
        case user
        case disputeCode = "dispute_code"
        case id
        case adminInformed
        case item
        case orderNo = "order_no"
    }
    
    var customerConfirmedClose: Bool?
    var sellerAskedForClose: Bool?
    var status: DisputesStatus?
    var createdAt: String?
    var admin: Admin?
    var problem: String?
    var user: DisputesUser?
    var disputeCode: String?
    var id: Int?
    var adminInformed: Bool?
    var item: String?
    var orderNo: String?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        customerConfirmedClose = try container.decodeIfPresent(Bool.self, forKey: .customerConfirmedClose)
        sellerAskedForClose = try container.decodeIfPresent(Bool.self, forKey: .sellerAskedForClose)
        status = try container.decodeIfPresent(DisputesStatus.self, forKey: .status)
        if let value = try? container.decode(Int.self, forKey:.createdAt) {
            createdAt = String(value)
        }else if let value = try? container.decode(String.self, forKey:.createdAt) {
            createdAt = value
        }
        admin = try container.decodeIfPresent(Admin.self, forKey: .admin)
        if let value = try? container.decode(Int.self, forKey:.problem) {
            problem = String(value)
        }else if let value = try? container.decode(String.self, forKey:.problem) {
            problem = value
        }
        user = try container.decodeIfPresent(DisputesUser.self, forKey: .user)
        if let value = try? container.decode(Int.self, forKey:.disputeCode) {
            disputeCode = String(value)
        }else if let value = try? container.decode(String.self, forKey:.disputeCode) {
            disputeCode = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        adminInformed = try container.decodeIfPresent(Bool.self, forKey: .adminInformed)
        if let value = try? container.decode(Int.self, forKey:.item) {
            item = String(value)
        }else if let value = try? container.decode(String.self, forKey:.item) {
            item = value
        }
        if let value = try? container.decode(Int.self, forKey:.orderNo) {
            orderNo = String(value)
        }else if let value = try? container.decode(String.self, forKey:.orderNo) {
            orderNo = value
        }
    }
    
}
