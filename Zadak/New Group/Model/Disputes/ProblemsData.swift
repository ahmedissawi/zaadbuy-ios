//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 9/6/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct ProblemsData: Codable {
    
    enum CodingKeys: String, CodingKey {
        case value
        case id
    }
    
    var value: String?
    var id: Int?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let name = try? container.decode(Int.self, forKey:.value) {
            value = String(name)
        }else if let name = try? container.decode(String.self, forKey:.value) {
            value = name
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
    }
    
}
