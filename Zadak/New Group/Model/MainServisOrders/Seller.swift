//
//  Seller.swift
//
//  Created by osamaaassi on 15/03/2021
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Seller: Codable {

  enum CodingKeys: String, CodingKey {
    case mobile
    case id
    case store
    case name
  }

  var mobile: String?
  var id: Int?
  var store: Store?
  var name: String?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(Int.self, forKey:.mobile) {                       
mobile = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.mobile) {
 mobile = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.id) {
 id = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.id) {
id = value 
}
    store = try container.decodeIfPresent(Store.self, forKey: .store)
    if let value = try? container.decode(Int.self, forKey:.name) {                       
name = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.name) {
 name = value                                                                                     
}
  }

}
