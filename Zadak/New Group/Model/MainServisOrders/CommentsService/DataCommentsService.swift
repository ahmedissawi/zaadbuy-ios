//
//  Data.swift
//
//  Created by osamaaassi on 15/03/2021
//  Copyright (c) . All rights reserved.
//

import Foundation

struct DataCommentsService: Codable {
    
    enum CodingKeys: String, CodingKey {
        case items
    }
    
    var items: [ItemsCommentsService]?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        items = try container.decodeIfPresent([ItemsCommentsService].self, forKey: .items)
    }
    
}
