//
//  CommentsService.swift
//
//  Created by osamaaassi on 15/03/2021
//  Copyright (c) . All rights reserved.
//

import Foundation

struct CommentsService: Codable {
    
    enum CodingKeys: String, CodingKey {
        case code
        case data
        case status
        case message
        case locale
    }
    
    var code: Int?
    var data: DataCommentsService?
    var status: Int?
    var message: String?
    var locale: String?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(String.self, forKey:.code) {
            code = Int(value)                                                                                     
        } else if let value = try? container.decode(Int.self, forKey:.code) {
            code = value 
        }
        data = try container.decodeIfPresent(DataCommentsService.self, forKey: .data)
        if let value = try? container.decode(String.self, forKey:.status) {
            status = Int(value)                                                                                     
        } else if let value = try? container.decode(Int.self, forKey:.status) {
            status = value 
        }
        if let value = try? container.decode(Int.self, forKey:.message) {                       
            message = String(value)                                                                                     
        }else if let value = try? container.decode(String.self, forKey:.message) {
            message = value                                                                                     
        }
        if let value = try? container.decode(Int.self, forKey:.locale) {                       
            locale = String(value)                                                                                     
        }else if let value = try? container.decode(String.self, forKey:.locale) {
            locale = value                                                                                     
        }
    }
    
}
