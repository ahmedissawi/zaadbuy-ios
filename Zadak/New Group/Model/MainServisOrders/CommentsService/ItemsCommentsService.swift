//
//  Items.swift
//
//  Created by osamaaassi on 15/03/2021
//  Copyright (c) . All rights reserved.
//

import Foundation

struct ItemsCommentsService: Codable {

  enum CodingKeys: String, CodingKey {
    case comment
    case type
    case name
    case userId = "user_id"
    case date
  }

  var comment: String?
  var type: String?
  var name: String?
  var userId: Int?
  var date: String?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(Int.self, forKey:.comment) {                       
comment = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.comment) {
 comment = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.type) {                       
type = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.type) {
 type = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.name) {                       
name = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.name) {
 name = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.userId) {
 userId = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.userId) {
userId = value 
}
    if let value = try? container.decode(Int.self, forKey:.date) {                       
date = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.date) {
 date = value                                                                                     
}
  }

}
