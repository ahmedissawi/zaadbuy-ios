//
//  MainServisOrders.swift
//
//  Created by osamaaassi on 15/03/2021
//  Copyright (c) . All rights reserved.
//

import Foundation

struct MainServisOrders: Codable {
    
    enum CodingKeys: String, CodingKey {
        case message
        case status
        case locale
        case code
        case data
    }
    
    var message: String?
    var status: Int?
    var locale: String?
    var code: Int?
    var data: DatasMainServise?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.message) {
            message = String(value)
        }else if let value = try? container.decode(String.self, forKey:.message) {
            message = value
        }
        if let value = try? container.decode(String.self, forKey:.status) {
            status = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.status) {
            status = value
        }
        if let value = try? container.decode(Int.self, forKey:.locale) {
            locale = String(value)
        }else if let value = try? container.decode(String.self, forKey:.locale) {
            locale = value
        }
        if let value = try? container.decode(String.self, forKey:.code) {
            code = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.code) {
            code = value
        }
        data = try container.decodeIfPresent(DatasMainServise.self, forKey: .data)
    }
    
}
