//
//  BankAccount.swift
//
//  Created by osamaaassi on 15/03/2021
//  Copyright (c) . All rights reserved.
//

import Foundation

struct BankAccountServiec: Codable {
    
    enum CodingKeys: String, CodingKey {
        case id
        case status
        case branch
        case notes
        case accountNo = "account_no"
        case bankId = "bank_id"
        case name
        case sellerId = "seller_id"
    }
    
    var id: Int?
    var name: String?
    var sellerId: Int?
    var branch: String?
    var accountNo: String?
    var notes: String?
    var status: String?
    var bankId: String?
    
    
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)                                                                                     
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value 
        }
        if let value = try? container.decode(Int.self, forKey:.status) {                       
            status = String(value)                                                                                     
        }else if let value = try? container.decode(String.self, forKey:.status) {
            status = value                                                                                     
        }
        if let value = try? container.decode(Int.self, forKey:.branch) {                       
            branch = String(value)                                                                                     
        }else if let value = try? container.decode(String.self, forKey:.branch) {
            branch = value                                                                                     
        }
        if let value = try? container.decode(Int.self, forKey:.notes) {                       
            notes = String(value)                                                                                     
        }else if let value = try? container.decode(String.self, forKey:.notes) {
            notes = value                                                                                     
        }
        if let value = try? container.decode(Int.self, forKey:.accountNo) {                       
            accountNo = String(value)                                                                                     
        }else if let value = try? container.decode(String.self, forKey:.accountNo) {
            accountNo = value                                                                                     
        }
        if let value = try? container.decode(Int.self, forKey:.bankId) {                       
            bankId = String(value)                                                                                     
        }else if let value = try? container.decode(String.self, forKey:.bankId) {
            bankId = value                                                                                     
        }
        if let value = try? container.decode(Int.self, forKey:.name) {                       
            name = String(value)                                                                                     
        }else if let value = try? container.decode(String.self, forKey:.name) {
            name = value                                                                                     
        }
        if let value = try? container.decode(String.self, forKey:.sellerId) {
            sellerId = Int(value)                                                                                     
        } else if let value = try? container.decode(Int.self, forKey:.sellerId) {
            sellerId = value 
        }
    }
    
}
