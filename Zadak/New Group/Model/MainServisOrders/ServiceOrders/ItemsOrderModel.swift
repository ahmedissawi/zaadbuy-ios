//
//  Items.swift
//
//  Created by osamaaassi on 15/03/2021
//  Copyright (c) . All rights reserved.
//

import Foundation

struct ItemsOrderModel: Codable {
    
    enum CodingKeys: String, CodingKey {
        case sellerDeliveryDate = "seller_delivery_date"
        case quantity
        case note
        case discount
        case coupon
        case payable
        case serviceOrderId = "service_order_id"
        case regularPrice = "regular_price"
        case paymentStatus = "payment_status"
        case title
        case status
        case id
    }
    
    var id: Int?
    var serviceOrderId: Int?
    var title: String?
    var quantity: Int?
    var note: String?
    var sellerDeliveryDate: String?
    var coupon: String?
    var regularPrice: Int?
    var discount: Int?
    var payable: Int?
    
    var paymentStatus: PaymentMethods?
    var status: PaymentMethods?
    
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.sellerDeliveryDate) {
            sellerDeliveryDate = String(value)
        }else if let value = try? container.decode(String.self, forKey:.sellerDeliveryDate) {
            sellerDeliveryDate = value
        }
        if let value = try? container.decode(String.self, forKey:.quantity) {
            quantity = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.quantity) {
            quantity = value
        }
        if let value = try? container.decode(Int.self, forKey:.note) {
            note = String(value)
        }else if let value = try? container.decode(String.self, forKey:.note) {
            note = value
        }
        if let value = try? container.decode(String.self, forKey:.discount) {
            discount = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.discount) {
            discount = value
        }
        if let value = try? container.decode(Int.self, forKey:.coupon) {
            coupon = String(value)
        }else if let value = try? container.decode(String.self, forKey:.coupon) {
            coupon = value
        }
        if let value = try? container.decode(String.self, forKey:.payable) {
            payable = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.payable) {
            payable = value
        }
        if let value = try? container.decode(String.self, forKey:.serviceOrderId) {
            serviceOrderId = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.serviceOrderId) {
            serviceOrderId = value
        }
        if let value = try? container.decode(String.self, forKey:.regularPrice) {
            regularPrice = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.regularPrice) {
            regularPrice = value
        }
        if let value = try? container.decode(Int.self, forKey:.title) {
            title = String(value)
        }else if let value = try? container.decode(String.self, forKey:.title) {
            title = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        
        paymentStatus = try container.decodeIfPresent(PaymentMethods.self, forKey: .paymentStatus)
        status = try container.decodeIfPresent(PaymentMethods.self, forKey: .status)

    }
    
}
