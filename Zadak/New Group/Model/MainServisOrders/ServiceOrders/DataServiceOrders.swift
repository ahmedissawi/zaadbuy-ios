//
//  Data.swift
//
//  Created by osamaaassi on 15/03/2021
//  Copyright (c) . All rights reserved.
//

import Foundation

struct DataServiceOrders: Codable {
    
    enum CodingKeys: String, CodingKey {
        case store
        case paymentStatus = "payment_status"
        case county
        case createdAt = "created_at"
        case phone
        case items
        case postalCode = "postal_code"
        case city
        case paymentMethods = "payment_methods"
       // case images
        case descriptionValue = "description"
        case total
        case email
        case status
        case seller
        case addressLine3 = "address_line_3"
        case toTime = "to_time"
        case orderNo = "order_no"
        case addressLine2 = "address_line_2"
        case mobile
        case fromTime = "from_time"
        case id
        case addressLine1 = "address_line_1"
        case requiredDate = "required_date"
        case subtotal
        case bankAccount = "bank_account"
        
        case sellerNotes = "seller_notes"
        case couponValue = "coupon_value"
        case coupon
    }
    
    var status: PaymentMethods?
    var paymentStatus: PaymentMethods?
    var seller: SellerServiceOrder?
    var store: StoreServiceOrder?
    
  
    
    var county: String?
    var createdAt: String?
    var phone: String?
    var items: [ItemsOrderModel]?
    var postalCode: String?
    var city: String?
    var paymentMethods: [PaymentMethods]?
   // var images: Any?
    var descriptionValue: String?
    var total: Int?
    var email: String?
    
    var addressLine3: String?
    var toTime: String?
    var orderNo: String?
    var addressLine2: String?
    var mobile: String?
    var fromTime: String?
    var id: Int?
    var addressLine1: String?
    var requiredDate: String?
    var subtotal: Int?
    var bankAccount: BankAccountServiec?
    
    var sellerNotes: String?
    var couponValue: Int?
    var coupon: Int?
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        store = try container.decodeIfPresent(StoreServiceOrder.self, forKey: .store)
        
        paymentStatus = try container.decodeIfPresent(PaymentMethods.self, forKey: .paymentStatus)
        status = try container.decodeIfPresent(PaymentMethods.self, forKey: .status)
        
        if let value = try? container.decode(Int.self, forKey:.sellerNotes) {
            sellerNotes = String(value)
        }else if let value = try? container.decode(String.self, forKey:.sellerNotes) {
            sellerNotes = value
        }
        
        if let value = try? container.decode(String.self, forKey:.couponValue) {
            couponValue = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.couponValue) {
            couponValue = value
        }
        
        if let value = try? container.decode(String.self, forKey:.coupon) {
            coupon = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.coupon) {
            coupon = value
        }
        
        
        if let value = try? container.decode(Int.self, forKey:.county) {
            county = String(value)
        }else if let value = try? container.decode(String.self, forKey:.county) {
            county = value
        }
        if let value = try? container.decode(Int.self, forKey:.createdAt) {
            createdAt = String(value)
        }else if let value = try? container.decode(String.self, forKey:.createdAt) {
            createdAt = value
        }
        if let value = try? container.decode(Int.self, forKey:.phone) {
            phone = String(value)
        }else if let value = try? container.decode(String.self, forKey:.phone) {
            phone = value
        }
        items = try container.decodeIfPresent([ItemsOrderModel].self, forKey: .items)
        if let value = try? container.decode(Int.self, forKey:.postalCode) {
            postalCode = String(value)
        }else if let value = try? container.decode(String.self, forKey:.postalCode) {
            postalCode = value
        }
        if let value = try? container.decode(Int.self, forKey:.city) {
            city = String(value)
        }else if let value = try? container.decode(String.self, forKey:.city) {
            city = value
        }
        paymentMethods = try container.decodeIfPresent([PaymentMethods].self, forKey: .paymentMethods)
     //   images = try container.decodeIfPresent([].self, forKey: .images)
        if let value = try? container.decode(Int.self, forKey:.descriptionValue) {
            descriptionValue = String(value)
        }else if let value = try? container.decode(String.self, forKey:.descriptionValue) {
            descriptionValue = value
        }
        if let value = try? container.decode(String.self, forKey:.total) {
            total = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.total) {
            total = value
        }
        if let value = try? container.decode(Int.self, forKey:.email) {
            email = String(value)
        }else if let value = try? container.decode(String.self, forKey:.email) {
            email = value
        }
        
        seller = try container.decodeIfPresent(SellerServiceOrder.self, forKey: .seller)
        if let value = try? container.decode(Int.self, forKey:.addressLine3) {
            addressLine3 = String(value)
        }else if let value = try? container.decode(String.self, forKey:.addressLine3) {
            addressLine3 = value
        }
        if let value = try? container.decode(Int.self, forKey:.toTime) {
            toTime = String(value)
        }else if let value = try? container.decode(String.self, forKey:.toTime) {
            toTime = value
        }
        if let value = try? container.decode(Int.self, forKey:.orderNo) {
            orderNo = String(value)
        }else if let value = try? container.decode(String.self, forKey:.orderNo) {
            orderNo = value
        }
        if let value = try? container.decode(Int.self, forKey:.addressLine2) {
            addressLine2 = String(value)
        }else if let value = try? container.decode(String.self, forKey:.addressLine2) {
            addressLine2 = value
        }
        if let value = try? container.decode(Int.self, forKey:.mobile) {
            mobile = String(value)
        }else if let value = try? container.decode(String.self, forKey:.mobile) {
            mobile = value
        }
        if let value = try? container.decode(Int.self, forKey:.fromTime) {
            fromTime = String(value)
        }else if let value = try? container.decode(String.self, forKey:.fromTime) {
            fromTime = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(Int.self, forKey:.addressLine1) {
            addressLine1 = String(value)
        }else if let value = try? container.decode(String.self, forKey:.addressLine1) {
            addressLine1 = value
        }
        if let value = try? container.decode(Int.self, forKey:.requiredDate) {
            requiredDate = String(value)
        }else if let value = try? container.decode(String.self, forKey:.requiredDate) {
            requiredDate = value
        }
        if let value = try? container.decode(String.self, forKey:.subtotal) {
            subtotal = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.subtotal) {
            subtotal = value
        }
        bankAccount = try container.decodeIfPresent(BankAccountServiec.self, forKey: .bankAccount)
    }
    
}
