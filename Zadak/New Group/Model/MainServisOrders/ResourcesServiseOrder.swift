//
//  Resources.swift
//
//  Created by osamaaassi on 15/03/2021
//  Copyright (c) . All rights reserved.
//

import Foundation

struct ResourcesServiseOrder: Codable {
    
    enum CodingKeys: String, CodingKey {
        case id
        case paymentStatus = "payment_status"
        case store
        case seller
        case status
        case createdAt = "created_at"
        case descriptionValue = "description"
        case orderNo = "order_no"
        case paymentMethods = "payment_methods"
        case bankAccount = "bank_account"
        
        case subtotal
        case total
        case couponValue = "coupon_value"
        case coupon
    }
    
    var id: Int?
    var orderNo: String?
    var descriptionValue: String?
    var seller: SellerServiceOrder?
    var store: StoreServiceOrder?
    var status: PaymentMethods?
    var paymentStatus: PaymentMethods?
    var paymentMethods: [PaymentMethods]?
    var bankAccount: BankAccountServiec?
    var createdAt: String?
    
    var subtotal: Int?
    var total: Int?
    var couponValue: String?
    var coupon: String?

    
    
    
    
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(String.self, forKey:.subtotal) {
            subtotal = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.subtotal) {
            subtotal = value
        }
        if let value = try? container.decode(String.self, forKey:.total) {
            total = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.total) {
            total = value
        }
        
        paymentStatus = try container.decodeIfPresent(PaymentMethods.self, forKey: .paymentStatus)
        paymentMethods = try container.decodeIfPresent([PaymentMethods].self, forKey: .paymentMethods)
        bankAccount = try container.decodeIfPresent(BankAccountServiec.self, forKey: .bankAccount)

        status = try container.decodeIfPresent(PaymentMethods.self, forKey: .status)
        store = try container.decodeIfPresent(StoreServiceOrder.self, forKey: .store)
        seller = try container.decodeIfPresent(SellerServiceOrder.self, forKey: .seller)
        
        if let value = try? container.decode(Int.self, forKey:.createdAt) {
            createdAt = String(value)
        }else if let value = try? container.decode(String.self, forKey:.createdAt) {
            createdAt = value
        }
        if let value = try? container.decode(Int.self, forKey:.descriptionValue) {
            descriptionValue = String(value)
        }else if let value = try? container.decode(String.self, forKey:.descriptionValue) {
            descriptionValue = value
        }
        if let value = try? container.decode(Int.self, forKey:.orderNo) {
            orderNo = String(value)
        }else if let value = try? container.decode(String.self, forKey:.orderNo) {
            orderNo = value
        }
        
        if let value = try? container.decode(Int.self, forKey:.couponValue) {
            couponValue = String(value)
        }else if let value = try? container.decode(String.self, forKey:.couponValue) {
            couponValue = value
        }
        
        if let value = try? container.decode(Int.self, forKey:.coupon) {
            coupon = String(value)
        }else if let value = try? container.decode(String.self, forKey:.coupon) {
            coupon = value
        }
    }
    
}
