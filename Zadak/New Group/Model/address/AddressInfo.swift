//
//  BaseClass.swift
//
//  Created by  Ahmed’s MacBook Pro on 6/12/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct AddressInfo: Codable {
    
    enum CodingKeys: String, CodingKey {
        case message
        case data
        case locale
        case code
        case status
    }
    
    var message: String?
    var data: [AddressData]?
    var locale: String?
    var code: Int?
    var status: Int?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.message) {
            message = String(value)
        }else if let value = try? container.decode(String.self, forKey:.message) {
            message = value
        }
        data = try container.decodeIfPresent([AddressData].self, forKey: .data)
        if let value = try? container.decode(Int.self, forKey:.locale) {
            locale = String(value)
        }else if let value = try? container.decode(String.self, forKey:.locale) {
            locale = value
        }
        if let value = try? container.decode(String.self, forKey:.code) {
            code = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.code) {
            code = value
        }
        if let value = try? container.decode(String.self, forKey:.status) {
            status = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.status) {
            status = value
        }
    }
    
}
