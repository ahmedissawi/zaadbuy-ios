//
//  Resources.swift
//
//  Created by Ahmed ios on 6/21/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Resources: Codable {
    
    enum CodingKeys: String, CodingKey {
        case id
        case status
        case seller
        case customer
        case userNotes = "user_notes"
        case accountNo = "account_no"
        case document
        case branch
        case transactionNo = "transaction_no"
        case amount
        case name
        case orderId = "order_id"
    }
    
    var id: Int?
    var status: String?
    var seller: String?
    var customer: String?
    var userNotes: String?
    var accountNo: String?
    var document: String?
    var branch: String?
    var transactionNo: String?
    var amount: Int?
    var name: String?
    var orderId: Int?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(Int.self, forKey:.status) {
            status = String(value)
        }else if let value = try? container.decode(String.self, forKey:.status) {
            status = value
        }
        if let value = try? container.decode(Int.self, forKey:.seller) {
            seller = String(value)
        }else if let value = try? container.decode(String.self, forKey:.seller) {
            seller = value
        }
        if let value = try? container.decode(Int.self, forKey:.customer) {
            customer = String(value)
        }else if let value = try? container.decode(String.self, forKey:.customer) {
            customer = value
        }
        if let value = try? container.decode(Int.self, forKey:.userNotes) {
            userNotes = String(value)
        }else if let value = try? container.decode(String.self, forKey:.userNotes) {
            userNotes = value
        }
        if let value = try? container.decode(Int.self, forKey:.accountNo) {
            accountNo = String(value)
        }else if let value = try? container.decode(String.self, forKey:.accountNo) {
            accountNo = value
        }
        if let value = try? container.decode(Int.self, forKey:.document) {
            document = String(value)
        }else if let value = try? container.decode(String.self, forKey:.document) {
            document = value
        }
        if let value = try? container.decode(Int.self, forKey:.branch) {
            branch = String(value)
        }else if let value = try? container.decode(String.self, forKey:.branch) {
            branch = value
        }
        if let value = try? container.decode(Int.self, forKey:.transactionNo) {
            transactionNo = String(value)
        }else if let value = try? container.decode(String.self, forKey:.transactionNo) {
            transactionNo = value
        }
        if let value = try? container.decode(String.self, forKey:.amount) {
            amount = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.amount) {
            amount = value
        }
        if let value = try? container.decode(Int.self, forKey:.name) {
            name = String(value)
        }else if let value = try? container.decode(String.self, forKey:.name) {
            name = value
        }
        if let value = try? container.decode(String.self, forKey:.orderId) {
            orderId = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.orderId) {
            orderId = value
        }
    }
    
    
}
