//
//  BaseClass.swift
//
//  Created by Ahmed ios on 6/21/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct myPaumentClass: Codable {
    
    enum CodingKeys: String, CodingKey {
        case locale
        case status
        case message
        case data
        case code
    }
    
    var locale: String?
    var status: Int?
    var message: String?
    var data: [Resources]?
    var code: Int?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.locale) {
            locale = String(value)
        }else if let value = try? container.decode(String.self, forKey:.locale) {
            locale = value
        }
        if let value = try? container.decode(String.self, forKey:.status) {
            status = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.status) {
            status = value
        }
        if let value = try? container.decode(Int.self, forKey:.message) {
            message = String(value)
        }else if let value = try? container.decode(String.self, forKey:.message) {
            message = value
        }
        data = try container.decodeIfPresent([Resources].self, forKey: .data)
        if let value = try? container.decode(String.self, forKey:.code) {
            code = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.code) {
            code = value
        }
    }
    
}

