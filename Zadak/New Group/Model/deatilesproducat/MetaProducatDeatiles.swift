//
//  Meta.swift
//
//  Created by  Ahmed’s MacBook Pro on 6/11/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct MetaProducatDeatiles: Codable {

  enum CodingKeys: String, CodingKey {
    case name
    case valuecolor
  }

  var name: String?
  var valuecolor: String?



    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.name) {
            name = String(value)
        }else if let value = try? container.decode(String.self, forKey:.name) {
            name = value
        }
        if let value = try? container.decode(Int.self, forKey:.valuecolor) {
            valuecolor = String(value)
        }else if let value = try? container.decode(String.self, forKey:.valuecolor) {
            valuecolor = value
        }
    }

}
