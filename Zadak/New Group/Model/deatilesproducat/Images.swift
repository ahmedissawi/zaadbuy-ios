//
//  Images.swift
//
//  Created by  Ahmed’s MacBook Pro on 6/11/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Images: Codable {
    
    enum CodingKeys: String, CodingKey {
        case title
        case alt
        case src
    }
    
    var title: String?
    var alt: String?
    var src: String?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.title) {
            title = String(value)
        }else if let value = try? container.decode(String.self, forKey:.title) {
            title = value
        }
        if let value = try? container.decode(Int.self, forKey:.alt) {                       
            alt = String(value)
        }else if let value = try? container.decode(String.self, forKey:.alt) {
            alt = value
        }
        if let value = try? container.decode(Int.self, forKey:.src) {
            src = String(value)
        }else if let value = try? container.decode(String.self, forKey:.src) {
            src = value
        }
    }
    
}
