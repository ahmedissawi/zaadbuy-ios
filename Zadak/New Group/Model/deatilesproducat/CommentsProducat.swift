//
//  Comments.swift
//
//  Created by  Ahmed’s MacBook Pro on 6/11/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct CommentsProducat: Codable {
    
    enum CodingKeys: String, CodingKey {
        case comment
        case date
        case user
    }
    
    var comment: String?
    var date: String?
    var user: UserRateProducat?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.comment) {
            comment = String(value)
        }else if let value = try? container.decode(String.self, forKey:.comment) {
            comment = value
        }
        if let value = try? container.decode(Int.self, forKey:.date) {
            date = String(value)
        }else if let value = try? container.decode(String.self, forKey:.date) {
            date = value
        }
        user = try container.decodeIfPresent(UserRateProducat.self, forKey: .user)
    }
    
}
