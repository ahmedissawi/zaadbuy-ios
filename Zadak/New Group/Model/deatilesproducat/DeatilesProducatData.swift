//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 6/11/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct DeatilesProducatData: Codable {
    
    enum CodingKeys: String, CodingKey {
        case comments
        case shortDescription = "short_description"
        case meta
        case quantity
        case discount
        case related
        case category
        case rates
        case rate
        case link
        case salePrice = "sale_price"
        case regularPrice = "regular_price"
        case title
        case seller
        case images
        case descriptionValue = "description"
        case id
        case favoriteItem = "favorite_item"
        case cartItem = "cart_item"
        
    }
    
    var comments: [CommentsProducat]?
    var shortDescription: String?
    var meta: [MetaProducatDeatiles]?
    var quantity: Int?
    var discount: Int?
    var related: [Related]?
    var category: Category?
    var rates: RatesPrdoucta?
    var rate: String?
    var salePrice: Double?
    var regularPrice: Double?
    var link: String?
    var title: String?
    var seller: Seller?
    var images: [Images]?
    var descriptionValue: String?
    var id: Int?
    var favoriteItem: Bool?
    var cartItem: Bool?
    
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        comments = try container.decodeIfPresent([CommentsProducat].self, forKey: .comments)
        favoriteItem = try container.decodeIfPresent(Bool.self, forKey: .favoriteItem)
        if let value = try? container.decode(Int.self, forKey:.shortDescription) {
            shortDescription = String(value)
        }else if let value = try? container.decode(String.self, forKey:.shortDescription) {
            shortDescription = value
        }
        meta = try container.decodeIfPresent([MetaProducatDeatiles].self, forKey: .meta)
        if let value = try? container.decode(String.self, forKey:.quantity) {
            quantity = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.quantity) {
            quantity = value
        }
        if let value = try? container.decode(String.self, forKey:.discount) {
            discount = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.discount) {
            discount = value
        }
        related = try container.decodeIfPresent([Related].self, forKey: .related)
        category = try container.decodeIfPresent(Category.self, forKey: .category)
        rates = try container.decodeIfPresent(RatesPrdoucta.self, forKey: .rates)
        if let value = try? container.decode(Int.self, forKey:.rate) {
            rate = String(value)
        }else if let value = try? container.decode(String.self, forKey:.rate) {
            rate = value
        }
        if let value = try? container.decode(String.self, forKey:.salePrice) {
            salePrice = Double(value)
        } else if let value = try? container.decode(Int.self, forKey:.salePrice) {
            salePrice = Double(value)
        }else{
            if let value = try? container.decode(Double.self, forKey:.salePrice) {
                salePrice = value
            }
        }
        
        if let value = try? container.decode(Int.self, forKey:.regularPrice) {
            regularPrice = Double(value)
        } else if let value = try? container.decode(Double.self, forKey:.regularPrice) {
            regularPrice = value
        }else{
            if let value = try? container.decode(String.self, forKey:.regularPrice){
                regularPrice = Double(value)
            }
        }
        if let value = try? container.decode(Int.self, forKey:.title) {
            title = String(value)
        }else if let value = try? container.decode(String.self, forKey:.title) {
            title = value
        }
        if let value = try? container.decode(Int.self, forKey:.link) {
            link = String(value)
        }else if let value = try? container.decode(String.self, forKey:.link) {
            link = value
        }
        cartItem = try container.decodeIfPresent(Bool.self, forKey: .cartItem)
        seller = try container.decodeIfPresent(Seller.self, forKey: .seller)
        images = try container.decodeIfPresent([Images].self, forKey: .images)
        if let value = try? container.decode(Int.self, forKey:.descriptionValue) {
            descriptionValue = String(value)
        }else if let value = try? container.decode(String.self, forKey:.descriptionValue) {
            descriptionValue = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
    }
    
}
