//
//  Seller.swift
//
//  Created by Ahmed ios on 6/6/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Seller: Codable {
    
    enum CodingKeys: String, CodingKey {
        case store
        case name
        case store_name
        case storeName
        case mobile
        case id
        case store_logo
        case store_id
        case storeCategory = "store_category"
        case storeAddress = "store_address"
        
    }
    
    var store: Store?
    var name: String?
    var storeName: String?
    var mobile: String?
    var store_logo: String?
    var id: Int?
    var store_name: String?
    var store_id: Int?
    
    var storeCategory: String?
    var storeAddress: String?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        store = try container.decodeIfPresent(Store.self, forKey: .store)
        
        if let value = try? container.decode(Int.self, forKey:.name) {
            name = String(value)
        }else if let value = try? container.decode(String.self, forKey:.name) {
            name = value
        }
        
        if let value = try? container.decode(Int.self, forKey:.storeName) {
            storeName = String(value)
        }else if let value = try? container.decode(String.self, forKey:.storeName) {
            storeName = value
        }
        if let value = try? container.decode(Int.self, forKey:.store_name) {
            store_name = String(value)
        }else if let value = try? container.decode(String.self, forKey:.store_name) {
            store_name = value
        }
        if let value = try? container.decode(Int.self, forKey:.store_logo) {
            store_logo = String(value)
        }else if let value = try? container.decode(String.self, forKey:.store_logo) {
            store_logo = value
        }
        if let value = try? container.decode(Int.self, forKey:.mobile) {
            mobile = String(value)
        }else if let value = try? container.decode(String.self, forKey:.mobile) {
            mobile = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(String.self, forKey:.store_id) {
            store_id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.store_id) {
            store_id = value
        }
        
        if let value = try? container.decode(Int.self, forKey:.storeCategory) {
            storeCategory = String(value)
             }else if let value = try? container.decode(String.self, forKey:.storeCategory) {
                storeCategory = value
             }
        if let value = try? container.decode(Int.self, forKey:.storeAddress) {
            storeAddress = String(value)
             }else if let value = try? container.decode(String.self, forKey:.storeAddress) {
                storeAddress = value
             }
        
    }
    
}
