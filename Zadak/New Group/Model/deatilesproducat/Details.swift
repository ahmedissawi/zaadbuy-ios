//
//  Details.swift
//
//  Created by  Ahmed’s MacBook Pro on 6/11/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Details: Codable {
    
    enum CodingKeys: String, CodingKey {
        case date
        case note
        case rate
        case user
    }
    
    var date: String?
    var note: String?
    var rate: Int?
    var user: UserRateProducat?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.date) {
            date = String(value)
        }else if let value = try? container.decode(String.self, forKey:.date) {
            date = value
        }
        if let value = try? container.decode(Int.self, forKey:.note) {
            note = String(value)
        }else if let value = try? container.decode(String.self, forKey:.note) {
            note = value
        }
        if let value = try? container.decode(String.self, forKey:.rate) {
            rate = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.rate) {
            rate = value
        }
        user = try container.decodeIfPresent(UserRateProducat.self, forKey: .user)
    }
    
}
