//
//  Related.swift
//
//  Created by  Ahmed’s MacBook Pro on 6/11/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Related: Codable {
    
    enum CodingKeys: String, CodingKey {
        case category
        case title
        case id
        case quantity
        case salePrice = "sale_price"
        case seller
        case favoriteItem = "favorite_item"
        case regularPrice = "regular_price"
        case discount
        case image
    }
    
    var category: Category?
    var title: String?
    var id: Int?
    var quantity: Int?
    var salePrice: Double?
    var seller: Seller?
    var regularPrice: Double?
    var discount: Int?
    var image: String?
    var favoriteItem: Bool?

    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        category = try container.decodeIfPresent(Category.self, forKey: .category)
        favoriteItem = try container.decodeIfPresent(Bool.self, forKey: .favoriteItem)
        if let value = try? container.decode(Int.self, forKey:.title) {                       
            title = String(value)                                                                                     
        }else if let value = try? container.decode(String.self, forKey:.title) {
            title = value                                                                                     
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)                                                                                     
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value 
        }
        if let value = try? container.decode(String.self, forKey:.quantity) {
            quantity = Int(value)                                                                                     
        } else if let value = try? container.decode(Int.self, forKey:.quantity) {
            quantity = value 
        }
        if let value = try? container.decode(String.self, forKey:.salePrice) {
            salePrice = Double(value)
        } else if let value = try? container.decode(Int.self, forKey:.salePrice) {
            salePrice =  Double(value)
        }else{
            if let value = try? container.decode(Double.self, forKey:.salePrice) {
                salePrice =  value
            }
        }
        seller = try container.decodeIfPresent(Seller.self, forKey: .seller)
        
        if let value = try? container.decode(String.self, forKey:.regularPrice) {
            regularPrice = Double(value)
        } else if let value = try? container.decode(Int.self, forKey:.regularPrice) {
            regularPrice = Double(value)
        }else{
            if let value = try? container.decode(Double.self, forKey:.regularPrice) {
                regularPrice = value
            }
        }
        if let value = try? container.decode(String.self, forKey:.discount) {
            discount = Int(value)                                                                                     
        } else if let value = try? container.decode(Int.self, forKey:.discount) {
            discount = value 
        }
        if let value = try? container.decode(Int.self, forKey:.image) {                       
            image = String(value)                                                                                     
        }else if let value = try? container.decode(String.self, forKey:.image) {
            image = value                                                                                     
        }
    }
    
}
