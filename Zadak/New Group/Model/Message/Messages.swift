//
//  Messages.swift
//
//  Created by  Ahmed’s MacBook Pro on 6/15/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Messages: Codable {
    
    enum CodingKeys: String, CodingKey {
        case message
        case id
        case createdAt = "created_at"
        case type
    }
    
    var message: String?
    var id: Int?
    var createdAt: String?
    var type: Int?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.message) {
            message = String(value)
        }else if let value = try? container.decode(String.self, forKey:.message) {
            message = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(Int.self, forKey:.createdAt) {
            createdAt = String(value)
        }else if let value = try? container.decode(String.self, forKey:.createdAt) {
            createdAt = value                                                                                     
        }
        if let value = try? container.decode(String.self, forKey:.type) {
            type = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.type) {
            type = value
        }
    }
    
}
