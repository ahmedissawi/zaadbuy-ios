//
//  Childrens.swift
//
//  Created by osamaaassi on 12/03/2021
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Childrens: Codable {

  enum CodingKeys: String, CodingKey {
    case name
    case id
    case childrens
    case title
  }

  var name: String?
  var id: Int?
  var childrens: [Childrens]?
  var title: String?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(Int.self, forKey:.name) {                       
name = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.name) {
 name = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.id) {
 id = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.id) {
id = value 
}
    childrens = try container.decodeIfPresent([Childrens].self, forKey: .childrens)
    if let value = try? container.decode(Int.self, forKey:.title) {                       
title = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.title) {
 title = value                                                                                     
}
  }

}
