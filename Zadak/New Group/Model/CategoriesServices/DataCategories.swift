//
//  Data.swift
//
//  Created by osamaaassi on 12/03/2021
//  Copyright (c) . All rights reserved.
//

import Foundation

struct DataCategories: Codable {

  enum CodingKeys: String, CodingKey {
    case id
    case title
    case name
    case childrens
  }

  var id: Int?
  var title: String?
  var name: String?
  var childrens: [Childrens]?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(String.self, forKey:.id) {
 id = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.id) {
id = value 
}
    if let value = try? container.decode(Int.self, forKey:.title) {                       
title = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.title) {
 title = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.name) {                       
name = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.name) {
 name = value                                                                                     
}
    childrens = try container.decodeIfPresent([Childrens].self, forKey: .childrens)
  }

}
