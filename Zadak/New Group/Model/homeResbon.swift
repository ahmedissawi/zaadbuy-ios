//
//  QuestionsRespon.swift
//
//  Created by Ahmed ios on 6/6/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct homeResbon: Codable {

  enum CodingKeys: String, CodingKey {
    case code
    case data
    case status
    case locale
    case message
  }

  var code: Int?
  var data: DataSection?
  var status: Int?
  var locale: String?
  var message: String?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(String.self, forKey:.code) {
        code = Int(value)
    } else if let value = try? container.decode(Int.self, forKey:.code) {
        code = value
    }
    data = try container.decodeIfPresent(DataSection.self, forKey: .data)
    if let value = try? container.decode(String.self, forKey:.status) {
        status = Int(value)
    } else if let value = try? container.decode(Int.self, forKey:.status) {
        status = value
    }
    if let value = try? container.decode(Int.self, forKey:.locale) {                       
        locale = String(value)
    }else if let value = try? container.decode(String.self, forKey:.locale) {
        locale = value
}
    if let value = try? container.decode(Int.self, forKey:.message) {                       
        message = String(value)                                                                                     
    }else if let value = try? container.decode(String.self, forKey:.message) {
        message = value                                                                                     
}
  }

}
