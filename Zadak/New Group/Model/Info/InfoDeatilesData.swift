//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 6/26/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct InfoDeatilesData: Codable {

  enum CodingKeys: String, CodingKey {
    case id
    case slug
    case title
    case name
    case descriptionValue = "description"
    case html
  }

  var id: Int?
  var slug: String?
  var title: String?
  var name: String?
  var descriptionValue: String?
  var html: String?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(String.self, forKey:.id) {
 id = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.id) {
id = value 
}
    if let value = try? container.decode(Int.self, forKey:.slug) {                       
slug = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.slug) {
 slug = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.title) {                       
title = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.title) {
 title = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.name) {                       
name = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.name) {
 name = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.descriptionValue) {                       
descriptionValue = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.descriptionValue) {
 descriptionValue = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.html) {                       
html = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.html) {
 html = value                                                                                     
}
  }

}
