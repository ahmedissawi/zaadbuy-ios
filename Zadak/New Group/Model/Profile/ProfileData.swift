//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 6/29/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct ProfileData: Codable {
    
    enum CodingKeys: String, CodingKey {
        case mobilePrefix = "mobile_prefix"
        case email
        case profile
        case mobile
        case name
        case preferences
        case id
        case addresses
    }
    
    var mobilePrefix: String?
    var email: String?
    var profile: Profile?
    var mobile: String?
    var name: String?
    var preferences: Preferences?
    var id: Int?
    var addresses: [Addresses]?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.mobilePrefix) {
            mobilePrefix = String(value)
        }else if let value = try? container.decode(String.self, forKey:.mobilePrefix) {
            mobilePrefix = value
        }
        if let value = try? container.decode(Int.self, forKey:.email) {
            email = String(value)
        }else if let value = try? container.decode(String.self, forKey:.email) {
            email = value
        }
        profile = try container.decodeIfPresent(Profile.self, forKey: .profile)
        if let value = try? container.decode(Int.self, forKey:.mobile) {
            mobile = String(value)
        }else if let value = try? container.decode(String.self, forKey:.mobile) {
            mobile = value
        }
        if let value = try? container.decode(Int.self, forKey:.name) {
            name = String(value)
        }else if let value = try? container.decode(String.self, forKey:.name) {
            name = value
        }
        preferences = try container.decodeIfPresent(Preferences.self, forKey: .preferences)
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        addresses = try container.decodeIfPresent([Addresses].self, forKey: .addresses)
    }
    
}
