//
//  PayMethodsClass.swift
//
//  Created by Ahmed ios on 6/14/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct PayMethodsItem: Codable {

  enum CodingKeys: String, CodingKey {
    case items
   
  }
 var items: [PayMethodsStruct]?


init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)

    items = try container.decodeIfPresent([PayMethodsStruct].self, forKey: .items)

  }

}
