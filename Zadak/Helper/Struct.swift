//
//  Struct.swift
//  BASIT
//
//  Created by ahmed on 2/4/19.
//  Copyright © 2019 ahmed. All rights reserved.
//

import UIKit
import AVFoundation
struct CurrentUser  {

    static var userInfo : UserStruct?  {
        set {
            guard newValue != nil else {
                UserDefaults.standard.removeObject(forKey: "CurrentUser");
                return;
            }
            let encodedData = try? PropertyListEncoder().encode(newValue)
            UserDefaults.standard.set(encodedData, forKey:"CurrentUser")
            UserDefaults.standard.synchronize();
        }
        get {
            if let data = UserDefaults.standard.value(forKey:"CurrentUser") as? Data {
                return try? PropertyListDecoder().decode(UserStruct.self, from:data)

            }
            return nil
        }

    }
    static var firtTime: Bool? {
        set {
            UserDefaults.standard.set(newValue, forKey: "firtTime")
        }
        get {
            if UserDefaults.standard.object(forKey: "firtTime") == nil {

                UserDefaults.standard.set(false, forKey: "firtTime")
                return false
            }
            return UserDefaults.standard.bool(forKey: "firtTime")
        }

    }
    static var SelectfirtTime: Bool? {
        set {
            UserDefaults.standard.set(newValue, forKey: "SelectfirtTime")
        }
        get {
            if UserDefaults.standard.object(forKey: "SelectfirtTime") == nil {

                UserDefaults.standard.set(false, forKey: "SelectfirtTime")
                return false
            }
            return UserDefaults.standard.bool(forKey: "SelectfirtTime")
        }

    }


}

struct CurrentAddress  {

    static var addressInfo : Addresses?  {
        set {
            guard newValue != nil else {
                UserDefaults.standard.removeObject(forKey: "Address");
                return;
            }
            let encodedData = try? PropertyListEncoder().encode(newValue)
            UserDefaults.standard.set(encodedData, forKey:"Address")
            UserDefaults.standard.synchronize();
        }
        get {
            if let data = UserDefaults.standard.value(forKey:"Address") as? Data {
                return try? PropertyListDecoder().decode(Addresses.self, from:data)
//return nil
            }
            return nil
        }

    }

    


}



