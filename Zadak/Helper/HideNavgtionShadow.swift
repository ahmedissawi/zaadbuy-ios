//
//  HideNavgtionShadow.swift
//  BO
//
//  Created by  Ahmed’s MacBook Pro on 4/20/19.
//  Copyright © 2019  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class HideNavgtionShadow: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let barButtonItemAppearance = UIBarButtonItem.appearance()
        barButtonItemAppearance.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.clear], for: .normal)
        navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationBar.shadowImage = UIImage()
        navigationBar.isTranslucent = true

    }
    


}
