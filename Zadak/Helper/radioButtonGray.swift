//
//  radioButtonGray.swift
//  Zafah
//
//  Created by  Ahmed’s MacBook Pro on 6/10/19.
//  Copyright © 2019  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class radioButtonGray: UIButton {

    var alternateButton:Array<radioButtonGray>?
    
    override func awakeFromNib() {
        self.layer.cornerRadius = 5
        self.layer.borderWidth = 1
        self.layer.masksToBounds = true
    }
    
    func unselectAlternateButtons(){
        if alternateButton != nil {
            self.isSelected = true
            
            for aButton:radioButtonGray in alternateButton! {
                aButton.isSelected = false
            }
        }else{
            toggleButton()
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        unselectAlternateButtons()
        super.touchesBegan(touches, with: event)
    }
    
    func toggleButton(){
        self.isSelected = !isSelected
    }
    
    override var isSelected: Bool {
        didSet {
            if isSelected {
        
                self.setTitleColor("0D132A".color, for: .normal)
                self.layer.borderColor = "0D132A".color.cgColor
              
                
            } else {
                self.setTitleColor("D0D2D3".color, for: .normal)
                self.layer.borderColor = "D0D2D3".color.cgColor
            }
        }
    }
   

}
