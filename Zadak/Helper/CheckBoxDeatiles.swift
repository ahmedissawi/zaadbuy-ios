//
//  CheckBoxDeatiles.swift
//  Zafah
//
//  Created by  Ahmed’s MacBook Pro on 6/9/19.
//  Copyright © 2019  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class CheckBoxDeatiles: UIButton {
    // Images
    let checkedImage = UIImage(named: "SerivesSelected")! as UIImage
    let uncheckedImage = UIImage(named: "SerivesCheck")! as UIImage
    
    // Bool property
    var isChecked: Bool = false {
        didSet{
            if isChecked == true {
                self.setImage(checkedImage, for: UIControl.State.normal)
            } else {
                self.setImage(uncheckedImage, for: UIControl.State.normal)
            }
        }
    }
    
    override func awakeFromNib() {
        self.addTarget(self, action:#selector(buttonClicked(sender:)), for: UIControl.Event.touchUpInside)
        self.isChecked = false
    }
    
    @objc func buttonClicked(sender: UIButton) {
        if sender == self {
            isChecked = !isChecked
        }
    }
   
}
